package com.dbapp.garden.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.dbapp.garden.entity.SysRoleUser;

@Mapper
public interface SysRoleUserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysRoleUser record);

    int insertSelective(SysRoleUser record);

    SysRoleUser selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysRoleUser record);

    int updateByPrimaryKey(SysRoleUser record);

	Integer deleteByUserId(Integer id);

	SysRoleUser findByUserId(Integer id);
}