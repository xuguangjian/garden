package com.dbapp.garden.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.dbapp.garden.entity.GardenDepartment;

@Mapper
public interface GardenDepartmentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenDepartment record);

    int insertSelective(GardenDepartment record);

    GardenDepartment selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenDepartment record);

    int updateByPrimaryKey(GardenDepartment record);

	List<GardenDepartment> getDepartmentList(Map<String, Object> paramMap);

	Integer getDepartmentCount(Map<String, Object> paramMap);
}