package com.dbapp.garden.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.dbapp.garden.entity.GardenTeacherLevel;
@Mapper
public interface GardenTeacherLevelMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenTeacherLevel record);

    int insertSelective(GardenTeacherLevel record);

    GardenTeacherLevel selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenTeacherLevel record);

    int updateByPrimaryKey(GardenTeacherLevel record);

	List<GardenTeacherLevel> getTeacherLevelList(Map<String, Object> paramMap);


	Integer getTeacherLevelCount(Map<String, Object> paramMap);
}