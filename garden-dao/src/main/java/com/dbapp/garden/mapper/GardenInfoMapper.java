package com.dbapp.garden.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.dbapp.garden.entity.GardenInfo;
@Mapper
public interface GardenInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenInfo record);

    int insertSelective(GardenInfo record);

    GardenInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenInfo record);

    int updateByPrimaryKey(GardenInfo record);

	List<GardenInfo> getGardenList(Map<String, Object> paramMap);

	Integer getGardenCount(Map<String, Object> paramMap);
}