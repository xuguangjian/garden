package com.dbapp.garden.mapper;

import com.dbapp.garden.entity.GardenClassCourseStudent;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface GardenClassCourseStudentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenClassCourseStudent record);

    int insertSelective(GardenClassCourseStudent record);

    GardenClassCourseStudent selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenClassCourseStudent record);

    int updateByPrimaryKey(GardenClassCourseStudent record);

    List<GardenClassCourseStudent> getClassStudentList(Map<String, Object> paramMap);

    Integer getClassStudentCount(Map<String, Object> paramMap);
}