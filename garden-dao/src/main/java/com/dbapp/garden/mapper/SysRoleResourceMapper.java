package com.dbapp.garden.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.dbapp.garden.entity.SysRoleResource;
@Mapper
public interface SysRoleResourceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysRoleResource record);

    int insertSelective(SysRoleResource record);

    SysRoleResource selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysRoleResource record);

    int updateByPrimaryKey(SysRoleResource record);

	Integer batchInsert(List<SysRoleResource> sysRoleResourceList);

	Integer deleteByRoleId(Integer roleId);
}