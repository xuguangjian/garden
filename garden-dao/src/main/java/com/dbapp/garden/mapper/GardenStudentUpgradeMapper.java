package com.dbapp.garden.mapper;

import com.dbapp.garden.entity.GardenStudentUpgrade;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface GardenStudentUpgradeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenStudentUpgrade record);

    int insertSelective(GardenStudentUpgrade record);

    GardenStudentUpgrade selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenStudentUpgrade record);

    int updateByPrimaryKey(GardenStudentUpgrade record);

    List<GardenStudentUpgrade> getStudentUpgradeList(Map<String, Object> paramMap);

    Integer getStudentUpgradeCount(Map<String, Object> paramMap);
}