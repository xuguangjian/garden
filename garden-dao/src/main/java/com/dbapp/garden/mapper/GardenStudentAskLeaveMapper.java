package com.dbapp.garden.mapper;

import com.dbapp.garden.entity.GardenStudentAskLeave;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface GardenStudentAskLeaveMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenStudentAskLeave record);

    int insertSelective(GardenStudentAskLeave record);

    GardenStudentAskLeave selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenStudentAskLeave record);

    int updateByPrimaryKey(GardenStudentAskLeave record);

    List<GardenStudentAskLeave> getAskLeaveList(Map<String, Object> paramMap);

    Integer getAskLeaveCount(Map<String, Object> paramMap);
}