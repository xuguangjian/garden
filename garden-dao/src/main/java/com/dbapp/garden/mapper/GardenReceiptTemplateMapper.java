package com.dbapp.garden.mapper;

import com.dbapp.garden.entity.GardenReceiptTemplate;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface GardenReceiptTemplateMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenReceiptTemplate record);

    int insertSelective(GardenReceiptTemplate record);

    GardenReceiptTemplate selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenReceiptTemplate record);

    int updateByPrimaryKey(GardenReceiptTemplate record);

    List<GardenReceiptTemplate> getReceiptTemplate(Map<String, Object> map);
}