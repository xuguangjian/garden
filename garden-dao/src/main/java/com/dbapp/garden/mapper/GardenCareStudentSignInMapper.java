package com.dbapp.garden.mapper;

import com.dbapp.garden.entity.GardenCareStudentSignIn;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface GardenCareStudentSignInMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenCareStudentSignIn record);

    int insertSelective(GardenCareStudentSignIn record);

    GardenCareStudentSignIn selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenCareStudentSignIn record);

    int updateByPrimaryKey(GardenCareStudentSignIn record);

    /**
     * 批量插入
     * @param signInList
     * @return
     */
    Integer batchInsert(List<GardenCareStudentSignIn> signInList);

    List<GardenCareStudentSignIn> getSignInRecordList(Map<String, Object> paramMap);

    Integer getSignInRecordCount(Map<String, Object> paramMap);
}