package com.dbapp.garden.mapper;

import com.dbapp.garden.entity.GardenImportBatch;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author gangzi
 */
@Mapper
public interface GardenImportBatchMapper {
    /**
     * 删
     * @param id
     * @return
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 增
     * @param record
     * @return
     */
    int insert(GardenImportBatch record);

    /**
     * 增
     * @param record
     * @return
     */
    int insertSelective(GardenImportBatch record);

    /**
     * 查
     * @param id
     * @return
     */
    GardenImportBatch selectByPrimaryKey(Integer id);

    /**
     * 改
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(GardenImportBatch record);

    /**
     * 改
     * @param record
     * @return
     */
    int updateByPrimaryKey(GardenImportBatch record);

    /**
     * 获取列表
     * @param paramMap
     * @return
     */
    List<GardenImportBatch> getBatchList(Map<String, Object> paramMap);

    /**
     * 获取数量
     * @param paramMap
     * @return
     */
    Integer getBatchCount(Map<String, Object> paramMap);
}