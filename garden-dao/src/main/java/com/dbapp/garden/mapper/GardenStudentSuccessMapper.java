package com.dbapp.garden.mapper;

import com.dbapp.garden.entity.GardenStudentSuccess;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GardenStudentSuccessMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenStudentSuccess record);

    int insertSelective(GardenStudentSuccess record);

    GardenStudentSuccess selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenStudentSuccess record);

    int updateByPrimaryKey(GardenStudentSuccess record);

    Integer deleteByBatchId(Integer id);
}