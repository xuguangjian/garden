package com.dbapp.garden.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.dbapp.garden.entity.GardenStudentState;

@Mapper
public interface GardenStudentStateMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenStudentState record);

    int insertSelective(GardenStudentState record);

    GardenStudentState selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenStudentState record);

    int updateByPrimaryKey(GardenStudentState record);

	List<GardenStudentState> getStudentStateList(Map<String, Object> paramMap);

	Integer getStudentStateCount(Map<String, Object> paramMap);
}