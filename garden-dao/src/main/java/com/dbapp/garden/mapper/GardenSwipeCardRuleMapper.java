package com.dbapp.garden.mapper;

import com.dbapp.garden.entity.GardenSwipeCardRule;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface GardenSwipeCardRuleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenSwipeCardRule record);

    int insertSelective(GardenSwipeCardRule record);

    GardenSwipeCardRule selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenSwipeCardRule record);

    int updateByPrimaryKey(GardenSwipeCardRule record);

    List<GardenSwipeCardRule> getCardRuleList(Map<String, Object> paramMap);

    Integer getCardRuleCount(Map<String, Object> paramMap);
}