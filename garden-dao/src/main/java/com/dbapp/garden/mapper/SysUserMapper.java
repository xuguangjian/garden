package com.dbapp.garden.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.dbapp.garden.entity.SysUser;

@Mapper
public interface SysUserMapper {
	int deleteByPrimaryKey(Integer id);

	int insert(SysUser record);

	int insertSelective(SysUser record);

	SysUser selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(SysUser record);

	int updateByPrimaryKey(SysUser record);

	/**
	 * 根据用户名查询用户
	 * 
	 * @param username
	 * @return
	 */
	SysUser querySingleUser(String username);

	List<SysUser> getUserList(Map<String, Object> paramMap);

	Integer getUserCount(Map<String, Object> paramMap);

}