package com.dbapp.garden.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.dbapp.garden.entity.GardenStudentSource;

@Mapper
public interface GardenStudentSourceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenStudentSource record);

    int insertSelective(GardenStudentSource record);

    GardenStudentSource selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenStudentSource record);

    int updateByPrimaryKey(GardenStudentSource record);

	Integer getStudentSourceCount(Map<String, Object> paramMap);

	List<GardenStudentSource> getStudentSourceList(Map<String, Object> paramMap);
}