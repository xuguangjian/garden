package com.dbapp.garden.mapper;

import com.dbapp.garden.entity.GardenCourseSeries;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface GardenCourseSeriesMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenCourseSeries record);

    int insertSelective(GardenCourseSeries record);

    GardenCourseSeries selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenCourseSeries record);

    int updateByPrimaryKey(GardenCourseSeries record);

    List<GardenCourseSeries> getCourseSeriesList(Map<String, Object> paramMap);

    Integer getCourseSeriesCount(Map<String, Object> paramMap);
}