package com.dbapp.garden.mapper;

import com.dbapp.garden.entity.GardenContractPackageInfo;
import com.dbapp.garden.entity.GardenContractPackageInfoExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GardenContractPackageInfoMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_contractpackage_info
     *
     * @mbg.generated
     */
    long countByExample(GardenContractPackageInfoExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_contractpackage_info
     *
     * @mbg.generated
     */
    int deleteByExample(GardenContractPackageInfoExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_contractpackage_info
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_contractpackage_info
     *
     * @mbg.generated
     */
    int insert(GardenContractPackageInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_contractpackage_info
     *
     * @mbg.generated
     */
    int insertSelective(GardenContractPackageInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_contractpackage_info
     *
     * @mbg.generated
     */
    List<GardenContractPackageInfo> selectByExample(GardenContractPackageInfoExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_contractpackage_info
     *
     * @mbg.generated
     */
    GardenContractPackageInfo selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_contractpackage_info
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(GardenContractPackageInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_contractpackage_info
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(GardenContractPackageInfo record);
}