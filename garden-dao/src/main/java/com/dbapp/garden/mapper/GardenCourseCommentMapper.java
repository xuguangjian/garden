package com.dbapp.garden.mapper;

import com.dbapp.garden.entity.GardenCourseComment;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface GardenCourseCommentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenCourseComment record);

    int insertSelective(GardenCourseComment record);

    GardenCourseComment selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenCourseComment record);

    int updateByPrimaryKey(GardenCourseComment record);

    List<GardenCourseComment> getCourseCommentList(Map<String, Object> paramMap);

    Integer getCourseCommentCount(Map<String, Object> paramMap);
}