package com.dbapp.garden.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.dbapp.garden.entity.GardenStudentArea;
@Mapper
public interface GardenStudentAreaMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenStudentArea record);

    int insertSelective(GardenStudentArea record);

    GardenStudentArea selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenStudentArea record);

    int updateByPrimaryKey(GardenStudentArea record);

	Integer getStudentAreaCount(Map<String, Object> paramMap);

	List<GardenStudentArea> getStudentAreaList(Map<String, Object> paramMap);

	Integer deleteByParentId(Integer id);
}