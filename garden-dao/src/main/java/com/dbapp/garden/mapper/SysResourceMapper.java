package com.dbapp.garden.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.dbapp.garden.entity.SysResource;
@Mapper
public interface SysResourceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysResource record);

    int insertSelective(SysResource record);

    SysResource selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysResource record);

    int updateByPrimaryKey(SysResource record);

    /**
     * 查询所有受保护资源
     * @param map
     * @return
     */
	List<SysResource> findAll(Map<String,Object> map);

    /**
     * 根据用户ID查询该用户的资源
     */

	List<SysResource> getUserResources(Integer valueOf);

    /**
     * 根据角色ID查询该角色的资源
     * @param id
     * @return
     */
	List<SysResource> findByRoleId(Integer id);

}