package com.dbapp.garden.mapper;

import com.dbapp.garden.entity.GardenClassroom;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface GardenClassroomMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenClassroom record);

    int insertSelective(GardenClassroom record);

    GardenClassroom selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenClassroom record);

    int updateByPrimaryKey(GardenClassroom record);

    List<GardenClassroom> getClassroomList(Map<String, Object> paramMap);

    Integer getClassroomCount(Map<String, Object> paramMap);
}