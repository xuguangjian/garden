package com.dbapp.garden.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.dbapp.garden.entity.GardenStudentFollow;

@Mapper
public interface GardenStudentFollowMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenStudentFollow record);

    int insertSelective(GardenStudentFollow record);

    GardenStudentFollow selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenStudentFollow record);

    int updateByPrimaryKey(GardenStudentFollow record);

	List<GardenStudentFollow> getStudentFollowList(Map<String, Object> paramMap);

	Integer getStudentFollowCount(Map<String, Object> paramMap);
}