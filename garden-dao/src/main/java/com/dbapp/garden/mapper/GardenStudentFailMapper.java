package com.dbapp.garden.mapper;

import com.dbapp.garden.entity.GardenStudentFail;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface GardenStudentFailMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenStudentFail record);

    int insertSelective(GardenStudentFail record);

    GardenStudentFail selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenStudentFail record);

    int updateByPrimaryKey(GardenStudentFail record);

    Integer batchInsert(List<GardenStudentFail> failList);

    Integer deleteByBatchId(Integer id);

    List<GardenStudentFail> getImportStudentFailList(Map<String, Object> paramMap);

    Integer getImportStudentFailCount(Map<String, Object> paramMap);
}