package com.dbapp.garden.mapper;

import com.dbapp.garden.entity.GardenStudentAssign;
import com.dbapp.garden.entity.GardenStudentInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface GardenStudentAssignMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenStudentAssign record);

    int insertSelective(GardenStudentAssign record);

    GardenStudentAssign selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenStudentAssign record);

    int updateByPrimaryKey(GardenStudentAssign record);

    List<GardenStudentAssign> getStudentAssignList(Map<String, Object> paramMap);

    Integer getStudentAssignCount(Map<String, Object> paramMap);
}