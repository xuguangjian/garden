package com.dbapp.garden.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.dbapp.garden.entity.GardenStudentContact;

@Mapper
public interface GardenStudentContactMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenStudentContact record);

    int insertSelective(GardenStudentContact record);

    GardenStudentContact selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenStudentContact record);

    int updateByPrimaryKey(GardenStudentContact record);

	List<GardenStudentContact> getStudentContactList(Map<String, Object> paramMap);

	Integer getStudentContactCount(Map<String, Object> paramMap);
}