package com.dbapp.garden.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.dbapp.garden.entity.GardenStudentImportance;

@Mapper
public interface GardenStudentImportanceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenStudentImportance record);

    int insertSelective(GardenStudentImportance record);

    GardenStudentImportance selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenStudentImportance record);

    int updateByPrimaryKey(GardenStudentImportance record);

	List<GardenStudentImportance> getStudentImportanceList(Map<String, Object> paramMap);

	Integer getStudentImportanceCount(Map<String, Object> paramMap);
}