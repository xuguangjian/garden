package com.dbapp.garden.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.dbapp.garden.entity.GardenDepositInfo;
import com.dbapp.garden.entity.GardenDepositInfoExample;
import com.dbapp.garden.entity.GardenDepositInfoExtra;
import com.dbapp.garden.entity.GardenStudentState;

@Mapper
public interface GardenDepositInfoMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_deposit_info
     *
     * @mbg.generated
     */
    long countByExample(GardenDepositInfoExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_deposit_info
     *
     * @mbg.generated
     */
    int deleteByExample(GardenDepositInfoExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_deposit_info
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_deposit_info
     *
     * @mbg.generated
     */
    int insert(GardenDepositInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_deposit_info
     *
     * @mbg.generated
     */
    int insertSelective(GardenDepositInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_deposit_info
     *
     * @mbg.generated
     */
    List<GardenDepositInfo> selectByExample(GardenDepositInfoExample example);
    
    /**
     * @param paramMap
     * @return
     */
    List<GardenDepositInfoExtra> selectForPage(Map paramMap);
    
    /**
     * @param paramMap
     * @return
     */
    int selectForPageCount(Map paramMap);
    
    /**
     * @return
     */
    List<GardenStudentState> selectStudentStateForDeposit(int gardenId);
    
    /**
     * @param gardenId
     * @return
     */
    int selectCurrentDayCount(int gardenId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_deposit_info
     *
     * @mbg.generated
     */
    GardenDepositInfo selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_deposit_info
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(GardenDepositInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_deposit_info
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(GardenDepositInfo record);
}