package com.dbapp.garden.mapper;

import com.dbapp.garden.entity.GardenContractTemplet;
import com.dbapp.garden.entity.GardenContractTempletExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GardenContractTempletMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_contract_templet
     *
     * @mbg.generated
     */
    long countByExample(GardenContractTempletExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_contract_templet
     *
     * @mbg.generated
     */
    int deleteByExample(GardenContractTempletExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_contract_templet
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_contract_templet
     *
     * @mbg.generated
     */
    int insert(GardenContractTemplet record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_contract_templet
     *
     * @mbg.generated
     */
    int insertSelective(GardenContractTemplet record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_contract_templet
     *
     * @mbg.generated
     */
    List<GardenContractTemplet> selectByExample(GardenContractTempletExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_contract_templet
     *
     * @mbg.generated
     */
    GardenContractTemplet selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_contract_templet
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(GardenContractTemplet record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table garden_contract_templet
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(GardenContractTemplet record);
}