package com.dbapp.garden.mapper;

import com.dbapp.garden.entity.GardenCourseRule;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface GardenCourseRuleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenCourseRule record);

    int insertSelective(GardenCourseRule record);

    GardenCourseRule selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenCourseRule record);

    int updateByPrimaryKey(GardenCourseRule record);

    List<GardenCourseRule> getCourseRuleList(Map<String, Object> map);
}