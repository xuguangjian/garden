package com.dbapp.garden.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.dbapp.garden.entity.SysRole;
@Mapper
public interface SysRoleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysRole record);

    int updateByPrimaryKey(SysRole record);

	List<SysRole> getRoleList(Map<String, Object> map);

	Integer getRoleCount(Map<String, Object> map);

	SysRole findRoleByName(Map<String,Object> map);

    /**
     * 根据用户名，获取用户对应的角色
     * @param id
     * @return
     */
    SysRole getRoleByUserId(Integer id);
}