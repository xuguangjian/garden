package com.dbapp.garden.mapper;

import com.dbapp.garden.entity.GardenCourseTime;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface GardenCourseTimeMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(GardenCourseTime record);

    int insertSelective(GardenCourseTime record);

    GardenCourseTime selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenCourseTime record);

    int updateByPrimaryKey(GardenCourseTime record);

    List<GardenCourseTime> getCourseTimeList(Map<String, Object> paramMap);

    Integer getCourseTimeCount(Map<String, Object> paramMap);
}