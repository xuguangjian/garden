package com.dbapp.garden.mapper;

import com.dbapp.garden.entity.GardenCourse;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface GardenCourseMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenCourse record);

    int insertSelective(GardenCourse record);

    GardenCourse selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenCourse record);

    int updateByPrimaryKey(GardenCourse record);

    List<GardenCourse> getCourseList(Map<String, Object> paramMap);

    Integer getCourseCount(Map<String, Object> paramMap);
}