package com.dbapp.garden.mapper;

import com.dbapp.garden.entity.GardenClassCourse;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface GardenClassCourseMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenClassCourse record);

    int insertSelective(GardenClassCourse record);

    GardenClassCourse selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenClassCourse record);

    int updateByPrimaryKey(GardenClassCourse record);

    List<GardenClassCourse> getClassCourseList(Map<String, Object> query);
}