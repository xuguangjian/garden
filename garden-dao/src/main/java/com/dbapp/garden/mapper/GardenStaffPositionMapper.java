package com.dbapp.garden.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.dbapp.garden.entity.GardenStaffPosition;
@Mapper
public interface GardenStaffPositionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenStaffPosition record);

    int insertSelective(GardenStaffPosition record);

    GardenStaffPosition selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenStaffPosition record);

    int updateByPrimaryKey(GardenStaffPosition record);

	List<GardenStaffPosition> getPositionList(Map<String, Object> paramMap);

	Integer getPositionCount(Map<String, Object> paramMap);

}