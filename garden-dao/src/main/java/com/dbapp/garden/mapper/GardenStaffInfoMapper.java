package com.dbapp.garden.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.dbapp.garden.entity.GardenStaffInfo;

@Mapper
public interface GardenStaffInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenStaffInfo record);

    int insertSelective(GardenStaffInfo record);

    GardenStaffInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenStaffInfo record);

    int updateByPrimaryKey(GardenStaffInfo record);

	List<GardenStaffInfo> getStaffList(Map<String, Object> paramMap);

	Integer getStaffCount(Map<String, Object> paramMap);
}