package com.dbapp.garden.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.dbapp.garden.entity.GardenStudentInfo;
@Mapper
public interface GardenStudentInfoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GardenStudentInfo record);

    int insertSelective(GardenStudentInfo record);

    GardenStudentInfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GardenStudentInfo record);

    int updateByPrimaryKey(GardenStudentInfo record);

	List<GardenStudentInfo> getStudentList(Map<String, Object> paramMap);

	Integer getStudentCount(Map<String, Object> paramMap);

	Integer batchDeleteStudent(Integer[] ids);

    Integer batchInsertStudent(List<GardenStudentInfo> studentList);

    List<GardenStudentInfo> getStudentByIds(Integer[] ids);

    Integer batchUpdateStudent(List<GardenStudentInfo> readyToUpdate);
}