package com.dbapp.garden.entity;

import java.util.Date;

public class GardenCourseSeries {
    private Integer id;

    private String courseSeriesName;

    private Integer courseSeriesSort;

    private String courseSeriesRemark;

    private Integer gardenId;

    private Date createTime;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCourseSeriesName() {
        return courseSeriesName;
    }

    public void setCourseSeriesName(String courseSeriesName) {
        this.courseSeriesName = courseSeriesName;
    }

    public Integer getCourseSeriesSort() {
        return courseSeriesSort;
    }

    public void setCourseSeriesSort(Integer courseSeriesSort) {
        this.courseSeriesSort = courseSeriesSort;
    }

    public String getCourseSeriesRemark() {
        return courseSeriesRemark;
    }

    public void setCourseSeriesRemark(String courseSeriesRemark) {
        this.courseSeriesRemark = courseSeriesRemark;
    }

    public Integer getGardenId() {
        return gardenId;
    }

    public void setGardenId(Integer gardenId) {
        this.gardenId = gardenId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}