package com.dbapp.garden.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class GardenStudentState {
    private Integer id;

    private String stateParam;

    private String stateProp;

    private String stateDesc;

    private Integer stateSort;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    private Date updateTime;

    private Integer gardenId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStateParam() {
        return stateParam;
    }

    public void setStateParam(String stateParam) {
        this.stateParam = stateParam;
    }

    public String getStateProp() {
        return stateProp;
    }

    public void setStateProp(String stateProp) {
        this.stateProp = stateProp;
    }

    public String getStateDesc() {
        return stateDesc;
    }

    public void setStateDesc(String stateDesc) {
        this.stateDesc = stateDesc;
    }

    public Integer getStateSort() {
        return stateSort;
    }

    public void setStateSort(Integer stateSort) {
        this.stateSort = stateSort;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getGardenId() {
        return gardenId;
    }

    public void setGardenId(Integer gardenId) {
        this.gardenId = gardenId;
    }
}