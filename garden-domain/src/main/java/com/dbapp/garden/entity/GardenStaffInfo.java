package com.dbapp.garden.entity;

import java.util.Date;

public class GardenStaffInfo {
    private Integer id;

    private String staffRealName;

    private String staffPhone;

    private Integer staffDepartment;

    private Integer staffPosition;

    private String staffEmail;

    private Integer staffState;

    private Date staffJoinTime;

    private String staffPermission;

    private String staffQq;

    private Integer gardenId;

    private Integer isMarketPerson;

    private Integer isTeacher;

    private Integer isAdviser;

    private String hasStudent;

    private Date createTime;

    private Date updateTime;

    private String staffRemark;
    

    private Integer userId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStaffRealName() {
        return staffRealName;
    }

    public void setStaffRealName(String staffRealName) {
        this.staffRealName = staffRealName;
    }

    public String getStaffPhone() {
        return staffPhone;
    }

    public void setStaffPhone(String staffPhone) {
        this.staffPhone = staffPhone;
    }

    public Integer getStaffDepartment() {
        return staffDepartment;
    }

    public void setStaffDepartment(Integer staffDepartment) {
        this.staffDepartment = staffDepartment;
    }

    public Integer getStaffPosition() {
        return staffPosition;
    }

    public void setStaffPosition(Integer staffPosition) {
        this.staffPosition = staffPosition;
    }

    public String getStaffEmail() {
        return staffEmail;
    }

    public void setStaffEmail(String staffEmail) {
        this.staffEmail = staffEmail;
    }

    public Integer getStaffState() {
        return staffState;
    }

    public void setStaffState(Integer staffState) {
        this.staffState = staffState;
    }

    public Date getStaffJoinTime() {
        return staffJoinTime;
    }

    public void setStaffJoinTime(Date staffJoinTime) {
        this.staffJoinTime = staffJoinTime;
    }

    public String getStaffPermission() {
        return staffPermission;
    }

    public void setStaffPermission(String staffPermission) {
        this.staffPermission = staffPermission;
    }

    public String getStaffQq() {
        return staffQq;
    }

    public void setStaffQq(String staffQq) {
        this.staffQq = staffQq;
    }

    public Integer getGardenId() {
        return gardenId;
    }

    public void setGardenId(Integer gardenId) {
        this.gardenId = gardenId;
    }

    public Integer getIsMarketPerson() {
        return isMarketPerson;
    }

    public void setIsMarketPerson(Integer isMarketPerson) {
        this.isMarketPerson = isMarketPerson;
    }

    public Integer getIsTeacher() {
        return isTeacher;
    }

    public void setIsTeacher(Integer isTeacher) {
        this.isTeacher = isTeacher;
    }

    public Integer getIsAdviser() {
        return isAdviser;
    }

    public void setIsAdviser(Integer isAdviser) {
        this.isAdviser = isAdviser;
    }

    public String getHasStudent() {
        return hasStudent;
    }

    public void setHasStudent(String hasStudent) {
        this.hasStudent = hasStudent;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getStaffRemark() {
        return staffRemark;
    }

    public void setStaffRemark(String staffRemark) {
        this.staffRemark = staffRemark;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}