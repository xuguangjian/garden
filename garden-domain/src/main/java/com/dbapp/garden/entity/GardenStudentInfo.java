package com.dbapp.garden.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class GardenStudentInfo {
    private Integer id;

    private String stuNo;

    private String stuNickName;

    private String stuName;

    private Integer stuSex;
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    private Date stuBirth;

    private String stuAge;

    private String stuPhone;

    private String stuTelephone;

    private String stuBackupPhone;

    private String stuParentRelationship;

    private String stuParentName;

    private Integer stuSource;

    private Date stuJoinDate;

    private Date stuAssignDate;

    private String stuWechatName;

    private String stuImportance;

    private String stuEmail;

    private String stuQq;

    private Integer stuState;

    private Integer stuAreaFirst;
    private Integer stuAreaSecond;

    private String stuIntentionCourse;

    private String stuCurrentClass;

    private String stuHomeAddress;

    private Date createTime;

    private Date updateTime;

    private Date stuIntentionTime;

    private Integer stuFirstContact;

    private String stuAffiliatedAdviser;

    private Integer stuMarketPerson;

    private String stuBasicInfo;

    private Integer stuReceptionist;

    private String stuGroup;

    private Integer stuCurrentScore;

    private Integer stuFollowStyle;

    private Integer stuMemberState;

    private Integer gardenId;
    
    private String stuSchool;

    private Integer stuLastAssign;

    private Integer stuAssignCount;

    public Integer getStuLastAssign() {
        return stuLastAssign;
    }

    public void setStuLastAssign(Integer stuLastAssign) {
        this.stuLastAssign = stuLastAssign;
    }

    public Integer getStuAssignCount() {
        return stuAssignCount;
    }

    public void setStuAssignCount(Integer stuAssignCount) {
        this.stuAssignCount = stuAssignCount;
    }

    public String getStuSchool() {
		return stuSchool;
	}

	public void setStuSchool(String stuSchool) {
		this.stuSchool = stuSchool;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStuNo() {
        return stuNo;
    }

    public void setStuNo(String stuNo) {
        this.stuNo = stuNo;
    }

    public String getStuNickName() {
        return stuNickName;
    }

    public void setStuNickName(String stuNickName) {
        this.stuNickName = stuNickName;
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public Integer getStuSex() {
        return stuSex;
    }

    public void setStuSex(Integer stuSex) {
        this.stuSex = stuSex;
    }

    public Date getStuBirth() {
        return stuBirth;
    }

    public void setStuBirth(Date stuBirth) {
        this.stuBirth = stuBirth;
    }

    public String getStuAge() {
        return stuAge;
    }

    public void setStuAge(String stuAge) {
        this.stuAge = stuAge;
    }

    public String getStuPhone() {
        return stuPhone;
    }

    public void setStuPhone(String stuPhone) {
        this.stuPhone = stuPhone;
    }

    public String getStuTelephone() {
        return stuTelephone;
    }

    public void setStuTelephone(String stuTelephone) {
        this.stuTelephone = stuTelephone;
    }

    public String getStuBackupPhone() {
        return stuBackupPhone;
    }

    public void setStuBackupPhone(String stuBackupPhone) {
        this.stuBackupPhone = stuBackupPhone;
    }

    public String getStuParentRelationship() {
        return stuParentRelationship;
    }

    public void setStuParentRelationship(String stuParentRelationship) {
        this.stuParentRelationship = stuParentRelationship;
    }

    public String getStuParentName() {
        return stuParentName;
    }

    public void setStuParentName(String stuParentName) {
        this.stuParentName = stuParentName;
    }

    public Integer getStuSource() {
        return stuSource;
    }

    public void setStuSource(Integer stuSource) {
        this.stuSource = stuSource;
    }

    public Date getStuJoinDate() {
        return stuJoinDate;
    }

    public void setStuJoinDate(Date stuJoinDate) {
        this.stuJoinDate = stuJoinDate;
    }

    public Date getStuAssignDate() {
        return stuAssignDate;
    }

    public void setStuAssignDate(Date stuAssignDate) {
        this.stuAssignDate = stuAssignDate;
    }

    public String getStuWechatName() {
        return stuWechatName;
    }

    public void setStuWechatName(String stuWechatName) {
        this.stuWechatName = stuWechatName;
    }

    public String getStuImportance() {
        return stuImportance;
    }

    public void setStuImportance(String stuImportance) {
        this.stuImportance = stuImportance;
    }

    public String getStuEmail() {
        return stuEmail;
    }

    public void setStuEmail(String stuEmail) {
        this.stuEmail = stuEmail;
    }

    public String getStuQq() {
        return stuQq;
    }

    public void setStuQq(String stuQq) {
        this.stuQq = stuQq;
    }

    public Integer getStuState() {
        return stuState;
    }

    public void setStuState(Integer stuState) {
        this.stuState = stuState;
    }

   

    public String getStuIntentionCourse() {
        return stuIntentionCourse;
    }

    public void setStuIntentionCourse(String stuIntentionCourse) {
        this.stuIntentionCourse = stuIntentionCourse;
    }

    public String getStuCurrentClass() {
        return stuCurrentClass;
    }

    public void setStuCurrentClass(String stuCurrentClass) {
        this.stuCurrentClass = stuCurrentClass;
    }

    public String getStuHomeAddress() {
        return stuHomeAddress;
    }

    public void setStuHomeAddress(String stuHomeAddress) {
        this.stuHomeAddress = stuHomeAddress;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getStuIntentionTime() {
        return stuIntentionTime;
    }

    public void setStuIntentionTime(Date stuIntentionTime) {
        this.stuIntentionTime = stuIntentionTime;
    }

    public Integer getStuFirstContact() {
        return stuFirstContact;
    }

    public void setStuFirstContact(Integer stuFirstContact) {
        this.stuFirstContact = stuFirstContact;
    }

    public String getStuAffiliatedAdviser() {
        return stuAffiliatedAdviser;
    }

    public void setStuAffiliatedAdviser(String stuAffiliatedAdviser) {
        this.stuAffiliatedAdviser = stuAffiliatedAdviser;
    }

    public Integer getStuAreaFirst() {
		return stuAreaFirst;
	}

	public void setStuAreaFirst(Integer stuAreaFirst) {
		this.stuAreaFirst = stuAreaFirst;
	}

	public Integer getStuAreaSecond() {
		return stuAreaSecond;
	}

	public void setStuAreaSecond(Integer stuAreaSecond) {
		this.stuAreaSecond = stuAreaSecond;
	}

	public Integer getStuMarketPerson() {
        return stuMarketPerson;
    }

    public void setStuMarketPerson(Integer stuMarketPerson) {
        this.stuMarketPerson = stuMarketPerson;
    }

    public String getStuBasicInfo() {
        return stuBasicInfo;
    }

    public void setStuBasicInfo(String stuBasicInfo) {
        this.stuBasicInfo = stuBasicInfo;
    }

    public Integer getStuReceptionist() {
        return stuReceptionist;
    }

    public void setStuReceptionist(Integer stuReceptionist) {
        this.stuReceptionist = stuReceptionist;
    }

    public String getStuGroup() {
        return stuGroup;
    }

    public void setStuGroup(String stuGroup) {
        this.stuGroup = stuGroup;
    }

    public Integer getStuCurrentScore() {
        return stuCurrentScore;
    }

    public void setStuCurrentScore(Integer stuCurrentScore) {
        this.stuCurrentScore = stuCurrentScore;
    }

    public Integer getStuFollowStyle() {
        return stuFollowStyle;
    }

    public void setStuFollowStyle(Integer stuFollowStyle) {
        this.stuFollowStyle = stuFollowStyle;
    }

    public Integer getStuMemberState() {
        return stuMemberState;
    }

    public void setStuMemberState(Integer stuMemberState) {
        this.stuMemberState = stuMemberState;
    }

    public Integer getGardenId() {
        return gardenId;
    }

    public void setGardenId(Integer gardenId) {
        this.gardenId = gardenId;
    }
}