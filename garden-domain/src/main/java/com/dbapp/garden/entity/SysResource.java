package com.dbapp.garden.entity;

import java.util.Date;

public class SysResource {
    private Integer id;

    private String resName;

    private Integer resType;

    private String resUrl;

    private String resCode;

    private Integer resParentId;

    private Integer resSort;

    private String resDesc;
    
    private String resIcon;

    private Integer isGodRes;

    public Integer getIsGodRes() {
        return isGodRes;
    }

    public void setIsGodRes(Integer isGodRes) {
        this.isGodRes = isGodRes;
    }

    public String getResIcon() {
		return resIcon;
	}

	public void setResIcon(String resIcon) {
		this.resIcon = resIcon;
	}

	private Date createTime;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getResName() {
        return resName;
    }

    public void setResName(String resName) {
        this.resName = resName;
    }

    public Integer getResType() {
        return resType;
    }

    public void setResType(Integer resType) {
        this.resType = resType;
    }

    public String getResUrl() {
        return resUrl;
    }

    public void setResUrl(String resUrl) {
        this.resUrl = resUrl;
    }

    public String getResCode() {
        return resCode;
    }

    public void setResCode(String resCode) {
        this.resCode = resCode;
    }

    public Integer getResParentId() {
        return resParentId;
    }

    public void setResParentId(Integer resParentId) {
        this.resParentId = resParentId;
    }

    public Integer getResSort() {
        return resSort;
    }

    public void setResSort(Integer resSort) {
        this.resSort = resSort;
    }

    public String getResDesc() {
        return resDesc;
    }

    public void setResDesc(String resDesc) {
        this.resDesc = resDesc;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}