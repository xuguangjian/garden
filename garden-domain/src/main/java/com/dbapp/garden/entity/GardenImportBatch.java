package com.dbapp.garden.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class GardenImportBatch {
    private Integer id;

    private String batchName;

    private String batchSource;

    private Integer batchCount;

    private Integer batchSuccess;

    private Integer repeatFail;

    private Integer otherFail;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    private Date createTime;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBatchName() {
        return batchName;
    }

    public void setBatchName(String batchName) {
        this.batchName = batchName;
    }

    public String getBatchSource() {
        return batchSource;
    }

    public void setBatchSource(String batchSource) {
        this.batchSource = batchSource;
    }

    public Integer getBatchCount() {
        return batchCount;
    }

    public void setBatchCount(Integer batchCount) {
        this.batchCount = batchCount;
    }

    public Integer getBatchSuccess() {
        return batchSuccess;
    }

    public void setBatchSuccess(Integer batchSuccess) {
        this.batchSuccess = batchSuccess;
    }

    public Integer getRepeatFail() {
        return repeatFail;
    }

    public void setRepeatFail(Integer repeatFail) {
        this.repeatFail = repeatFail;
    }

    public Integer getOtherFail() {
        return otherFail;
    }

    public void setOtherFail(Integer otherFail) {
        this.otherFail = otherFail;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}