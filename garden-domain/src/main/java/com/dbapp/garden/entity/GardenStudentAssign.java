package com.dbapp.garden.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class GardenStudentAssign {
    private Integer id;

    private String beforeAssignTeacher;

    private String afterAssignTeacher;

    private Integer studentId;

    private Integer assignOperator;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    private Date createTime;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBeforeAssignTeacher() {
        return beforeAssignTeacher;
    }

    public void setBeforeAssignTeacher(String beforeAssignTeacher) {
        this.beforeAssignTeacher = beforeAssignTeacher;
    }

    public String getAfterAssignTeacher() {
        return afterAssignTeacher;
    }

    public void setAfterAssignTeacher(String afterAssignTeacher) {
        this.afterAssignTeacher = afterAssignTeacher;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public Integer getAssignOperator() {
        return assignOperator;
    }

    public void setAssignOperator(Integer assignOperator) {
        this.assignOperator = assignOperator;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}