package com.dbapp.garden.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class GardenStudentFollow {
    private Integer id;

    private String followParam;

    private Integer followSort;

    private String followDesc;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFollowParam() {
        return followParam;
    }

    public void setFollowParam(String followParam) {
        this.followParam = followParam;
    }

    public Integer getFollowSort() {
        return followSort;
    }

    public void setFollowSort(Integer followSort) {
        this.followSort = followSort;
    }

    public String getFollowDesc() {
        return followDesc;
    }

    public void setFollowDesc(String followDesc) {
        this.followDesc = followDesc;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}