package com.dbapp.garden.entity;

import java.util.Date;

public class GardenStudentUpgrade {
    private Integer id;

    private Integer studentId;

    private String beforeCourseId;

    private String afterCourseId;

    private String remark;

    private Integer operator;

    private Date createTime;

    private Integer gardenId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public String getBeforeCourseId() {
        return beforeCourseId;
    }

    public void setBeforeCourseId(String beforeCourseId) {
        this.beforeCourseId = beforeCourseId;
    }

    public String getAfterCourseId() {
        return afterCourseId;
    }

    public void setAfterCourseId(String afterCourseId) {
        this.afterCourseId = afterCourseId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getOperator() {
        return operator;
    }

    public void setOperator(Integer operator) {
        this.operator = operator;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getGardenId() {
        return gardenId;
    }

    public void setGardenId(Integer gardenId) {
        this.gardenId = gardenId;
    }
}