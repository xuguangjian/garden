package com.dbapp.garden.entity;

import java.util.Date;

public class GardenCourseRule {
    private Integer id;

    private Integer arriveLate;

    private Integer absentCourse;

    private Integer deductCourse;

    private Date createTime;

    private Date updateTime;

    private Integer gardenId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getArriveLate() {
        return arriveLate;
    }

    public void setArriveLate(Integer arriveLate) {
        this.arriveLate = arriveLate;
    }

    public Integer getAbsentCourse() {
        return absentCourse;
    }

    public void setAbsentCourse(Integer absentCourse) {
        this.absentCourse = absentCourse;
    }

    public Integer getDeductCourse() {
        return deductCourse;
    }

    public void setDeductCourse(Integer deductCourse) {
        this.deductCourse = deductCourse;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getGardenId() {
        return gardenId;
    }

    public void setGardenId(Integer gardenId) {
        this.gardenId = gardenId;
    }
}