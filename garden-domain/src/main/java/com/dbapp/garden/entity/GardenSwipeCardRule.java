package com.dbapp.garden.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class GardenSwipeCardRule {
    private Integer id;

    private String rulesName;

    private Integer rulesState;

    private String swipeTime;

    private Integer feeType;

    private Float deductHour;

    private Integer courseHourType;

    private Integer swipeCardCount;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    private Date createTime;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRulesName() {
        return rulesName;
    }

    public void setRulesName(String rulesName) {
        this.rulesName = rulesName;
    }

    public Integer getRulesState() {
        return rulesState;
    }

    public void setRulesState(Integer rulesState) {
        this.rulesState = rulesState;
    }

    public String getSwipeTime() {
        return swipeTime;
    }

    public void setSwipeTime(String swipeTime) {
        this.swipeTime = swipeTime;
    }

    public Integer getFeeType() {
        return feeType;
    }

    public void setFeeType(Integer feeType) {
        this.feeType = feeType;
    }

    public Float getDeductHour() {
        return deductHour;
    }

    public void setDeductHour(Float deductHour) {
        this.deductHour = deductHour;
    }

    public Integer getCourseHourType() {
        return courseHourType;
    }

    public void setCourseHourType(Integer courseHourType) {
        this.courseHourType = courseHourType;
    }

    public Integer getSwipeCardCount() {
        return swipeCardCount;
    }

    public void setSwipeCardCount(Integer swipeCardCount) {
        this.swipeCardCount = swipeCardCount;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}