package com.dbapp.garden.entity;

import java.util.Date;

public class GardenSmallTicketInfo {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column garden_smallticket_info.id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column garden_smallticket_info.smallticket
     *
     * @mbg.generated
     */
    private String smallticket;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column garden_smallticket_info.garden_id
     *
     * @mbg.generated
     */
    private Integer garden_id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column garden_smallticket_info.createDate
     *
     * @mbg.generated
     */
    private Date createDate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column garden_smallticket_info.updateDate
     *
     * @mbg.generated
     */
    private Date updateDate;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column garden_smallticket_info.id
     *
     * @return the value of garden_smallticket_info.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column garden_smallticket_info.id
     *
     * @param id the value for garden_smallticket_info.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column garden_smallticket_info.smallticket
     *
     * @return the value of garden_smallticket_info.smallticket
     *
     * @mbg.generated
     */
    public String getSmallticket() {
        return smallticket;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column garden_smallticket_info.smallticket
     *
     * @param smallticket the value for garden_smallticket_info.smallticket
     *
     * @mbg.generated
     */
    public void setSmallticket(String smallticket) {
        this.smallticket = smallticket == null ? null : smallticket.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column garden_smallticket_info.garden_id
     *
     * @return the value of garden_smallticket_info.garden_id
     *
     * @mbg.generated
     */
    public Integer getGarden_id() {
        return garden_id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column garden_smallticket_info.garden_id
     *
     * @param garden_id the value for garden_smallticket_info.garden_id
     *
     * @mbg.generated
     */
    public void setGarden_id(Integer garden_id) {
        this.garden_id = garden_id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column garden_smallticket_info.createDate
     *
     * @return the value of garden_smallticket_info.createDate
     *
     * @mbg.generated
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column garden_smallticket_info.createDate
     *
     * @param createDate the value for garden_smallticket_info.createDate
     *
     * @mbg.generated
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column garden_smallticket_info.updateDate
     *
     * @return the value of garden_smallticket_info.updateDate
     *
     * @mbg.generated
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column garden_smallticket_info.updateDate
     *
     * @param updateDate the value for garden_smallticket_info.updateDate
     *
     * @mbg.generated
     */
    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}