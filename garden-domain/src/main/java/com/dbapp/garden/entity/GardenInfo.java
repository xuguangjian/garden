package com.dbapp.garden.entity;

import java.util.Date;

public class GardenInfo {
    private Integer id;

    private String gardenName;

    private String gardenManager;

    private String gardenManagerPhone;

    private String gardenAddress;

    private Date createTime;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGardenName() {
        return gardenName;
    }

    public void setGardenName(String gardenName) {
        this.gardenName = gardenName;
    }

    public String getGardenManager() {
        return gardenManager;
    }

    public void setGardenManager(String gardenManager) {
        this.gardenManager = gardenManager;
    }

    public String getGardenManagerPhone() {
        return gardenManagerPhone;
    }

    public void setGardenManagerPhone(String gardenManagerPhone) {
        this.gardenManagerPhone = gardenManagerPhone;
    }

    public String getGardenAddress() {
        return gardenAddress;
    }

    public void setGardenAddress(String gardenAddress) {
        this.gardenAddress = gardenAddress;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}