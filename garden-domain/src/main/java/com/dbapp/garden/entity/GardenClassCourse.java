package com.dbapp.garden.entity;

import java.util.Date;

public class GardenClassCourse {
    private Integer id;

    private String className;

    private String primaryTeacher;

    private String assistTeacher;

    private Integer classTime;

    private Date classDate;

    private Integer classroom;

    private Integer course;

    private Date createTime;

    private Date updateTime;

    private Integer gardenId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getPrimaryTeacher() {
        return primaryTeacher;
    }

    public void setPrimaryTeacher(String primaryTeacher) {
        this.primaryTeacher = primaryTeacher;
    }

    public String getAssistTeacher() {
        return assistTeacher;
    }

    public void setAssistTeacher(String assistTeacher) {
        this.assistTeacher = assistTeacher;
    }

    public Integer getClassTime() {
        return classTime;
    }

    public void setClassTime(Integer classTime) {
        this.classTime = classTime;
    }

    public Date getClassDate() {
        return classDate;
    }

    public void setClassDate(Date classDate) {
        this.classDate = classDate;
    }

    public Integer getClassroom() {
        return classroom;
    }

    public void setClassroom(Integer classroom) {
        this.classroom = classroom;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getGardenId() {
        return gardenId;
    }

    public void setGardenId(Integer gardenId) {
        this.gardenId = gardenId;
    }
}