package com.dbapp.garden.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class GardenStudentImportance {
    private Integer id;

    private String importanceParam;

    private String importanceDesc;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImportanceParam() {
        return importanceParam;
    }

    public void setImportanceParam(String importanceParam) {
        this.importanceParam = importanceParam;
    }

    public String getImportanceDesc() {
        return importanceDesc;
    }

    public void setImportanceDesc(String importanceDesc) {
        this.importanceDesc = importanceDesc;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}