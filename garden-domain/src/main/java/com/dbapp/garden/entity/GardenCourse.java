package com.dbapp.garden.entity;

import java.util.Date;

public class GardenCourse {
    private Integer id;

    private String courseName;

    private Integer courseSeries;

    private String suitableAgeFromYear;

    private String suitableAgeFromMonth;

    private String suitableAgeToYear;

    private String suitableAgeToMonth;

    private Integer totalCapacity;

    private Integer memberCount;

    private Integer experienceCount;

    private Integer courseSort;

    private String courseRemark;

    private Float deductLessonHour;

    private Date createTime;

    private Date updateTime;

    private Integer gardenId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Integer getCourseSeries() {
        return courseSeries;
    }

    public void setCourseSeries(Integer courseSeries) {
        this.courseSeries = courseSeries;
    }

    public String getSuitableAgeFromYear() {
        return suitableAgeFromYear;
    }

    public void setSuitableAgeFromYear(String suitableAgeFromYear) {
        this.suitableAgeFromYear = suitableAgeFromYear;
    }

    public String getSuitableAgeFromMonth() {
        return suitableAgeFromMonth;
    }

    public void setSuitableAgeFromMonth(String suitableAgeFromMonth) {
        this.suitableAgeFromMonth = suitableAgeFromMonth;
    }

    public String getSuitableAgeToYear() {
        return suitableAgeToYear;
    }

    public void setSuitableAgeToYear(String suitableAgeToYear) {
        this.suitableAgeToYear = suitableAgeToYear;
    }

    public String getSuitableAgeToMonth() {
        return suitableAgeToMonth;
    }

    public void setSuitableAgeToMonth(String suitableAgeToMonth) {
        this.suitableAgeToMonth = suitableAgeToMonth;
    }

    public Integer getTotalCapacity() {
        return totalCapacity;
    }

    public void setTotalCapacity(Integer totalCapacity) {
        this.totalCapacity = totalCapacity;
    }

    public Integer getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(Integer memberCount) {
        this.memberCount = memberCount;
    }

    public Integer getExperienceCount() {
        return experienceCount;
    }

    public void setExperienceCount(Integer experienceCount) {
        this.experienceCount = experienceCount;
    }

    public Integer getCourseSort() {
        return courseSort;
    }

    public void setCourseSort(Integer courseSort) {
        this.courseSort = courseSort;
    }

    public String getCourseRemark() {
        return courseRemark;
    }

    public void setCourseRemark(String courseRemark) {
        this.courseRemark = courseRemark;
    }

    public Float getDeductLessonHour() {
        return deductLessonHour;
    }

    public void setDeductLessonHour(Float deductLessonHour) {
        this.deductLessonHour = deductLessonHour;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getGardenId() {
        return gardenId;
    }

    public void setGardenId(Integer gardenId) {
        this.gardenId = gardenId;
    }
}