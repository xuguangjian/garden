package com.dbapp.garden.entity;

import com.dbapp.garden.entity.GardenDepositInfo;

public class GardenDepositInfoExtra extends GardenDepositInfo {

	private String studentName;
	
	private String studentParentPhone;
	
	private String studentStateName;
	
	private String payWay;
	
	private String payStateName;
	
	private String recordUserName;

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getPayWay() {
		return payWay;
	}

	public void setPayWay(String payWay) {
		this.payWay = payWay;
	}

	public String getPayStateName() {
		return payStateName;
	}

	public void setPayStateName(String payStateName) {
		this.payStateName = payStateName;
	}

	public String getStudentParentPhone() {
		return studentParentPhone;
	}

	public void setStudentParentPhone(String studentParentPhone) {
		this.studentParentPhone = studentParentPhone;
	}

	public String getRecordUserName() {
		return recordUserName;
	}

	public void setRecordUserName(String recordUserName) {
		this.recordUserName = recordUserName;
	}

	public String getStudentStateName() {
		return studentStateName;
	}

	public void setStudentStateName(String studentStateName) {
		this.studentStateName = studentStateName;
	}
	
	
	
}
