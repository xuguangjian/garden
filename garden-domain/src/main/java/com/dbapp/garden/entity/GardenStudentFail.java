package com.dbapp.garden.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class GardenStudentFail {
    private Integer id;

    private String stuName;

    private String stuNickname;

    private String stuParentName;

    private String stuParentRelationship;

    private String stuPhone;

    private Integer stuFailReason;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    private Date createTime;

    private Date updateTime;

    private Integer gardenId;

    private Integer batchId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public String getStuNickname() {
        return stuNickname;
    }

    public void setStuNickname(String stuNickname) {
        this.stuNickname = stuNickname;
    }

    public String getStuParentName() {
        return stuParentName;
    }

    public void setStuParentName(String stuParentName) {
        this.stuParentName = stuParentName;
    }

    public String getStuParentRelationship() {
        return stuParentRelationship;
    }

    public void setStuParentRelationship(String stuParentRelationship) {
        this.stuParentRelationship = stuParentRelationship;
    }

    public String getStuPhone() {
        return stuPhone;
    }

    public void setStuPhone(String stuPhone) {
        this.stuPhone = stuPhone;
    }

    public Integer getStuFailReason() {
        return stuFailReason;
    }

    public void setStuFailReason(Integer stuFailReason) {
        this.stuFailReason = stuFailReason;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getGardenId() {
        return gardenId;
    }

    public void setGardenId(Integer gardenId) {
        this.gardenId = gardenId;
    }

    public Integer getBatchId() {
        return batchId;
    }

    public void setBatchId(Integer batchId) {
        this.batchId = batchId;
    }
}