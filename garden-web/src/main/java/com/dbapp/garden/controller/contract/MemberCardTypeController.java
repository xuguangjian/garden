package com.dbapp.garden.controller.contract;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.dbapp.garden.config.security.PermissionUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.dbapp.garden.common.DataTableUtils;
import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.entity.GardenContractPackageInfo;
import com.dbapp.garden.entity.GardenMemberCardTypeInfo;
import com.dbapp.garden.entity.GardenPayWayInfo;
import com.dbapp.garden.service.IGardenMemberCardTypeInfoService;
import com.dbapp.garden.service.IGardenPayWayInfoService;

@Controller
public class MemberCardTypeController extends BaseContractController {

	@Autowired
	IGardenMemberCardTypeInfoService typeService;
	
	@PostMapping("/membercardtypelist")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_MEMBER_CARD_TYPE') or hasAuthority('PERMISSION_MEMBER_CARD_TYPE_VIEW')")
	public Map<String, ?> memberCardTypeList(HttpServletRequest request, @SessionAttribute("user") PermissionUser user, String param) {
		int gardenId = this.getSelfGardenId(request);
		Map<String, Object> paramMap = DataTableUtils.parseParam(param);
		if (paramMap == null) {
			return RenderJSON.makeMap(-1, "参数不合法！", null, "");
		}
		String sEcho = (String) paramMap.get("sEcho");
		String sSearch = (String) paramMap.get("sSearch");
		int iDisplayStart = (int) paramMap.get("iDisplayStart");
		int iDisplayLength = (int) paramMap.get("iDisplayLength");
		paramMap.put("gardenId",user.getGardenId());
		List<GardenMemberCardTypeInfo> memberCardTypeList = typeService.getMemberCardTypeList(gardenId, sSearch, iDisplayStart, iDisplayLength);
		int count = typeService.getMemberCardTypeCount(gardenId, sSearch);
		return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, memberCardTypeList, "");
	}
	
	@PostMapping("/membercardtype")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_MEMBER_CARD_TYPE_ADD')")
	public Map<String, ?> addMemberCardType(@SessionAttribute("user") PermissionUser user,
			@RequestBody @Valid GardenMemberCardTypeInfo typeInfo) {
		if(typeInfo == null){
			return RenderJSON.makeStandMap(2, "对不起，添加失败！", "error");
		}

		if(StringUtils.isBlank(typeInfo.getTypeName())){
			return RenderJSON.makeStandMap(2, "对不起，添加失败,类型名称为空！", "error");
		}

		if(typeInfo.getTypeDiscount() == null){
			return RenderJSON.makeStandMap(2, "对不起，添加失败,商城折扣为空！", "error");
		}

		typeInfo.setCreateDate(new Date());
		typeInfo.setUpdateDate(new Date());
		typeInfo.setGarden_id(user.getGardenId());
		typeInfo = typeService.saveMemberCardType(typeInfo);

		if (typeInfo != null) {
			return RenderJSON.makeStandMap(1, "恭喜您，添加成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，添加失败！", "error");
		}
	}
	
	@DeleteMapping("/membercardtype/{id}")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_MEMBER_CARD_TYPE_DELETE')")
	public Map<String, ?> deleteMemberCardType(@SessionAttribute("user") PermissionUser user, @PathVariable("id") Integer id) {
		if(id == null){
			return RenderJSON.makeStandMap(2, "对不起，删除失败！", "error");
		}

		GardenMemberCardTypeInfo typeInfo = typeService.findById(id);
		if(typeInfo == null){
			return RenderJSON.makeStandMap(2, "对不起，删除失败！", "error");
		}
		if (!typeInfo.getGarden_id().equals(user.getGardenId())){
			return RenderJSON.makeStandMap(2, "对不起，您没有删除权限！", "error");
		}

		int result = typeService.deleteMemberCardTypeInfo(id);
		if (result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，删除成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
		}
	}
	
	@PutMapping("/membercardtype/{id}")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_MEMBER_CARD_TYPE_EDIT')")
	public Map<String, ?> editMemberCardType(@SessionAttribute("user") PermissionUser user, @PathVariable("id") Integer id,
			@RequestBody @Valid GardenMemberCardTypeInfo typeInfo) {
		if(id == null || typeInfo == null){
			return RenderJSON.makeStandMap(2, "对不起，参数不正确，编辑失败！", "error");
		}
		GardenMemberCardTypeInfo tInfo = typeService.findById(id);
		if(tInfo == null){
			return RenderJSON.makeStandMap(2, "对不起，编辑失败！", "error");
		}

		if(StringUtils.isBlank(typeInfo.getTypeName())){
			return RenderJSON.makeStandMap(2, "对不起，编辑失败,类型名称为空！", "error");
		}

		if(typeInfo.getTypeDiscount() == null){
			return RenderJSON.makeStandMap(2, "对不起，编辑失败,商城折扣为空！", "error");
		}
		if (!tInfo.getGarden_id().equals(user.getGardenId())){
			return RenderJSON.makeStandMap(2, "对不起，您不具备编辑权限", "error");
		}

		typeInfo.setUpdateDate(new Date());
		typeInfo.setGarden_id(user.getGardenId());
		typeInfo.setId(id);
		typeInfo = typeService.saveMemberCardType(typeInfo);
		if (typeInfo != null) {
			return RenderJSON.makeStandMap(1, "恭喜您，编辑成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
		}

	}
}
