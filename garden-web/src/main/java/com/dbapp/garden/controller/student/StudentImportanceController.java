package com.dbapp.garden.controller.student;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dbapp.garden.common.DataTableUtils;
import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.entity.GardenStudentImportance;
import com.dbapp.garden.service.GardenStudentImportanceService;
import com.dbapp.garden.vm.StudentImportanceVm;

/**
 * 重要程度 控制器类
 * 
 * @author gangzi
 *
 */
@Controller
public class StudentImportanceController {

//	private static final Logger loggger = LoggerFactory.getLogger(StuBasicInfoConfigController.class);

	@Autowired
	private GardenStudentImportanceService stuImportanceService;


	@PostMapping("/studentimportancelist")
	@ResponseBody
	// @PreAuthorize("hasAuthority('PERMISSION_STUDENT_SOURCE_LIST')")
	public Map<String, ?> studentImportanceList(HttpServletRequest request, String param) {

		Map<String, Object> paramMap = DataTableUtils.parseParam(param);
		if (paramMap == null) {
			return RenderJSON.makeMap(-1, "参数不合法！", null, "");
		}
		String sEcho = (String) paramMap.get("sEcho");
		String sSearch = (String) paramMap.get("sSearch");
		int iDisplayStart = (int) paramMap.get("iDisplayStart");
		int iDisplayLength = (int) paramMap.get("iDisplayLength");

		paramMap.clear();
		paramMap.put("search", sSearch);
		paramMap.put("start", iDisplayStart);
		paramMap.put("limit", iDisplayLength);

		List<GardenStudentImportance> studentList = stuImportanceService.getStudentImportanceList(paramMap);
		Integer count = stuImportanceService.getStudentImportanceCount(paramMap);

		return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, studentList, "");
	}

	@PostMapping("/studentimportanceadd")
	@ResponseBody
	public Map<String, ?> addStudentImportance(HttpServletRequest request,
			@RequestBody @Valid StudentImportanceVm stuImportanceVm) {
		GardenStudentImportance stuImportance = new GardenStudentImportance();
		stuImportance.setImportanceParam(stuImportanceVm.getImportanceParam());
		stuImportance.setImportanceDesc(stuImportanceVm.getImportanceDesc());
		stuImportance.setCreateTime(new Date());
		stuImportance.setUpdateTime(new Date());

		Integer result = stuImportanceService.save(stuImportance);
		
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，添加成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，添加失败！", "error");
		}

	}
	@DeleteMapping("/studentimportancedelete/{id}")
	@ResponseBody
	public Map<String, ?> deleteStudentImportance(HttpServletRequest request,
			@PathVariable("id") Integer id) {
		
		Integer result = stuImportanceService.delete(id);
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，删除成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
		}
		
	}
	@PostMapping("/studentimportanceedit/{id}")
	@ResponseBody
	public Map<String, ?> editStudentImportance(HttpServletRequest request,
			@PathVariable("id") Integer id,@RequestBody @Valid StudentImportanceVm stuImportanceVm) {
		
		GardenStudentImportance stuImportance = new GardenStudentImportance();
		stuImportance.setId(id);
		stuImportance.setImportanceParam(stuImportanceVm.getImportanceParam());
		stuImportance.setImportanceDesc(stuImportanceVm.getImportanceDesc());
		stuImportance.setUpdateTime(new Date());
		
		
		Integer result = stuImportanceService.save(stuImportance);
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，编辑成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
		}
		
	}
}
