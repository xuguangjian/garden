package com.dbapp.garden.controller.teaching;

import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.entity.GardenCourseRule;
import com.dbapp.garden.service.GardenCourseRuleService;
import com.dbapp.garden.vm.CourseRuleVm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/13.
 */
@Controller
public class GardenCourseRuleController {

    @Autowired
    private GardenCourseRuleService courseRuleService;

    @GetMapping("/courserule")
    @ResponseBody
    public Map<String,?> getCourseRule(){
        Map<String,Object>  map=new HashMap<>(16);
        List<GardenCourseRule> courseRuleList=courseRuleService.getCourseRuleList(map);
        if (courseRuleList!=null&&!courseRuleList.isEmpty()){
            return RenderJSON.makeStandMap(1,"恭喜您，获取成功",courseRuleList.get(0));
        }else{
            return RenderJSON.makeStandMap(1,"对不起，课程规定获取失败！","error");
        }

    }

    @PutMapping("/courserule/{id}")
    @ResponseBody
    public Map<String,?> editCourseRule(@PathVariable("id") Integer id,@RequestBody @Valid CourseRuleVm ruleVm){
        GardenCourseRule rule=courseRuleService.findById(id);
        rule.setAbsentCourse(ruleVm.getAbsentCourse());
        rule.setArriveLate(ruleVm.getArriveLate());
        rule.setDeductCourse(ruleVm.getDeductCourse());
        rule.setId(id);
        rule.setUpdateTime(new Date());

        Integer result=courseRuleService.save(rule);
        if (result!=null&&result>0){
            return RenderJSON.makeStandMap(1,"恭喜您，编辑成功！","ok");
        }else{
            return RenderJSON.makeStandMap(1,"对不起，编辑失败！","error");
        }
    }

}
