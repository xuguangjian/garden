package com.dbapp.garden.controller.teaching;

import com.dbapp.garden.common.DataTableUtils;
import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.entity.GardenCourse;
import com.dbapp.garden.entity.GardenCourseSeries;
import com.dbapp.garden.entity.GardenStudentInfo;
import com.dbapp.garden.entity.GardenStudentUpgrade;
import com.dbapp.garden.service.GardenCourseSeriesService;
import com.dbapp.garden.service.GardenCourseService;
import com.dbapp.garden.service.GardenStudentInfoService;
import com.dbapp.garden.service.GardenStudentUpgradeService;
import com.dbapp.garden.vm.StudentInfoSearchVm;
import com.dbapp.garden.vm.StudentUpgradeVm;
import com.dbapp.garden.vo.GardenCourseSeriesVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author by gangzi on 2017/11/20.
 *         会员升班
 */

@Controller
public class GardenStudentUpgradeController {

    @Autowired
    private GardenStudentInfoService stuService;
    @Autowired
    private GardenStudentUpgradeService upgradeService;

    @Autowired
    private GardenCourseService courseService;

    @Autowired
    private GardenCourseSeriesService seriesService;
    @PostMapping("/studentupgradelist")
    @ResponseBody
    @PreAuthorize("hasAuthority('PERMISSION_STUDENT_INFO')")
    public Map<String, ?> studentList(HttpServletRequest request, String param, StudentInfoSearchVm searchVm) {


        Map<String, Object> paramMap = DataTableUtils.parseParam(param);

        if (paramMap == null) {
            return RenderJSON.makeMap(-1, "参数不合法！", null, "");
        }
        String sEcho = (String) paramMap.get("sEcho");
        String sSearch = (String) paramMap.get("sSearch");
        int iDisplayStart = (int) paramMap.get("iDisplayStart");
        int iDisplayLength = (int) paramMap.get("iDisplayLength");

        paramMap.clear();

        paramMap.put("start", iDisplayStart);
        paramMap.put("limit", iDisplayLength);

        List<GardenStudentInfo> studentList = stuService.getStudentList(paramMap);
        Integer count = stuService.getStudentCount(paramMap);


        return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, studentList, "");
    }


    @GetMapping("/student_upgrade/{id}")
    public String toUpgradePage(@PathVariable("id") Integer id,Model model){

        GardenStudentInfo studentInfo=stuService.findById(id);
        GardenCourse course=courseService.findById(Integer.parseInt(studentInfo.getStuCurrentClass()));
        Map<String,Object> map=new HashMap<>(16);
        List<GardenCourseSeries> series=seriesService.getCouseSeriesList(map);
        List<GardenCourseSeriesVo> seriesVos=series.stream().map(s->{
            GardenCourseSeriesVo seriesVo=new GardenCourseSeriesVo();
            Map<String,Object> query=new HashMap<>(16);
            query.put("courseSeries",s.getId());
            List<GardenCourse> courses=courseService.getCourseList(query);
            seriesVo.setId(s.getId());
            seriesVo.setCourses(courses);
            seriesVo.setCourseSeriesName(s.getCourseSeriesName());
            return seriesVo;
        }).collect(Collectors.toList());
        model.addAttribute("student",studentInfo);
        model.addAttribute("currentCourse",course);
        model.addAttribute("seriesVos",seriesVos);
        return "teaching/student_upgrade";
    }

    @PostMapping("/student_upgrade")
    @ResponseBody
    @PreAuthorize("hasAuthority('PERMISSION_STUDENT_INFO')")
    @Transactional(rollbackFor = Exception.class)
    public Map<String, ?> studentUpgrade(HttpServletRequest request,@RequestBody @Valid StudentUpgradeVm upgradeVm) {
        GardenStudentUpgrade studentUpgrade=new GardenStudentUpgrade();
        studentUpgrade.setCreateTime(new Date());
        studentUpgrade.setStudentId(upgradeVm.getStudentId());
        studentUpgrade.setBeforeCourseId(upgradeVm.getBeforeCourseId());
        studentUpgrade.setAfterCourseId(upgradeVm.getAfterCourseId());
        studentUpgrade.setRemark(upgradeVm.getRemark());
        Integer result= upgradeService.save(studentUpgrade);

        GardenStudentInfo studentInfo=stuService.findById(upgradeVm.getStudentId());
        studentInfo.setStuCurrentClass(upgradeVm.getAfterCourseId());
        stuService.save(studentInfo);
        if (result!=null&&result>0){
            return RenderJSON.makeStandMap(1,"恭喜您，升级班级成功！","ok");
        }else{
            return RenderJSON.makeStandMap(1,"对不起，升级班级失败！","error");
        }

    }

    @PostMapping("/student_upgrade_record")
    @ResponseBody
    public Map<String, ?> studentUpgradeRecord(HttpServletRequest request,String param) {

        Map<String, Object> paramMap = DataTableUtils.parseParam(param);

        if (paramMap == null) {
            return RenderJSON.makeMap(-1, "参数不合法！", null, "");
        }
        String sEcho = (String) paramMap.get("sEcho");
        String sSearch = (String) paramMap.get("sSearch");
        int iDisplayStart = (int) paramMap.get("iDisplayStart");
        int iDisplayLength = (int) paramMap.get("iDisplayLength");

        paramMap.clear();
        paramMap.put("start", iDisplayStart);
        paramMap.put("search", sSearch);
        paramMap.put("limit", iDisplayLength);

        List<GardenStudentUpgrade> studentList = upgradeService.getStudentUpgradeList(paramMap);
        Integer count = upgradeService.getStudentUpgradeCount(paramMap);

        return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, studentList, "");
    }

    @DeleteMapping("/student_upgrade_record/{id}")
    @ResponseBody
    public Map<String, ?> deleteStudentUpgradeRecord(HttpServletRequest request,@PathVariable("id") Integer id) {

        Integer result=upgradeService.delete(id);

        if (request!=null&&result>0){
            return RenderJSON.makeStandMap(1, "恭喜您，删除成功","ok");
        }else{
            return RenderJSON.makeStandMap(1, "对不起，删除失败","error");
        }

    }
}
