package com.dbapp.garden.controller.student;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dbapp.garden.common.DataTableUtils;
import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.entity.GardenStudentContact;
import com.dbapp.garden.service.GardenStudentContactService;
import com.dbapp.garden.vm.StudentContactVm;

/**
 * 重要程度 控制器类
 * 
 * @author gangzi
 *
 */
@Controller
public class StudentContactController {

//	private static final Logger loggger = LoggerFactory.getLogger(StuBasicInfoConfigController.class);

	@Autowired
	private GardenStudentContactService stuContactService;

	@PostMapping("/studentcontactlist")
	@ResponseBody
	// @PreAuthorize("hasAuthority('PERMISSION_STUDENT_SOURCE_LIST')")
	public Map<String, ?> studentContactList(HttpServletRequest request, String param) {

		Map<String, Object> paramMap = DataTableUtils.parseParam(param);
		if (paramMap == null) {
			return RenderJSON.makeMap(-1, "参数不合法！", null, "");
		}
		String sEcho = (String) paramMap.get("sEcho");
		String sSearch = (String) paramMap.get("sSearch");
		int iDisplayStart = (int) paramMap.get("iDisplayStart");
		int iDisplayLength = (int) paramMap.get("iDisplayLength");

		paramMap.clear();
		paramMap.put("search", sSearch);
		paramMap.put("start", iDisplayStart);
		paramMap.put("limit", iDisplayLength);

		List<GardenStudentContact> studentList = stuContactService.getStudentContactList(paramMap);
		Integer count = stuContactService.getStudentContactCount(paramMap);

		return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, studentList, "");
	}

	@PostMapping("/studentcontactadd")
	@ResponseBody
	public Map<String, ?> addStudentContact(HttpServletRequest request,
			@RequestBody @Valid StudentContactVm stuContactVm) {
		GardenStudentContact stuContact = new GardenStudentContact();
		stuContact.setContactParam(stuContactVm.getContactParam());
		stuContact.setContactDesc(stuContactVm.getContactDesc());
		stuContact.setCreateTime(new Date());
		stuContact.setUpdateTime(new Date());

		Integer result = stuContactService.save(stuContact);
		
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，添加成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，添加失败！", "error");
		}

	}
	@DeleteMapping("/studentcontactdelete/{id}")
	@ResponseBody
	public Map<String, ?> deleteStudentContact(HttpServletRequest request,
			@PathVariable("id") Integer id) {
		
		Integer result = stuContactService.delete(id);
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，删除成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
		}
		
	}
	@PostMapping("/studentcontactedit/{id}")
	@ResponseBody
	public Map<String, ?> editStudentContact(HttpServletRequest request,
			@PathVariable("id") Integer id,@RequestBody @Valid StudentContactVm stuContactVm) {
		
		GardenStudentContact stuContact = new GardenStudentContact();
		stuContact.setId(id);
		stuContact.setContactParam(stuContactVm.getContactParam());
		stuContact.setContactDesc(stuContactVm.getContactDesc());
		stuContact.setUpdateTime(new Date());
		
		
		Integer result = stuContactService.save(stuContact);
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，编辑成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
		}
		
	}
}
