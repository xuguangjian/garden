package com.dbapp.garden.controller.teaching;

import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.common.WeekUtils;
import com.dbapp.garden.entity.GardenClassCourse;
import com.dbapp.garden.entity.GardenCourseTime;
import com.dbapp.garden.service.GardenClassCourseService;
import com.dbapp.garden.service.GardenCourseTimeService;
import com.dbapp.garden.vo.WeekVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author by gangzi on 2017/11/14.
 * 日常教务管理 控制器类
 */
@Controller
public class GardenDailyTeachingController {

    @Autowired
    private GardenCourseTimeService timeService;

    @Autowired
    private GardenClassCourseService classCourseService;

    @GetMapping("/daily_teaching")
    public String toDailyTeachingPage(HttpServletRequest request){
        return "teaching/daily_teaching";
    }


    @GetMapping("/week")
    @ResponseBody
    public Map<String,?> getWeekInfo(Integer year){
        if (year==null||year==0){
            year= LocalDate.now().getYear();
        }
        List<WeekVo> weekVoList=getWeekOfYear(year);

        return RenderJSON.makeStandMap(1,"恭喜您，获取成功！",weekVoList);
    }

    @GetMapping("/teacher_course")
    @ResponseBody
    public Map<String,?> getCourseInfo(String startDate) throws ParseException {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        Date da=sdf.parse(startDate);
        Set<String> weeks=WeekUtils.getWeekDays(da);
        Map<String,Object> map=new HashMap<>(16);
        map.put("timeStatus","1");

        List<GardenCourseTime> timeList=timeService.getCourseTimeList(map);
        Map<Integer,Map<Date,GardenClassCourse>> data=timeList.stream().collect(Collectors.toMap(GardenCourseTime::getId,time->{
            Map<String,Object> query=new HashMap<>(16);
            query.put("classTime",time.getId());
            //以当前上课时间段为条件，查询符合的所有课程
            List<GardenClassCourse> classCourses=classCourseService.getClassCourseList(query);

            //过滤属于本周范围内的课程
            List<GardenClassCourse> weekClass=classCourses.stream().filter(cou->cou!=null).filter(course->{
                Date date=course.getClassDate();

                String sDate=sdf.format(date);
                return weeks.contains(sDate);
            }).collect(Collectors.toList());
//            return new HashMap<Date, GardenClassCourse>(16);
            return weekClass.stream().collect(Collectors.toMap(GardenClassCourse::getClassDate,cou->cou));
        }));
        map.clear();
        map.put("time",timeList);
        map.put("course",data);
        return RenderJSON.makeStandMap(1,"恭喜您，获取成功！",map);

    }

    private List<WeekVo> getWeekOfYear(int year) {
        List<WeekVo> weekVos=new ArrayList<>();
        int weekCount= WeekUtils.getMaxWeekNumOfYear(year);
        Date today=new Date();
        int currentWeek=WeekUtils.getWeekOfYear(today);
        int currentYear= LocalDate.now().getYear();
        for (int i=1;i<=weekCount;i++){
            WeekVo weekVo=new WeekVo();
            weekVo.setWeek(i);
            weekVo.setYear(year);
            weekVo.setStartDate(WeekUtils.getFirstDayOfWeek(year,i));
            weekVo.setEndDate(WeekUtils.getLastDayOfWeek(year,i));
            if (currentYear==year&&currentWeek==i){
                weekVo.setCurrentWeek(true);
            }

            weekVos.add(weekVo);
        }
        return weekVos;
    }

}
