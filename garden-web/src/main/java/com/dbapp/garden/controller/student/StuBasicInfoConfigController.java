package com.dbapp.garden.controller.student;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dbapp.garden.common.DataTableUtils;
import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.entity.GardenStudentSource;
import com.dbapp.garden.service.GardenStudentSourceService;
import com.dbapp.garden.vm.StudentSourceVm;

/**
 * 学员管理基础信息设置 控制器类
 * 
 * @author gangzi
 *
 */
@Controller
public class StuBasicInfoConfigController {

	// private static final Logger log =
	// LoggerFactory.getLogger(StuBasicInfoConfigController.class);

	@Autowired
	private GardenStudentSourceService stuSourceService;

	@GetMapping("/student_basic")
	public String toBasicInfoPage(HttpServletRequest request) {
		return "student/student_basic";
	}

	@PostMapping("/studentsourcelist")
	@ResponseBody
	// @PreAuthorize("hasAuthority('PERMISSION_STUDENT_SOURCE_LIST')")
	public Map<String, ?> studentSourceList(HttpServletRequest request, String param) {

		Map<String, Object> paramMap = DataTableUtils.parseParam(param);
		if (paramMap == null) {
			return RenderJSON.makeMap(-1, "参数不合法！", null, "");
		}
		String sEcho = (String) paramMap.get("sEcho");
		String sSearch = (String) paramMap.get("sSearch");
		int iDisplayStart = (int) paramMap.get("iDisplayStart");
		int iDisplayLength = (int) paramMap.get("iDisplayLength");

		paramMap.clear();
		paramMap.put("search", sSearch);
		paramMap.put("start", iDisplayStart);
		paramMap.put("limit", iDisplayLength);

		List<GardenStudentSource> studentList = stuSourceService.getStudentSourceList(paramMap);
		Integer count = stuSourceService.getStudentSourceCount(paramMap);

		return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, studentList, "");
	}

	@PostMapping("/studentsourceadd")
	@ResponseBody
	public Map<String, ?> addStudentSource(HttpServletRequest request,
			@RequestBody @Valid StudentSourceVm stuSourceVm) {
		GardenStudentSource stuSource = new GardenStudentSource();
		stuSource.setSourceName(stuSourceVm.getSourceName());
		stuSource.setDetailSource(stuSourceVm.getDetailSource());
		stuSource.setCreateTime(new Date());
		stuSource.setUpdateTime(new Date());

		Integer result = stuSourceService.save(stuSource);
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，添加成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，添加失败！", "error");
		}

	}

	@DeleteMapping("/studentsourcedelete/{id}")
	@ResponseBody
	public Map<String, ?> deleteStudentSource(HttpServletRequest request, @PathVariable("id") Integer id) {

		Integer result = stuSourceService.delete(id);
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，删除成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
		}

	}

	@PostMapping("/studentsourceedit/{id}")
	@ResponseBody
	public Map<String, ?> editStudentSource(HttpServletRequest request, @PathVariable("id") Integer id,
			@RequestBody @Valid StudentSourceVm stuSourceVm) {
		GardenStudentSource stuSource = new GardenStudentSource();
		stuSource.setId(id);
		stuSource.setSourceName(stuSourceVm.getSourceName());
		stuSource.setDetailSource(stuSourceVm.getDetailSource());
		stuSource.setUpdateTime(new Date());

		Integer result = stuSourceService.save(stuSource);
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，编辑成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
		}

	}
}
