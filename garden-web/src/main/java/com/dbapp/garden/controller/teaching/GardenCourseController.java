package com.dbapp.garden.controller.teaching;

import com.dbapp.garden.common.DataTableUtils;
import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.entity.GardenCourse;
import com.dbapp.garden.entity.GardenCourseSeries;
import com.dbapp.garden.service.GardenCourseSeriesService;
import com.dbapp.garden.service.GardenCourseService;
import com.dbapp.garden.vm.CourseVm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 课程 控制器类
 *
 * @author gangzi
 */
@Controller
public class GardenCourseController {

//	private static final Logger loggger = LoggerFactory.getLogger(GardenCourseSeriesController.class);

    @Autowired
    private GardenCourseService courseService;

    @Autowired
    private GardenCourseSeriesService seriesService;


    @PostMapping("/courselist")
    @ResponseBody
    @PreAuthorize("hasAuthority('PERMISSION_COURSE')")
    public Map<String, ?> courseList(HttpServletRequest request, String param) {

        Map<String, Object> paramMap = DataTableUtils.parseParam(param);
        if (paramMap == null) {
            return RenderJSON.makeMap(-1, "参数不合法！", null, "");
        }
        String sEcho = (String) paramMap.get("sEcho");
        String sSearch = (String) paramMap.get("sSearch");
        int iDisplayStart = (int) paramMap.get("iDisplayStart");
        int iDisplayLength = (int) paramMap.get("iDisplayLength");

        paramMap.clear();
        paramMap.put("search", sSearch);
        paramMap.put("start", iDisplayStart);
        paramMap.put("limit", iDisplayLength);

        List<GardenCourse> studentList = courseService.getCourseList(paramMap);
        Integer count = courseService.getCourseCount(paramMap);

        return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, studentList, "");
    }

    @PostMapping("/course")
    @ResponseBody
    @PreAuthorize("hasAuthority('PERMISSION_COURSE_ADD')")
    public Map<String, ?> addCourse(HttpServletRequest request,
                                    @RequestBody @Valid CourseVm courseVm) {
        GardenCourse course = new GardenCourse();

        course.setCourseName(courseVm.getCourseName());
        course.setCourseSeries(courseVm.getCourseSeries());
        course.setCourseRemark(courseVm.getCourseRemark());
        course.setCourseSort(courseVm.getCourseSort());
        course.setDeductLessonHour(courseVm.getDeductLessonHour());
        course.setExperienceCount(courseVm.getExperienceCount());
        course.setMemberCount(courseVm.getMemberCount());
        course.setSuitableAgeFromYear(courseVm.getSuitableAgeFromYear());
        course.setSuitableAgeFromMonth(courseVm.getSuitableAgeFromMonth());
        course.setSuitableAgeToYear(courseVm.getSuitableAgeToYear());
        course.setSuitableAgeToMonth(courseVm.getSuitableAgeToMonth());
        course.setTotalCapacity(courseVm.getTotalCapacity());

        course.setCreateTime(new Date());
        course.setUpdateTime(new Date());

        Integer result = courseService.save(course);

        if (result != null && result > 0) {
            return RenderJSON.makeStandMap(1, "恭喜您，添加成功！", "ok");
        } else {
            return RenderJSON.makeStandMap(1, "对不起，添加失败！", "error");
        }

    }

    @DeleteMapping("/course/{id}")
    @ResponseBody
    @PreAuthorize("hasAuthority('PERMISSION_COURSE_DELETE')")
    public Map<String, ?> deleteCourse(HttpServletRequest request,
                                       @PathVariable("id") Integer id) {

        Integer result = courseService.delete(id);
        if (result != null && result > 0) {
            return RenderJSON.makeStandMap(1, "恭喜您，删除成功！", "ok");
        } else {
            return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
        }

    }

    @PutMapping("/course/{id}")
    @ResponseBody
    @PreAuthorize("hasAuthority('PERMISSION_COURSE_EDIT')")
    public Map<String, ?> editCourse(HttpServletRequest request,
                                     @PathVariable("id") Integer id, @RequestBody @Valid CourseVm courseVm) {

        GardenCourse course = courseService.findById(id);
        if (course == null) {
            return RenderJSON.makeStandMap(1, "对不起，该课程不存在，编辑失败！", "error");
        }
        course.setCourseName(courseVm.getCourseName());
        course.setCourseSeries(courseVm.getCourseSeries());
        course.setCourseRemark(courseVm.getCourseRemark());
        course.setCourseSort(courseVm.getCourseSort());
        course.setDeductLessonHour(courseVm.getDeductLessonHour());
        course.setExperienceCount(courseVm.getExperienceCount());
        course.setMemberCount(courseVm.getMemberCount());
        course.setSuitableAgeFromYear(courseVm.getSuitableAgeFromYear());
        course.setSuitableAgeFromMonth(courseVm.getSuitableAgeFromMonth());
        course.setSuitableAgeToYear(courseVm.getSuitableAgeToYear());
        course.setSuitableAgeToMonth(courseVm.getSuitableAgeToMonth());
        course.setTotalCapacity(courseVm.getTotalCapacity());

        course.setId(id);
        course.setUpdateTime(new Date());

        Integer result = courseService.save(course);

        if (result != null && result > 0) {
            return RenderJSON.makeStandMap(1, "恭喜您，编辑成功！", "ok");
        } else {
            return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
        }

    }

    @GetMapping("/course/{id}")
    @PreAuthorize("hasAuthority('PERMISSION_COURSE_EDIT')")
    public String editCoursePage(HttpServletRequest request,
                                 @PathVariable("id") Integer id, Model model) {
        Map<String,Object> map=new HashMap<>(16);
        List<GardenCourseSeries> series=seriesService.getCouseSeriesList(map);
        GardenCourse course = courseService.findById(id);
        model.addAttribute("series",series);
        model.addAttribute("course", course);
        return "teaching/course_edit";
    }
}
