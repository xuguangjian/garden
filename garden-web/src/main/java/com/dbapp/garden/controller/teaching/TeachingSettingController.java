package com.dbapp.garden.controller.teaching;

import com.dbapp.garden.entity.GardenCourse;
import com.dbapp.garden.entity.GardenCourseSeries;
import com.dbapp.garden.service.GardenCourseSeriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author  by gangzi on 2017/11/9.
 */
@Controller
public class TeachingSettingController {

    @Autowired
    private GardenCourseSeriesService seriesService;

    @GetMapping("/teaching_setting")
    public String toTeachingSettingPage(HttpServletRequest request,Model model){
        Map<String,Object> map=new HashMap<>(16);
        List<GardenCourseSeries> series=seriesService.getCouseSeriesList(map);
        model.addAttribute("series",series);
        return "teaching/teaching_setting";
    }


}
