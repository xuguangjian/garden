package com.dbapp.garden.controller.teaching;

import com.dbapp.garden.common.DataTableUtils;
import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.entity.GardenCourseTime;
import com.dbapp.garden.service.GardenCourseTimeService;
import com.dbapp.garden.vm.CourseTimeVm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/12.
 *         课程时间设置 控制器类
 */

@Controller
public class GardenCourseTimeController {

    @Autowired
    private GardenCourseTimeService timeService;


    @PostMapping("/coursetimelist")
    @ResponseBody
    @PreAuthorize("hasAuthority('PERMISSION_COURSE_TIME')")
    public Map<String, ?> courseTimeList(HttpServletRequest request, String param) {

        Map<String, Object> paramMap = DataTableUtils.parseParam(param);
        if (paramMap == null) {
            return RenderJSON.makeMap(-1, "参数不合法！", null, "");
        }
        String sEcho = (String) paramMap.get("sEcho");
        String sSearch = (String) paramMap.get("sSearch");
        int iDisplayStart = (int) paramMap.get("iDisplayStart");
        int iDisplayLength = (int) paramMap.get("iDisplayLength");

        paramMap.clear();
        paramMap.put("search", sSearch);
        paramMap.put("start", iDisplayStart);
        paramMap.put("limit", iDisplayLength);

        List<GardenCourseTime> courseTimeList = timeService.getCourseTimeList(paramMap);
        Integer count = timeService.getCourseCount(paramMap);

        return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, courseTimeList, "");
    }

    @PostMapping("/coursetime")
    @ResponseBody
    @PreAuthorize("hasAuthority('PERMISSION_COURSE_TIME_ADD')")
    public Map<String, ?> addCourseTime(HttpServletRequest request,
                                        @RequestBody @Valid CourseTimeVm courseTimeVm) {

        GardenCourseTime courseTime = new GardenCourseTime();
        courseTime.setTimeRemark(courseTimeVm.getTimeRemark());
        courseTime.setTimeSort(courseTimeVm.getTimeSort());
        courseTime.setCourseTime(courseTimeVm.getCourseTime());
        courseTime.setCreateTime(new Date());
        courseTime.setUpdateTime(new Date());
        Integer result = timeService.save(courseTime);

        if (result != null && result > 0) {
            return RenderJSON.makeStandMap(1, "恭喜您，添加成功！", "ok");
        } else {
            return RenderJSON.makeStandMap(1, "对不起，添加失败！", "error");
        }

    }

    @DeleteMapping("/coursetime/{id}")
    @ResponseBody
    @PreAuthorize("hasAuthority('PERMISSION_COURSE_TIME_DELETE')")
    public Map<String, ?> deleteCourseTime(HttpServletRequest request,
                                           @PathVariable("id") Integer id) {

        Integer result = timeService.delete(id);
        if (result != null && result > 0) {
            return RenderJSON.makeStandMap(1, "恭喜您，删除成功！", "ok");
        } else {
            return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
        }

    }

    @PutMapping("/coursetime/{id}")
    @ResponseBody
    @PreAuthorize("hasAuthority('PERMISSION_COURSE_TIME_EDIT')")
    public Map<String, ?> editCourseTime(HttpServletRequest request,
                                         @PathVariable("id") Integer id, @RequestBody @Valid CourseTimeVm courseTimeVm) {

        GardenCourseTime courseTime = timeService.findById(id);
        if (courseTime == null) {
            return RenderJSON.makeStandMap(1, "对不起，该上课时间不存在，编辑失败！", "error");
        }

        courseTime.setTimeRemark(courseTimeVm.getTimeRemark());
        courseTime.setTimeSort(courseTimeVm.getTimeSort());
        courseTime.setCourseTime(courseTimeVm.getCourseTime());
        courseTime.setId(id);
        courseTime.setUpdateTime(new Date());
        Integer result = timeService.save(courseTime);

        if (result != null && result > 0) {
            return RenderJSON.makeStandMap(1, "恭喜您，编辑成功！", "ok");
        } else {
            return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
        }

    }

    @PutMapping("/coursetimestate/{id}")
    @ResponseBody
    @PreAuthorize("hasAuthority('PERMISSION_COURSE_TIME_EDIT')")
    public Map<String, ?> editCourseTimeState(HttpServletRequest request,
                                              @PathVariable("id") Integer id, Integer state) {

        GardenCourseTime courseTime = timeService.findById(id);
        if (courseTime == null) {
            return RenderJSON.makeStandMap(1, "对不起，该上课时间不存在，编辑失败！", "error");
        }
        courseTime.setTimeStatus(state);
        Integer result = timeService.save(courseTime);

        if (result != null && result > 0) {
            return RenderJSON.makeStandMap(1, "恭喜您，状态更新成功！", "ok");
        } else {
            return RenderJSON.makeStandMap(1, "对不起，状态更新失败！", "error");
        }

    }

}
