package com.dbapp.garden.controller.teacher;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.dbapp.garden.config.security.PermissionUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.dbapp.garden.common.DataTableUtils;
import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.entity.GardenTeacherLevel;
import com.dbapp.garden.service.GardenTeacherLevelService;
import com.dbapp.garden.vm.TeacherLevelVm;

/**
 * @author gangzi
 */
@Controller
public class TeacherLevelController {

	// private static final Logger loggger =
	// LoggerFactory.getLogger(StuBasicInfoConfigController.class);

	@Autowired
	private GardenTeacherLevelService teacherLevelService;

	@GetMapping("/teacherlevel")
	public String toTeacherLevelPage(){
		return "level/levellist";
	}
	@PostMapping("/teacherlevellist")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_TEACHER')")
	public Map<String, ?> teacherLevelList(@SessionAttribute("user") PermissionUser user, String param) {

		Map<String, Object> paramMap = DataTableUtils.parseParam(param);
		if (paramMap == null) {
			return RenderJSON.makeMap(-1, "参数不合法！", null, "");
		}
		String sEcho = (String) paramMap.get("sEcho");
		String sSearch = (String) paramMap.get("sSearch");
		int iDisplayStart = (int) paramMap.get("iDisplayStart");
		int iDisplayLength = (int) paramMap.get("iDisplayLength");

		paramMap.clear();
		paramMap.put("search", sSearch);
		paramMap.put("start", iDisplayStart);
		paramMap.put("limit", iDisplayLength);
		paramMap.put("gardenId",user.getGardenId());

		List<GardenTeacherLevel> levelList = teacherLevelService.getTeacherLevelList(paramMap);
		Integer count = teacherLevelService.getTeacherLevelCount(paramMap);

		return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, levelList, "");
	}

	@PostMapping("/teacherlevel")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_TEACHER_ADD')")
	public Map<String, ?> addTeacherLevel(@SessionAttribute("user") PermissionUser user,
			@RequestBody @Valid TeacherLevelVm teacherLevelVm) {
		GardenTeacherLevel level = new GardenTeacherLevel();
		level.setLevelParam(teacherLevelVm.getLevelParam());
		level.setLevelDesc(teacherLevelVm.getLevelDesc());
		level.setCreateTime(new Date());
		level.setUpdateTime(new Date());
		level.setGardenId(user.getGardenId());

		Integer result = teacherLevelService.save(level);

		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，添加成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，添加失败！", "error");
		}

	}

	@DeleteMapping("/teacherlevel/{id}")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_TEACHER_DELETE')")
	public Map<String, ?> deleteTeacherLevel(@SessionAttribute("user") PermissionUser user, @PathVariable("id") Integer id) {

		GardenTeacherLevel level=teacherLevelService.findById(id);
		if (level==null){
			return RenderJSON.makeStandMap(1, "对不起，该项不存在，删除失败！", "error");
		}
		if (!level.getGardenId().equals(user.getGardenId())){
			return RenderJSON.makeStandMap(1, "对不起，您无权操作！", "error");
		}
		Integer result = teacherLevelService.delete(id);
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，删除成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
		}

	}

	@PutMapping("/teacherlevel/{id}")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_TEACHER_EDIT')")
	public Map<String, ?> editTeacherLevel(@SessionAttribute("user") PermissionUser user, @PathVariable("id") Integer id,
			@RequestBody @Valid TeacherLevelVm teacherLevelVm) {

		GardenTeacherLevel level = teacherLevelService.findById(id);

		if (level==null){
			return RenderJSON.makeStandMap(2, "对不起，该项不存在，编辑失败！", "error");
		}

		if (!level.getGardenId().equals(user.getGardenId())){
			return RenderJSON.makeStandMap(2, "对不起，您无权操作！", "error");
		}
		level.setId(id);
		level.setLevelParam(teacherLevelVm.getLevelParam());
		level.setLevelDesc(teacherLevelVm.getLevelDesc());
		
		level.setUpdateTime(new Date());

		Integer result = teacherLevelService.save(level);
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，编辑成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
		}

	}
}
