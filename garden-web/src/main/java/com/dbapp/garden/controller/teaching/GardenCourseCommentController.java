package com.dbapp.garden.controller.teaching;

import com.dbapp.garden.common.DataTableUtils;
import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.entity.GardenCourseComment;
import com.dbapp.garden.service.GardenCourseCommentService;
import com.dbapp.garden.vm.CourseCommentVm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/13.
 */

@Controller
public class GardenCourseCommentController {

    @Autowired
    private GardenCourseCommentService commentService;


    @PostMapping("/coursecommentlist")
    @ResponseBody
    @PreAuthorize("hasAuthority('PERMISSION_COURSE_COMMENT')")
    public Map<String, ?> courseCommentList(HttpServletRequest request, String param) {

        Map<String, Object> paramMap = DataTableUtils.parseParam(param);
        if (paramMap == null) {
            return RenderJSON.makeMap(-1, "参数不合法！", null, "");
        }
        String sEcho = (String) paramMap.get("sEcho");
        String sSearch = (String) paramMap.get("sSearch");
        int iDisplayStart = (int) paramMap.get("iDisplayStart");
        int iDisplayLength = (int) paramMap.get("iDisplayLength");

        paramMap.clear();
        paramMap.put("search", sSearch);
        paramMap.put("start", iDisplayStart);
        paramMap.put("limit", iDisplayLength);

        List<GardenCourseComment> commentList = commentService.getCourseCommentList(paramMap);
        Integer count = commentService.getCourseCommentCount(paramMap);

        return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, commentList, "");
    }

    @PostMapping("/coursecomment")
    @ResponseBody
    @PreAuthorize("hasAuthority('PERMISSION_COURSE_COMMENT_ADD')")
    public Map<String, ?> addCourseComment(HttpServletRequest request,
                                    @RequestBody @Valid CourseCommentVm commentVm) {

        GardenCourseComment comment = new GardenCourseComment();
        comment.setCommentName(commentVm.getCommentName());
        comment.setCommentScore(commentVm.getCommentScore());
        comment.setCommentSort(commentVm.getCommentSort());

        comment.setCreateTime(new Date());
        comment.setUpdateTime(new Date());

        Integer result = commentService.save(comment);

        if (result != null && result > 0) {
            return RenderJSON.makeStandMap(1, "恭喜您，添加成功！", "ok");
        } else {
            return RenderJSON.makeStandMap(1, "对不起，添加失败！", "error");
        }

    }

    @DeleteMapping("/coursecomment/{id}")
    @ResponseBody
    @PreAuthorize("hasAuthority('PERMISSION_COURSE_COMMENT_DELETE')")
    public Map<String, ?> deleteCourseComment(HttpServletRequest request,
                                       @PathVariable("id") Integer id) {

        Integer result = commentService.delete(id);
        if (result != null && result > 0) {
            return RenderJSON.makeStandMap(1, "恭喜您，删除成功！", "ok");
        } else {
            return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
        }

    }

    @PutMapping("/coursecomment/{id}")
    @ResponseBody
    @PreAuthorize("hasAuthority('PERMISSION_COURSE_COMMENT_EDIT')")
    public Map<String, ?> editCourseComment(HttpServletRequest request,
                                     @PathVariable("id") Integer id, @RequestBody @Valid CourseCommentVm commentVm) {

        GardenCourseComment comment = commentService.findById(id);
        if (comment == null) {
            return RenderJSON.makeStandMap(1, "对不起，该课评分值不存在，编辑失败！", "error");
        }
        comment.setCommentName(commentVm.getCommentName());
        comment.setCommentScore(commentVm.getCommentScore());
        comment.setCommentSort(commentVm.getCommentSort());

        comment.setId(id);
        comment.setUpdateTime(new Date());

        Integer result = commentService.save(comment);

        if (result != null && result > 0) {
            return RenderJSON.makeStandMap(1, "恭喜您，编辑成功！", "ok");
        } else {
            return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
        }

    }
}
