package com.dbapp.garden.controller.garden;

import com.dbapp.garden.common.DataTableUtils;
import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.entity.GardenInfo;
import com.dbapp.garden.service.GardenInfoService;
import com.dbapp.garden.vm.GardenInfoVm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author gangzi
 */
@Controller
public class GardenInfoController {
	@Autowired
	private GardenInfoService gardenInfoService;
	
	@GetMapping("/garden")
	@PreAuthorize("hasAuthority('PERMISSION_GARDEN')")
	public String toGardenPage(){
		
		return "garden/gardenlist";
	}
	@GetMapping("/garden/{id}")
	@PreAuthorize("hasAuthority('PERMISSION_GARDEN_VIEW')")
	public String viewGarden(@PathVariable("id") Integer id,Model model){
		GardenInfo garden=gardenInfoService.findById(id);
		
		model.addAttribute("garden", garden);
		return "garden/gardenview";
	}
	@PostMapping("/gardenlist")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_GARDEN')")
	public Map<String, ?> gardenList(HttpServletRequest request, String param) {

		Map<String, Object> paramMap = DataTableUtils.parseParam(param);
		if (paramMap == null) {
			return RenderJSON.makeMap(-1, "参数不合法！", null, "");
		}
		String sEcho = (String) paramMap.get("sEcho");
		String sSearch = (String) paramMap.get("sSearch");
		int iDisplayStart = (int) paramMap.get("iDisplayStart");
		int iDisplayLength = (int) paramMap.get("iDisplayLength");

		paramMap.clear();
		paramMap.put("search", sSearch);
		paramMap.put("start", iDisplayStart);
		paramMap.put("limit", iDisplayLength);

		List<GardenInfo> gardenList = gardenInfoService.getGardenList(paramMap);
		Integer count = gardenInfoService.getGardenCount(paramMap);

		return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, gardenList, "");
	}

	@PostMapping("/garden")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_GARDEN_ADD')")
	public Map<String, ?> addGarden(HttpServletRequest request,
			@RequestBody @Valid GardenInfoVm gardenInfoVm) {
		GardenInfo gardenInfo = new GardenInfo();
		gardenInfo.setGardenAddress(gardenInfoVm.getGardenAddress());
		gardenInfo.setGardenManager(gardenInfoVm.getGardenManager());
		gardenInfo.setGardenManagerPhone(gardenInfoVm.getGardenManagerPhone());
		gardenInfo.setGardenName(gardenInfoVm.getGardenName());
		gardenInfo.setCreateTime(new Date());
		gardenInfo.setUpdateTime(new Date());

		Integer result = gardenInfoService.save(gardenInfo);
		
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，添加成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，添加失败！", "error");
		}

	}
	
	
	@DeleteMapping("/garden/{id}")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_GARDEN_DELETE')")
	public Map<String, ?> deleteGarden(@PathVariable("id") Integer id) {
		
		Integer result = gardenInfoService.delete(id);
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，删除成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
		}
		
	}
	@PutMapping("/garden/{id}")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_GARDEN_EDIT')")
	public Map<String, ?> editGarden(@PathVariable("id") Integer id,@RequestBody @Valid GardenInfoVm gardenInfoVm) {
		
		GardenInfo gardenInfo = new GardenInfo();
		gardenInfo.setId(id);
		gardenInfo.setGardenAddress(gardenInfoVm.getGardenAddress());
		gardenInfo.setGardenManager(gardenInfoVm.getGardenManager());
		gardenInfo.setGardenManagerPhone(gardenInfoVm.getGardenManagerPhone());
		gardenInfo.setGardenName(gardenInfoVm.getGardenName());
		gardenInfo.setUpdateTime(new Date());

		Integer result = gardenInfoService.save(gardenInfo);
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，编辑成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
		}
		
	}
}
