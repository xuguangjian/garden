package com.dbapp.garden.controller.teaching;

import com.dbapp.garden.common.DataTableUtils;
import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.entity.GardenClassroom;
import com.dbapp.garden.entity.GardenCourseSeries;
import com.dbapp.garden.service.GardenClassroomService;
import com.dbapp.garden.service.GardenCourseSeriesService;
import com.dbapp.garden.vm.ClassroomVm;
import com.dbapp.garden.vm.CourseSeriesVm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 教室 控制器类
 * 
 * @author gangzi
 *
 */
@Controller
public class GardenClassroomController {

//	private static final Logger loggger = LoggerFactory.getLogger(GardenCourseSeriesController.class);

	@Autowired
	private GardenClassroomService classroomService;


	@PostMapping("/classroomlist")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_CLASSROOM')")
	public Map<String, ?> classroomList(HttpServletRequest request, String param) {

		Map<String, Object> paramMap = DataTableUtils.parseParam(param);
		if (paramMap == null) {
			return RenderJSON.makeMap(-1, "参数不合法！", null, "");
		}
		String sEcho = (String) paramMap.get("sEcho");
		String sSearch = (String) paramMap.get("sSearch");
		int iDisplayStart = (int) paramMap.get("iDisplayStart");
		int iDisplayLength = (int) paramMap.get("iDisplayLength");

		paramMap.clear();
		paramMap.put("search", sSearch);
		paramMap.put("start", iDisplayStart);
		paramMap.put("limit", iDisplayLength);

		List<GardenClassroom> classroomList = classroomService.getClassroomList(paramMap);
		Integer count = classroomService.getClassroomCount(paramMap);

		return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, classroomList, "");
	}

	@PostMapping("/classroom")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_CLASSROOM_ADD')")
	public Map<String, ?> addClassroom(HttpServletRequest request,
			@RequestBody @Valid ClassroomVm roomVm) {
		GardenClassroom room = new GardenClassroom();
		room.setClassroomName(roomVm.getClassroomName());
		room.setClassroomRemark(roomVm.getClassroomRemark());
		room.setClassroomSort(roomVm.getClassroomSort());
		room.setCreateTime(new Date());
		room.setUpdateTime(new Date());

		Integer result = classroomService.save(room);
		
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，添加成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，添加失败！", "error");
		}

	}
	@DeleteMapping("/classroom/{id}")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_CLASSROOM_DELETE')")
	public Map<String, ?> deleteClassroom(HttpServletRequest request,
			@PathVariable("id") Integer id) {
		
		Integer result = classroomService.delete(id);
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，删除成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
		}
		
	}
	@PutMapping("/classroom/{id}")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_CLASSROOM_EDIT')")
	public Map<String, ?> editClassroom(HttpServletRequest request,
			@PathVariable("id") Integer id,@RequestBody @Valid ClassroomVm roomVm) {

		GardenClassroom room = classroomService.findById(id);
		if (room==null){
			return RenderJSON.makeStandMap(1, "对不起，该教室不存在，编辑失败！", "error");
		}
		room.setClassroomName(roomVm.getClassroomName());
		room.setClassroomRemark(roomVm.getClassroomRemark());
		room.setClassroomSort(roomVm.getClassroomSort());
		room.setId(id);
		room.setUpdateTime(new Date());

		Integer result = classroomService.save(room);

		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，编辑成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
		}
		
	}
}
