package com.dbapp.garden.controller.teaching;

import com.dbapp.garden.common.DataTableUtils;
import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.config.security.PermissionUser;
import com.dbapp.garden.controller.base.BaseController;
import com.dbapp.garden.entity.GardenClassCourse;
import com.dbapp.garden.entity.GardenClassCourseStudent;
import com.dbapp.garden.entity.GardenStudentAskLeave;
import com.dbapp.garden.entity.GardenStudentInfo;
import com.dbapp.garden.service.GardenClassCourseService;
import com.dbapp.garden.service.GardenClassCourseStudentService;
import com.dbapp.garden.service.GardenStudentAskLeaveService;
import com.dbapp.garden.service.GardenStudentInfoService;
import com.dbapp.garden.vm.StudentAskLeaveVm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/19.
 *         <p>
 *         学员请假管理
 */
@Controller
public class GardenStudentAskLeaveController{

    @Autowired
    private GardenStudentAskLeaveService askLeaveService;

    @Autowired
    private GardenClassCourseStudentService courseStudentService;

    @Autowired
    private GardenStudentInfoService studentInfoService;

    @Autowired
    private GardenClassCourseService classCourseService;

    @PostMapping("/classstudent")
    @ResponseBody
    public Map<String, ?> getReadyToClassStudentList(HttpServletRequest request, String param) {
        Map<String, Object> paramMap = DataTableUtils.parseParam(param);
        if (paramMap == null) {
            return RenderJSON.makeMap(-1, "参数不合法！", null, "");
        }
        String sEcho = (String) paramMap.get("sEcho");
        String sSearch = (String) paramMap.get("sSearch");
        int iDisplayStart = (int) paramMap.get("iDisplayStart");
        int iDisplayLength = (int) paramMap.get("iDisplayLength");

        paramMap.clear();
        paramMap.put("search", sSearch);
        paramMap.put("start", iDisplayStart);
        paramMap.put("limit", iDisplayLength);

        List<GardenClassCourseStudent> studentList = courseStudentService.getClassStudentList(paramMap);
        Integer count = courseStudentService.getClassStudentCount(paramMap);

        return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, studentList, "");
    }

    @GetMapping("/ask_leave/{id}")
    public String toAskLeaveRegister(HttpServletRequest request, @PathVariable("id") Integer id, Model model) {
        GardenClassCourseStudent classCourseStudent = courseStudentService.findById(id);
        Integer studentId = classCourseStudent.getStudentId();
        Integer classId = classCourseStudent.getClassId();
        GardenStudentInfo studentInfo = studentInfoService.findById(studentId);
        GardenClassCourse classCourse = classCourseService.findById(classId);
        model.addAttribute("student", studentInfo);
        model.addAttribute("classCourse", classCourse);
        model.addAttribute("classCourseStudent", classCourseStudent);
        return "teaching/ask_leave";
    }

    @PostMapping("/ask_leave")
    @ResponseBody
    @Transactional(rollbackFor = Exception.class)
    public Map<String, ?> getReadyToClassStudentList(HttpServletRequest request, @RequestBody @Valid StudentAskLeaveVm leaveVm) {
        PermissionUser permissionUser = (PermissionUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        GardenStudentAskLeave leave = new GardenStudentAskLeave();
        leave.setClassId(leaveVm.getClassCourseId());
        leave.setDeductHour(leaveVm.getDeductHour());
        leave.setStudentId(leaveVm.getStudentId());
        leave.setLeaveReason(leaveVm.getLeaveReason());
        leave.setCreateTime(new Date());
        //操作人
        leave.setOperator(permissionUser.getUid());
        Integer result = askLeaveService.save(leave);
        //更新学员签到状态
        if (result != null && result > 0) {
            return RenderJSON.makeStandMap(1, "恭喜您，请假成功！", "ok");
        } else {
            return RenderJSON.makeStandMap(1, "对不起，请假失败！", "ok");
        }

    }

    @PostMapping("/leaverecord")
    @ResponseBody
    public Map<String, ?> getStudentLeaveRecordList(HttpServletRequest request, String param) {
        Map<String, Object> paramMap = DataTableUtils.parseParam(param);
        if (paramMap == null) {
            return RenderJSON.makeMap(-1, "参数不合法！", null, "");
        }
        String sEcho = (String) paramMap.get("sEcho");
        String sSearch = (String) paramMap.get("sSearch");
        int iDisplayStart = (int) paramMap.get("iDisplayStart");
        int iDisplayLength = (int) paramMap.get("iDisplayLength");

        paramMap.clear();
        paramMap.put("search", sSearch);
        paramMap.put("start", iDisplayStart);
        paramMap.put("limit", iDisplayLength);

        List<GardenStudentAskLeave> studentLeaveList = askLeaveService.getAskLeaveList(paramMap);
        Integer count = askLeaveService.getAskLeaveCount(paramMap);

        return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, studentLeaveList, "");
    }

}
