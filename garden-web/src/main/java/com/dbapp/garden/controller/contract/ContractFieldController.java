package com.dbapp.garden.controller.contract;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dbapp.garden.common.DataTableUtils;
import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.entity.GardenContractField;
import com.dbapp.garden.entity.GardenContractPackageInfo;
import com.dbapp.garden.service.IGardenContractFieldService;
import com.dbapp.garden.vo.ContractFieldComponent;
import com.dbapp.garden.vo.ContractFieldType;

@Controller
public class ContractFieldController extends BaseContractController {
	
	@Autowired
	IGardenContractFieldService fieldService;
	
	@GetMapping("/contractfield_basic")
	public String toBasicInfoPage(HttpServletRequest request) {
		return "contract/contractfield_basic";
	}
	
	@PostMapping("/contactfieldlist")
	@ResponseBody
//	@PreAuthorize("hasAuthority('PERMISSION_CONTRACTPACKAGE_LIST')")
	public Map<String, ?> contractFieldList(HttpServletRequest request, String param) {
		int gardenId = this.getSelfGardenId(request);
		Map<String, Object> paramMap = DataTableUtils.parseParam(param);
		if (paramMap == null) {
			return RenderJSON.makeMap(-1, "参数不合法！", null, "");
		}
		String sEcho = (String) paramMap.get("sEcho");
		String sSearch = (String) paramMap.get("sSearch");
		int iDisplayStart = (int) paramMap.get("iDisplayStart");
		int iDisplayLength = (int) paramMap.get("iDisplayLength");
		System.out.println("search=>" + this.changeObjToStr(paramMap));
		List<GardenContractField> fields = fieldService.getGardenContractFields(gardenId, sSearch, iDisplayStart, iDisplayLength);
		int count = fieldService.getGardenContractFieldCount(gardenId, sSearch);

		List<Map> ret = new ArrayList<Map>();
		if(fields != null)
			for(GardenContractField field: fields){
				Map temp = this.changeObjToMap(field);
				if(temp == null) continue;
				temp.put("fieldTypeName",ContractFieldType.getTypeName(field.getFieldType()));
				temp.put("fieldComponentName", ContractFieldComponent.getComponentName(field.getFieldComponent()));
				temp.put("isShowName", field.getIsShow().intValue() == 1?"显示":"不显示");
				temp.put("isRequireName", field.getIsRequire().intValue() == 1?"是":"不是");
				ret.add(temp);
			}
		return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, ret, "");
	}
	
	@PostMapping("/contractfieldadd")
	@ResponseBody
	public Map<String, ?> addContractField(HttpServletRequest request,
			@RequestBody @Valid GardenContractField fieldInfo) {
		if(fieldInfo == null)
			return RenderJSON.makeStandMap(1, "对不起，添加失败！", "error");
		fieldInfo.setCreateDate(new Date());
		fieldInfo.setUpdateDate(new Date());
		int gardenId = this.getSelfGardenId(request);
		fieldInfo.setGarden_id(gardenId);
		int order = fieldService.getMaxOrderField(gardenId);
		order++;
		fieldInfo.setFieldOrder(order);
		fieldInfo = fieldService.saveContractField(fieldInfo);
		if (fieldInfo != null) {
			return RenderJSON.makeStandMap(1, "恭喜您，添加成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，添加失败！", "error");
		}
	}
	
	@DeleteMapping("/contractfielddelete/{id}")
	@ResponseBody
	public Map<String, ?> deleteContractField(HttpServletRequest request, @PathVariable("id") Integer id) {
		if(id == null)
			return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
		int gardenId = this.getSelfGardenId(request);
		
		GardenContractField fieldInfo = fieldService.findById(id);
		if(fieldInfo == null || fieldInfo.getGarden_id().intValue() != gardenId)
			return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
		
		int result = fieldService.deleteContractField(id);
		if (result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，删除成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
		}
	}
	
	@PostMapping("/contractfieldedit/{id}")
	@ResponseBody
	public Map<String, ?> editContractField(HttpServletRequest request, @PathVariable("id") Integer id,
			@RequestBody @Valid GardenContractField fieldInfo) {
		if(id == null || fieldInfo == null)
			return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
		int gardenId = this.getSelfGardenId(request);
		GardenContractField pInfo = fieldService.findById(id);
		if(pInfo == null || pInfo.getGarden_id().intValue() != gardenId)
			return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
		fieldInfo.setId(id);
		fieldInfo.setUpdateDate(new Date());
		fieldInfo.setGarden_id(gardenId);
		fieldInfo = fieldService.saveContractField(fieldInfo);
		if (fieldInfo != null) {
			return RenderJSON.makeStandMap(1, "恭喜您，编辑成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
		}
	}
	
	@PostMapping("/contractfieldup/{id}")
	@ResponseBody
	public Map<String, ?> upContractField(HttpServletRequest request, @PathVariable("id") Integer id) {
		if(id == null)
			return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
		int gardenId = this.getSelfGardenId(request);
		GardenContractField pInfo = fieldService.findById(id);
		if(pInfo == null || pInfo.getGarden_id().intValue() != gardenId)
			return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
		
		GardenContractField fieldInfo = fieldService.upOrder(id);
		if (fieldInfo != null) {
			return RenderJSON.makeStandMap(1, "恭喜您，编辑成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
		}
	}
}
