package com.dbapp.garden.controller.contract;

import com.dbapp.garden.common.DataTableUtils;
import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.config.security.PermissionUser;
import com.dbapp.garden.entity.GardenPayWayInfo;
import com.dbapp.garden.service.IGardenPayWayInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 支付方式设置 控制器类
 */
@Controller
public class PayWayController extends BaseContractController {
	
	@Autowired
	IGardenPayWayInfoService payWayService;
	
	@PostMapping("/paywaylist")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_PAYWAY') or hasAuthority('PERMISSION_PAYWAY_VIEW')")
	public Map<String, ?> payWayList(HttpServletRequest request,@SessionAttribute("user") PermissionUser user, String param) {
		int gardenId = this.getSelfGardenId(request);
		Map<String, Object> paramMap = DataTableUtils.parseParam(param);
		if (paramMap == null) {
			return RenderJSON.makeMap(-1, "参数不合法！", null, "");
		}
		String sEcho = (String) paramMap.get("sEcho");
		String sSearch = (String) paramMap.get("sSearch");
		int iDisplayStart = (int) paramMap.get("iDisplayStart");
		int iDisplayLength = (int) paramMap.get("iDisplayLength");

		paramMap.put("gardenId",user.getGardenId());

		List<GardenPayWayInfo> contractPackageList = payWayService.getPayWayList(gardenId, sSearch, iDisplayStart, iDisplayLength);
		int count = payWayService.getPayWayCount(gardenId, sSearch);

		return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, contractPackageList, "");
	}
	
	@PostMapping("/payway")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_PAYWAY_ADD')")
	public Map<String, ?> addPayWay(@SessionAttribute("user") PermissionUser user, @RequestBody @Valid GardenPayWayInfo payWayInfo) {
		if(payWayInfo == null){
			return RenderJSON.makeStandMap(2, "对不起，添加失败！", "error");
		}

		if(StringUtils.isBlank(payWayInfo.getPayWay())){
			return RenderJSON.makeStandMap(2, "对不起，编辑失败,参数为空！", "error");
		}

		payWayInfo.setCreateDate(new Date());
		payWayInfo.setUpdateDate(new Date());

		payWayInfo.setGarden_id(user.getGardenId());

		payWayInfo = payWayService.savePayWay(payWayInfo);
		
		if (payWayInfo != null) {
			return RenderJSON.makeStandMap(1, "恭喜您，添加成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，添加失败！", "error");
		}
	}
	
	@DeleteMapping("/payway/{id}")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_PAYWAY_DELETE')")
	public Map<String, ?> deletePayWay(@SessionAttribute("user") PermissionUser user, @PathVariable("id") Integer id) {
		if(id == null){
			return RenderJSON.makeStandMap(1, "对不起，参数有问题，删除失败！", "error");
		}

		GardenPayWayInfo payInfo = payWayService.findById(id);
		if(payInfo == null ){
			return RenderJSON.makeStandMap(1, "对不起，该支付方式不存在，删除失败！", "error");
		}

		if (!payInfo.getGarden_id().equals(user.getGardenId())){
			return RenderJSON.makeStandMap(2, "对不起，您没有操作权限，无法删除", "error");
		}
		int result = payWayService.deletePayWay(id);
		if (result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，删除成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
		}
	}
	
	@PutMapping("/payway/{id}")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_PAYWAY_EDIT')")
	public Map<String, ?> editPayWay(@SessionAttribute("user") PermissionUser user, @PathVariable("id") Integer id,
			@RequestBody @Valid GardenPayWayInfo payWayInfo) {
		if(id == null || payWayInfo == null){
			return RenderJSON.makeStandMap(2, "对不起，参数有问题，编辑失败！", "error");
		}

		GardenPayWayInfo payInfo = payWayService.findById(id);
		if(payInfo == null){
			return RenderJSON.makeStandMap(2, "对不起，该支付方式不存在，编辑失败！", "error");
		}

		if(StringUtils.isBlank(payWayInfo.getPayWay())){
			return RenderJSON.makeStandMap(2, "对不起，编辑失败,参数为空！", "error");
		}

		payWayInfo.setUpdateDate(new Date());
		payWayInfo.setGarden_id(user.getGardenId());
		payWayInfo.setId(id);
		payWayInfo = payWayService.savePayWay(payWayInfo);
		if (payWayInfo != null) {
			return RenderJSON.makeStandMap(1, "恭喜您，编辑成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
		}

	}
}
