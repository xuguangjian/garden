package com.dbapp.garden.controller.student;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dbapp.garden.common.DataTableUtils;
import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.entity.GardenStudentState;
import com.dbapp.garden.service.GardenStudentStateService;
import com.dbapp.garden.vm.StudentStateVm;

/**
 * 学员状态设置 控制器类
 * 
 * @author gangzi
 *
 */
@Controller
public class StudentStateController {

//	private static final Logger loggger = LoggerFactory.getLogger(StuBasicInfoConfigController.class);

	@Autowired
	private GardenStudentStateService stuStateService;


	@PostMapping("/studentstatelist")
	@ResponseBody
	// @PreAuthorize("hasAuthority('PERMISSION_STUDENT_SOURCE_LIST')")
	public Map<String, ?> studentStateList(HttpServletRequest request, String param) {

		Map<String, Object> paramMap = DataTableUtils.parseParam(param);
		if (paramMap == null) {
			return RenderJSON.makeMap(-1, "参数不合法！", null, "");
		}
		String sEcho = (String) paramMap.get("sEcho");
		String sSearch = (String) paramMap.get("sSearch");
		int iDisplayStart = (int) paramMap.get("iDisplayStart");
		int iDisplayLength = (int) paramMap.get("iDisplayLength");

		paramMap.clear();
		paramMap.put("search", sSearch);
		paramMap.put("start", iDisplayStart);
		paramMap.put("limit", iDisplayLength);

		List<GardenStudentState> studentList = stuStateService.getStudentStateList(paramMap);
		Integer count = stuStateService.getStudentStateCount(paramMap);

		return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, studentList, "");
	}

	@PostMapping("/studentstateadd")
	@ResponseBody
	public Map<String, ?> addStudentState(HttpServletRequest request,
			@RequestBody @Valid StudentStateVm stuStateVm) {
		GardenStudentState stuState = new GardenStudentState();
		stuState.setStateParam(stuStateVm.getStateParam());
		stuState.setStateProp(stuStateVm.getStateProp());
		stuState.setStateDesc(stuStateVm.getStateDesc());
		stuState.setStateSort(stuStateVm.getStateSort());
		stuState.setGardenId(1);
		stuState.setCreateTime(new Date());
		stuState.setUpdateTime(new Date());

		Integer result = stuStateService.save(stuState);
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，添加成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，添加失败！", "error");
		}

	}
	@DeleteMapping("/studentstatedelete/{id}")
	@ResponseBody
	public Map<String, ?> deleteStudentState(HttpServletRequest request,
			@PathVariable("id") Integer id) {
		
		Integer result = stuStateService.delete(id);
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，删除成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
		}
		
	}
	@PostMapping("/studentstateedit/{id}")
	@ResponseBody
	public Map<String, ?> editStudentState(HttpServletRequest request,
			@PathVariable("id") Integer id,@RequestBody @Valid StudentStateVm stuStateVm) {
		GardenStudentState stuState = new GardenStudentState();
		stuState.setId(id);
		stuState.setStateParam(stuStateVm.getStateParam());
		stuState.setStateProp(stuStateVm.getStateProp());
		stuState.setStateDesc(stuStateVm.getStateDesc());
		stuState.setStateSort(stuStateVm.getStateSort());
		
		stuState.setUpdateTime(new Date());
		
		
		Integer result = stuStateService.save(stuState);
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，编辑成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
		}
		
	}
}
