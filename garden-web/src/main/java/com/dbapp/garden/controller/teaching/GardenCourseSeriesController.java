package com.dbapp.garden.controller.teaching;

import com.dbapp.garden.common.DataTableUtils;
import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.entity.GardenCourseSeries;
import com.dbapp.garden.service.GardenCourseSeriesService;
import com.dbapp.garden.vm.CourseSeriesVm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 课程系列 控制器类
 * 
 * @author gangzi
 *
 */
@Controller
public class GardenCourseSeriesController {

//	private static final Logger loggger = LoggerFactory.getLogger(GardenCourseSeriesController.class);

	@Autowired
	private GardenCourseSeriesService seriesService;


	@PostMapping("/serieslist")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_SERIES')")
	public Map<String, ?> courseSeriesList(HttpServletRequest request, String param) {

		Map<String, Object> paramMap = DataTableUtils.parseParam(param);
		if (paramMap == null) {
			return RenderJSON.makeMap(-1, "参数不合法！", null, "");
		}
		String sEcho = (String) paramMap.get("sEcho");
		String sSearch = (String) paramMap.get("sSearch");
		int iDisplayStart = (int) paramMap.get("iDisplayStart");
		int iDisplayLength = (int) paramMap.get("iDisplayLength");

		paramMap.clear();
		paramMap.put("search", sSearch);
		paramMap.put("start", iDisplayStart);
		paramMap.put("limit", iDisplayLength);

		List<GardenCourseSeries> studentList = seriesService.getCouseSeriesList(paramMap);
		Integer count = seriesService.getCouseSeriesCount(paramMap);

		return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, studentList, "");
	}

	@PostMapping("/series")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_SERIES_ADD')")
	public Map<String, ?> addCourseSeries(HttpServletRequest request,
			@RequestBody @Valid CourseSeriesVm seriesVm) {
		GardenCourseSeries series = new GardenCourseSeries();
		series.setCourseSeriesName(seriesVm.getCourseSeriesName());
		series.setCourseSeriesSort(seriesVm.getCourseSeriesSort());
		series.setCourseSeriesRemark(seriesVm.getCourseSeriesRemark());
		series.setCreateTime(new Date());
		series.setUpdateTime(new Date());

		Integer result = seriesService.save(series);
		
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，添加成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，添加失败！", "error");
		}

	}
	@DeleteMapping("/series/{id}")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_SERIES_DELETE')")
	public Map<String, ?> deleteCourseSeries(HttpServletRequest request,
			@PathVariable("id") Integer id) {
		
		Integer result = seriesService.delete(id);
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，删除成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
		}
		
	}
	@PutMapping("/series/{id}")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_SERIES_EDIT')")
	public Map<String, ?> editCourseSeries(HttpServletRequest request,
			@PathVariable("id") Integer id,@RequestBody @Valid CourseSeriesVm seriesVm) {

		GardenCourseSeries series = seriesService.findById(id);
		if (series==null){
			return RenderJSON.makeStandMap(1, "对不起，该课程系列不存在，编辑失败！", "error");
		}
		series.setCourseSeriesName(seriesVm.getCourseSeriesName());
		series.setCourseSeriesSort(seriesVm.getCourseSeriesSort());
		series.setCourseSeriesRemark(seriesVm.getCourseSeriesRemark());
		series.setId(id);
		series.setUpdateTime(new Date());

		Integer result = seriesService.save(series);

		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，编辑成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
		}
		
	}
}
