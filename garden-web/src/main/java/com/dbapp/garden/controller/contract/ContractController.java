package com.dbapp.garden.controller.contract;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 合同基础信息管理 控制器类
 *
 * @author gangzi
 */
@Controller
public class ContractController {

    @GetMapping("/contract_basic_info")
    public String toBasicInfoPage(HttpServletRequest request) {
        return "contract/basic_info";
    }

    @GetMapping("/contract")
    public String toContract(HttpServletRequest request) {
        return "contract/contract";
    }
}
