package com.dbapp.garden.controller.contract;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dbapp.garden.common.DataTableUtils;
import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.entity.GardenDepositInfo;
import com.dbapp.garden.entity.GardenDepositInfoExtra;
import com.dbapp.garden.entity.GardenStudentState;
import com.dbapp.garden.service.IGardenDepositInfoService;

@Controller
public class DepositInfoController extends BaseContractController {
	@Autowired
	IGardenDepositInfoService depositService;
	
	@GetMapping("/studentstatefordepositlist")
	@ResponseBody
	public Map<String, ?> studentStateList(HttpServletRequest request) {
		int gardenId = this.getSelfGardenId(request);
		
		List<GardenStudentState> states = depositService.getStudentStateList(gardenId);
		return RenderJSON.makeStandMap(1, "获取成功", states);
	}
	
	@PostMapping("/depositlist")
	@ResponseBody
//	@PreAuthorize("hasAuthority('PERMISSION_CONTRACTPACKAGE_LIST')")
	public Map<String, ?> depositList(HttpServletRequest request, String param) {
		int gardenId = this.getSelfGardenId(request);
		Map<String, Object> paramMap = DataTableUtils.parseParam(param);
		if (paramMap == null) {
			return RenderJSON.makeMap(-1, "参数不合法！", null, "");
		}
		String sEcho = (String) paramMap.get("sEcho");
		String sSearch = (String) paramMap.get("sSearch");
		int iDisplayStart = (int) paramMap.get("iDisplayStart");
		int iDisplayLength = (int) paramMap.get("iDisplayLength");
		List<GardenDepositInfoExtra> deposits = depositService.getDepositExtratList(gardenId, sSearch, iDisplayStart, iDisplayLength);
		int count = depositService.getDepositCount(gardenId, sSearch);

		return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, deposits, "");
	}
	
	@PostMapping("/depositadd")
	@ResponseBody
	public Map<String, ?> addDeposit(HttpServletRequest request,
			@RequestBody @Valid GardenDepositInfo depositInfo) {
		if(depositInfo == null)
			return RenderJSON.makeStandMap(1, "对不起，添加失败！", "error");
		depositInfo.setCreateDate(new Date());
		depositInfo.setUpdateDate(new Date());
		int gardenId = this.getSelfGardenId(request);
		depositInfo.setGarden_id(gardenId);
		depositInfo.setDepositNumber(depositService.generateDepositNumber(gardenId));
		depositInfo = depositService.saveDeposit(depositInfo);
		if (depositInfo != null) {
			return RenderJSON.makeStandMap(1, "恭喜您，添加成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，添加失败！", "error");
		}
	}
	
	@DeleteMapping("/depositdelete/{id}")
	@ResponseBody
	public Map<String, ?> deleteDeposit(HttpServletRequest request, @PathVariable("id") Integer id) {
		if(id == null)
			return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
		int gardenId = this.getSelfGardenId(request);
		GardenDepositInfo depositInfo = depositService.findById(id);
		if(depositInfo == null || depositInfo.getGarden_id().intValue() != gardenId)
			return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
		
		int result = depositService.deleteDeposit(id);
		if (result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，删除成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
		}
	}
	
	@PostMapping("/depositedit/{id}")
	@ResponseBody
	public Map<String, ?> editDeposit(HttpServletRequest request, @PathVariable("id") Integer id,
			@RequestBody @Valid GardenDepositInfo depositInfo) {
		if(id == null || depositInfo == null)
			return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
		int gardenId = this.getSelfGardenId(request);
		GardenDepositInfo pInfo = depositService.findById(id);
		if(pInfo == null || pInfo.getGarden_id().intValue() != gardenId)
			return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
		depositInfo.setId(id);
		depositInfo.setUpdateDate(new Date());
		depositInfo.setGarden_id(gardenId);
		depositInfo = depositService.saveDeposit(depositInfo);
		if (depositInfo != null) {
			return RenderJSON.makeStandMap(1, "恭喜您，编辑成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
		}
	}
}
