package com.dbapp.garden.controller.position;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.dbapp.garden.config.security.PermissionUser;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.dbapp.garden.common.DataTableUtils;
import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.entity.GardenStaffPosition;
import com.dbapp.garden.service.GardenStaffPositionService;
import com.dbapp.garden.vm.GardenStaffPositionVm;
import com.dbapp.garden.vo.GardenStaffPositionVo;

@Controller
public class GardenStaffPositionController {

	@Autowired
	private GardenStaffPositionService gardenStaffPositionService;

	@GetMapping("/position")
	@PreAuthorize("hasAuthority('PERMISSION_POSITION')")
	public String toGardenStaffPositionPage(Model model) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<GardenStaffPosition> gardenList = gardenStaffPositionService.getPositionList(paramMap);
		model.addAttribute("positions", gardenList);

		return "position/positionlist";
	}

	@GetMapping("/positionedit/{id}")
	@PreAuthorize("hasAuthority('PERMISSION_POSITION_EDIT')")
	public String editPosition(@PathVariable("id") Integer id, Model model) {
		GardenStaffPosition position = gardenStaffPositionService.findById(id);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<GardenStaffPosition> gardenList = gardenStaffPositionService.getPositionList(paramMap);
		model.addAttribute("positions", gardenList);
		model.addAttribute("position", position);
		return "position/positionedit";
	}

	@PostMapping("/positionlist")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_POSITION')")
	public Map<String, ?> positionList(HttpServletRequest request, String param) {

		Map<String, Object> paramMap = DataTableUtils.parseParam(param);
		if (paramMap == null) {
			return RenderJSON.makeMap(-1, "参数不合法！", null, "");
		}
		String sEcho = (String) paramMap.get("sEcho");
		String sSearch = (String) paramMap.get("sSearch");
		int iDisplayStart = (int) paramMap.get("iDisplayStart");
		int iDisplayLength = (int) paramMap.get("iDisplayLength");

		paramMap.clear();
		paramMap.put("search", sSearch);
		paramMap.put("start", iDisplayStart);
		paramMap.put("limit", iDisplayLength);

		List<GardenStaffPosition> gardenList = gardenStaffPositionService.getPositionList(paramMap);
		Integer count = gardenStaffPositionService.getPositionCount(paramMap);
		List<GardenStaffPositionVo> gardenPositionVoList = gardenList.stream().map(x -> {
			GardenStaffPositionVo gardenPositionVo = new GardenStaffPositionVo();
			
			try {
				BeanUtils.copyProperties(gardenPositionVo, x);
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			if(x.getPositionParent()>0){
				GardenStaffPosition position=gardenStaffPositionService.findById(x.getPositionParent());
				gardenPositionVo.setPositionParentName(position.getPositionName());
			}
			
			return gardenPositionVo;
	}).collect(Collectors.toList());
		return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, gardenPositionVoList, "");
	}

	@PostMapping("/positionadd")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_POSITION_ADD')")
	public Map<String, ?> addPosition(HttpServletRequest request,
			@RequestBody @Valid GardenStaffPositionVm gardenStaffPositionVm) {
		GardenStaffPosition gardenStaffPosition = new GardenStaffPosition();
		gardenStaffPosition.setPositionName(gardenStaffPositionVm.getPositionName());
		gardenStaffPosition.setPositionDesc(gardenStaffPositionVm.getPositionDesc());
		gardenStaffPosition.setPositionParent(gardenStaffPositionVm.getPositionParent());
		gardenStaffPosition.setPositionSort(gardenStaffPositionVm.getPositionSort());
		// 这里需要处理
		gardenStaffPosition.setGardenId(gardenStaffPositionVm.getGardenId());

		gardenStaffPosition.setCreateTime(new Date());
		gardenStaffPosition.setUpdateTime(new Date());

		Integer result = gardenStaffPositionService.save(gardenStaffPosition);

		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，添加成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，添加失败！", "error");
		}

	}

	@DeleteMapping("/positiondelete/{id}")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_POSITION_DELETE')")
	public Map<String, ?> deletePosition(HttpServletRequest request, @PathVariable("id") Integer id,@SessionAttribute("user") PermissionUser user) {

		Integer result = gardenStaffPositionService.delete(id);
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，删除成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
		}

	}

	@PostMapping("/positionedit/{id}")
	@ResponseBody
	@PreAuthorize("hasAuthority('PERMISSION_POSITION_EDIT')")
	public Map<String, ?> editPosition(HttpServletRequest request, @PathVariable("id") Integer id,
			@RequestBody @Valid GardenStaffPositionVm gardenStaffPositionVm) {

		GardenStaffPosition gardenStaffPosition = new GardenStaffPosition();
		gardenStaffPosition.setId(id);
		gardenStaffPosition.setPositionName(gardenStaffPositionVm.getPositionName());
		gardenStaffPosition.setPositionDesc(gardenStaffPositionVm.getPositionDesc());
		gardenStaffPosition.setPositionParent(gardenStaffPositionVm.getPositionParent());
		gardenStaffPosition.setPositionSort(gardenStaffPositionVm.getPositionSort());

		gardenStaffPosition.setUpdateTime(new Date());

		Integer result = gardenStaffPositionService.save(gardenStaffPosition);
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，编辑成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
		}
	}
}
