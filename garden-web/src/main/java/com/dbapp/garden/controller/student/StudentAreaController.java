package com.dbapp.garden.controller.student;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dbapp.garden.common.DataTableUtils;
import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.entity.GardenStudentArea;
import com.dbapp.garden.service.GardenStudentAreaService;
import com.dbapp.garden.vm.StudentAreaVm;
import com.dbapp.garden.vo.StudentAreaVo;

/**
 * 学员地区设置 控制器类
 * 
 * @author gangzi
 *
 */
@Controller
public class StudentAreaController {

	// private static final Logger loggger =
	// LoggerFactory.getLogger(StuBasicInfoConfigController.class);

	@Autowired
	private GardenStudentAreaService stuAreaService;

	@PostMapping("/studentarealist")
	@ResponseBody
	// @PreAuthorize("hasAuthority('PERMISSION_STUDENT_SOURCE_LIST')")
	public Map<String, ?> studentAreaList(HttpServletRequest request, String param) {

		Map<String, Object> paramMap = DataTableUtils.parseParam(param);
		if (paramMap == null) {
			return RenderJSON.makeMap(-1, "参数不合法！", null, "");
		}
		String sEcho = (String) paramMap.get("sEcho");
		String sSearch = (String) paramMap.get("sSearch");
		int iDisplayStart = (int) paramMap.get("iDisplayStart");
		int iDisplayLength = (int) paramMap.get("iDisplayLength");

		paramMap.clear();
		paramMap.put("search", sSearch);
		paramMap.put("start", iDisplayStart);
		paramMap.put("limit", iDisplayLength);
		paramMap.put("areaParentId", 0);
		List<GardenStudentArea> studentList = stuAreaService.getStudentAreaList(paramMap);
		Integer count = stuAreaService.getStudentAreaCount(paramMap);
		List<StudentAreaVo> returnList = studentList.stream().map(stuArea -> {
			StudentAreaVo stuAreaVo = new StudentAreaVo();
			stuAreaVo.setStuArea(stuArea);
			paramMap.clear();
			paramMap.put("areaParentId", stuArea.getId());
			// 获取该一级区域的二级地区
			List<GardenStudentArea> childstudentAreaList = stuAreaService.getStudentAreaList(paramMap);
			stuAreaVo.setChildStuArea(childstudentAreaList);
			return stuAreaVo;
		}).collect(Collectors.toList());
		return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, returnList, "");
	}

	@GetMapping("/studentchildarea/{pId}")
	@ResponseBody
	// @PreAuthorize("hasAuthority('PERMISSION_STUDENT_SOURCE_LIST')")
	public Map<String, ?> studentAreaList(HttpServletRequest request, @PathVariable("pId") Integer pId) {

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("areaParentId", pId);
		
		List<GardenStudentArea> childAreaList = stuAreaService.getStudentAreaList(paramMap);

		return RenderJSON.makeStandMap(1, "获取成功", childAreaList);
	}

	@PostMapping("/studentareaadd")
	@ResponseBody
	public Map<String, ?> addStudentArea(HttpServletRequest request, @RequestBody @Valid StudentAreaVm stuAreaVm) {
		GardenStudentArea stuArea = new GardenStudentArea();
		stuArea.setAreaName(stuAreaVm.getAreaName());
		stuArea.setAreaParentId(stuAreaVm.getAreaParentId());
		stuArea.setCreateTime(new Date());
		stuArea.setUpdateTime(new Date());

		Integer result = stuAreaService.save(stuArea);

		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，添加成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，添加失败！", "error");
		}

	}

	@DeleteMapping("/studentareadelete/{id}")
	@ResponseBody
	public Map<String, ?> deleteStudentArea(HttpServletRequest request, @PathVariable("id") Integer id) {

		Integer result = stuAreaService.delete(id);
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，删除成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
		}

	}

	@PostMapping("/studentareaedit/{id}")
	@ResponseBody
	public Map<String, ?> editStudentArea(HttpServletRequest request, @PathVariable("id") Integer id,
			@RequestBody @Valid StudentAreaVm stuAreaVm) {

		GardenStudentArea stuArea = new GardenStudentArea();
		stuArea.setId(id);
		stuArea.setAreaName(stuAreaVm.getAreaName());
		stuArea.setAreaParentId(stuAreaVm.getAreaParentId());
		stuArea.setUpdateTime(new Date());

		Integer result = stuAreaService.save(stuArea);
		if (result != null && result > 0) {
			return RenderJSON.makeStandMap(1, "恭喜您，编辑成功！", "ok");
		} else {
			return RenderJSON.makeStandMap(1, "对不起，编辑失败！", "error");
		}

	}
}
