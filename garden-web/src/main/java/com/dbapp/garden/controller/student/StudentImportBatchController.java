package com.dbapp.garden.controller.student;

import com.dbapp.garden.common.DataTableUtils;
import com.dbapp.garden.common.RenderJSON;
import com.dbapp.garden.entity.GardenCareStudentSignIn;
import com.dbapp.garden.entity.GardenImportBatch;
import com.dbapp.garden.entity.GardenStudentFail;
import com.dbapp.garden.entity.GardenStudentInfo;
import com.dbapp.garden.service.GardenImportBatchService;
import com.dbapp.garden.service.GardenStudentFailService;
import com.dbapp.garden.service.GardenStudentInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/7.
 */
@Controller
public class StudentImportBatchController {
    @Autowired
    private GardenImportBatchService batchService;


    @Autowired
    private GardenStudentFailService studentFailService;

    @Autowired
    private GardenStudentInfoService studentInfoService;

    @PostMapping("/batchlist")
    @ResponseBody
    // @PreAuthorize("hasAuthority('PERMISSION_STUDENT_INFO')")
    public Map<String, ?> batchList(HttpServletRequest request, String param) {

        Map<String, Object> paramMap = DataTableUtils.parseParam(param);
        if (paramMap == null) {
            return RenderJSON.makeMap(-1, "参数不合法！", null, "");
        }
        String sEcho = (String) paramMap.get("sEcho");
        String sSearch = (String) paramMap.get("sSearch");
        int iDisplayStart = (int) paramMap.get("iDisplayStart");
        int iDisplayLength = (int) paramMap.get("iDisplayLength");

        paramMap.clear();
        paramMap.put("search", sSearch);
        paramMap.put("start", iDisplayStart);
        paramMap.put("limit", iDisplayLength);

        List<GardenImportBatch> batchList = batchService.getBatchList(paramMap);
        Integer count = batchService.getBatchCount(paramMap);

        return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, batchList, "");
    }

    @GetMapping("/studentimportsuccess/{id}")
    public String toSuccessPage(HttpServletRequest request, @PathVariable("id") Integer id,Model model) {

        GardenImportBatch batch=batchService.findById(id);

        model.addAttribute("batch",batch);

        return "student/studentimportsuccess";
    }

    @GetMapping("/studentimportfail/{id}")
    public String toFailPage(HttpServletRequest request, @PathVariable("id") Integer id,Model model) {

        GardenImportBatch batch=batchService.findById(id);

        model.addAttribute("batch",batch);
        return "student/studentimportfail";
    }

    @DeleteMapping("/importbatch/{id}")
    @ResponseBody
    public Map<String, ?> deleteImportBatch(HttpServletRequest request, @PathVariable("id") Integer id) {
        Integer result = batchService.delete(id);
        if (result != null && result > 0) {
            return RenderJSON.makeStandMap(1, "恭喜您，删除成功！", "ok");
        } else {
            return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
        }

    }

    @PostMapping("/studentsuccesslist")
    @ResponseBody
    // @PreAuthorize("hasAuthority('PERMISSION_STUDENT_INFO')")
    public Map<String, ?> importStudentFailList(HttpServletRequest request, String param,Integer batchId) {

        Map<String, Object> paramMap = DataTableUtils.parseParam(param);
        if (paramMap == null) {
            return RenderJSON.makeMap(-1, "参数不合法！", null, "");
        }
        String sEcho = (String) paramMap.get("sEcho");
        String sSearch = (String) paramMap.get("sSearch");
        int iDisplayStart = (int) paramMap.get("iDisplayStart");
        int iDisplayLength = (int) paramMap.get("iDisplayLength");

        paramMap.clear();
        paramMap.put("search", sSearch);
        paramMap.put("start", iDisplayStart);
        paramMap.put("limit", iDisplayLength);
        paramMap.put("batchId", batchId);

        List<GardenStudentInfo> batchList = studentInfoService.getStudentList(paramMap);
        Integer count = studentInfoService.getStudentCount(paramMap);

        return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, batchList, "");
    }

    @PostMapping("/studentfaillist")
    @ResponseBody
    // @PreAuthorize("hasAuthority('PERMISSION_STUDENT_INFO')")
    public Map<String, ?> importStudentSuccessList(HttpServletRequest request, String param,Integer batchId) {

        Map<String, Object> paramMap = DataTableUtils.parseParam(param);
        if (paramMap == null) {
            return RenderJSON.makeMap(-1, "参数不合法！", null, "");
        }
        String sEcho = (String) paramMap.get("sEcho");
        String sSearch = (String) paramMap.get("sSearch");
        int iDisplayStart = (int) paramMap.get("iDisplayStart");
        int iDisplayLength = (int) paramMap.get("iDisplayLength");

        paramMap.clear();
        paramMap.put("search", sSearch);
        paramMap.put("start", iDisplayStart);
        paramMap.put("limit", iDisplayLength);
        paramMap.put("batchId", batchId);

        List<GardenStudentFail> batchList = studentFailService.getImportStudentFailList(paramMap);
        Integer count = studentFailService.getImportStudentFailCount(paramMap);

        return RenderJSON.makePagenateMap(1, "获取成功", sEcho, count, count, batchList, "");
    }

    @DeleteMapping("/importfailstudent/{id}")
    @ResponseBody
    public Map<String, ?> deleteImportFailStudent(HttpServletRequest request, @PathVariable("id") Integer id) {
        Integer result = studentFailService.delete(id);
        if (result != null && result > 0) {
            return RenderJSON.makeStandMap(1, "恭喜您，删除成功！", "ok");
        } else {
            return RenderJSON.makeStandMap(1, "对不起，删除失败！", "error");
        }
    }

}
