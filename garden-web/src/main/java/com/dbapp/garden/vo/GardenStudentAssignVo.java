package com.dbapp.garden.vo;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * @author by gangzi on 2017/11/9.
 */
public class GardenStudentAssignVo {
    private Integer id;

    private String beforeAssignTeacher;

    private String afterAssignTeacher;

    private Integer studentId;

    private String  assignOperator;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    private Date createTime;

    private String studentName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBeforeAssignTeacher() {
        return beforeAssignTeacher;
    }

    public void setBeforeAssignTeacher(String beforeAssignTeacher) {
        this.beforeAssignTeacher = beforeAssignTeacher;
    }

    public String getAfterAssignTeacher() {
        return afterAssignTeacher;
    }

    public void setAfterAssignTeacher(String afterAssignTeacher) {
        this.afterAssignTeacher = afterAssignTeacher;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public String getAssignOperator() {
        return assignOperator;
    }

    public void setAssignOperator(String assignOperator) {
        this.assignOperator = assignOperator;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }
}
