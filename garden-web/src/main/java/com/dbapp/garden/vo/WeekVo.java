package com.dbapp.garden.vo;

import java.util.Date;
import java.util.List;

/**
 * 每年第几周的Vo类
 *
 * @author by gangzi on 2017/11/17.
 */
public class WeekVo {
    /**
     * 年
     */
    private int year;
    /**
     * 第几周
     */
    private int week;
    /**
     * 本周开始日期
     */
    private Date startDate;
    /**
     * 本周结束日期
     */
    private Date endDate;

    /**
     * 是否是当前周
     */
    private boolean isCurrentWeek=false;

    public boolean isCurrentWeek() {
        return isCurrentWeek;
    }

    public void setCurrentWeek(boolean currentWeek) {
        isCurrentWeek = currentWeek;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
