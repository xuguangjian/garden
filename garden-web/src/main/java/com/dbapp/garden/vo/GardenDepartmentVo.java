package com.dbapp.garden.vo;

public class GardenDepartmentVo {
	private Integer id;

    private String deptName;

    private String deptDesc;

    private String deptParentName;

    private Integer deptSort;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDeptDesc() {
		return deptDesc;
	}

	public void setDeptDesc(String deptDesc) {
		this.deptDesc = deptDesc;
	}

	public String getDeptParentName() {
		return deptParentName;
	}

	public void setDeptParentName(String deptParentName) {
		this.deptParentName = deptParentName;
	}

	public Integer getDeptSort() {
		return deptSort;
	}

	public void setDeptSort(Integer deptSort) {
		this.deptSort = deptSort;
	}

}
