package com.dbapp.garden.vo;

public enum ContractFieldType {
	TEXT_FIELD (1, "文本"),
	NUMBER_FIELD (2, "数字"),
	DATE_FIELD (3, "日期");
	
	private int fieldTypeValue;
	private String fieldTypeName;
	
	private ContractFieldType(int v, String n){
		this.fieldTypeName = n;
		this.fieldTypeValue = v;
	}

	public String getFieldTypeName() {
		return fieldTypeName;
	}

	public void setFieldTypeName(String fieldTypeName) {
		this.fieldTypeName = fieldTypeName;
	}

	public int getFieldTypeValue() {
		return fieldTypeValue;
	}

	public void setFieldTypeValue(int fieldTypeValue) {
		this.fieldTypeValue = fieldTypeValue;
	}
	
	public static String getTypeName(int type){
		ContractFieldType[] values = ContractFieldType.values();
		for(ContractFieldType t: values){
			if(t.getFieldTypeValue() != type) continue;
			
			return t.getFieldTypeName();
		}
		
		return "未知";
	}
}
