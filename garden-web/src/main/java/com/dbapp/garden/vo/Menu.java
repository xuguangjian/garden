package com.dbapp.garden.vo;

import java.io.Serializable;
import java.util.List;

public class Menu implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2417604619689007454L;
	
	
	private String name;
	private String url;
	private Integer id;
	private Integer parentId;
	private String icon;
	private Integer sort;
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	private List<Menu> childMenu;
	private List<String> childUrls;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public List<Menu> getChildMenu() {
		return childMenu;
	}

	public void setChildMenu(List<Menu> childMenu) {
		this.childMenu = childMenu;
	}

	public List<String> getChildUrls() {
		return childUrls;
	}

	public void setChildUrls(List<String> childUrls) {
		this.childUrls = childUrls;
	}
}
