package com.dbapp.garden.vo;

public class GardenStaffPositionVo {
	private Integer id;

    private String positionName;

    private String positionDesc;

    private String positionParentName;

    private Integer positionSort;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public String getPositionDesc() {
		return positionDesc;
	}

	public void setPositionDesc(String positionDesc) {
		this.positionDesc = positionDesc;
	}

	public String getPositionParentName() {
		return positionParentName;
	}

	public void setPositionParentName(String positionParentName) {
		this.positionParentName = positionParentName;
	}

	public Integer getPositionSort() {
		return positionSort;
	}

	public void setPositionSort(Integer positionSort) {
		this.positionSort = positionSort;
	}
}
