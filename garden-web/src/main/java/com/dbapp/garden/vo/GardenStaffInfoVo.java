package com.dbapp.garden.vo;

import java.util.Date;

/**
 * @author by gangzi on 2017/11/22.
 */
public class GardenStaffInfoVo {
    private Integer id;

    private String staffRealName;

    private String staffPhone;

    private String staffDepartment;

    private String staffPosition;

    private String userName;

    private Integer staffState;

    private String staffPermission;

    private String staffQq;

    private Integer isMarketPerson;

    private Integer isTeacher;

    private Integer isAdviser;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStaffRealName() {
        return staffRealName;
    }

    public void setStaffRealName(String staffRealName) {
        this.staffRealName = staffRealName;
    }

    public String getStaffPhone() {
        return staffPhone;
    }

    public void setStaffPhone(String staffPhone) {
        this.staffPhone = staffPhone;
    }

    public String getStaffDepartment() {
        return staffDepartment;
    }

    public void setStaffDepartment(String staffDepartment) {
        this.staffDepartment = staffDepartment;
    }

    public String getStaffPosition() {
        return staffPosition;
    }

    public void setStaffPosition(String staffPosition) {
        this.staffPosition = staffPosition;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getStaffState() {
        return staffState;
    }

    public void setStaffState(Integer staffState) {
        this.staffState = staffState;
    }

    public String getStaffPermission() {
        return staffPermission;
    }

    public void setStaffPermission(String staffPermission) {
        this.staffPermission = staffPermission;
    }

    public String getStaffQq() {
        return staffQq;
    }

    public void setStaffQq(String staffQq) {
        this.staffQq = staffQq;
    }

    public Integer getIsMarketPerson() {
        return isMarketPerson;
    }

    public void setIsMarketPerson(Integer isMarketPerson) {
        this.isMarketPerson = isMarketPerson;
    }

    public Integer getIsTeacher() {
        return isTeacher;
    }

    public void setIsTeacher(Integer isTeacher) {
        this.isTeacher = isTeacher;
    }

    public Integer getIsAdviser() {
        return isAdviser;
    }

    public void setIsAdviser(Integer isAdviser) {
        this.isAdviser = isAdviser;
    }
}
