package com.dbapp.garden.vo;

import java.util.List;

import com.dbapp.garden.entity.GardenStudentArea;

public class StudentAreaVo {
	private GardenStudentArea stuArea;
	private List<GardenStudentArea> childStuArea;
	public GardenStudentArea getStuArea() {
		return stuArea;
	}
	public void setStuArea(GardenStudentArea stuArea) {
		this.stuArea = stuArea;
	}
	public List<GardenStudentArea> getChildStuArea() {
		return childStuArea;
	}
	public void setChildStuArea(List<GardenStudentArea> childStuArea) {
		this.childStuArea = childStuArea;
	}
	
}
