package com.dbapp.garden.vo;

public enum ContractFieldComponent {

	INPUT_TEXT (1, "输入框"),
	COMBOX_TEXT(2, "下拉框"),
	RADIO_TEXT(3, "单选框"),
	CHECKBOX_TEXT(4, "复选框");
	
	private int fieldComponentValue;
	private String fieldComponentName;
	
	private ContractFieldComponent(int v, String n){
		this.fieldComponentName = n;
		this.fieldComponentValue = v;
	}

	public int getFieldComponentValue() {
		return fieldComponentValue;
	}

	public void setFieldComponentValue(int fieldComponentValue) {
		this.fieldComponentValue = fieldComponentValue;
	}

	public String getFieldComponentName() {
		return fieldComponentName;
	}

	public void setFieldComponentName(String fieldComponentName) {
		this.fieldComponentName = fieldComponentName;
	}
	
	public static String getComponentName(int type){
		ContractFieldComponent[] values = ContractFieldComponent.values();
		for(ContractFieldComponent t: values){
			if(t.getFieldComponentValue() != type) continue;
			
			return t.getFieldComponentName();
		}
		
		return "未知";
	}
	
}
