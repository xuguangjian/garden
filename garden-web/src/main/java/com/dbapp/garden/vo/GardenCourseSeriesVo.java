package com.dbapp.garden.vo;

import com.dbapp.garden.entity.GardenCourse;

import java.util.List;

/**
 * @author by gangzi on 2017/11/20.
 */
public class GardenCourseSeriesVo {

    private Integer id;

    private String courseSeriesName;

    List<GardenCourse> courses;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCourseSeriesName() {
        return courseSeriesName;
    }

    public void setCourseSeriesName(String courseSeriesName) {
        this.courseSeriesName = courseSeriesName;
    }

    public List<GardenCourse> getCourses() {
        return courses;
    }

    public void setCourses(List<GardenCourse> courses) {
        this.courses = courses;
    }
}
