package com.dbapp.garden.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

/**
 * @author gangzi
 */
@WebFilter(filterName="xssFilter",urlPatterns="/*")
public class XSSFilter implements Filter {
	
    @Override
    public void destroy() {
        System.out.println("过滤器销毁");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        String method="";
        if (request instanceof HttpServletRequest){
            method=((HttpServletRequest) request).getMethod();
        }
        if ("GET".equalsIgnoreCase(method)){
            chain.doFilter(request, response);
        }else{

            System.out.println("执行XSS过滤操作，开发中。。。");
            chain.doFilter(request, response);
        }


    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        System.out.println("过滤器初始化");
    }

}
