package com.dbapp.garden.handler;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.dbapp.garden.common.RenderJSON;

@ControllerAdvice
public class GlobalExceptionHandler {
	private static Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	public static final String DEFAULT_ERROR_VIEW = "error";

	// @ExceptionHandler(value = Exception.class)
	// public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception
	// e) throws Exception {
	// ModelAndView mav = new ModelAndView();
	// mav.addObject("exception", e);
	// mav.addObject("url", req.getRequestURL());
	// mav.setViewName(DEFAULT_ERROR_VIEW);
	// return mav;
	// }

	//
	//
	// /**
	// * 400 - Bad Request
	// */
	// @ResponseStatus(HttpStatus.BAD_REQUEST)
	// @ExceptionHandler(MissingServletRequestParameterException.class)
	// public AjaxResult
	// handleMissingServletRequestParameterException(MissingServletRequestParameterException
	// e) {
	// logger.error("缺少请求参数", e);
	// return new AjaxResult().failure("required_parameter_is_not_present");
	// }
	//
	// /**
	// * 400 - Bad Request
	// */
	// @ResponseStatus(HttpStatus.BAD_REQUEST)
	// @ExceptionHandler(HttpMessageNotReadableException.class)
	// public AjaxResult
	// handleHttpMessageNotReadableException(HttpMessageNotReadableException e)
	// {
	// logger.error("参数解析失败", e);
	// return new AjaxResult().failure("could_not_read_json");
	// }
	//
	// /**
	// * 400 - Bad Request
	// */
	// @ResponseStatus(HttpStatus.BAD_REQUEST)
	// @ExceptionHandler(MethodArgumentNotValidException.class)
	// @ResponseBody
	// public Map<String, ?>
	// handleMethodArgumentNotValidException(MethodArgumentNotValidException e)
	// {
	// logger.error("参数验证失败");
	// BindingResult result = e.getBindingResult();
	// FieldError error = result.getFieldError();
	// String field = error.getField();
	// String code = error.getDefaultMessage();
	// String message = String.format("%s:%s", field, code);
	// return RenderJSON.makeStandMap(1, message, code);
	// }

	//
	// /**
	// * 400 - Bad Request
	// */
	// @ResponseStatus(HttpStatus.BAD_REQUEST)
	// @ExceptionHandler(BindException.class)
	// public AjaxResult handleBindException(BindException e) {
	// logger.error("参数绑定失败", e);
	// BindingResult result = e.getBindingResult();
	// FieldError error = result.getFieldError();
	// String field = error.getField();
	// String code = error.getDefaultMessage();
	// String message = String.format("%s:%s", field, code);
	// return new AjaxResult().failure(message);
	// }
	//
	// /**
	// * 400 - Bad Request
	// */
	// @ResponseStatus(HttpStatus.BAD_REQUEST)
	// @ExceptionHandler(ConstraintViolationException.class)
	// public AjaxResult handleServiceException(ConstraintViolationException e)
	// {
	// logger.error("参数验证失败", e);
	// Set<ConstraintViolation<?>> violations = e.getConstraintViolations();
	// ConstraintViolation<?> violation = violations.iterator().next();
	// String message = violation.getMessage();
	// return new AjaxResult().failure("parameter:" + message);
	// }
	//
	// /**
	// * 400 - Bad Request
	// */
	// @ResponseStatus(HttpStatus.BAD_REQUEST)
	// @ExceptionHandler(ValidationException.class)
	// public AjaxResult handleValidationException(ValidationException e) {
	// logger.error("参数验证失败", e);
	// return new AjaxResult().failure("validation_exception");
	// }
	//
	// /**
	// * 405 - Method Not Allowed
	// */
	@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	@ResponseBody
	public Map<String,?> handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
		logger.error("不支持当前请求方法", e);
		return RenderJSON.makeStandMap(405, "请求方法不对", "");
	}
	//
	// /**
	// * 415 - Unsupported Media Type
	// */
	// @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
	// @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
	// public AjaxResult handleHttpMediaTypeNotSupportedException(Exception e) {
	// logger.error("不支持当前媒体类型", e);
	// return new AjaxResult().failure("content_type_not_supported");
	// }
	//
	// /**
	// * 500 - Internal Server Error
	// */
	// @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	// @ExceptionHandler(ServiceException.class)
	// public AjaxResult handleServiceException(ServiceException e) {
	// logger.error("业务逻辑异常", e);
	// return new AjaxResult().failure("业务逻辑异常：" + e.getMessage());
	// }
	//
	// /**
	// * 500 - Internal Server Error
	// */
	// @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	// @ExceptionHandler(Exception.class)
	// public AjaxResult handleException(Exception e) {
	// logger.error("通用异常", e);
	// return new AjaxResult().failure("通用异常：" + e.getMessage());
	// }
	//
	// /**
	// * 操作数据库出现异常:名称重复，外键关联
	// */
	// @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	// @ExceptionHandler(DataIntegrityViolationException.class)
	// public AjaxResult handleException(DataIntegrityViolationException e) {
	// logger.error("操作数据库出现异常:", e);
	// return new AjaxResult().failure("操作数据库出现异常：字段重复、有外键关联等");
	// }

	// @ExceptionHandler(value = { ConstraintViolationException.class })
	// @ResponseStatus(HttpStatus.BAD_REQUEST)
	// public ApiErrorResponse
	// constraintViolationException(ConstraintViolationException ex) {
	// return new ApiErrorResponse(500, 5001, ex.getMessage());
	// }
	//
	// @ExceptionHandler(value = { IllegalArgumentException.class })
	// @ResponseStatus(HttpStatus.BAD_REQUEST)
	// public ApiErrorResponse IllegalArgumentException(IllegalArgumentException
	// ex) {
	// return new ApiErrorResponse(501, 5002, ex.getMessage());
	// }
	//
//	@ExceptionHandler(value = { NoHandlerFoundException.class })
	// @ExceptionHandler(value = { Exception.class })
//	@ResponseStatus(HttpStatus.NOT_FOUND)
//	public ModelAndView noHandlerFoundException(HttpServletRequest req, Exception ex) {
//		System.out.println("404处理执行了。。。");
//		ModelAndView mav = new ModelAndView();
//		mav.addObject("exception", ex);
//		mav.addObject("url", req.getRequestURL());
//		mav.setViewName("error/404");
//		return mav;
//	}

	//
	//
//	@ExceptionHandler(value = { Exception.class })
//	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
//	public ModelAndView unknownException(HttpServletRequest req, Exception ex) {
//		System.out.println("500处理执行了。。。");
//		ModelAndView mav = new ModelAndView();
//		mav.addObject("exception", ex);
//		mav.addObject("url", req.getRequestURL());
//		mav.setViewName("error/500");
//		return mav;
//	}
}
