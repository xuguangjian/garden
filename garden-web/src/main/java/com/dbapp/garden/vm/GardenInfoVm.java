package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

public class GardenInfoVm {
	@NotBlank
	private String gardenName;

    private String gardenManager;

    private String gardenManagerPhone;

    private String gardenAddress;

	public String getGardenName() {
		return gardenName;
	}

	public void setGardenName(String gardenName) {
		this.gardenName = gardenName;
	}

	public String getGardenManager() {
		return gardenManager;
	}

	public void setGardenManager(String gardenManager) {
		this.gardenManager = gardenManager;
	}

	public String getGardenManagerPhone() {
		return gardenManagerPhone;
	}

	public void setGardenManagerPhone(String gardenManagerPhone) {
		this.gardenManagerPhone = gardenManagerPhone;
	}

	public String getGardenAddress() {
		return gardenAddress;
	}

	public void setGardenAddress(String gardenAddress) {
		this.gardenAddress = gardenAddress;
	}
    
    
}
