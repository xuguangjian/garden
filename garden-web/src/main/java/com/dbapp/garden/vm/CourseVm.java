package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * @author  by gangzi on 2017/11/12.
 */
public class CourseVm {

    @NotBlank(message = "课程名称不能为空")
    private String courseName;

    @NotNull(message = "所属课程系列不能为空")
    private Integer courseSeries;

    private String suitableAge;

    private Integer totalCapacity;

    private Integer memberCount;

    private Integer experienceCount;

    private Integer courseSort;

    private String courseRemark;

    private Float deductLessonHour;

    private String suitableAgeFromYear;

    private String suitableAgeFromMonth;

    private String suitableAgeToYear;

    private String suitableAgeToMonth;

    public String getSuitableAgeFromYear() {
        return suitableAgeFromYear;
    }

    public void setSuitableAgeFromYear(String suitableAgeFromYear) {
        this.suitableAgeFromYear = suitableAgeFromYear;
    }

    public String getSuitableAgeFromMonth() {
        return suitableAgeFromMonth;
    }

    public void setSuitableAgeFromMonth(String suitableAgeFromMonth) {
        this.suitableAgeFromMonth = suitableAgeFromMonth;
    }

    public String getSuitableAgeToYear() {
        return suitableAgeToYear;
    }

    public void setSuitableAgeToYear(String suitableAgeToYear) {
        this.suitableAgeToYear = suitableAgeToYear;
    }

    public String getSuitableAgeToMonth() {
        return suitableAgeToMonth;
    }

    public void setSuitableAgeToMonth(String suitableAgeToMonth) {
        this.suitableAgeToMonth = suitableAgeToMonth;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Integer getCourseSeries() {
        return courseSeries;
    }

    public void setCourseSeries(Integer courseSeries) {
        this.courseSeries = courseSeries;
    }

    public String getSuitableAge() {
        return suitableAge;
    }

    public void setSuitableAge(String suitableAge) {
        this.suitableAge = suitableAge;
    }

    public Integer getTotalCapacity() {
        return totalCapacity;
    }

    public void setTotalCapacity(Integer totalCapacity) {
        this.totalCapacity = totalCapacity;
    }

    public Integer getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(Integer memberCount) {
        this.memberCount = memberCount;
    }

    public Integer getExperienceCount() {
        return experienceCount;
    }

    public void setExperienceCount(Integer experienceCount) {
        this.experienceCount = experienceCount;
    }

    public Integer getCourseSort() {
        return courseSort;
    }

    public void setCourseSort(Integer courseSort) {
        this.courseSort = courseSort;
    }

    public String getCourseRemark() {
        return courseRemark;
    }

    public void setCourseRemark(String courseRemark) {
        this.courseRemark = courseRemark;
    }

    public Float getDeductLessonHour() {
        return deductLessonHour;
    }

    public void setDeductLessonHour(Float deductLessonHour) {
        this.deductLessonHour = deductLessonHour;
    }
}
