package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

public class StudentStateVm {
	@NotBlank
	private String stateParam;

    private String stateProp;

    private String stateDesc;

    private Integer stateSort;

	public String getStateParam() {
		return stateParam;
	}

	public void setStateParam(String stateParam) {
		this.stateParam = stateParam;
	}

	public String getStateProp() {
		return stateProp;
	}

	public void setStateProp(String stateProp) {
		this.stateProp = stateProp;
	}

	public String getStateDesc() {
		return stateDesc;
	}

	public void setStateDesc(String stateDesc) {
		this.stateDesc = stateDesc;
	}

	public Integer getStateSort() {
		return stateSort;
	}

	public void setStateSort(Integer stateSort) {
		this.stateSort = stateSort;
	}
    
}
