package com.dbapp.garden.vm;

import javax.validation.constraints.NotNull;

/**
 * @author by gangzi on 2017/11/20.
 */
public class StudentUpgradeVm {

    @NotNull(message = "学员ID不能为空")
    private Integer studentId;


    private String beforeCourseId;

    private String afterCourseId;

    private String remark;

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public String getBeforeCourseId() {
        return beforeCourseId;
    }

    public void setBeforeCourseId(String beforeCourseId) {
        this.beforeCourseId = beforeCourseId;
    }

    public String getAfterCourseId() {
        return afterCourseId;
    }

    public void setAfterCourseId(String afterCourseId) {
        this.afterCourseId = afterCourseId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
