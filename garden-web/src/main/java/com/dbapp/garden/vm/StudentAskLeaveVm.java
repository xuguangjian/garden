package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * @author by gangzi on 2017/11/20.
 */
public class StudentAskLeaveVm {

    @NotNull(message = "学员ID不能为空")
    private Integer studentId;

    @NotNull(message = "课程ID不能为空")
    private Integer classCourseId;

    private Float deductHour;

    private String message;

    @NotBlank(message = "请假事由不能为空！")
    private String leaveReason;

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public Integer getClassCourseId() {
        return classCourseId;
    }

    public void setClassCourseId(Integer classCourseId) {
        this.classCourseId = classCourseId;
    }

    public Float getDeductHour() {
        return deductHour;
    }

    public void setDeductHour(Float deductHour) {
        this.deductHour = deductHour;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLeaveReason() {
        return leaveReason;
    }

    public void setLeaveReason(String leaveReason) {
        this.leaveReason = leaveReason;
    }
}
