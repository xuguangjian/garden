package com.dbapp.garden.vm;

/**
 * @author by gangzi on 2017/11/22.
 */
public class StaffInfoSearchVm {

    private String staffRealName;
    private Integer staffState;
    private Integer staffDepartment;
    private String staffPhone;
    private Integer staffPosition;

    public String getStaffRealName() {
        return staffRealName;
    }

    public void setStaffRealName(String staffRealName) {
        this.staffRealName = staffRealName;
    }

    public Integer getStaffState() {
        return staffState;
    }

    public void setStaffState(Integer staffState) {
        this.staffState = staffState;
    }

    public Integer getStaffDepartment() {
        return staffDepartment;
    }

    public void setStaffDepartment(Integer staffDepartment) {
        this.staffDepartment = staffDepartment;
    }

    public String getStaffPhone() {
        return staffPhone;
    }

    public void setStaffPhone(String staffPhone) {
        this.staffPhone = staffPhone;
    }

    public Integer getStaffPosition() {
        return staffPosition;
    }

    public void setStaffPosition(Integer staffPosition) {
        this.staffPosition = staffPosition;
    }
}
