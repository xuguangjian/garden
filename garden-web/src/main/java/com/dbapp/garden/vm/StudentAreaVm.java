package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

public class StudentAreaVm {
	@NotBlank(message="地区不能为空")
	private String areaName;

    private Integer areaParentId;

    private Integer gardenId;

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Integer getAreaParentId() {
		return areaParentId;
	}

	public void setAreaParentId(Integer areaParentId) {
		this.areaParentId = areaParentId;
	}

	public Integer getGardenId() {
		return gardenId;
	}

	public void setGardenId(Integer gardenId) {
		this.gardenId = gardenId;
	}
    
    
}
