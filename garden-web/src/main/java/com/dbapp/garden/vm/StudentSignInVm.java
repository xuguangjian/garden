package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author by gangzi on 2017/11/14.
 */
public class StudentSignInVm {

    @NotNull(message = "签到类型不能为空")
    private Integer signInType;

    private Date signInTime;

    @NotBlank(message = "学员ID不能为空")
    private String studentId;

    private String signInRemark;

    private Integer gardenId;

    public Integer getSignInType() {
        return signInType;
    }

    public void setSignInType(Integer signInType) {
        this.signInType = signInType;
    }

    public Date getSignInTime() {
        return signInTime;
    }

    public void setSignInTime(Date signInTime) {
        this.signInTime = signInTime;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getSignInRemark() {
        return signInRemark;
    }

    public void setSignInRemark(String signInRemark) {
        this.signInRemark = signInRemark;
    }

    public Integer getGardenId() {
        return gardenId;
    }

    public void setGardenId(Integer gardenId) {
        this.gardenId = gardenId;
    }
}
