package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

public class GardenDepartmentVm {
	@NotBlank
	private String deptName;

	private String deptDesc;

	private Integer deptParent;

	private Integer deptSort;

	private Integer gardenId;

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDeptDesc() {
		return deptDesc;
	}

	public void setDeptDesc(String deptDesc) {
		this.deptDesc = deptDesc;
	}

	public Integer getDeptParent() {
		return deptParent;
	}

	public void setDeptParent(Integer deptParent) {
		this.deptParent = deptParent;
	}

	public Integer getDeptSort() {
		return deptSort;
	}

	public void setDeptSort(Integer deptSort) {
		this.deptSort = deptSort;
	}

	public Integer getGardenId() {
		return gardenId;
	}

	public void setGardenId(Integer gardenId) {
		this.gardenId = gardenId;
	}
	
	
}
