package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

public class StudentContactVm {
	@NotBlank
	private String contactParam;

    private String contactDesc;

	public String getContactParam() {
		return contactParam;
	}

	public void setContactParam(String contactParam) {
		this.contactParam = contactParam;
	}

	public String getContactDesc() {
		return contactDesc;
	}

	public void setContactDesc(String contactDesc) {
		this.contactDesc = contactDesc;
	}
    
    
}
