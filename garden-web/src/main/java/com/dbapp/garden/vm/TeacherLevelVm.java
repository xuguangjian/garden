package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

public class TeacherLevelVm {
	@NotBlank
	private String levelParam;

    private String levelDesc;

    private Integer gardenId;

	public String getLevelParam() {
		return levelParam;
	}

	public void setLevelParam(String levelParam) {
		this.levelParam = levelParam;
	}

	public String getLevelDesc() {
		return levelDesc;
	}

	public void setLevelDesc(String levelDesc) {
		this.levelDesc = levelDesc;
	}

	public Integer getGardenId() {
		return gardenId;
	}

	public void setGardenId(Integer gardenId) {
		this.gardenId = gardenId;
	}
    
    
}
