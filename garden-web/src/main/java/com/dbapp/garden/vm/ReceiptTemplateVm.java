package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author by gangzi on 2017/11/14.
 */
public class ReceiptTemplateVm {

    @NotBlank(message = "模版内容不能空！")
    private String content;

    private Integer type;

    private Integer gardenId;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getGardenId() {
        return gardenId;
    }

    public void setGardenId(Integer gardenId) {
        this.gardenId = gardenId;
    }
}
