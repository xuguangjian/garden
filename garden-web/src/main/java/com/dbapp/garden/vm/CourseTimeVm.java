package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author by gangzi on 2017/11/13.
 */
public class CourseTimeVm {

    @NotBlank(message = "上课时间不能为空！")
    private String courseTime;

    private Integer timeSort;

    private String timeRemark;

    public String getCourseTime() {
        return courseTime;
    }

    public void setCourseTime(String courseTime) {
        this.courseTime = courseTime;
    }

    public Integer getTimeSort() {
        return timeSort;
    }

    public void setTimeSort(Integer timeSort) {
        this.timeSort = timeSort;
    }

    public String getTimeRemark() {
        return timeRemark;
    }

    public void setTimeRemark(String timeRemark) {
        this.timeRemark = timeRemark;
    }
}
