package com.dbapp.garden.vm;

import java.util.List;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author gangzi
 */
public class SysRoleVm {

	@NotBlank(message = "角色名不能为空")
	private String roleName;
	private Integer gardenId;
	@NotBlank(message = "角色码不能为空")
	private String roleCode;
	private String roleDesc;
	private List<Integer> resIds;


	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getRoleDesc() {
		return roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	public List<Integer> getResIds() {
		return resIds;
	}

	public void setResIds(List<Integer> resIds) {
		this.resIds = resIds;
	}



	public Integer getGardenId() {
		return gardenId;
	}

	public void setGardenId(Integer gardenId) {
		this.gardenId = gardenId;
	}
}
