package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author by gangzi on 2017/11/13.
 */
public class CourseCommentVm {
    @NotBlank(message = "课评名称不能为空！")
    private String commentName;

    private Integer commentScore;

    private Integer commentSort;

    private Integer gardenId;

    public String getCommentName() {
        return commentName;
    }

    public void setCommentName(String commentName) {
        this.commentName = commentName;
    }

    public Integer getCommentScore() {
        return commentScore;
    }

    public void setCommentScore(Integer commentScore) {
        this.commentScore = commentScore;
    }

    public Integer getCommentSort() {
        return commentSort;
    }

    public void setCommentSort(Integer commentSort) {
        this.commentSort = commentSort;
    }

    public Integer getGardenId() {
        return gardenId;
    }

    public void setGardenId(Integer gardenId) {
        this.gardenId = gardenId;
    }
}
