package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

public class SysUserEditVm {

	@NotBlank
	private String userPhone;

	@NotBlank
	private String userRealName;

	private Integer userRole;

	public Integer getUserRole() {
		return userRole;
	}

	public void setUserRole(Integer userRole) {
		this.userRole = userRole;
	}

	
	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getUserRealName() {
		return userRealName;
	}

	public void setUserRealName(String userRealName) {
		this.userRealName = userRealName;
	}
}
