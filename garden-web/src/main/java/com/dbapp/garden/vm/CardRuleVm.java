package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

import java.util.Date;

/**
 * @author by gangzi on 2017/11/12.
 */
public class CardRuleVm {
    @NotBlank
    private String rulesName;

    private Integer rulesState;

    private String swipeTime;

    private Integer feeType;

    private Float deductHour;

    private Integer courseHourType;

    private Integer swipeCardCount;

    public Float getDeductHour() {
        return deductHour;
    }

    public void setDeductHour(Float deductHour) {
        this.deductHour = deductHour;
    }

    public Integer getCourseHourType() {
        return courseHourType;
    }

    public void setCourseHourType(Integer courseHourType) {
        this.courseHourType = courseHourType;
    }

    public String getRulesName() {
        return rulesName;
    }

    public void setRulesName(String rulesName) {
        this.rulesName = rulesName;
    }

    public Integer getRulesState() {
        return rulesState;
    }

    public void setRulesState(Integer rulesState) {
        this.rulesState = rulesState;
    }

    public String getSwipeTime() {
        return swipeTime;
    }

    public void setSwipeTime(String swipeTime) {
        this.swipeTime = swipeTime;
    }

    public Integer getFeeType() {
        return feeType;
    }

    public void setFeeType(Integer feeType) {
        this.feeType = feeType;
    }


    public Integer getSwipeCardCount() {
        return swipeCardCount;
    }

    public void setSwipeCardCount(Integer swipeCardCount) {
        this.swipeCardCount = swipeCardCount;
    }
}
