package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * @author by gangzi on 2017/11/15.
 */
public class GardenImportBatchVm {
    @NotNull(message = "ID不能为空")
    private Integer id;
    @NotBlank(message = "批次名称")
    private String batchName;

    private String batchSource;

    private Integer batchCount;
    @NotNull(message = "批次类型")
    private Integer batchType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBatchName() {
        return batchName;
    }

    public void setBatchName(String batchName) {
        this.batchName = batchName;
    }

    public String getBatchSource() {
        return batchSource;
    }

    public void setBatchSource(String batchSource) {
        this.batchSource = batchSource;
    }

    public Integer getBatchCount() {
        return batchCount;
    }

    public void setBatchCount(Integer batchCount) {
        this.batchCount = batchCount;
    }

    public Integer getBatchType() {
        return batchType;
    }

    public void setBatchType(Integer batchType) {
        this.batchType = batchType;
    }
}
