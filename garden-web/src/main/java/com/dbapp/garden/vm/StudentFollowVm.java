package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

public class StudentFollowVm {
	@NotBlank
	private String followParam;

    private Integer followSort;

    private String followDesc;

	public String getFollowParam() {
		return followParam;
	}

	public void setFollowParam(String followParam) {
		this.followParam = followParam;
	}

	public Integer getFollowSort() {
		return followSort;
	}

	public void setFollowSort(Integer followSort) {
		this.followSort = followSort;
	}

	public String getFollowDesc() {
		return followDesc;
	}

	public void setFollowDesc(String followDesc) {
		this.followDesc = followDesc;
	}
    
    
}
