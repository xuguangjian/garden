package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

public class StudentSourceVm {

	@NotBlank
	private String sourceName;
	
	private String detailSource;

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getDetailSource() {
		return detailSource;
	}

	public void setDetailSource(String detailSource) {
		this.detailSource = detailSource;
	}
	
	
	
}
