package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author by gangzi on 2017/11/17.
 */
public class StudentUpdateVm {

    @NotEmpty(message = "学员ID不能为空")
    Integer[] ids;
    Integer personId;

    public Integer[] getIds() {
        return ids;
    }

    public void setIds(Integer[] ids) {
        this.ids = ids;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }
}
