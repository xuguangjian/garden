package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author  by gangzi on 2017/11/10.
 */
public class CourseSeriesVm {

    private Integer id;

    @NotBlank(message = "课程系列名称不能为空")
    private String courseSeriesName;

    private Integer courseSeriesSort;

    private String courseSeriesRemark;

    private Integer gardenId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCourseSeriesName() {
        return courseSeriesName;
    }

    public void setCourseSeriesName(String courseSeriesName) {
        this.courseSeriesName = courseSeriesName;
    }

    public Integer getCourseSeriesSort() {
        return courseSeriesSort;
    }

    public void setCourseSeriesSort(Integer courseSeriesSort) {
        this.courseSeriesSort = courseSeriesSort;
    }

    public String getCourseSeriesRemark() {
        return courseSeriesRemark;
    }

    public void setCourseSeriesRemark(String courseSeriesRemark) {
        this.courseSeriesRemark = courseSeriesRemark;
    }

    public Integer getGardenId() {
        return gardenId;
    }

    public void setGardenId(Integer gardenId) {
        this.gardenId = gardenId;
    }
}
