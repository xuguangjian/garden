package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

public class GardenStaffPositionVm {
	
	@NotBlank
	private String positionName;

    private String positionDesc;

    private Integer positionParent;

    private Integer positionSort;

    private Integer gardenId;

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public String getPositionDesc() {
		return positionDesc;
	}

	public void setPositionDesc(String positionDesc) {
		this.positionDesc = positionDesc;
	}

	public Integer getPositionParent() {
		return positionParent;
	}

	public void setPositionParent(Integer positionParent) {
		this.positionParent = positionParent;
	}

	public Integer getPositionSort() {
		return positionSort;
	}

	public void setPositionSort(Integer positionSort) {
		this.positionSort = positionSort;
	}

	public Integer getGardenId() {
		return gardenId;
	}

	public void setGardenId(Integer gardenId) {
		this.gardenId = gardenId;
	}
}
