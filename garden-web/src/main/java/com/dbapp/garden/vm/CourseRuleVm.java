package com.dbapp.garden.vm;

/**
 * @author by gangzi on 2017/11/13.
 */
public class CourseRuleVm {
    private Integer arriveLate;

    private Integer absentCourse;

    private Integer deductCourse;

    public Integer getArriveLate() {
        return arriveLate;
    }

    public void setArriveLate(Integer arriveLate) {
        this.arriveLate = arriveLate;
    }

    public Integer getAbsentCourse() {
        return absentCourse;
    }

    public void setAbsentCourse(Integer absentCourse) {
        this.absentCourse = absentCourse;
    }

    public Integer getDeductCourse() {
        return deductCourse;
    }

    public void setDeductCourse(Integer deductCourse) {
        this.deductCourse = deductCourse;
    }
}
