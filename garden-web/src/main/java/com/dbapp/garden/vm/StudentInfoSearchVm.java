package com.dbapp.garden.vm;

/**
 * @author by gangzi on 2017/11/17.
 * 学员核心资料列表、我的学员列表搜索Vm
 */
public class StudentInfoSearchVm {
    private String stuName;
    private Integer stuSex;
    private Integer stuStatus;
    private Integer gardenId;

    private String birthFrom;
    private String birthTo;

    public String getBirthFrom() {
        return birthFrom;
    }

    public void setBirthFrom(String birthFrom) {
        this.birthFrom = birthFrom;
    }

    public String getBirthTo() {
        return birthTo;
    }

    public void setBirthTo(String birthTo) {
        this.birthTo = birthTo;
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public Integer getStuSex() {
        return stuSex;
    }

    public void setStuSex(Integer stuSex) {
        this.stuSex = stuSex;
    }

    public Integer getStuStatus() {
        return stuStatus;
    }

    public void setStuStatus(Integer stuStatus) {
        this.stuStatus = stuStatus;
    }

    public Integer getGardenId() {
        return gardenId;
    }

    public void setGardenId(Integer gardenId) {
        this.gardenId = gardenId;
    }
}
