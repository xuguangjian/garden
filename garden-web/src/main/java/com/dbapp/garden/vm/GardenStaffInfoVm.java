package com.dbapp.garden.vm;

import java.util.Date;

import org.hibernate.validator.constraints.NotBlank;

public class GardenStaffInfoVm {
	 @NotBlank
	private String staffRealName;

	 @NotBlank
    private String staffPhone;

    private Integer staffDepartment;

    private Integer staffPosition;

    @NotBlank
    private String staffEmail;

    private Integer staffState;

    private Date staffJoinTime;

    private String staffPermission;

    private String staffQq;

    private Integer gardenId;

    private Integer isMarketPerson;

    private Integer isTeacher;

    private Integer isAdviser;

    private String hasStudent;

    private String staffRemark;
    
    @NotBlank
    private String userName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getStaffRealName() {
		return staffRealName;
	}

	public void setStaffRealName(String staffRealName) {
		this.staffRealName = staffRealName;
	}

	public String getStaffPhone() {
		return staffPhone;
	}

	public void setStaffPhone(String staffPhone) {
		this.staffPhone = staffPhone;
	}

	public Integer getStaffDepartment() {
		return staffDepartment;
	}

	public void setStaffDepartment(Integer staffDepartment) {
		this.staffDepartment = staffDepartment;
	}

	public Integer getStaffPosition() {
		return staffPosition;
	}

	public void setStaffPosition(Integer staffPosition) {
		this.staffPosition = staffPosition;
	}

	public String getStaffEmail() {
		return staffEmail;
	}

	public void setStaffEmail(String staffEmail) {
		this.staffEmail = staffEmail;
	}

	public Integer getStaffState() {
		return staffState;
	}

	public void setStaffState(Integer staffState) {
		this.staffState = staffState;
	}

	public Date getStaffJoinTime() {
		return staffJoinTime;
	}

	public void setStaffJoinTime(Date staffJoinTime) {
		this.staffJoinTime = staffJoinTime;
	}

	public String getStaffPermission() {
		return staffPermission;
	}

	public void setStaffPermission(String staffPermission) {
		this.staffPermission = staffPermission;
	}

	public String getStaffQq() {
		return staffQq;
	}

	public void setStaffQq(String staffQq) {
		this.staffQq = staffQq;
	}

	public Integer getGardenId() {
		return gardenId;
	}

	public void setGardenId(Integer gardenId) {
		this.gardenId = gardenId;
	}

	public Integer getIsMarketPerson() {
		return isMarketPerson;
	}

	public void setIsMarketPerson(Integer isMarketPerson) {
		this.isMarketPerson = isMarketPerson;
	}

	public Integer getIsTeacher() {
		return isTeacher;
	}

	public void setIsTeacher(Integer isTeacher) {
		this.isTeacher = isTeacher;
	}

	public Integer getIsAdviser() {
		return isAdviser;
	}

	public void setIsAdviser(Integer isAdviser) {
		this.isAdviser = isAdviser;
	}

	public String getHasStudent() {
		return hasStudent;
	}

	public void setHasStudent(String hasStudent) {
		this.hasStudent = hasStudent;
	}

	public String getStaffRemark() {
		return staffRemark;
	}

	public void setStaffRemark(String staffRemark) {
		this.staffRemark = staffRemark;
	}
    

}
