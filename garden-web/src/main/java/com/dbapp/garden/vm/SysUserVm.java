package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

public class SysUserVm {
	@NotBlank
	private String userName;

	@NotBlank
	private String userPassword;

	@NotBlank
	private String userPhone;

	@NotBlank
	private String userRealName;

	private Integer userRole;

	public Integer getUserRole() {
		return userRole;
	}

	public void setUserRole(Integer userRole) {
		this.userRole = userRole;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getUserRealName() {
		return userRealName;
	}

	public void setUserRealName(String userRealName) {
		this.userRealName = userRealName;
	}
}
