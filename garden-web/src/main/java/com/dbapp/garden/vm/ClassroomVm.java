package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author by gangzi on 2017/11/12.
 */
public class ClassroomVm {


    @NotBlank(message = "教室名称不能为空")
    private String classroomName;

    private Integer classroomSort;

    private String classroomRemark;

    private Integer gardenId;

    public String getClassroomName() {
        return classroomName;
    }

    public void setClassroomName(String classroomName) {
        this.classroomName = classroomName;
    }

    public Integer getClassroomSort() {
        return classroomSort;
    }

    public void setClassroomSort(Integer classroomSort) {
        this.classroomSort = classroomSort;
    }

    public String getClassroomRemark() {
        return classroomRemark;
    }

    public void setClassroomRemark(String classroomRemark) {
        this.classroomRemark = classroomRemark;
    }

    public Integer getGardenId() {
        return gardenId;
    }

    public void setGardenId(Integer gardenId) {
        this.gardenId = gardenId;
    }
}
