package com.dbapp.garden.vm;

import org.hibernate.validator.constraints.NotBlank;

public class StudentImportanceVm {
	
	@NotBlank
	 private String importanceParam;

	    private String importanceDesc;

		public String getImportanceParam() {
			return importanceParam;
		}

		public void setImportanceParam(String importanceParam) {
			this.importanceParam = importanceParam;
		}

		public String getImportanceDesc() {
			return importanceDesc;
		}

		public void setImportanceDesc(String importanceDesc) {
			this.importanceDesc = importanceDesc;
		}
	    
	    

}
