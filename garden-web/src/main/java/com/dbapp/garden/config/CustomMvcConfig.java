package com.dbapp.garden.config;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;

@Configuration
// @EnableWebMvc // 1
// @EnableScheduling
// @ComponentScan("com.dbapp.economy") //配置拦截器和字符转换器时候，用于自动扫描注册组件
public class CustomMvcConfig extends WebMvcConfigurerAdapter implements ApplicationContextAware {// 2

//	private ApplicationContext applicationContext;

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		return bCryptPasswordEncoder;
	}
	// @Bean
	// public InternalResourceViewResolver viewResolver() {
	// InternalResourceViewResolver viewResolver = new
	// InternalResourceViewResolver();
	// viewResolver.setPrefix("/WEB-INF/classes/views/");
	// viewResolver.setSuffix(".jsp");
	// viewResolver.setViewClass(JstlView.class);
	// return viewResolver;
	// }

//	@Override
//	public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
//		this.applicationContext = applicationContext;
//	}


	// @Bean
	// // 1
	// public DemoInterceptor demoInterceptor() {
	// return new DemoInterceptor();
	// }

	// @Override
	// public void addInterceptors(InterceptorRegistry registry) {// 2
	// registry.addInterceptor(demoInterceptor());
	// }
	// 这种便于统一管理，这种方式更好
	// @Override
	// public void addViewControllers(ViewControllerRegistry registry) {
	// registry.addViewController("/home").setViewName("/home");
	// registry.addViewController("/index").setViewName("/index");
	//// registry.addViewController("/toUpload").setViewName("/upload");
	//// registry.addViewController("/converter").setViewName("/converter");
	//// registry.addViewController("/sse").setViewName("/sse");
	//// registry.addViewController("/async").setViewName("/async");
	// }

	// @Override
	// public void configurePathMatch(PathMatchConfigurer configurer) {
	// configurer.setUseSuffixPatternMatch(false);
	// }

	// @Bean
	// public MultipartResolver multipartResolver() {
	// CommonsMultipartResolver multipartResolver = new
	// CommonsMultipartResolver();
	// multipartResolver.setMaxUploadSize(1000000);
	// return multipartResolver;
	// }

	// @Override
	// public void extendMessageConverters(List<HttpMessageConverter<?>>
	// converters) {
	// converters.add(converter());
	// }
	//
	// @Bean
	// public MyMessageConverter converter(){
	// return new MyMessageConverter();
	// }
	/**
	 * Message externalization/internationalization
	 */
	// @Bean
	// public ResourceBundleMessageSource messageSource() {
	// ResourceBundleMessageSource resourceBundleMessageSource = new
	// ResourceBundleMessageSource();
	// resourceBundleMessageSource.setBasename("Messages");
	// return resourceBundleMessageSource;
	// }
	@Bean
	public SpringSecurityDialect springSecurityDialect() {
		return new SpringSecurityDialect();
	}

	@Override
	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		// TODO Auto-generated method stub
		
	}
	
//	@Bean
//	public SpringStandardDialect springStandardDialect() {
//		return new SpringStandardDialect();
//	}
	/* **************************************************************** */
	/* THYMELEAF-SPECIFIC ARTIFACTS */
	/* TemplateResolver <- TemplateEngine <- ViewResolver */
	/* **************************************************************** */

	// @Bean
	// public SpringResourceTemplateResolver templateResolver(){
	// SpringResourceTemplateResolver templateResolver = new
	// SpringResourceTemplateResolver();
	// templateResolver.setApplicationContext(this.applicationContext);
	// templateResolver.setPrefix("/templates/");
	// templateResolver.setSuffix(".html");
	// templateResolver.setTemplateMode("LEGACYHTML5");
	//// templateResolver.setTemplateMode(TemplateMode.HTML);
	// // Template cache is true by default. Set to false if you want
	// // templates to be automatically updated when modified.
	// templateResolver.setCacheable(true);
	// return templateResolver;
	// }

	// @Bean
	// public SpringTemplateEngine templateEngine(){
	// SpringTemplateEngine templateEngine = new SpringTemplateEngine();
	//// ((Object) templateEngine).setEnableSpringELCompiler(true); // Compiled
	// SpringEL should speed up executions
	// templateEngine.setTemplateResolver(templateResolver());
	// templateEngine.addDialect(new SpringSecurityDialect());
	// return templateEngine;
	// }

	// @Bean
	// public ThymeleafViewResolver viewResolver(){
	// ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
	// viewResolver.setTemplateEngine(templateEngine());
	// return viewResolver;
	// }

	/* ******************************************************************* */
	/* Defines callback methods to customize the Java-based configuration */
	/* for Spring MVC enabled via {@code @EnableWebMvc} */
	/* ******************************************************************* */

	/**
	 * Dispatcher configuration for serving static resources
	 */
	// @Override
	// public void addResourceHandlers(final ResourceHandlerRegistry registry) {
	// super.addResourceHandlers(registry);
	// registry.addResourceHandler("/images/**").addResourceLocations("/images/");
	// registry.addResourceHandler("/css/**").addResourceLocations("/css/");
	// registry.addResourceHandler("/js/**").addResourceLocations("/js/");
	// }

}
