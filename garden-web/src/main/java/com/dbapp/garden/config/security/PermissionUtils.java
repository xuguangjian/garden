package com.dbapp.garden.config.security;

/**
 * @author by gangzi on 2017/11/21.
 */
public class PermissionUtils {

    private static final String GOD="GOD";
    private static final String ADMIN="ADMIN";

    /**
     * 判断当前登录系统的用户是不是GOD，注意整个系统只有一个GOD用户，只有GOD用户才拥有添加园所的功能
     * 当且仅当 角色码(roleCode)为GOD,gardenId为0时，才是GOD用户
     * @return
     */
    public static boolean isGod(PermissionUser permissionUser){
        String roleCode=permissionUser.getPermissionCode();
        Integer gardenId=permissionUser.getGardenId();
        if (GOD.equals(roleCode)&&gardenId!=null&&gardenId==0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 判断是不是各个园所的管理员
     * 当且仅当 角色码(roleCode)为ADMIn,gardenId大于0时，才是GOD用户
     * @return
     */
    public static boolean idAdmin(PermissionUser permissionUser){
        String roleCode=permissionUser.getPermissionCode();
        Integer gardenId=permissionUser.getGardenId();
        if (ADMIN.equals(roleCode)&&gardenId!=null&&gardenId>0){
            return true;
        }else{
            return false;
        }
    }
}
