package com.dbapp.garden.config.security;

import java.io.Serializable;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 * @author gangzi
 */
public class PermissionUser extends User implements Serializable {
    /**
	 * 自定义权限用户类
	 */
	private static final long serialVersionUID = -453688265265982903L;
    /**
     * 当前用户的ID
     */
	private Integer uid;

    /**
     * 当前用户的权限码，做数据权限校验用
     */
	private String permissionCode;

    /**
     * 当前用户所属园所ID
     */
	private Integer gardenId;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getPermissionCode() {
        return permissionCode;
    }

    public void setPermissionCode(String permissionCode) {
        this.permissionCode = permissionCode;
    }

    public Integer getGardenId() {
        return gardenId;
    }

    public void setGardenId(Integer gardenId) {
        this.gardenId = gardenId;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public PermissionUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public PermissionUser(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }
}

