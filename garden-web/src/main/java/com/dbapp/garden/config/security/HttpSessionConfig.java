package com.dbapp.garden.config.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@Configuration
@EnableRedisHttpSession
public class HttpSessionConfig {
	@Value("${redis.host}")
	private String redisHost;
	
	@Value("${redis.port}")
	private int redisPort;
	
	AuthenticationManager am;
	
	AuthenticationProvider ap;
	DaoAuthenticationProvider dao;
	UserDetailsService uds;
	UserDetails ud;
	@Bean
    public LettuceConnectionFactory connectionFactory() {
		LettuceConnectionFactory connectionFactory=new LettuceConnectionFactory(redisHost,redisPort);
		
            return connectionFactory; 
    }
	
}
