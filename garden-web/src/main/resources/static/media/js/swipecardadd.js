var SwipeCardAdd = function () {
    var handleTimePickers = function () {

        if (jQuery().timepicker) {

            $('.timepicker-24').timepicker({
                minuteStep: 1,
                showSeconds: false,
                showMeridian: false
            });
        }
    }

    return {
        init: function () {
            handleTimePickers();

        }
    };

}();