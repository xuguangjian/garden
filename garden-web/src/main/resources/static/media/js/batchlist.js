var BatchList = function () {
    let
        AUTHORITY = $("#authority").attr("content");
    var initTable = function () {
        // 渲染列表
        var oTable = $("#table_batch")
            .DataTable(
                {
                    "bAutoWidth": true,
                    // "bProcessing" : true,
                    "bFilter": true,
                    // "bStateSave" : true,
                    // "sPaginationType" : "full_numbers",
                    "aaSorting": [[0, 'desc']],
                    bSort: false,
                    "oLanguage": { // 汉化
                        "sProcessing": "正在加载数据...",
                        "sLengthMenu": "显示_MENU_条",
                        "sZeroRecords": "没有您要搜索的内容",
                        "sInfo": "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
                        "sInfoEmpty": "记录数为0",
                        "sInfoFiltered": "(全部记录数 _MAX_  条)",
                        "sInfoPostFix": "",
                        "sSearch": "搜索",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "首页",
                            "sPrevious": " 上一页 ",
                            "sNext": " 下一页 ",
                            "sLast": " 末页 "
                        }
                    },
                    "aoColumns": [
                        {
                            "mData": "batchName",

                            "mRender": function (value, method, row) {
                                return '<input type="text" class="m-wrap small" value="'
                                    + value + '" id="stuName">'
                            }
                        },
                        {
                            "mData":"batchSource"
                        },

                        {
                            "mData": "batchCount"
                        },
                        {
                            "mData": "batchSuccess"

                        },
                        {
                            "mData": "repeatFail"
                        },
                        {
                            "mData": "otherFail"
                        },
                        {
                            "mData": "createTime"
                        },
                        {
                            "mData": "batchSuccess"
                        },

                        {
                            "mData": function (source, type, val) {
                                return source;
                            },
                            "mRender": function (value, method, row) {
                                var btn = '';


                                let
                                    failed = '<a class="stu_fail" style="margin-left:2px;text-decoration : none" data-id="'
                                        + value.id
                                        + '" >[失败名单]</a>';
                                let
                                    success = '<a class="stu_success" style="margin-left:5px;text-decoration : none" data-id="'
                                        + value.id
                                        + '" >[成功名单]</a>';
                                let
                                    del = '<a class="batch_delete" style="margin-left:5px;text-decoration : none" data-id="'
                                        + value.id
                                        + '">[删除]</a>';


                               btn+=failed;
                               btn+=success;
                                if (AUTHORITY
                                        .includes("PERMISSION_STUDENT_INFO_DELETE")) {
                                    btn += del;
                                }
                                return btn;
                            }
                        }],
                    bServerSide: true,
                    sAjaxSource: "/batchlist",
                    fnServerData: function (source, data, fnCallBack) {
                        // alert("data:"+JSON.stringify(data));
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: source,
                            data: "param=" + JSON.stringify(data),
                            // beforeSend:beginWait(),
                            success: function (d) {
                                if (d.retCode != "1")
                                // bootbox.alert(d.retMsg);
                                    alert(d.retMsg);
                                else {
                                    // $("#total").html(d.retObj.iTotalRecords);
                                    fnCallBack(d.retObj);
                                }
                            },
                            failure: function (d) {
                                // bootbox.alert("服务器无响应");
                                alert("服务器无响应");
                            },
                            // complete:clearWait()
                        });
                    }
                });

        jQuery('#table_batch_wrapper .dataTables_filter input').addClass(
            "m-wrap small"); // modify table search input
        jQuery('#table_batch_wrapper .dataTables_length select').addClass(
            "m-wrap small"); // modify table per page dropdown
        jQuery('#table_batch_wrapper .dataTables_length select').select2(); // initialzie
    };

    var event = function () {

        // 1.删除批次信息
        $("#table_batch").on("click", "td a.batch_delete", function () {
            var roleId = $(this).attr("data-id");
            layer.confirm('您确定要删除吗，删除后不可恢复？', {
                title: '删除导入批次',
                btn: ['是', '否'],
                btnAlign: 'c'
                // 按钮
            }, function () {

                deleteImportBatch(roleId);
            }, function () {
                layer.msg('取消删除！', {
                    icon: 2
                });
            });
        });
        // 2.查看导入成功名单
        $("#table_batch").on("click", "td a.stu_success", function () {
            var roleId = $(this).attr("data-id");
            layer.open({
                title: '查看成功名单',
                type: 2,
                area: ["80%", "70%"],
                scrollbar: false, // 父页面 滚动条 禁止
                content: "/studentimportsuccess/" + roleId,

            });

        });
        //3.查看失败名单
        $("#table_batch").on("click", "td a.stu_fail", function () {
            var roleId = $(this).attr("data-id");
            layer.open({
                title: '查看失败名单',
                type: 2,
                //skin: 'layui-layer-lan', // 加上边框
                // area: ['620px', 'auto'], //宽高
                area: ["80%", "70%"],
                scrollbar: false, // 父页面 滚动条 禁止
                content: "/studentimportfail/" + roleId,

            });

        });

    };

    function deleteImportBatch(data) {
        $
            .ajax({
                type: "DELETE",
                dataType: "json",
                url: "/importbatch/"+data,
                contentType: "application/json;charset=utf-8",
                data: data
            })
            .done(
                function (data) {
                    if (data && data.retCode == 1) {
                        layer.msg(data.retMsg, {
                            icon: 1
                        });
                        setTimeout(
                            'layer.closeAll();$("#table_batch").DataTable().ajax.reload();',
                            800)

                    } else {
                        var s = layer.msg("对不起，删除失败！", {
                            icon: 2
                        });
                        layer.close(s);
                    }

                }).fail(function (d) {
            layer.msg("服务器无响应", {
                icon: 2
            });

        });
    }

    return {
        init: function () {

            initTable();
            event();
        }
    };

}();