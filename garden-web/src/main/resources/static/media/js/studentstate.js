var StudentState = function() {
	let
	AUTHORITY = $("#authority").attr("content");
	var initTable = function() {
		// 渲染列表
		var oTable = $("#state_table")
				.DataTable(
						{
							"bAutoWidth" : true,
							// "bProcessing" : true,
							"bFilter" : true,
							// "bStateSave" : true,
							// "sPaginationType" : "full_numbers",
							"aaSorting" : [ [ 0, 'desc' ] ],
							bSort : false,
							"oLanguage" : { // 汉化
								"sProcessing" : "正在加载数据...",
								"sLengthMenu" : "显示_MENU_条",
								"sZeroRecords" : "没有您要搜索的内容",
								"sInfo" : "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
								"sInfoEmpty" : "记录数为0",
								"sInfoFiltered" : "(全部记录数 _MAX_  条)",
								"sInfoPostFix" : "",
								"sSearch" : "搜索",
								"sUrl" : "",
								"oPaginate" : {
									"sFirst" : "首页",
									"sPrevious" : " 上一页 ",
									"sNext" : " 下一页 ",
									"sLast" : " 末页 "
								}
							},
							"aoColumns" : [
									{
										"mData" : "stateParam"
									},
									{
										"mData" : "stateProp"
									},
									{
										"mData" : "stateSort"
									},
									{
										"mData" : "stateDesc"
									},
									{
										"mData" : "createTime"
									},
									{
										"mData" : "updateTime"
									},
									{
										"mData" : function(source, type, val) {
											return source;
										},
										"mRender" : function(value, method, row) {
											var btn = '';

											let
											edit = '<a class="btn purple stu_state_edit" style="margin-left:10px;" data-id="'
													+ value.id
													+ '" data-state_param="'
													+ value.stateParam
													+ '" data-state_desc="'
													+ value.stateDesc
													+ '" data-state_sort="'+value.stateSort+'" data-state_prop="'+value.stateProp+'"><i class="icon-edit"></i> 编辑</a>';
											let
											del = '<a class="btn red stu_state_delete" style="margin-left:10px;" data-id="'
													+ value.id
													+ '"><i class="icon-trash"></i> 删除</a>';

											if (AUTHORITY
													.includes("PERMISSION_ROLE_EDIT")) {
												btn += edit;
											}
											if (AUTHORITY
													.includes("PERMISSION_ROLE_DELETE")) {
												btn += del;
											}
											return btn;
										}
									} ],
							bServerSide : true,
							sAjaxSource : "/studentstatelist",
							fnServerData : function(source, data, fnCallBack) {
								// alert("data:"+JSON.stringify(data));
								$.ajax({
									type : "POST",
									dataType : "json",
									url : source,
									data : "param=" + JSON.stringify(data),
									// beforeSend:beginWait(),
									success : function(d) {
										if (d.retCode != "1")
											// bootbox.alert(d.retMsg);
											alert(d.retMsg);
										else {
											// $("#total").html(d.retObj.iTotalRecords);
											fnCallBack(d.retObj);
										}
									},
									failure : function(d) {
										// bootbox.alert("服务器无响应");
										alert("服务器无响应");
									},
								// complete:clearWait()
								});
							}
						});

		jQuery('#state_table_wrapper .dataTables_filter input').addClass(
				"m-wrap small");
		jQuery('#state_table_wrapper .dataTables_length select').addClass(
				"m-wrap small"); 
		jQuery('#state_table_wrapper .dataTables_length select').select2(); 
																		
	};

	var event = function() {
		// 1.删除
		$("#state_table").on("click", "td a.stu_state_delete", function() {
			var roleId = $(this).attr("data-id");
			layer.confirm('您确定要删除吗，删除后不可恢复？', {
				title : '删除学员状态',
				btn : [ '是', '否' ],
				btnAlign : 'c'
			// 按钮
			}, function() {
				deleteStuState(roleId);
				// layer.alert("删除了，ID为："+roleId);
			}, function() {
				layer.msg('取消删除！', {
					icon : 2
				});
			});
		});
		// 2.编辑
		$("#state_table").on(
				"click",
				"td a.stu_state_edit",
				function() {

					// 将数据回显在弹框中
					var that = $(this);
					var roleId = that.attr("data-id");
					var state_param = that.attr("data-state_param");
					var state_desc = that.attr("data-state_desc");
					var state_prop = that.attr("data-state_prop");
					var state_sort = that.attr("data-state_sort");

					var $sourceName = $("#stateParam");
					var $stateDesc = $("#stateDesc");
					
					var $stateSort = $("#stateSort");

					$sourceName.val(state_param);
					$stateDesc.val(state_desc);
					$stateSort.val(state_sort);
					
					$("input[name='stateProp'][value='"+state_prop+"']").prop("checked",true);
					layer.open({
						title : '编辑学员状态',
						btn : [ '是', '否' ],
						type : 1,
						// skin: 'layui-layer-rim', //加上边框
						// area: ['620px', 'auto'], //宽高
						area : "35%",
						content : $('#state_edit_alert'),
						yes : function(index, layerno) {
					
							if (!$sourceName.val()) {
								layer.msg("亲，学员状态参数不能为空！", {
									icon : 2
								});
								return;
							}
					
							var form_data = $("#state_edit_form").serializeArray();
						    var post_data = {};
						    $.map(form_data, function(n, i){
						    	post_data[n['name']] = n['value'];
						    });	
							submitForm(JSON.stringify(post_data), "/studentstateedit/"+roleId);
						},
						btn2 : function(index, layerno) {
							layer.msg("取消了", {
								icon : 2
							});
						}
					});

				});

		// 3.添加
		$("#btn_stu_state_add").click(function() {
//			alert($("#stu_state_form").serialize());
			var state_param = $("#state_param").val();
			if (!state_param) {
				layer.msg("亲，学员状态参数不能为空！", {
					icon : 2
				});
				return;
			}
	
			var form_data = $("#stu_state_form").serializeArray();
		    var post_data = {};
		    $.map(form_data, function(n, i){
		    	post_data[n['name']] = n['value'];
		    });	
			submitForm(JSON.stringify(post_data), "/studentstateadd");
		});
	};
	function deleteStuState(roleId) {
		$.ajax({
			type : "DELETE",
			dataType : "json",
			url : "/studentstatedelete/" + roleId,
		// contentType: "application/json;charset=utf-8",
		// data : "roleId="+roleId
		// ,beforeSend : beginWait("研判报告导出中，请稍稍后。。。")
		// ,complete : clearWait()
		}).done(function(data) {
			if (data && data.retCode == 1) {
				layer.msg(data.retMsg, {
					icon : 1
				});
//				setTimeout("window.location.reload();", 800)
				setTimeout('$("#state_table").DataTable().ajax.reload();', 800)

			} else {
				var s = layer.msg("对不起，删除失败！", {
					icon : 2
				});
				layer.close(s);
			}

		}).fail(function(d) {
			layer.msg("服务器无响应", {
				icon : 2
			});

		});
	}
	/**
	 * 提交表单
	 * 
	 * @param data
	 */
	function submitForm(data, url) {
		$.ajax({
			type : "POST",
			dataType : "json",
			url : url,
			contentType : "application/json;charset=utf-8",
			data : data
		// ,beforeSend : beginWait("研判报告导出中，请稍稍后。。。")
		// ,complete : clearWait()
		}).done(function(data) {
			if (data && data.retCode == 1) {
				var l = layer.msg(data.retMsg, {
					icon : 1
				});
				setTimeout('layer.closeAll();$("#state_table").DataTable().ajax.reload();', 1000)

			} else {
				var s = layer.msg("对不起，添加失败！", {
					icon : 3
				});
				layer.close(s);
			}

		}).fail(function(d) {
			layer.msg("服务器无响应");

		});
	}

	return {
		init : function() {
			initTable();
			event();
		}
	};

}();