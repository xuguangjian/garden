var CourseRule = function() {



	var getCourseRuleData = function(id) {

		$.ajax({
			type : "GET",
			dataType : "json",
			url : "/courserule"
		}).done(function(data) {
			if (data.retObj==="error"){
                layer.msg(data.retMsg,{icon:2});
			}else{
				var rule=data.retObj;
				$("#course_rule_id").val(rule.id);
				$("#arriveLate").val(rule.arriveLate);
				$("#absentCourse").val(rule.absentCourse);
				$("#deductCourse").val(rule.deductCourse);
			}


		}).fail(function(d) {
			layer.msg("服务器无响应");
		});

	};

	var event=function () {
		$("#btn_course_rule_update").click(function (e) {
			e.preventDefault();
			var id=$("#course_rule_id").val();
            var form_data = $("#course_rule_form").serializeArray();
            var post_data = {};
            $.map(form_data, function (n, i) {

                var key = n['name'];
                var value = n['value'];
                if (key && post_data.hasOwnProperty(key)) {
                    let originValue = post_data[key];
                    let newValue = originValue + "," + value;
                    post_data[key] = newValue;
                } else {
                    post_data[key] = value;
                }

            });
             // alert(JSON.stringify(post_data));
             submitForm(JSON.stringify(post_data), "PUT", "/courserule/"+id);

        });


        /**
         * 提交表单
         *
         * @param data
         */
        function submitForm(data, type, url) {
            $.ajax({
                type: type,
                dataType: "json",
                url: url,
                contentType: "application/json;charset=utf-8",
                data: data
            }).done(function (data) {
                if (data && data.retCode == 1) {
                    var l = layer.msg(data.retMsg, {
                        icon: 1
                    });
                    setTimeout('layer.closeAll();$("#tab_3_3").load(); ', 800)

                } else {
                    var s = layer.msg("对不起，添加失败！", {
                        icon: 3
                    });
                    layer.close(s);
                }

            }).fail(function (d) {
                layer.msg("服务器无响应");

            });
        }
    };

	return {

		init : function() {
            getCourseRuleData();
            event();
		}

	};

}();