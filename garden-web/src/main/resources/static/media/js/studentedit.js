var StudentEdit = function () {

	//日期选择框
    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl : App.isRTL(),
                language: 'zh-CN',
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });
        }
    }

    //下拉框
    var handleActionSelect = function () {        
    	$(".dropdown-menu li a").click(function(){
    		var $btn=$(this).parents("ul").siblings('button.btn');
    		var $hidden=$(this).parent().siblings("input.dropdown_select");
    		$hidden.val($(this).attr("data-value"));
    		$btn.text($(this).text());
    	});
    		
    	
    }
    //所属区域联动
    var areaCascade=function(){
    	$("#stu_area").change(function(){
    		var selectId=$(this).val();
    		if(selectId){
    			getChildArea(selectId);
    		}else{
    			//直接将二级区域置为“请选择”
    			addChildDataToSelect();
    		}
    		
    	});
    	//获取一级地区对应二级地区
    	function getChildArea(pId){
    		$.ajax({
    			type : "GET",
    			dataType : "json",
    			url : "/studentchildarea/" + pId
    		}).done(function(data) {
    			if (data && data.retCode == 1) {
    				addChildDataToSelect(data.retObj);
    			} else {
    				var s = layer.msg("对不起，二级地区获取失败！", {
    					icon : 2
    				});
    				layer.close(s);
    			}

    		}).fail(function(d) {
    			layer.msg("服务器无响应", {
    				icon : 2
    			});

    		});
    	}
    	//给二级区域绑定数据
    	function addChildDataToSelect(data){
    
    		if(data&&data.length>0){
    			var options="";
    			$.each(data, function(index, area) {
        			options+='<option value="'+area.id+'">'+area.areaName+'</option>'
        		});
    			var $stu_child_area=$("#stu_child_area");
        		$stu_child_area.html("");
        		$stu_child_area.html(options);
    		}	
    	}
    };

    return {
        init: function () {
        	 
        	 handleDatePickers();
        	 handleActionSelect();
        	 areaCascade();
        }

    };
    
    

}();