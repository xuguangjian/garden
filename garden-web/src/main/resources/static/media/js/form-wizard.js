var FormWizard = function () {


    return {
        //main function to initiate the module
        init: function () {
        	
            var form = $('#submit_form');
            var error = $('.alert-error', form);
            var success = $('.alert-success', form);
            
            form.validate({
            	debug:true,
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'validate-inline', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
               // onsubmit:true,
                //onfocusout:true,
                onfocusout: function(element) {$(element).valid()},
               // onkeyup:true,
                onkeyup: function(element) {$(element).valid()},
               // onclick:true,
                rules: {
                    roleName: {
                        //minlength: 5,
                        required: true,
                        remote:"/rolenamecheck"
                    },
                    password: {
                        //minlength: 5,
                        required: true
                    },
                    rpassword: {               
                        required: true,
                        equalTo: "#submit_form_password"
                    }       
                },
                messages: {
                	roleName: {
	                    required: "角色名不能为空.",
	                    remote:"角色名已存在，请更换！"
	                },
	                password: {
	                    required: "角色码不能为空."
	                }
	            },
                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    //App.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.help-inline').removeClass('ok'); // display OK icon
                    $(element)
                        .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label) {
                	label.addClass('valid ok') // mark the current input as valid and display OK icon
                		 .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                    alert("submitted");   
                    //form.submit();
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                }

            });          
        }
    };
}();