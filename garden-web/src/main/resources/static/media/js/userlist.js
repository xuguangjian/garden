var UserList = function() {

	let
	AUTHORITY = $("#authority").attr("content");

	var initTable = function() {
		// 渲染列表
		var oTable = $("#table")
				.DataTable(
						{
							"bAutoWidth" : true,
							// "bProcessing" : true,
							"bFilter" : true,
							// "bStateSave" : true,
							// "sPaginationType" : "full_numbers",
							"aaSorting" : [ [ 0, 'desc' ] ],
							bSort : false,
							"oLanguage" : { // 汉化
								"sProcessing" : "正在加载数据...",
								"sLengthMenu" : "显示_MENU_条",
								"sZeroRecords" : "没有您要搜索的内容",
								"sInfo" : "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
								"sInfoEmpty" : "记录数为0",
								"sInfoFiltered" : "(全部记录数 _MAX_  条)",
								"sInfoPostFix" : "",
								"sSearch" : "搜索",
								"sUrl" : "",
								"oPaginate" : {
									"sFirst" : "首页",
									"sPrevious" : " 上一页 ",
									"sNext" : " 下一页 ",
									"sLast" : " 末页 "
								}
							},
							"aoColumns" : [
									{
										"mData" : "userName",
									},
									{
										"mData" : "userRealName",
									},
									{
										"mData" : "userPhone",
									},
									{
										"mData" : "userRegisterTime",
									},
									{
										"mData" : function(source, type, val) {
											return source;
										},
										"mRender" : function(value, method, row) {
											var btn = '';
											// let view='<a href="/roleview"
											// class="btn green" ><i
											// class="icon-eye-open"></i>
											// 查看</a>';
											let
											edit = '<a class="btn purple user_edit" style="margin-left:10px;" data-id="'
													+ value.id
													+ '" ><i class="icon-edit"></i> 编辑</a>';
											let
											del = '<a class="btn black user_delete" style="margin-left:10px;" data-id="'
													+ value.id
													+ '"><i class="icon-trash"></i> 删除</a>';
											// if(AUTHORITY.includes("PERMISSION_ROLE_VIEW")){
											// btn+=view;
											// }
											if (AUTHORITY
													.includes("PERMISSION_USER_EDIT")) {
												btn += edit;
											}
											if (AUTHORITY
													.includes("PERMISSION_USER_DELETE")) {
												btn += del;
											}
											return btn;
										}
									} ],
							bServerSide : true,
							sAjaxSource : "/userlist",
							fnServerData : function(source, data, fnCallBack) {
								// alert("data:"+JSON.stringify(data));
								$.ajax({
									type : "POST",
									dataType : "json",
									url : source,
									data : "param=" + JSON.stringify(data),
									// beforeSend:beginWait(),
									success : function(d) {
										if (d.retCode != "1")
											// bootbox.alert(d.retMsg);
											alert(d.retMsg);
										else {
											// $("#total").html(d.retObj.iTotalRecords);
											fnCallBack(d.retObj);
										}
									},
									failure : function(d) {
										// bootbox.alert("服务器无响应");
										alert("服务器无响应");
									},
								// complete:clearWait()
								});
							}
						});

		jQuery('#table_wrapper .dataTables_filter input').addClass(
				"m-wrap small"); // modify
		// table
		// search
		// input
		jQuery('#table_wrapper .dataTables_length select').addClass(
				"m-wrap small"); // modify
		// table
		// per
		// page
		// dropdown
		jQuery('#table_wrapper .dataTables_length select').select2(); // initialzie
		// select2
		// dropdown

		$('#table_column_toggler input[type="checkbox"]').change(function() {

			var column = oTable.column($(this).attr('data-column'));

			// Toggle the visibility
			column.visible(!column.visible());
		});
	};

	var event = function() {
		// 1.删除
		$("#table").on("click", "td a.user_delete", function() {
			var roleId = $(this).attr("data-id");
			layer.confirm('您确定要删除吗，删除后不可恢复？', {
				title : '删除系统用户',
				btn : [ '是', '否' ],
				btnAlign : 'c'
			// 按钮
			}, function() {
				deleteUser(roleId);
			}, function() {
				layer.msg('取消删除！', {
					icon : 2
				});
			});
		});
		// 2.编辑
		$("#table")
				.on(
						"click",
						"td a.user_edit",
						function() {
							var roleId = $(this).attr("data-id");
							layer
									.open({
										title : '编辑系统用户',
										btn : [ '是', '否' ],
										type : 2,
										skin : 'layui-layer-rim', // 加上边框
										area : ["40%","70%"],
										content : "/useredit/"+roleId,
										yes : function(index, layerno) {
											var body = layer.getChildFrame('body', index);

											var $form = body.find("#add_user_form");

											if (!$form.valid()) {
												$form.find("label.error").hide();
												return;
											}

											 var form_data = $form.serializeArray();
											 var post_data = {};
											
											 $.map(form_data, function(n, i){
											 post_data[n['name']] = n['value'];
											 });
//												alert(JSON.stringify(post_data));			    
											 submitForm(JSON.stringify(post_data),"/useredit/"+roleId);
											
										},
										btn2 : function(index, layerno) {
											layer.msg("取消了", {
												icon : 2
											});
										}
									});

						});

		// 3.添加
		$("#btn_add").click(function() {
			// 页面层
			layer.open({
				title : '添加系统用户',
				btn : [ '是', '否' ],
				type : 2,
				skin : 'layui-layer-rim', // 加上边框
				// area: ['620px', 'auto'], //宽高
				area : [ "40%", "70%" ],
				content : "/useradd",
				scrollbar : false,
				yes : function(index, layerno) {
					var body = layer.getChildFrame('body', index);

					var $form = body.find("#add_user_form");

					if (!$form.valid()) {
						$form.find("label.error").hide();
						return;
					}

					 var form_data = $form.serializeArray();
					 var post_data = {};
					
					 $.map(form_data, function(n, i){
					 post_data[n['name']] = n['value'];
					 });
									    
					 submitForm(JSON.stringify(post_data),"/useradd");
				},
				btn2 : function(index, layerno) {
					layer.msg("取消了", {
						icon : 2
					});
				}
			});
		});
	};
	function deleteUser(roleId) {
		$.ajax({
			type : "DELETE",
			dataType : "json",
			url : "/userdelete/" + roleId,
		}).done(function(data) {
			if (data && data.retCode == 1) {
				layer.msg(data.retMsg, {
					icon : 1
				});
				setTimeout('$("#table").DataTable().ajax.reload();', 800)

			} else {
				var s = layer.msg("对不起，删除失败！", {
					icon : 2
				});
				layer.close(s);
			}

		}).fail(function(d) {
			layer.msg("服务器无响应", {
				icon : 2
			});

		});
	}
	/**
	 * 提交表单
	 * 
	 * @param data
	 */
	function submitForm(data,url) {
		$.ajax({
			type : "POST",
			dataType : "json",
			url : url,
			contentType : "application/json;charset=utf-8",
			data : data
		}).done(function(data) {
			if (data && data.retCode == 1) {
				var l = layer.msg(data.retMsg, {
					icon : 1
				});
				setTimeout('layer.closeAll();$("#table").DataTable().ajax.reload();', 800)

			} else {
				var s = layer.msg("对不起，添加失败！", {
					icon : 3
				});
				layer.close(s);
			}

		}).fail(function(d) {
			layer.msg("服务器无响应");

		});
	}

	return {
		init : function() {
			initTable();
			event();

		}
	};

}();