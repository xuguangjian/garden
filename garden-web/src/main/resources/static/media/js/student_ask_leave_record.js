var StudentAskLeaveRecord = function () {
    let
        AUTHORITY = $("#authority").attr("content");
    var initTable = function () {
        // 渲染列表
        var oTable = $("#leave_record_table")
            .DataTable(
                {
                    "bAutoWidth": true,
                    // "bProcessing" : true,
                    "bFilter": true,
                    // "bStateSave" : true,
                    // "sPaginationType" : "full_numbers",
                    "aaSorting": [[0, 'desc']],
                    bSort: false,
                    "oLanguage": { // 汉化
                        "sProcessing": "正在加载数据...",
                        "sLengthMenu": "显示_MENU_条",
                        "sZeroRecords": "没有您要搜索的内容",
                        "sInfo": "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
                        "sInfoEmpty": "记录数为0",
                        "sInfoFiltered": "(全部记录数 _MAX_  条)",
                        "sInfoPostFix": "",
                        "sSearch": "搜索",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "首页",
                            "sPrevious": " 上一页 ",
                            "sNext": " 下一页 ",
                            "sLast": " 末页 "
                        }
                    },
                    "aoColumns": [
                        {
                            "mData": "studentId"
                        },
                        {
                            "mData": "classId"
                        },

                        {
                            "mData": "classId"
                        },
                        {
                            "mData": "leaveReason"
                        },
                        {
                            "mData": "classId"
                        },
                        {
                            "mData": "classId"
                        },
                        {
                            "mData": "classId"
                        },
                        {
                            "mData": "classId"
                        },
                        {
                            "mData": "classId"
                        },
                        {
                            "mData": "classId"
                        }],
                    bServerSide: true,
                    sAjaxSource: "/leaverecord",
                    fnServerData: function (source, data, fnCallBack) {
                        // alert("data:"+JSON.stringify(data));
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: source,
                            data: "param=" + JSON.stringify(data),
                            // beforeSend:beginWait(),
                            success: function (d) {
                                if (d.retCode != "1")
                                    alert(d.retMsg);
                                else {
                                    fnCallBack(d.retObj);
                                }
                            },
                            failure: function (d) {

                                alert("服务器无响应");
                            },
                        });
                    }
                });

        jQuery('#leave_record_table_wrapper .dataTables_filter input').addClass(
            "m-wrap small");
        jQuery('#leave_record_table_wrapper .dataTables_length select').addClass(
            "m-wrap small");
        jQuery('#leave_record_table_wrapper .dataTables_length select').select2();

    };

    // var event = function () {
    //
    // };
    //

    return {
        init: function () {
            initTable();
            // event();
        }
    };

}();