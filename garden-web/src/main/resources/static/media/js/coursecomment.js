var CourseComment = function () {
    let
        AUTHORITY = $("#authority").attr("content");
    var initTable = function () {
        // 渲染列表
        var oTable = $("#comment_table")
            .DataTable(
                {
                    "bAutoWidth": true,
                    // "bProcessing" : true,
                    "bFilter": true,
                    // "bStateSave" : true,
                    // "sPaginationType" : "full_numbers",
                    "aaSorting": [[0, 'desc']],
                    bSort: false,
                    "oLanguage": { // 汉化
                        "sProcessing": "正在加载数据...",
                        "sLengthMenu": "显示_MENU_条",
                        "sZeroRecords": "没有您要搜索的内容",
                        "sInfo": "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
                        "sInfoEmpty": "记录数为0",
                        "sInfoFiltered": "(全部记录数 _MAX_  条)",
                        "sInfoPostFix": "",
                        "sSearch": "搜索",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "首页",
                            "sPrevious": " 上一页 ",
                            "sNext": " 下一页 ",
                            "sLast": " 末页 "
                        }
                    },
                    "aoColumns": [
                        {
                            "mData": "commentName"
                        },
                        {
                            "mData": "commentScore"
                        },
                        {
                            "mData": "commentSort"
                        },
                        {
                            "mData": function (source, type, val) {
                                return source;
                            },
                            "mRender": function (value, method, row) {
                                var btn = '';

                                let
                                    edit = '<a class="btn purple comment_edit" style="margin-left:10px;" data-id="'
                                        + value.id
                                        + '" data-comment_name="'
                                        + value.commentName
                                        + '" data-comment_score="'
                                        + value.commentScore
                                        + '" data-comment_sort="' + value.commentSort + '"><i class="icon-edit"></i> 编辑</a>';
                                let
                                    del = '<a class="btn red comment_delete" style="margin-left:10px;" data-id="'
                                        + value.id
                                        + '"><i class="icon-trash"></i> 删除</a>';

                                if (AUTHORITY
                                        .includes("PERMISSION_SERIES_EDIT")) {
                                    btn += edit;
                                }
                                if (AUTHORITY
                                        .includes("PERMISSION_SERIES_DELETE")) {
                                    btn += del;
                                }
                                return btn;
                            }
                        }],
                    bServerSide: true,
                    sAjaxSource: "/coursecommentlist",
                    fnServerData: function (source, data, fnCallBack) {
                        // alert("data:"+JSON.stringify(data));
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: source,
                            data: "param=" + JSON.stringify(data),
                            // beforeSend:beginWait(),
                            success: function (d) {
                                if (d.retCode != "1")
                                    alert(d.retMsg);
                                else {
                                    fnCallBack(d.retObj);
                                }
                            },
                            failure: function (d) {

                                alert("服务器无响应");
                            },
                        });
                    }
                });

        jQuery('#comment_table_wrapper .dataTables_filter input').addClass(
            "m-wrap small");
        jQuery('#comment_table_wrapper .dataTables_length select').addClass(
            "m-wrap small");
        jQuery('#comment_table_wrapper .dataTables_length select').select2();

    };

    var event = function () {
        // 1.删除
        $("#comment_table").on("click", "td a.comment_delete", function () {
            var roleId = $(this).attr("data-id");
            layer.confirm('您确定要删除吗，删除后不可恢复？', {
                title: '删除课评分值',
                btn: ['是', '否'],
                btnAlign: 'c'
                // 按钮
            }, function () {
                deleteCourseComment(roleId);
            }, function () {
            });
        });
        // 2.编辑
        $("#comment_table").on(
            "click",
            "td a.comment_edit",
            function () {

                var $that=$(this);
                var roleId = $that.attr("data-id");
                var name = $that.attr("data-comment_name");
                var score = $that.attr("data-comment_score");
                var sort = $that.attr("data-comment_sort");

                $("#e_commentName").val(name);
                $("#e_commentScore").val(score);
                $("#e_commentSort").val(sort);

                layer.open({
                    title: '编辑课评分值',
                    btn: ['是', '否'],
                    type: 1,
                    // skin: 'layui-layer-rim', //加上边框
                    // area: ['620px', 'auto'], //宽高
                    area: ["40%","40%"],
                    content: $("#comment_edit_alert"),
                    yes: function (index, layerno) {

                        var $form = $(layerno).find("#comment_edit_form");

                        if (!$("#e_commentName").val()) {
                            layer.msg("亲，课评名称不能为空！", {
                                icon: 2
                            });
                            return;
                        }
                        var form_data = $form.serializeArray();
                        var post_data = {};
                        $.map(form_data, function (n, i) {

                            var key = n['name'];
                            var value = n['value'];
                            if (key && post_data.hasOwnProperty(key)) {
                                let originValue = post_data[key];
                                let newValue = originValue + "," + value;
                                post_data[key] = newValue;
                            } else {
                                post_data[key] = value;
                            }

                        });
                        submitForm(JSON.stringify(post_data), "PUT", "/coursecomment/" + roleId);
                    },
                    btn2: function (index, layerno) {
                    }
                });

            });

        // 3.添加
        $("#btn_comment_add").click(function () {

            var commentName = $("#commentName").val();
            if (!commentName) {
                layer.msg("亲，课评分值名称不能为空！", {
                    icon: 2
                });
                return;
            }
            var form_data = $("#course_comment_form").serializeArray();
            var post_data = {};
            $.map(form_data, function (n, i) {

                var key = n['name'];
                var value = n['value'];
                if (key && post_data.hasOwnProperty(key)) {
                    let originValue = post_data[key];
                    let newValue = originValue + "," + value;
                    post_data[key] = newValue;
                } else {
                    post_data[key] = value;
                }

            });
            submitForm(JSON.stringify(post_data), "POST", "/coursecomment");
            //触发reset按钮，清空表单
            $("input[type=reset]").trigger("click");
        });
    };

    function deleteCourseComment(roleId) {
        $.ajax({
            type: "DELETE",
            dataType: "json",
            url: "/coursecomment/" + roleId,
        }).done(function (data) {
            if (data && data.retCode == 1) {
                layer.msg(data.retMsg, {
                    icon: 1
                });
//
                setTimeout('$("#cmoment_table").DataTable().ajax.reload();', 800)

            } else {
                var s = layer.msg("对不起，删除失败！", {
                    icon: 2
                });
                layer.close(s);
            }

        }).fail(function (d) {
            layer.msg("服务器无响应", {
                icon: 2
            });

        });
    }

    /**
     * 提交表单
     *
     * @param data
     */
    function submitForm(data, type, url) {
        $.ajax({
            type: type,
            dataType: "json",
            url: url,
            contentType: "application/json;charset=utf-8",
            data: data
        }).done(function (data) {
            if (data && data.retCode == 1) {
                var l = layer.msg(data.retMsg, {
                    icon: 1
                });
                setTimeout('layer.closeAll();$("#comment_table").DataTable().ajax.reload();', 800)

            } else {
                var s = layer.msg("对不起，添加失败！", {
                    icon: 3
                });
                layer.close(s);
            }

        }).fail(function (d) {
            layer.msg("服务器无响应");

        });
    }

    return {
        init: function () {
            initTable();
            event();
        }
    };

}();