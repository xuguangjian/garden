var StudentAskLeave = function () {
    let
        AUTHORITY = $("#authority").attr("content");
    var initTable = function () {
        // 渲染列表
        var oTable = $("#leave_table")
            .DataTable(
                {
                    "bAutoWidth": true,
                    // "bProcessing" : true,
                    "bFilter": true,
                    // "bStateSave" : true,
                    // "sPaginationType" : "full_numbers",
                    "aaSorting": [[0, 'desc']],
                    bSort: false,
                    "oLanguage": { // 汉化
                        "sProcessing": "正在加载数据...",
                        "sLengthMenu": "显示_MENU_条",
                        "sZeroRecords": "没有您要搜索的内容",
                        "sInfo": "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
                        "sInfoEmpty": "记录数为0",
                        "sInfoFiltered": "(全部记录数 _MAX_  条)",
                        "sInfoPostFix": "",
                        "sSearch": "搜索",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "首页",
                            "sPrevious": " 上一页 ",
                            "sNext": " 下一页 ",
                            "sLast": " 末页 "
                        }
                    },
                    "aoColumns": [
                        {
                            "mData": "studentId"
                        },
                        {
                            "mData": "classId"
                        },

                        {
                            "mData": "classId"
                        },
                        {
                            "mData": "classId"
                        },
                        {
                            "mData": "classId"
                        },
                        {
                            "mData": "classId"
                        },
                        {
                            "mData": "classId"
                        },

                        {
                            "mData": function (source, type, val) {
                                return source;
                            },
                            "mRender": function (value, method, row) {
                                var btn = '';
                                let txt = "请假";

                                let
                                    update = '<a class="btn blue ask_leave" style="margin-left:10px;" data-id="'
                                        + value.id
                                        + '"><i class="icon-edit"></i>'+txt+'</a>';

                                if (AUTHORITY
                                        .includes("PERMISSION_CARD_RULE_EDIT")) {
                                    btn += update;
                                }

                                return btn;
                            }
                        }],
                    bServerSide: true,
                    sAjaxSource: "/classstudent",
                    fnServerData: function (source, data, fnCallBack) {
                        // alert("data:"+JSON.stringify(data));
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: source,
                            data: "param=" + JSON.stringify(data),
                            // beforeSend:beginWait(),
                            success: function (d) {
                                if (d.retCode != "1")
                                    alert(d.retMsg);
                                else {
                                    fnCallBack(d.retObj);
                                }
                            },
                            failure: function (d) {

                                alert("服务器无响应");
                            },
                        });
                    }
                });

        jQuery('#leave_table_wrapper .dataTables_filter input').addClass(
            "m-wrap small");
        jQuery('#leave_table_wrapper .dataTables_length select').addClass(
            "m-wrap small");
        jQuery('#leave_table_wrapper .dataTables_length select').select2();

    };

    var event = function () {
        // 1.删除
        $("#card_table").on("click", "td a.card_rule_delete", function () {
            var roleId = $(this).attr("data-id");
            layer.confirm('您确定要删除吗，删除后不可恢复？', {
                title: '删除按此刷卡设置',
                btn: ['是', '否'],
                btnAlign: 'c'
                // 按钮
            }, function () {
                deleteCardRule(roleId);
            }, function () {
            });
        });
        //修改状态
        $("#card_table").on("click", "td a.card_rule_state", function () {
            var roleId = $(this).attr("data-id");
            let txt=$(this).text();

            let message="您确定要";
            let status=1;
            if("结束"==txt){
                message+="结束吗?";
                status=0;
            }else{
                message+="启用吗？";
            }
            layer.confirm(message, {
                title: '修改按此刷卡规则状态',
                btn: ['是', '否'],
                btnAlign: 'c'
                // 按钮
            }, function () {
                updateCardRuleStatus(roleId,status);
            }, function () {
            });
        });
        // 2.编辑
        $("#leave_table").on(
            "click",
            "td a.ask_leave",
            function () {
                var roleId = $(this).attr("data-id");
                layer.open({
                    title: '学员请假登记',
                    btn: ['是', '否'],
                    type: 2,
                    // skin: 'layui-layer-rim', //加上边框
                    // area: ['620px', 'auto'], //宽高
                    area: ["60%", "80%"],
                    scrollbar:false,
                    content: "/ask_leave/" + roleId,
                    yes: function (index, layerno) {
                        var body = layer.getChildFrame('body', index);
                        var $posName = body.find("textarea[name='leaveReason']");
                        var $form = body.find("#stu_ask_form");

                        var $chkMessage=body.find("#chkMessage");

                        if ($chkMessage.is(':checked')){
                            if (!body.find("textarea[name='message']").val()){
                                layer.msg("亲，请编辑短信内容！", {
                                    icon: 2
                                });
                                return;
                            }
                        }

                        if (!$posName.val()) {
                            layer.msg("亲，请假事由不能为空！", {
                                icon: 2
                            });
                            return;
                        }

                        var form_data = $form.serializeArray();
                        var post_data = {};

                        $.map(form_data, function (n, i) {

                            var key = n['name'];
                            var value = n['value'];
                            if (key && post_data.hasOwnProperty(key)) {
                                let
                                    originValue = post_data[key];
                                let
                                    newValue = originValue + "," + value;
                                post_data[key] = newValue;
                            } else {
                                post_data[key] = value;
                            }

                        });
							// alert(JSON.stringify(post_data));

                         submitForm(JSON.stringify(post_data), "POST", "/ask_leave");
                    },
                    btn2: function (index, layerno) {
                    }
                });

            });

        // 3.添加
        $("#btn_card_rules_add").click(function () {
            // 页面层
            layer.open({
                title: '添加按此刷卡信息',
                btn: ['是', '否'],
                type: 2,
                // skin: 'layui-layer-lan', //加上边框
                // area: ['620px', 'auto'], //宽高
                scrollbar: false, // 父页面 滚动条 禁止
                area: ["90%", "60%"],
                content: "/swipe_card_add",
                anim: 1,
                yes: function (index, layerno) {
                    var body = layer.getChildFrame('body', index);
                    var $posName = body.find("#rule_name");
                    var $form = body.find("#swipe_card_add_form");

                    if (!$posName.val()) {
                        layer.msg("亲，规则名不能为空！", {
                            icon: 2
                        });
                        return;
                    }

                    var form_data = $form.serializeArray();
                    var post_data = {};

                    $.map(form_data, function (n, i) {

                        var key = n['name'];
                        var value = n['value'];
                        if (key && post_data.hasOwnProperty(key)) {
                            let
                                originValue = post_data[key];
                            let
                                newValue = originValue + "," + value;
                            post_data[key] = newValue;
                        } else {
                            post_data[key] = value;
                        }

                    });
//							alert(JSON.stringify(post_data));

                    submitForm(JSON.stringify(post_data), "POST", "/cardrule");

                },
                btn2: function (index, layerno) {
                }
            });

        });
    };

    function updateCardRuleStatus(roleId,status) {
        $.ajax({
            type: "PUT",
            dataType: "json",
            url: "/cardrulestate/" + roleId+"?state="+status,
        }).done(function (data) {
            if (data && data.retCode == 1) {
                layer.msg(data.retMsg, {
                    icon: 1
                });
//
                setTimeout('$("#card_table").DataTable().ajax.reload();', 800)

            } else {
                var s = layer.msg("对不起，状态更新失败了！", {
                    icon: 2
                });
                layer.close(s);
            }

        }).fail(function (d) {
            layer.msg("服务器无响应", {
                icon: 2
            });

        });
    }

    function deleteCardRule(roleId) {
        $.ajax({
            type: "DELETE",
            dataType: "json",
            url: "/cardrule/" + roleId,
        }).done(function (data) {
            if (data && data.retCode == 1) {
                layer.msg(data.retMsg, {
                    icon: 1
                });
//
                setTimeout('$("#card_table").DataTable().ajax.reload();', 800)

            } else {
                var s = layer.msg("对不起，删除失败！", {
                    icon: 2
                });
                layer.close(s);
            }

        }).fail(function (d) {
            layer.msg("服务器无响应", {
                icon: 2
            });

        });
    }

    /**
     * 提交表单
     *
     * @param data
     */
    function submitForm(data, type, url) {
        $.ajax({
            type: type,
            dataType: "json",
            url: url,
            contentType: "application/json;charset=utf-8",
            data: data
        }).done(function (data) {
            if (data && data.retCode == 1) {
                var l = layer.msg(data.retMsg, {
                    icon: 1
                });
                setTimeout('layer.closeAll();$("#card_table").DataTable().ajax.reload();', 800)

            } else {
                var s = layer.msg("对不起，添加失败！", {
                    icon: 3
                });
                layer.close(s);
            }

        }).fail(function (d) {
            layer.msg("服务器无响应");

        });
    }

    return {
        init: function () {
            initTable();
            event();
        }
    };

}();