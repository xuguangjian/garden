var CareStudentList = function () {
    let
        AUTHORITY = $("#authority").attr("content");
    var initTable = function () {
        // 渲染列表
        var oTable = $("#table")
            .DataTable(
                {
                    "bAutoWidth": true,
                    //"scrollX": true,
                    // "bProcessing" : true,
                    "bFilter": true,
                     "bStateSave" : true,
                    // "sPaginationType" : "full_numbers",
                    "aaSorting": [[0, 'desc']],
                    bSort: false,
                    "oLanguage": { // 汉化
                        "sProcessing": "正在加载数据...",
                        "sLengthMenu": "显示_MENU_条",
                        "sZeroRecords": "没有您要搜索的内容",
                        "sInfo": "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
                        "sInfoEmpty": "记录数为0",
                        "sInfoFiltered": "(全部记录数 _MAX_  条)",
                        "sInfoPostFix": "",
                        "sSearch": "搜索",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "首页",
                            "sPrevious": " 上一页 ",
                            "sNext": " 下一页 ",
                            "sLast": " 末页 "
                        }
                    },
                    "aoColumns": [
                        {
                            "mData": function (source, type, val) {
                                return source;
                            },
                            "mRender": function (value, method, row) {
                                return '<span class="center"><input type="checkbox" value="'
                                    + value.id + '"></span>'
                            }
                        },
                        {
                            "mData": function (source, type, val) {
                                return source;
                            },

                            "mRender": function (value, method, row) {
                                return '<a id="stu_view" href="javascript:;" data-id="'
                                    + value.id
                                    + '">'
                                    + value.id
                                    + '<i class="icon-user icon-white"></i></a>'
                            }
                        },
                        {
                            "mData": "stuName"
                        },
                        {
                            "mData": "stuBirth"
                        },
                        {
                            "mData": "stuSex",
                            "mRender": function (value, method, row) {
                                return value == 1 ? "男" : "女";
                            }
                        },
                        {
                            "mData": "stuAge"
                        },
                        {
                            "mData": "stuParentName"
                        },
                        {
                            "mData": "stuPhone"
                        },
                        {
                            "mData": "stuState"
                        },
                        {
                            "mData": function (source, type, val) {
                                return source;
                            },
                            "sWidth":"160px",
                            "mRender": function (value, method, row) {
                                var btn = '';

                                let
                                    edit = '<a class="btn purple care_stu_edit" data-id="'
                                        + value.id
                                        + '" ><i class="icon-edit"></i>点击签到</a>';


                                if (AUTHORITY
                                        .includes("PERMISSION_STUDENT_INFO_EDIT")) {
                                    btn += edit;
                                }

                                return btn;
                            }
                        }],
                    bServerSide: true,
                    sAjaxSource: "/signinstudentlist?signType=1",
                    fnServerData: function (source, data, fnCallBack) {
                        // alert("data:"+JSON.stringify(data));
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: source,
                            data: "param=" + JSON.stringify(data),
                            // beforeSend:beginWait(),
                            success: function (d) {
                                if (d.retCode != "1")
                                // bootbox.alert(d.retMsg);
                                    alert(d.retMsg);
                                else {
                                    // $("#total").html(d.retObj.iTotalRecords);
                                    fnCallBack(d.retObj);
                                }
                            },
                            failure: function (d) {
                                // bootbox.alert("服务器无响应");
                                alert("服务器无响应");
                            },
                            // complete:clearWait()
                        });
                    }
                });
        var oTable = $("#table_sign_in_home")
            .DataTable(
                {
                    "bAutoWidth": true,
                    //"scrollX": true,
                    // "bProcessing" : true,
                    "bFilter": true,
                     "bStateSave" : true,
                    // "sPaginationType" : "full_numbers",
                    "aaSorting": [[0, 'desc']],
                    bSort: false,
                    "oLanguage": { // 汉化
                        "sProcessing": "正在加载数据...",
                        "sLengthMenu": "显示_MENU_条",
                        "sZeroRecords": "没有您要搜索的内容",
                        "sInfo": "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
                        "sInfoEmpty": "记录数为0",
                        "sInfoFiltered": "(全部记录数 _MAX_  条)",
                        "sInfoPostFix": "",
                        "sSearch": "搜索",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "首页",
                            "sPrevious": " 上一页 ",
                            "sNext": " 下一页 ",
                            "sLast": " 末页 "
                        }
                    },
                    "aoColumns": [
                        {
                            "mData": function (source, type, val) {
                                return source;
                            },
                            "mRender": function (value, method, row) {
                                return '<span class="center"><input type="checkbox" value="'
                                    + value.id + '"></span>'
                            }
                        },
                        {
                            "mData": function (source, type, val) {
                                return source;
                            },

                            "mRender": function (value, method, row) {
                                return '<a id="stu_view" href="javascript:;" data-id="'
                                    + value.id
                                    + '">'
                                    + value.id
                                    + '<i class="icon-user icon-white"></i></a>'
                            }
                        },
                        {
                            "mData": "stuName"
                        },
                        {
                            "mData": "stuBirth"
                        },
                        {
                            "mData": "stuSex",
                            "mRender": function (value, method, row) {
                                return value == 1 ? "男" : "女";
                            }
                        },
                        {
                            "mData": "stuAge"
                        },
                        {
                            "mData": "stuParentName"
                        },
                        {
                            "mData": "stuPhone"
                        },
                        {
                            "mData": "stuState"
                        },
                        {
                            "mData": function (source, type, val) {
                                return source;
                            },
                            "sWidth":"160px",
                            "mRender": function (value, method, row) {
                                var btn = '';

                                let
                                    edit = '<a class="btn purple care_stu_edit" data-id="'
                                        + value.id
                                        + '" ><i class="icon-edit"></i>点击签到</a>';


                                if (AUTHORITY
                                        .includes("PERMISSION_STUDENT_INFO_EDIT")) {
                                    btn += edit;
                                }

                                return btn;
                            }
                        }],
                    bServerSide: true,
                    sAjaxSource: "/signinstudentlist?signType=2",
                    fnServerData: function (source, data, fnCallBack) {
                        // alert("data:"+JSON.stringify(data));
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: source,
                            data: "param=" + JSON.stringify(data),
                            // beforeSend:beginWait(),
                            success: function (d) {
                                if (d.retCode != "1")
                                // bootbox.alert(d.retMsg);
                                    alert(d.retMsg);
                                else {
                                    // $("#total").html(d.retObj.iTotalRecords);
                                    fnCallBack(d.retObj);
                                }
                            },
                            failure: function (d) {
                                // bootbox.alert("服务器无响应");
                                alert("服务器无响应");
                            },
                            // complete:clearWait()
                        });
                    }
                });


        var oTable_record = $("#table_sign_record")
            .DataTable(
                {
                    "bAutoWidth": true,
                    //"scrollX": true,
                    // "bProcessing" : true,
                    "bFilter": true,
                    // "bStateSave" : true,
                    // "sPaginationType" : "full_numbers",
                    "aaSorting": [[0, 'desc']],
                    bSort: false,
                    "oLanguage": { // 汉化
                        "sProcessing": "正在加载数据...",
                        "sLengthMenu": "显示_MENU_条",
                        "sZeroRecords": "没有您要搜索的内容",
                        "sInfo": "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
                        "sInfoEmpty": "记录数为0",
                        "sInfoFiltered": "(全部记录数 _MAX_  条)",
                        "sInfoPostFix": "",
                        "sSearch": "搜索",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "首页",
                            "sPrevious": " 上一页 ",
                            "sNext": " 下一页 ",
                            "sLast": " 末页 "
                        }
                    },
                    "aoColumns": [
                        {
                            "mData":"studentId"
                        },
                        {
                            "mData": "signInType",
                            "mRender": function (value, method, row) {
                                return value == 1 ? "上学签到" : "放学签到";
                            }
                        },
                        {
                            "mData": "signInTime"
                        },
                        {
                            "mData": "signInRemark",
                            "mRender":function (value, method, row) {
                                if(!value){
                                    return "<a style='font-size: 30px;text-decoration: none;'>+</a>";
                                }else{
                                    return "<a>"+value+"</a>";
                                }
                            }
                        },
                        {
                            "mData": function (source, type, val) {
                                return source;
                            },
                            "sWidth":"160px",
                            "mRender": function (value, method, row) {
                                var btn = '';

                                let
                                    edit = '<a class="btn purple sign_del" data-id="'
                                        + value.id
                                        + '" ><i class="icon-edit"></i>撤销签到</a>';


                                if (AUTHORITY
                                        .includes("PERMISSION_STUDENT_INFO_EDIT")) {
                                    btn += edit;
                                }

                                return btn;
                            }
                        }],
                    bServerSide: true,
                    sAjaxSource: "/signinrecordlist?signType=1",
                    fnServerData: function (source, data, fnCallBack) {
                        // alert("data:"+JSON.stringify(data));
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: source,
                            data: "param=" + JSON.stringify(data),
                            // beforeSend:beginWait(),
                            success: function (d) {
                                if (d.retCode != "1")
                                // bootbox.alert(d.retMsg);
                                    alert(d.retMsg);
                                else {
                                    // $("#total").html(d.retObj.iTotalRecords);
                                    fnCallBack(d.retObj);
                                }
                            },
                            failure: function (d) {
                                // bootbox.alert("服务器无响应");
                                alert("服务器无响应");
                            },
                            // complete:clearWait()
                        });
                    }
                });

        jQuery('#table_wrapper .dataTables_filter input').addClass(
            "m-wrap small"); // modify table search input
        jQuery('#table_wrapper .dataTables_length select').addClass(
            "m-wrap small"); // modify table per page dropdown
        jQuery('#table_wrapper .dataTables_length select').select2();
    };

    //日期选择框
    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl : App.isRTL(),
                language: 'zh-CN',
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });
        }
    }

    var event = function () {
        //0.刷卡签到
        $("#card_sign_in").keydown(function (e) {

            if (e.keyCode == "13") {//keyCode=13是回车键
                alert($(this).val());
            }
        });

        // 1.学员签到
        $("#table").on("click", "td a.care_stu_edit", function () {
            var roleId = $(this).attr("data-id");
            layer.confirm('确定签到吗？', {
                title: '托班学员签到',
                btn: ['是', '否'],
                btnAlign: 'c'
                // 按钮
            }, function () {
                var data = {};
                data.signInType=1;
                data.studentId=roleId;
                // signInStudent("signType=1&stuId="+roleId,"GET","/carestudentsign");
                signInStudent(JSON.stringify(data),"POST","/carestudentsign");
            }, function () {
            });
        });
        $("#table_sign_in_home").on("click", "td a.care_stu_edit", function () {
            var roleId = $(this).attr("data-id");
            layer.confirm('确定签到吗？', {
                title: '托班学员签到',
                btn: ['是', '否'],
                btnAlign: 'c'
                // 按钮
            }, function () {
                var data = {};
                data.signInType=2;
                data.studentId=roleId;
                // signInStudent("signType=1&stuId="+roleId,"GET","/carestudentsign");
                signInStudent(JSON.stringify(data),"POST","/carestudentsign");
            }, function () {
            });
        });
        //2.撤销签到
        $("#table_sign_record").on("click", "td a.sign_del", function () {
            var roleId = $(this).attr("data-id");
            layer.confirm('确定撤销签到吗？', {
                title: '托班学员撤销签到',
                btn: ['是', '否'],
                btnAlign: 'c'
                // 按钮
            }, function () {

                signInStudent("","DELETE","/carestudentsign/"+roleId);
            }, function () {
            });
        });
        //全选
        $("#checkAll").click(function () {

            if ($(this).is(':checked')) {
                // alert("全选");
                $("#table input[type='checkbox']").prop("checked", "checked");
            } else {
                // alert("没选");
                $("#table input[type='checkbox']").removeAttr("checked");
            }
        });
        $("#checkAllHome").click(function () {

            if ($(this).is(':checked')) {
                // alert("全选");
                $("#table_sign_in_home input[type='checkbox']").prop("checked", "checked");
            } else {
                // alert("没选");
                $("#table_sign_in_home input[type='checkbox']").removeAttr("checked");
            }
        });

        //3.批量签到
        $("#btn_batch_sign").click(function () {
            var valArr = new Array();
            $("#table tbody input[type='checkbox']").each(function (i) {
                if ($(this).is(':checked')) {
                    valArr.push($(this).val());
                }
            });
            if (valArr.length == 0) {
                layer.msg("请至少选中一个学员进行签到！", {icon: 2});
                return;
            }

            var data = {};
            data.signInType=1;
            data.studentId=valArr.join(",");
            // signInStudent("signType=1&stuId="+roleId,"GET","/carestudentsign");
            signInStudent(JSON.stringify(data),"POST","/carestudentsign");
        });
        $("#btn_batch_sign_home").click(function () {
            var valArr = new Array();
            $("#table_sign_in_home tbody input[type='checkbox']").each(function (i) {
                if ($(this).is(':checked')) {
                    valArr.push($(this).val());
                }
            });
            if (valArr.length == 0) {
                layer.msg("请至少选中一个学员进行签到！", {icon: 2});
                return;
            }

            var data = {};
            data.signInType=2;
            data.studentId=valArr.join(",");
            // signInStudent("signType=1&stuId="+roleId,"GET","/carestudentsign");
            signInStudent(JSON.stringify(data),"POST","/carestudentsign");
        });

    };

    function signInStudent(d,type,url) {
        $.ajax({
                type: type,
                dataType: "json",
                url: url,
                contentType: "application/json;charset=utf-8",
                data: d
            }).done(
                function (data) {
                    if (data && data.retCode == 1) {
                        layer.msg(data.retMsg, {
                            icon: 1
                        });
                        setTimeout('layer.closeAll();',800);
                        //刷新三个表格
                        $("#table").DataTable().ajax.reload();
                        $("#table_sign_record").DataTable().ajax.reload();
                        $("#table_sign_in_home").DataTable().ajax.reload();

                    } else {
                        var s = layer.msg("对不起，签到失败！", {
                            icon: 2
                        });
                        layer.close(s);
                    }

                }).fail(function (d) {
            layer.msg("服务器无响应", {
                icon: 2
            });

        });
    }

    /**
     * 提交表单
     *
     * @param data
     */
    function submitForm(data, type, url) {
        $.ajax({
            type: type,
            dataType: "json",
            url: url,
            contentType: "application/json;charset=utf-8",
            data: data
        })
            .done(
                function (data) {
                    if (data && data.retCode == 1) {
                       layer.msg(data.retMsg, {
                            icon: 1
                        });
                        setTimeout(
                            'layer.closeAll();$("#table").DataTable().ajax.reload();',
                            1000)

                    } else {
                        var s = layer.msg("对不起，添加失败！", {
                            icon: 3
                        });
                        layer.close(s);
                    }

                }).fail(function (d) {
            layer.msg("服务器无响应");

        });
    }

    return {
        init: function () {

            initTable();
            event();
            handleDatePickers();
        }
    };

}();