var StudentUpgradeRecord = function () {
    let
        AUTHORITY = $("#authority").attr("content");
    var initTable = function () {
        // 渲染列表
        var oTable = $("#upgrade_record_table")
            .DataTable(
                {
                    "bAutoWidth": true,
                    // "bProcessing" : true,
                    "bFilter": true,
                    // "bStateSave" : true,
                    // "sPaginationType" : "full_numbers",
                    "aaSorting": [[0, 'desc']],
                    bSort: false,
                    "oLanguage": { // 汉化
                        "sProcessing": "正在加载数据...",
                        "sLengthMenu": "显示_MENU_条",
                        "sZeroRecords": "没有您要搜索的内容",
                        "sInfo": "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
                        "sInfoEmpty": "记录数为0",
                        "sInfoFiltered": "(全部记录数 _MAX_  条)",
                        "sInfoPostFix": "",
                        "sSearch": "搜索",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "首页",
                            "sPrevious": " 上一页 ",
                            "sNext": " 下一页 ",
                            "sLast": " 末页 "
                        }
                    },
                    "aoColumns": [
                        {
                            "mData": "id"
                        },
                        {
                            "mData": "id"
                        },

                        {
                            "mData": "id"
                        },
                        {
                            "mData": "id"
                        },
                        {
                            "mData": "id"
                        },
                        {
                            "mData": "id"
                        },
                        {
                            "mData": "id"
                        },
                        {
                            "mData": function (source, type, val) {
                                return source;
                            },
                            "mRender": function (value, method, row) {
                                var btn = '';

                                let
                                    update = '<a class="btn red upgrade_delete" style="margin-left:10px;" data-id="'
                                        + value.id
                                        + '"><i class="icon-trash"></i>删除</a>';

                                if (AUTHORITY
                                        .includes("PERMISSION_CARD_RULE_EDIT")) {
                                    btn += update;
                                }

                                return btn;
                            }
                        }],
                    bServerSide: true,
                    sAjaxSource: "/student_upgrade_record",
                    fnServerData: function (source, data, fnCallBack) {
                        // alert("data:"+JSON.stringify(data));
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: source,
                            data: "param=" + JSON.stringify(data),
                            // beforeSend:beginWait(),
                            success: function (d) {
                                if (d.retCode != "1")
                                    alert(d.retMsg);
                                else {
                                    fnCallBack(d.retObj);
                                }
                            },
                            failure: function (d) {

                                alert("服务器无响应");
                            },
                        });
                    }
                });

        jQuery('#upgrade_record_table_wrapper .dataTables_filter input').addClass(
            "m-wrap small");
        jQuery('#upgrade_record_table_wrapper .dataTables_length select').addClass(
            "m-wrap small");
        jQuery('#upgrade_record_table_wrapper .dataTables_length select').select2();

    };

    var event = function () {

        // 1.删除
        $("#upgrade_record_table").on("click", "td a.upgrade_delete", function() {
            var roleId = $(this).attr("data-id");
            layer.confirm('您确定要删除吗，删除后不可恢复？', {
                title : '删除会员升班记录',
                btn : [ '是', '否' ],
                btnAlign : 'c'
                // 按钮
            }, function() {
                deleteUpgradeRecord(roleId);
                // layer.alert("删除了，ID为："+roleId);
            });
        });


    };

    function deleteUpgradeRecord(roleId) {
        $.ajax({
            type : "DELETE",
            dataType : "json",
            url : "/student_upgrade_record/" + roleId
        }).done(function(data) {
            if (data && data.retCode == 1) {
                layer.msg(data.retMsg, {
                    icon : 1
                });
//				setTimeout("window.location.reload();", 800)
                setTimeout('$("#upgrade_record_table").DataTable().ajax.reload();', 800)

            } else {
                var s = layer.msg("对不起，删除失败！", {
                    icon : 2
                });
                layer.close(s);
            }

        }).fail(function(d) {
            layer.msg("服务器无响应", {
                icon : 2
            });

        });
    }


    return {
        init: function () {
            initTable();
            event();
        }
    };

}();