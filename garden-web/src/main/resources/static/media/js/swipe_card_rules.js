var SwipeCardRules = function () {
    let
        AUTHORITY = $("#authority").attr("content");
    var initTable = function () {
        // 渲染列表
        var oTable = $("#card_table")
            .DataTable(
                {
                    "bAutoWidth": true,
                    // "bProcessing" : true,
                    "bFilter": true,
                    // "bStateSave" : true,
                    // "sPaginationType" : "full_numbers",
                    "aaSorting": [[0, 'desc']],
                    bSort: false,
                    "oLanguage": { // 汉化
                        "sProcessing": "正在加载数据...",
                        "sLengthMenu": "显示_MENU_条",
                        "sZeroRecords": "没有您要搜索的内容",
                        "sInfo": "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
                        "sInfoEmpty": "记录数为0",
                        "sInfoFiltered": "(全部记录数 _MAX_  条)",
                        "sInfoPostFix": "",
                        "sSearch": "搜索",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "首页",
                            "sPrevious": " 上一页 ",
                            "sNext": " 下一页 ",
                            "sLast": " 末页 "
                        }
                    },
                    "aoColumns": [
                        {
                            "mData": "rulesName"
                        },
                        {
                            "mData": "swipeTime"
                        },
                        {
                            "mData": function (source, type, val) {
                                return source;
                            }, "mRender": function (value, method, row) {
                            if (value.feeType == 3) {

                                return "免费"
                            } else if (value.feeType == 1) {
                                return "扣课时 " + value.deductHour;
                            } else {
                                return "扣积分";
                            }
                        }
                        },
                        {
                            "mData": "createTime"
                        },
                        {
                            "mData": "rulesState",
                            "mRender":function (value,method,row) {
                                return value==1?"启用中":"已结束";
                            }
                        },
                        {
                            "mData": function (source, type, val) {
                                return source;
                            },
                            "mRender": function (value, method, row) {
                                var btn = '';
                                let txt = "结束";
                                if (value.rulesState == 0) {
                                    txt = "启用";
                                }
                                let
                                    edit = '<a class="btn purple card_rule_edit" style="margin-left:10px;" data-id="'
                                        + value.id
                                        + '"><i class="icon-edit"></i> 编辑</a>';
                                let
                                    update = '<a class="btn blue card_rule_state" style="margin-left:10px;" data-id="'
                                        + value.id
                                        + '"><i class="icon-edit"></i>'+txt+'</a>';
                                let
                                    del = '<a class="btn red card_rule_delete" style="margin-left:10px;" data-id="'
                                        + value.id
                                        + '"><i class="icon-trash"></i> 删除</a>';

                                if (AUTHORITY
                                        .includes("PERMISSION_CARD_RULE_EDIT")) {
                                    btn += edit + update;
                                }
                                if (AUTHORITY
                                        .includes("PERMISSION_CARD_RULE_DELETE")) {
                                    btn += del;
                                }
                                return btn;
                            }
                        }],
                    bServerSide: true,
                    sAjaxSource: "/cardrulelist",
                    fnServerData: function (source, data, fnCallBack) {
                        // alert("data:"+JSON.stringify(data));
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: source,
                            data: "param=" + JSON.stringify(data),
                            // beforeSend:beginWait(),
                            success: function (d) {
                                if (d.retCode != "1")
                                    alert(d.retMsg);
                                else {
                                    fnCallBack(d.retObj);
                                }
                            },
                            failure: function (d) {

                                alert("服务器无响应");
                            },
                        });
                    }
                });

        jQuery('#card_table_wrapper .dataTables_filter input').addClass(
            "m-wrap small");
        jQuery('#card_table_wrapper .dataTables_length select').addClass(
            "m-wrap small");
        jQuery('#card_table_wrapper .dataTables_length select').select2();

    };

    var event = function () {
        // 1.删除
        $("#card_table").on("click", "td a.card_rule_delete", function () {
            var roleId = $(this).attr("data-id");
            layer.confirm('您确定要删除吗，删除后不可恢复？', {
                title: '删除按此刷卡设置',
                btn: ['是', '否'],
                btnAlign: 'c'
                // 按钮
            }, function () {
                deleteCardRule(roleId);
            }, function () {
            });
        });
        //修改状态
        $("#card_table").on("click", "td a.card_rule_state", function () {
            var roleId = $(this).attr("data-id");
            let txt=$(this).text();

            let message="您确定要";
            let status=1;
            if("结束"==txt){
                message+="结束吗?";
                status=0;
            }else{
                message+="启用吗？";
            }
            layer.confirm(message, {
                title: '修改按此刷卡规则状态',
                btn: ['是', '否'],
                btnAlign: 'c'
                // 按钮
            }, function () {
                updateCardRuleStatus(roleId,status);
            }, function () {
            });
        });
        // 2.编辑
        $("#card_table").on(
            "click",
            "td a.card_rule_edit",
            function () {
                var roleId = $(this).attr("data-id");
                layer.open({
                    title: '编辑按次刷卡规则',
                    btn: ['是', '否'],
                    type: 2,
                    // skin: 'layui-layer-rim', //加上边框
                    // area: ['620px', 'auto'], //宽高
                    area: ["90%", "60%"],
                    content: "/swipe_card/" + roleId,
                    yes: function (index, layerno) {
                        var body = layer.getChildFrame('body', index);
                        var $posName = body.find("#rule_name");
                        var $form = body.find("#swipe_card_add_form");

                        if (!$posName.val()) {
                            layer.msg("亲，规则名不能为空！", {
                                icon: 2
                            });
                            return;
                        }

                        var form_data = $form.serializeArray();
                        var post_data = {};

                        $.map(form_data, function (n, i) {

                            var key = n['name'];
                            var value = n['value'];
                            if (key && post_data.hasOwnProperty(key)) {
                                let
                                    originValue = post_data[key];
                                let
                                    newValue = originValue + "," + value;
                                post_data[key] = newValue;
                            } else {
                                post_data[key] = value;
                            }

                        });
//							alert(JSON.stringify(post_data));

                        submitForm(JSON.stringify(post_data), "PUT", "/cardrule/"+roleId);
                    },
                    btn2: function (index, layerno) {
                    }
                });

            });

        // 3.添加
        $("#btn_card_rules_add").click(function () {
            // 页面层
            layer.open({
                title: '添加按此刷卡信息',
                btn: ['是', '否'],
                type: 2,
                // skin: 'layui-layer-lan', //加上边框
                // area: ['620px', 'auto'], //宽高
                scrollbar: false, // 父页面 滚动条 禁止
                area: ["90%", "60%"],
                content: "/swipe_card_add",
                anim: 1,
                yes: function (index, layerno) {
                    var body = layer.getChildFrame('body', index);
                    var $posName = body.find("#rule_name");
                    var $form = body.find("#swipe_card_add_form");

                    if (!$posName.val()) {
                        layer.msg("亲，规则名不能为空！", {
                            icon: 2
                        });
                        return;
                    }

                    var form_data = $form.serializeArray();
                    var post_data = {};

                    $.map(form_data, function (n, i) {

                        var key = n['name'];
                        var value = n['value'];
                        if (key && post_data.hasOwnProperty(key)) {
                            let
                                originValue = post_data[key];
                            let
                                newValue = originValue + "," + value;
                            post_data[key] = newValue;
                        } else {
                            post_data[key] = value;
                        }

                    });
//							alert(JSON.stringify(post_data));

                    submitForm(JSON.stringify(post_data), "POST", "/cardrule");

                },
                btn2: function (index, layerno) {
                }
            });

        });
    };

    function updateCardRuleStatus(roleId,status) {
        $.ajax({
            type: "PUT",
            dataType: "json",
            url: "/cardrulestate/" + roleId+"?state="+status,
        }).done(function (data) {
            if (data && data.retCode == 1) {
                layer.msg(data.retMsg, {
                    icon: 1
                });
//
                setTimeout('$("#card_table").DataTable().ajax.reload();', 800)

            } else {
                var s = layer.msg("对不起，状态更新失败了！", {
                    icon: 2
                });
                layer.close(s);
            }

        }).fail(function (d) {
            layer.msg("服务器无响应", {
                icon: 2
            });

        });
    }

    function deleteCardRule(roleId) {
        $.ajax({
            type: "DELETE",
            dataType: "json",
            url: "/cardrule/" + roleId,
        }).done(function (data) {
            if (data && data.retCode == 1) {
                layer.msg(data.retMsg, {
                    icon: 1
                });
//
                setTimeout('$("#card_table").DataTable().ajax.reload();', 800)

            } else {
                var s = layer.msg("对不起，删除失败！", {
                    icon: 2
                });
                layer.close(s);
            }

        }).fail(function (d) {
            layer.msg("服务器无响应", {
                icon: 2
            });

        });
    }

    /**
     * 提交表单
     *
     * @param data
     */
    function submitForm(data, type, url) {
        $.ajax({
            type: type,
            dataType: "json",
            url: url,
            contentType: "application/json;charset=utf-8",
            data: data
        }).done(function (data) {
            if (data && data.retCode == 1) {
                var l = layer.msg(data.retMsg, {
                    icon: 1
                });
                setTimeout('layer.closeAll();$("#card_table").DataTable().ajax.reload();', 800)

            } else {
                var s = layer.msg("对不起，添加失败！", {
                    icon: 3
                });
                layer.close(s);
            }

        }).fail(function (d) {
            layer.msg("服务器无响应");

        });
    }

    return {
        init: function () {
            initTable();
            event();
        }
    };

}();