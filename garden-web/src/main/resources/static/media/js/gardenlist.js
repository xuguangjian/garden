var GardenList = function() {
	let
	AUTHORITY = $("#authority").attr("content");
	var initTable = function() {
		// 渲染列表
		var oTable = $("#table")
				.DataTable(
						{
							"bAutoWidth" : true,
							// "bProcessing" : true,
							"bFilter" : true,
							// "bStateSave" : true,
							// "sPaginationType" : "full_numbers",
							"aaSorting" : [ [ 0, 'desc' ] ],
							bSort : false,
							"oLanguage" : { // 汉化
								"sProcessing" : "正在加载数据...",
								"sLengthMenu" : "显示_MENU_条",
								"sZeroRecords" : "没有您要搜索的内容",
								"sInfo" : "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
								"sInfoEmpty" : "记录数为0",
								"sInfoFiltered" : "(全部记录数 _MAX_  条)",
								"sInfoPostFix" : "",
								"sSearch" : "搜索",
								"sUrl" : "",
								"oPaginate" : {
									"sFirst" : "首页",
									"sPrevious" : " 上一页 ",
									"sNext" : " 下一页 ",
									"sLast" : " 末页 "
								}
							},
							"aoColumns" : [
									{
										"mData" : "gardenName",
									},
									{
										"mData" : "gardenManager",
									},
									{
										"mData" : "gardenManagerPhone",
									},
									{
										"mData" : "gardenAddress",
									},
									{
										"mData" : function(source, type, val) {
											return source;
										},
										"mRender" : function(value, method, row) {
											var btn = '';
											let
											view = '<a class="btn green garden_view" data-id="'+value.id+'"><i class="icon-eye-open"></i> 查看</a>';
											let
											edit = '<a class="btn purple garden_edit" style="margin-left:10px;" data-id="'
													+ value.id
													+ '" data-garden_name="'
													+ value.gardenName
													+ '" data-garden_phone="'
													+ value.gardenManagerPhone
													+ '" data-garden_manager="'
													+ value.gardenManager
													+ '" data-garden_address="'
													+ value.gardenAddress
													+ '"><i class="icon-edit"></i> 编辑</a>';
											let
											del = '<a class="btn black garden_delete" style="margin-left:10px;" data-id="'
													+ value.id
													+ '"><i class="icon-trash"></i> 删除</a>';
											if (AUTHORITY
													.includes("PERMISSION_GARDEN_VIEW")) {
												btn += view;
											}
											if (AUTHORITY
													.includes("PERMISSION_GARDEN_EDIT")) {
												btn += edit;
											}
											if (AUTHORITY
													.includes("PERMISSION_GARDEN_DELETE")) {
												btn += del;
											}
											return btn;
										}
									} ],
							bServerSide : true,
							sAjaxSource : "/gardenlist",
							fnServerData : function(source, data, fnCallBack) {
								$.ajax({
									type : "POST",
									dataType : "json",
									url : source,
									data : "param=" + JSON.stringify(data),
									success : function(d) {
											fnCallBack(d.retObj);
									},
									failure : function(d) {
										alert("服务器无响应");
									},
								});
							}
						});

		jQuery('#table_wrapper .dataTables_filter input').addClass("m-wrap small");
		jQuery('#table_wrapper .dataTables_length select').addClass("m-wrap small");
		jQuery('#table_wrapper .dataTables_length select').select2();
	};

	var event = function() {
		// 1.删除
		$("#table").on("click", "td a.garden_delete", function() {
			var roleId = $(this).attr("data-id");
			layer.confirm('您确定要删除吗，删除后不可恢复？', {
				title : '删除园所',
				btn : [ '是', '否' ],
				btnAlign : 'c'
			// 按钮
			}, function() {
				deleteGarden(roleId);
			});
		});
		// 2.编辑
		$("#table").on(
				"click",
				"td a.garden_edit",
				function() {
					var that = $(this);
					var roleId = that.attr("data-id");
					var garden_name = that.attr("data-garden_name");
					var garden_phone = that.attr("data-garden_phone");
					var garden_manager = that.attr("data-garden_manager");
					var garden_address = that.attr("data-garden_address");
					$("#e_gardenName").val(garden_name);
					$("#e_gardenManagerPhone").val(garden_phone);
					$("#e_gardenAddress").val(garden_address);
					$("#e_gardenManager").val(garden_manager);
					layer.open({
						title : '编辑园所',
						btn : [ '是', '否' ],
						type : 1,
						skin : 'layui-layer-rim', // 加上边框
						// area: ['620px', 'auto'], //宽高
						area : "60%",
						content : $('#tab2'),
						yes : function(index, layerno) {
							if (!$("#e_gardenName").val()) {
								layer.msg("亲，园所名称不能为空！", {
									icon : 2
								});
								return;
							}

							var form_data = $("#edit_garden_form")
									.serializeArray();
							var post_data = {};

							$.map(form_data, function(n, i) {
								post_data[n['name']] = n['value'];
							});

							submitForm($("#edit_garden_form input"),"PUT", JSON.stringify(post_data), "/garden/" + roleId);

						}
					});

				});

		// 3.添加园所
		$("#btn_add").click(
				function() {
					// 页面层
					layer.open({
						title : '添加园所',
						btn : [ '是', '否' ],
						type : 1,
						skin : 'layui-layer-rim', // 加上边框
						// area: ['620px', 'auto'], //宽高
						area : "60%",
                        scrollbar:false,
						content : $('#tab1'),
						yes : function(index, layerno) {
							if (!$("#gardenName").val()) {
								layer.msg("亲，园所名称不能为空！", {
									icon : 2
								});
								return;
							}
							var form_data = $("#add_garden_form").serializeArray();
							var post_data = {};
							$.map(form_data, function(n, i) {
								post_data[n['name']] = n['value'];
							});

							submitForm($("#add_garden_form input"),"POST", JSON.stringify(post_data), "/garden");
						}
					});
				});

		// 4.查看
		$("#table").on("click", "td a.garden_view", function() {
			var roleId = $(this).attr("data-id");
			layer.open({
				title : '查看园所',
				//btn : [ '是', '否' ],
				type : 2,
				skin : 'layui-layer-rim', // 加上边框
				area : "60%",
				scrollbar:false,
				content : "/garden/"+roleId
			});
		});
	};
	function deleteGarden(roleId) {
		$.ajax({
			type : "DELETE",
			dataType : "json",
			url : "/garden/" + roleId,
		}).done(function(data) {
			if (data && data.retCode == 1) {
				layer.msg(data.retMsg, {
					icon : 1
				});
				setTimeout('$("#table").DataTable().ajax.reload();', 800)

			} else {
				var s = layer.msg("对不起，删除失败！", {
					icon : 2
				});
				layer.close(s);
			}
		}).fail(function(d) {
			layer.msg("服务器无响应", {
				icon : 2
			});

		});
	}
	/**
	 * 提交表单
	 * 
	 * @param data
	 */
	function submitForm(form,type, data, url) {
		$
				.ajax({
					type : type,
					dataType : "json",
					url : url,
					contentType : "application/json;charset=utf-8",
					data : data
				})
				.done(
						function(data) {
							if (data && data.retCode == 1) {
								var l = layer.msg(data.retMsg, {
									icon : 1
								});

								setTimeout(
										'layer.closeAll();$("#table").DataTable().ajax.reload();',
										800);
								form.val("");

							} else {
								var s = layer.msg("对不起，添加失败！", {
									icon : 3
								});
								layer.close(s);
							}
						}).fail(function(d) {
					layer.msg("服务器无响应");

				});
	}

	return {
		init : function() {

			initTable();
			event();
		}
	};

}();