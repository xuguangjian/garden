var Series = function() {
	let
	AUTHORITY = $("#authority").attr("content");
	var initTable = function() {
		// 渲染列表
		var oTable = $("#series_table")
				.DataTable(
						{
							"bAutoWidth" : true,
							// "bProcessing" : true,
							"bFilter" : true,
							// "bStateSave" : true,
							// "sPaginationType" : "full_numbers",
							"aaSorting" : [ [ 0, 'desc' ] ],
							bSort : false,
							"oLanguage" : { // 汉化
								"sProcessing" : "正在加载数据...",
								"sLengthMenu" : "显示_MENU_条",
								"sZeroRecords" : "没有您要搜索的内容",
								"sInfo" : "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
								"sInfoEmpty" : "记录数为0",
								"sInfoFiltered" : "(全部记录数 _MAX_  条)",
								"sInfoPostFix" : "",
								"sSearch" : "搜索",
								"sUrl" : "",
								"oPaginate" : {
									"sFirst" : "首页",
									"sPrevious" : " 上一页 ",
									"sNext" : " 下一页 ",
									"sLast" : " 末页 "
								}
							},
							"aoColumns" : [
									{
										"mData" : "courseSeriesName"
									},
									{
										"mData" : "courseSeriesSort"
									},
									{
										"mData" : "courseSeriesRemark"
									},
									{
										"mData" : "createTime"
									},
									{
										"mData" : function(source, type, val) {
											return source;
										},
										"mRender" : function(value, method, row) {
											var btn = '';

											let
											edit = '<a class="btn purple series_edit" style="margin-left:10px;" data-id="'
													+ value.id
													+ '" data-series_name="'
													+ value.courseSeriesName
													+ '" data-series_sort="'
													+ value.courseSeriesSort
													+ '" data-series_remark="'+value.courseSeriesRemark+'"><i class="icon-edit"></i> 编辑</a>';
											let
											del = '<a class="btn red series_delete" style="margin-left:10px;" data-id="'
													+ value.id
													+ '"><i class="icon-trash"></i> 删除</a>';

											if (AUTHORITY
													.includes("PERMISSION_SERIES_EDIT")) {
												btn += edit;
											}
											if (AUTHORITY
													.includes("PERMISSION_SERIES_DELETE")) {
												btn += del;
											}
											return btn;
										}
									} ],
							bServerSide : true,
							sAjaxSource : "/serieslist",
							fnServerData : function(source, data, fnCallBack) {
								// alert("data:"+JSON.stringify(data));
								$.ajax({
									type : "POST",
									dataType : "json",
									url : source,
									data : "param=" + JSON.stringify(data),
									// beforeSend:beginWait(),
									success : function(d) {
										if (d.retCode != "1")
											alert(d.retMsg);
										else {
											fnCallBack(d.retObj);
										}
									},
									failure : function(d) {

										alert("服务器无响应");
									},
								});
							}
						});

		jQuery('#series_table_wrapper .dataTables_filter input').addClass(
				"m-wrap small");
		jQuery('#series_table_wrapper .dataTables_length select').addClass(
				"m-wrap small"); 
		jQuery('#series_table_wrapper .dataTables_length select').select2();
																		
	};

	var event = function() {
		// 1.删除
		$("#series_table").on("click", "td a.series_delete", function() {
			var roleId = $(this).attr("data-id");
			layer.confirm('您确定要删除吗，删除后不可恢复？', {
				title : '删除课程系列',
				btn : [ '是', '否' ],
				btnAlign : 'c'
			// 按钮
			}, function() {
                deleteSeries(roleId);
			}, function() {
			});
		});
		// 2.编辑
		$("#series_table").on(
				"click",
				"td a.series_edit",
				function() {

					// 将数据回显在弹框中
					var that = $(this);
					var roleId = that.attr("data-id");
					var series_name = that.attr("data-series_name");
					var series_sort = that.attr("data-series_sort");
					var series_remark = that.attr("data-series_remark");

					var $seriesName = $("#e_series_name");
					var $seriesSort = $("#e_series_sort");
					var $seriesRemark = $("#e_series_remark");

                    $seriesName.val(series_name);
                    $seriesSort.val(series_sort);
                    $seriesRemark.val(series_remark);

					layer.open({
						title : '编辑课程系列',
						btn : [ '是', '否' ],
						type : 1,
						// skin: 'layui-layer-rim', //加上边框
						// area: ['620px', 'auto'], //宽高
						area : "35%",
						content : $('#series_edit_alert'),
						yes : function(index, layerno) {
							if (!$seriesName.val()) {
								layer.msg("亲，课程系列名称不能为空！", {
									icon : 2
								});
								return;
							}
							var form_data = $("#series_edit_form").serializeArray();
						    var post_data = {};
                            $.map(form_data, function(n, i) {

                                var key=n['name'];
                                var value=n['value'];
                                if(key&&post_data.hasOwnProperty(key) ){
                                    let originValue=post_data[key];
                                    let newValue=originValue+","+value;
                                    post_data[key]=newValue;
                                }else{
                                    post_data[key] = value;
                                }

                            });
							submitForm(JSON.stringify(post_data),"PUT","/series/"+roleId);
						},
						btn2 : function(index, layerno) {
						}
					});

				});

		// 3.添加
		$("#btn_series_add").click(function() {
			
			var importance_param = $("#series_name").val();
			if (!importance_param) {
				layer.msg("亲，课程系列名称不能为空！", {
					icon : 2
				});
				return;
			}
			var form_data = $("#course_series_form").serializeArray();
		    var post_data = {};
            $.map(form_data, function(n, i) {

                var key=n['name'];
                var value=n['value'];
                if(key&&post_data.hasOwnProperty(key) ){
                    let originValue=post_data[key];
                    let newValue=originValue+","+value;
                    post_data[key]=newValue;
                }else{
                    post_data[key] = value;
                }

            });
			submitForm(JSON.stringify(post_data),"POST","/series");
		});
	};
	function deleteSeries(roleId) {
		$.ajax({
			type : "DELETE",
			dataType : "json",
			url : "/series/" + roleId,
		}).done(function(data) {
			if (data && data.retCode == 1) {
				layer.msg(data.retMsg, {
					icon : 1
				});
//
				setTimeout('$("#series_table").DataTable().ajax.reload();', 800)

			} else {
				var s = layer.msg("对不起，删除失败！", {
					icon : 2
				});
				layer.close(s);
			}

		}).fail(function(d) {
			layer.msg("服务器无响应", {
				icon : 2
			});

		});
	}
	/**
	 * 提交表单
	 * 
	 * @param data
	 */
	function submitForm(data,type ,url) {
		$.ajax({
			type : type,
			dataType : "json",
			url : url,
			contentType : "application/json;charset=utf-8",
			data : data
		}).done(function(data) {
			if (data && data.retCode == 1) {
				var l = layer.msg(data.retMsg, {
					icon : 1
				});
				setTimeout('layer.closeAll();$("#series_table").DataTable().ajax.reload();', 800)

			} else {
				var s = layer.msg("对不起，添加失败！", {
					icon : 3
				});
				layer.close(s);
			}

		}).fail(function(d) {
			layer.msg("服务器无响应");

		});
	}

	return {
		init : function() {
			initTable();
			event();
		}
	};

}();