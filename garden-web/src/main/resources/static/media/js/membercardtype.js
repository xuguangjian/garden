var MemberCardType = function() {
	let
	AUTHORITY = $("#authority").attr("content");
	var initTable = function() {
		// 渲染列表
		var oTable = $("#membercardtypetable")
				.DataTable(
						{
							"bAutoWidth" : true,
							// "bProcessing" : true,
							"bFilter" : true,
							// "bStateSave" : true,
							// "sPaginationType" : "full_numbers",
							"aaSorting" : [ [ 0, 'desc' ] ],
							bSort : false,
							"oLanguage" : { // 汉化
								"sProcessing" : "正在加载数据...",
								"sLengthMenu" : "显示_MENU_条",
								"sZeroRecords" : "没有您要搜索的内容",
								"sInfo" : "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
								"sInfoEmpty" : "记录数为0",
								"sInfoFiltered" : "(全部记录数 _MAX_  条)",
								"sInfoPostFix" : "",
								"sSearch" : "搜索",
								"sUrl" : "",
								"oPaginate" : {
									"sFirst" : "首页",
									"sPrevious" : " 上一页 ",
									"sNext" : " 下一页 ",
									"sLast" : " 末页 "
								}
							},
							"aoColumns" : [
									{
										"mData" : "typeName"
									},
									{
										"mData" : "typeDiscount"
									},
									{
										"mData" : "typeOrder"
									},
									{
										"mData" : "typeRemark"
									},
									{
										"mData" : function(source, type, val) {
											return source;
										},
										"mRender" : function(value, method, row) {
											var btn = '';

											let
											edit = '<a class="btn purple membercardtype_edit" style="margin-left:10px;" data-id="'
													+ value.id
													+ '" typeName="'
													+ value.typeName
													+ '" typeDiscount="'
													+ value.typeDiscount
													+ '" typeOrder="'
													+ value.typeOrder
													+ '" typeRemark="'
													+ value.typeRemark
													+ '"><i class="icon-edit"></i> 编辑</a>';
											let
											del = '<a class="btn red membercardtype_delete" style="margin-left:10px;" data-id="'
													+ value.id
													+ '"><i class="icon-trash"></i> 删除</a>';

											if (AUTHORITY
													.includes("PERMISSION_MEMBER_CARD_TYPE_EDIT")) {
												btn += edit;
											}
											if (AUTHORITY
													.includes("PERMISSION_MEMBER_CARD_TYPE_DELETE")) {
												btn += del;
											}
											return btn;
										}
									} ],
							bServerSide : true,
							sAjaxSource : "/membercardtypelist",
							fnServerData : function(source, data, fnCallBack) {
								// alert("data:"+JSON.stringify(data));
								$.ajax({
									type : "POST",
									dataType : "json",
									url : source,
									data : "param=" + JSON.stringify(data),
									// beforeSend:beginWait(),
									success : function(d) {
										if (d.retCode != "1")
											// bootbox.alert(d.retMsg);
											alert(d.retMsg);
										else {
											// $("#total").html(d.retObj.iTotalRecords);
											fnCallBack(d.retObj);
										}
									},
									failure : function(d) {
										// bootbox.alert("服务器无响应");
										alert("服务器无响应");
									},
								// complete:clearWait()
								});
							}
						});

		jQuery('#membercardtypetable_wrapper .dataTables_filter input').addClass(
				"m-wrap small");
		jQuery('#membercardtypetable_wrapper .dataTables_length select').addClass(
				"m-wrap small"); 
		jQuery('#membercardtypetable_wrapper .dataTables_length select').select2();
																		
	};

	var event = function() {
		// 1.删除
		$("#membercardtypetable").on("click", "td a.membercardtype_delete", function() {
			var typeId = $(this).attr("data-id");
			layer.confirm('您确定要删除吗，删除后不可恢复？', {
				title : '删除会员卡类型',
				btn : [ '是', '否' ],
				btnAlign : 'c'
			}, function() {
				deleteMemberCardType(typeId);
			});
		});
		// 2.编辑
		$("#membercardtypetable").on(
				"click",
				"td a.membercardtype_edit",
				function() {

					// 将数据回显在弹框中
					var that = $(this);
					var typeId = that.attr("data-id");
					var typeName = that.attr("typeName");
					var typeDiscount = that.attr("typeDiscount");
					var typeOrder = that.attr("typeOrder");
					var typeRemark = that.attr("typeRemark");
					
					var $typeName = $("#type_name");
					var $typeDiscount = $("#type_discount");
					var $typeOrder = $("#type_order");
					var $typeRemark = $("#type_remark");

					$typeName.val(typeName);
					$typeDiscount.val(typeDiscount);
					$typeOrder.val(typeOrder);
					$typeRemark.val(typeRemark);

					layer.open({
						title : '编辑会员卡类型',
						btn : [ '是', '否' ],
						type : 1,
						// skin: 'layui-layer-rim', //加上边框
						// area: ['620px', 'auto'], //宽高
						area : "35%",
						content : $('#tab4'),
						scrollbar:false,
						yes : function(index, layerno) {
							if (!$typeName.val()) {
								layer.msg("亲，类型名称不能为空！", {
									icon : 2
								});
								return;
							}
							if (!$typeDiscount.val()) {
								layer.msg("亲，商城折扣不能为空！", {
									icon : 2
								});
								return;
							}
							var data = {};
							data.typeName = $typeName.val();
							data.typeDiscount = $typeDiscount.val();
							data.typeOrder = $typeOrder.val();
							data.typeRemark = $typeRemark.val();

							submitForm(JSON.stringify(data),"PUT","/membercardtype/" + typeId);
						}
					});

				});

		// 3.添加
		$("#btn_membercardtype_add").click(function() {
			
			var typeName = $("#typeName").val();
			if (!typeName) {
				layer.msg("亲，类型名称不能为空！", {
					icon : 2
				});
				return;
			}
			var typeDiscount = $("#typeDiscount").val();
			if (!typeDiscount) {
				layer.msg("亲，商城折扣不能为空！", {
					icon : 2
				});
				return;
			}
			var typeOrder = $("#typeOrder").val();
			var typeRemark = $("#typeRemark").val();
			
			var data = {};
			data.typeName = typeName;
			data.typeDiscount = typeDiscount;
			data.typeOrder = typeOrder;
			data.typeRemark = typeRemark;
			
			submitForm(JSON.stringify(data),"POST", "/membercardtype");
            //触发reset按钮，清空表单
            $("#membercardtype_form input[type=reset]").trigger("click");
		});
	};
	function deleteMemberCardType(typeId) {
		$.ajax({
			type : "DELETE",
			dataType : "json",
			url : "/membercardtype/" + typeId,
		}).done(function(data) {
			if (data && data.retCode == 1) {
				layer.msg(data.retMsg, {
					icon : 1
				});
//				setTimeout("window.location.reload();", 800)
				setTimeout('$("#membercardtypetable").DataTable().ajax.reload();', 800)

			} else {
				var s = layer.msg(data.retMsg, {
					icon : 2
				});
				layer.close(s);
			}

		}).fail(function(d) {
			layer.msg("服务器无响应", {
				icon : 2
			});

		});
	}
	/**
	 * 提交表单
	 * 
	 * @param data
	 */
	function submitForm(data,type, url) {
		$.ajax({
			type : type,
			dataType : "json",
			url : url,
			contentType : "application/json;charset=utf-8",
			data : data
		}).done(function(data) {
			if (data && data.retCode == 1) {
				var l = layer.msg(data.retMsg, {
					icon : 1
				});
				setTimeout('layer.closeAll();$("#membercardtypetable").DataTable().ajax.reload();', 1000)

			} else {
				var s = layer.msg(data.retMsg, {
					icon : 3
				});
				layer.close(s);
			}

		}).fail(function(d) {
			layer.msg("服务器无响应");

		});
	}

	return {
		init : function() {
			initTable();
			event();
		}
	};

}();