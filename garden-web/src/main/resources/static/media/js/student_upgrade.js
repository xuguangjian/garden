var StudentUpgrade = function () {
    let
        AUTHORITY = $("#authority").attr("content");
    var initTable = function () {
        // 渲染列表
        var oTable = $("#student_upgrade_table")
            .DataTable(
                {
                    "bAutoWidth": true,
                    // "bProcessing" : true,
                    "bFilter": true,
                    // "bStateSave" : true,
                    // "sPaginationType" : "full_numbers",
                    "aaSorting": [[0, 'desc']],
                    bSort: false,
                    "oLanguage": { // 汉化
                        "sProcessing": "正在加载数据...",
                        "sLengthMenu": "显示_MENU_条",
                        "sZeroRecords": "没有您要搜索的内容",
                        "sInfo": "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
                        "sInfoEmpty": "记录数为0",
                        "sInfoFiltered": "(全部记录数 _MAX_  条)",
                        "sInfoPostFix": "",
                        "sSearch": "搜索",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "首页",
                            "sPrevious": " 上一页 ",
                            "sNext": " 下一页 ",
                            "sLast": " 末页 "
                        }
                    },
                    "aoColumns": [
                        {
                            "mData": "id"
                        },
                        {
                            "mData": "stuName"
                        },

                        {
                            "mData": "stuPhone"
                        },
                        {
                            "mData": "stuAge"
                        },
                        {
                            "mData": "stuCurrentClass"
                        },
                        {
                            "mData": "stuState"
                        },
                        {
                            "mData": function (source, type, val) {
                                return source;
                            },
                            "mRender": function (value, method, row) {
                                var btn = '';
                                let txt = "升班";

                                let
                                    update = '<a class="btn blue stu_upgrade" style="margin-left:10px;" data-id="'
                                        + value.id
                                        + '"><i class="icon-edit"></i>'+txt+'</a>';

                                if (AUTHORITY
                                        .includes("PERMISSION_CARD_RULE_EDIT")) {
                                    btn += update;
                                }

                                return btn;
                            }
                        }],
                    bServerSide: true,
                    sAjaxSource: "/studentupgradelist",
                    fnServerData: function (source, data, fnCallBack) {
                        // alert("data:"+JSON.stringify(data));
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: source,
                            data: "param=" + JSON.stringify(data),
                            // beforeSend:beginWait(),
                            success: function (d) {
                                if (d.retCode != "1")
                                    alert(d.retMsg);
                                else {
                                    fnCallBack(d.retObj);
                                }
                            },
                            failure: function (d) {

                                alert("服务器无响应");
                            },
                        });
                    }
                });

        jQuery('#student_upgrade_table_wrapper .dataTables_filter input').addClass(
            "m-wrap small");
        jQuery('#student_upgrade_table_wrapper .dataTables_length select').addClass(
            "m-wrap small");
        jQuery('#student_upgrade_table_wrapper .dataTables_length select').select2();

    };

    var event = function () {

        // 1.升班
        $("#student_upgrade_table").on(
            "click",
            "td a.stu_upgrade",
            function () {
                var roleId = $(this).attr("data-id");
                layer.open({
                    title: '学员升班',
                    btn: ['是', '否'],
                    type: 2,
                    // skin: 'layui-layer-rim', //加上边框
                    // area: ['620px', 'auto'], //宽高
                    area: ["60%", "80%"],
                    scrollbar:false,
                    content: "/student_upgrade/"+roleId,
                    yes: function (index, layerno) {
                        var body = layer.getChildFrame('body', index);

                        var $form = body.find("#student_upgrade_form");

                        var form_data = $form.serializeArray();
                        var post_data = {};

                        $.map(form_data, function (n, i) {

                            var key = n['name'];
                            var value = n['value'];
                            if (key && post_data.hasOwnProperty(key)) {
                                let
                                    originValue = post_data[key];
                                let
                                    newValue = originValue + "," + value;
                                post_data[key] = newValue;
                            } else {
                                post_data[key] = value;
                            }

                        });
							 // alert(JSON.stringify(post_data));

                         submitForm(JSON.stringify(post_data), "POST", "/student_upgrade");
                    },
                    btn2: function (index, layerno) {
                    }
                });

            });

    };

    /**
     * 提交表单
     *
     * @param data
     */
    function submitForm(data, type, url) {
        $.ajax({
            type: type,
            dataType: "json",
            url: url,
            contentType: "application/json;charset=utf-8",
            data: data
        }).done(function (data) {
            if (data && data.retCode == 1) {
                var l = layer.msg(data.retMsg, {
                    icon: 1
                });
                setTimeout('layer.closeAll();$("#student_upgrade_table").DataTable().ajax.reload();', 800)

            } else {
                var s = layer.msg("对不起，添加失败！", {
                    icon: 3
                });
                layer.close(s);
            }

        }).fail(function (d) {
            layer.msg("服务器无响应");

        });
    }

    return {
        init: function () {
            initTable();
            event();
        }
    };

}();