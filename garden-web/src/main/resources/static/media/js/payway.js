var PayWay = function() {
	let
	AUTHORITY = $("#authority").attr("content");
	var initTable = function() {
		// 渲染列表
		var oTable = $("#paywaytable")
				.DataTable(
						{
							"bAutoWidth" : true,
							// "bProcessing" : true,
							"bFilter" : true,
							// "bStateSave" : true,
							// "sPaginationType" : "full_numbers",
							"aaSorting" : [ [ 0, 'desc' ] ],
							bSort : false,
							"oLanguage" : { // 汉化
								"sProcessing" : "正在加载数据...",
								"sLengthMenu" : "显示_MENU_条",
								"sZeroRecords" : "没有您要搜索的内容",
								"sInfo" : "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
								"sInfoEmpty" : "记录数为0",
								"sInfoFiltered" : "(全部记录数 _MAX_  条)",
								"sInfoPostFix" : "",
								"sSearch" : "搜索",
								"sUrl" : "",
								"oPaginate" : {
									"sFirst" : "首页",
									"sPrevious" : " 上一页 ",
									"sNext" : " 下一页 ",
									"sLast" : " 末页 "
								}
							},
							"aoColumns" : [
									{
										"mData" : "payWay"
									},
									{
										"mData" : "payWayRemark"
									},
									{
										"mData" : function(source, type, val) {
											return source;
										},
										"mRender" : function(value, method, row) {
											var btn = '';

											let
											edit = '<a class="btn purple payway_edit" style="margin-left:10px;" data-id="'
													+ value.id
													+ '" payWay="'
													+ value.payWay
													+ '" payWayRemark="'
													+ value.payWayRemark
													+ '"><i class="icon-edit"></i> 编辑</a>';
											let
											del = '<a class="btn red payway_delete" style="margin-left:10px;" data-id="'
													+ value.id
													+ '"><i class="icon-trash"></i> 删除</a>';

											if (AUTHORITY
													.includes("PERMISSION_PAYWAY_EDIT")) {
												btn += edit;
											}
											if (AUTHORITY
													.includes("PERMISSION_PAYWAY_DELETE")) {
												btn += del;
											}
											return btn;
										}
									} ],
							bServerSide : true,
							sAjaxSource : "/paywaylist",
							fnServerData : function(source, data, fnCallBack) {
								// alert("data:"+JSON.stringify(data));
								$.ajax({
									type : "POST",
									dataType : "json",
									url : source,
									data : "param=" + JSON.stringify(data),
									// beforeSend:beginWait(),
									success : function(d) {
										if (d.retCode != "1")
											// bootbox.alert(d.retMsg);
											alert(d.retMsg);
										else {
											// $("#total").html(d.retObj.iTotalRecords);
											fnCallBack(d.retObj);
										}
									},
									failure : function(d) {
										// bootbox.alert("服务器无响应");
										alert("服务器无响应");
									},
								// complete:clearWait()
								});
							}
						});

		jQuery('#paywaytable_wrapper .dataTables_filter input').addClass(
				"m-wrap small");
		jQuery('#paywaytable_wrapper .dataTables_length select').addClass(
				"m-wrap small"); 
		jQuery('#paywaytable_wrapper .dataTables_length select').select2();
																		
	};

	var event = function() {
		// 1.删除
		$("#paywaytable").on("click", "td a.payway_delete", function() {
			var paywayId = $(this).attr("data-id");
			layer.confirm('您确定要删除吗，删除后不可恢复？', {
				title : '删除支付方式',
				btn : [ '是', '否' ],
				btnAlign : 'c'
			// 按钮
			}, function() {
				deletePayWay(paywayId);
				// layer.alert("删除了，ID为："+roleId);
			});
		});
		// 2.编辑
		$("#paywaytable").on(
				"click",
				"td a.payway_edit",
				function() {

					// 将数据回显在弹框中
					var that = $(this);
					var paywayId = that.attr("data-id");
					var payWay = that.attr("payWay");
					var remark = that.attr("payWayRemark");
					
					var $payWay = $("#payway_edit");
					var $remark = $("#payway_remark");

					$payWay.val(payWay);
					$remark.val(remark);

					layer.open({
						title : '编辑支付方式',
						btn : [ '是', '否' ],
						type : 1,
						// skin: 'layui-layer-rim', //加上边框
						// area: ['620px', 'auto'], //宽高
						area : "35%",
						content : $('#tab3'),
						scrollbar:false,
						yes : function(index, layerno) {
							if (!$payWay.val()) {
								layer.msg("亲，参数不能为空！", {
									icon : 2
								});
								return;
							}
							var data = {};
							data.payWay = $payWay.val();
							data.payWayRemark = $remark.val();

							submitForm(JSON.stringify(data),"PUT",
									"/payway/" + paywayId);
						}
					});

				});

		// 3.添加
		$("#btn_payway_add").click(function() {
			
			var payWay = $("#payWay").val();
			if (!payWay) {
				layer.msg("亲，参数不能为空！", {
					icon : 2
				});
				return;
			}
			var remark = $("#paywayRemark").val();
			
			var data = {};
			data.payWay = payWay;
			data.payWayRemark = remark;
			
			submitForm(JSON.stringify(data),"POST","/payway");
            //触发reset按钮，清空表单
            $("#payway_form input[type=reset]").trigger("click");
		});
	};
	function deletePayWay(paywayId) {
		$.ajax({
			type : "DELETE",
			dataType : "json",
			url : "/payway/" + paywayId,
		}).done(function(data) {
			if (data && data.retCode == 1) {
				layer.msg(data.retMsg, {
					icon : 1
				});
				setTimeout('$("#paywaytable").DataTable().ajax.reload();', 800)

			} else {
				var s = layer.msg(data.retMsg, {
					icon : 2
				});
				layer.close(s);
			}

		}).fail(function(d) {
			layer.msg("服务器无响应", {
				icon : 2
			});

		});
	}
	/**
	 * 提交表单
	 * 
	 * @param data
	 */
	function submitForm(data,type, url) {
		$.ajax({
			type : type,
			dataType : "json",
			url : url,
			contentType : "application/json;charset=utf-8",
			data : data
		}).done(function(data) {
			if (data && data.retCode == 1) {
				var l = layer.msg(data.retMsg, {
					icon : 1
				});
				setTimeout('layer.closeAll();$("#paywaytable").DataTable().ajax.reload();', 1000)

			} else {
				var s = layer.msg(data.retMsg, {
					icon : 3
				});
				layer.close(s);
			}

		}).fail(function(d) {
			layer.msg("服务器无响应");

		});
	}

	return {
		init : function() {
			initTable();
			event();
		}
	};

}();