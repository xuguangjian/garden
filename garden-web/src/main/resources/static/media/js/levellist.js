var LevelList = function() {
	let
	AUTHORITY = $("#authority").attr("content");
	var initTable = function() {
		// 渲染列表
		var oTable = $("#table")
				.DataTable(
						{
							"bAutoWidth" : false,
							// "bProcessing" : true,
							"bFilter" : true,
							// "bStateSave" : true,
							// "sPaginationType" : "full_numbers",
							"aaSorting" : [ [ 0, 'desc' ] ],
							bSort : false,
							"oLanguage" : { // 汉化
								"sProcessing" : "正在加载数据...",
								"sLengthMenu" : "显示_MENU_条",
								"sZeroRecords" : "没有您要搜索的内容",
								"sInfo" : "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
								"sInfoEmpty" : "记录数为0",
								"sInfoFiltered" : "(全部记录数 _MAX_  条)",
								"sInfoPostFix" : "",
								"sSearch" : "搜索",
								"sUrl" : "",
								"oPaginate" : {
									"sFirst" : "首页",
									"sPrevious" : " 上一页 ",
									"sNext" : " 下一页 ",
									"sLast" : " 末页 "
								}
							},
							"aoColumns" : [
									{
										"mData" : "levelParam",
										"sWidth":"20%",
									},
									{
										"mData" : "levelDesc",
										"sWidth":"60%"
									},
									{
										"mData" : function(source, type, val) {
											return source;
										},
										"sWidth":"20%",
										"mRender" : function(value, method, row) {
											var btn = '';
											let
											edit = '<a class="btn purple level_edit" style="margin-left:10px;" data-id="'
													+ value.id
													+ '" data-level_param="'+value.levelParam+'" data-level_desc="'+value.levelDesc+'"><i class="icon-edit"></i> 编辑</a>';
											let
											del = '<a class="btn black level_delete" style="margin-left:10px;" data-id="'
													+ value.id
													+ '"><i class="icon-trash"></i> 删除</a>';
											if (AUTHORITY
													.includes("PERMISSION_TEACHER_EDIT")) {
												btn += edit;
											}
											if (AUTHORITY
													.includes("PERMISSION_TEACHER_DELETE")) {
												btn += del;
											}
											return btn;
										}
									} ],
							bServerSide : true,
							sAjaxSource : "/teacherlevellist",
							fnServerData : function(source, data, fnCallBack) {
								// alert("data:"+JSON.stringify(data));
								$.ajax({
									type : "POST",
									dataType : "json",
									url : source,
									data : "param=" + JSON.stringify(data),
									// beforeSend:beginWait(),
									success : function(d) {
										if (d.retCode != "1")
											// bootbox.alert(d.retMsg);
											alert(d.retMsg);
										else {
											// $("#total").html(d.retObj.iTotalRecords);
											fnCallBack(d.retObj);
										}
									},
									failure : function(d) {
										// bootbox.alert("服务器无响应");
										alert("服务器无响应");
									},
								// complete:clearWait()
								});
							}
						});

		jQuery('#table_wrapper .dataTables_filter input').addClass(
				"m-wrap small"); // modify table search input
		jQuery('#table_wrapper .dataTables_length select').addClass(
				"m-wrap small"); // modify table per page dropdown
		jQuery('#table_wrapper .dataTables_length select').select2();
	};

	var event = function() {
		// 1.删除
		$("#table").on("click", "td a.level_delete", function() {
			var roleId = $(this).attr("data-id");
			layer.confirm('您确定要删除吗，删除后不可恢复？', {
				title : '删除教师级别',
				btn : [ '是', '否' ],
				btnAlign : 'c'
			// 按钮
			}, function() {
				deleteLevel(roleId);
			});
		});
		// 2.编辑
		$("#table").on(
				"click",
				"td a.level_edit",
				function() {
					var roleId = $(this).attr("data-id");
					var level_param = $(this).attr("data-level_param");
					var level_desc = $(this).attr("data-level_desc");
					
					var $levelParam=$("#e_levelParam");
					var $levelDesc=$("#e_levelDesc");
					
					$levelParam.val(level_param);
					$levelDesc.val(level_desc);
				
					layer.open({
						title : '编辑教师级别',
						btn : [ '是', '否' ],
						type : 1,
						skin : 'layui-layer-rim', // 加上边框
						// area: ['620px', 'auto'], //宽高
						area : "40%",
						content : $("#tab2"),
						yes : function(index, layerno) {
							
							if (!$levelParam.val()) {
								layer.msg("亲,教师级别参数不能为空！", {
									icon : 2
								});
								return;
							}
							var $form=$("#edit_level_form");
							var form_data = $form.serializeArray();
							var post_data = {};

							$.map(form_data, function(n, i) {
								post_data[n['name']] = n['value'];
							});

							submitForm($form.find("input"),"PUT", JSON
									.stringify(post_data), "/teacherlevel/"
									+ roleId);

						},
						btn2 : function(index, layerno) {
							layer.msg("取消了", {
								icon : 2
							});
						}
					});

				});

		// 3.添加教师级别
		$("#btn_add").click(
				function(e) {
					e.preventDefault();
					if (!$("#levelParam").val()) {
						layer.msg("亲，教师级别参数不能为空！", {
							icon : 2
						});
						return;
					}

					var form_data = $("#add_level_form")
							.serializeArray();
					var post_data = {};

					$.map(form_data, function(n, i) {
						post_data[n['name']] = n['value'];
					});

					submitForm($("#add_level_form input"),"POST", JSON
							.stringify(post_data), "/teacherlevel");
				});
	};
	function deleteLevel(roleId) {
		$.ajax({
			type : "DELETE",
			dataType : "json",
			url : "/teacherlevel/" + roleId,
		}).done(function(data) {
			if (data && data.retCode == 1) {
				layer.msg(data.retMsg, {
					icon : 1
				});
				setTimeout('$("#table").DataTable().ajax.reload();', 800)

			} else {
				var s = layer.msg("对不起，删除失败！", {
					icon : 2
				});
				layer.close(s);
			}
		}).fail(function(d) {
			layer.msg("服务器无响应", {
				icon : 2
			});

		});
	}
	/**
	 * 提交表单
	 * 
	 * @param data
	 */
	function submitForm(form,type, data, url) {
		$
				.ajax({
					type : type,
					dataType : "json",
					url : url,
					contentType : "application/json;charset=utf-8",
					data : data
				})
				.done(
						function(data) {
							if (data && data.retCode == 1) {
								var l = layer.msg(data.retMsg, {
									icon : 1
								});

								setTimeout('layer.closeAll();$("#table").DataTable().ajax.reload();', 800);
								form.val("");

							} else {
								var s = layer.msg(data.retMsg, {
									icon : 3
								});
								layer.close(s);
							}
						}).fail(function(d) {
					layer.msg("服务器无响应");

				});
	}

	return {
		init : function() {

			initTable();
			event();
		}
	};

}();