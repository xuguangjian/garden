var StudentImportance = function() {
	let
	AUTHORITY = $("#authority").attr("content");
	var initTable = function() {
		// 渲染列表
		var oTable = $("#importance_table")
				.DataTable(
						{
							"bAutoWidth" : true,
							// "bProcessing" : true,
							"bFilter" : true,
							// "bStateSave" : true,
							// "sPaginationType" : "full_numbers",
							"aaSorting" : [ [ 0, 'desc' ] ],
							bSort : false,
							"oLanguage" : { // 汉化
								"sProcessing" : "正在加载数据...",
								"sLengthMenu" : "显示_MENU_条",
								"sZeroRecords" : "没有您要搜索的内容",
								"sInfo" : "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
								"sInfoEmpty" : "记录数为0",
								"sInfoFiltered" : "(全部记录数 _MAX_  条)",
								"sInfoPostFix" : "",
								"sSearch" : "搜索",
								"sUrl" : "",
								"oPaginate" : {
									"sFirst" : "首页",
									"sPrevious" : " 上一页 ",
									"sNext" : " 下一页 ",
									"sLast" : " 末页 "
								}
							},
							"aoColumns" : [
									{
										"mData" : "importanceParam"
									},
									{
										"mData" : "importanceDesc"
									},
									{
										"mData" : "createTime"
									},
									{
										"mData" : "updateTime"
									},
									{
										"mData" : function(source, type, val) {
											return source;
										},
										"mRender" : function(value, method, row) {
											var btn = '';

											let
											edit = '<a class="btn purple stu_importance_edit" style="margin-left:10px;" data-id="'
													+ value.id
													+ '" data-importance_param="'
													+ value.importanceParam
													+ '" data-importance_desc="'
													+ value.importanceDesc
													+ '"><i class="icon-edit"></i> 编辑</a>';
											let
											del = '<a class="btn red stu_importance_delete" style="margin-left:10px;" data-id="'
													+ value.id
													+ '"><i class="icon-trash"></i> 删除</a>';

											if (AUTHORITY
													.includes("PERMISSION_ROLE_EDIT")) {
												btn += edit;
											}
											if (AUTHORITY
													.includes("PERMISSION_ROLE_DELETE")) {
												btn += del;
											}
											return btn;
										}
									} ],
							bServerSide : true,
							sAjaxSource : "/studentimportancelist",
							fnServerData : function(source, data, fnCallBack) {
								// alert("data:"+JSON.stringify(data));
								$.ajax({
									type : "POST",
									dataType : "json",
									url : source,
									data : "param=" + JSON.stringify(data),
									// beforeSend:beginWait(),
									success : function(d) {
										if (d.retCode != "1")
											// bootbox.alert(d.retMsg);
											alert(d.retMsg);
										else {
											// $("#total").html(d.retObj.iTotalRecords);
											fnCallBack(d.retObj);
										}
									},
									failure : function(d) {
										// bootbox.alert("服务器无响应");
										alert("服务器无响应");
									},
								// complete:clearWait()
								});
							}
						});

		jQuery('#importance_table_wrapper .dataTables_filter input').addClass(
				"m-wrap small");
		jQuery('#importance_table_wrapper .dataTables_length select').addClass(
				"m-wrap small"); 
		jQuery('#importance_table_wrapper .dataTables_length select').select2(); 
																		
	};

	var event = function() {
		// 1.删除
		$("#importance_table").on("click", "td a.stu_importance_delete", function() {
			var roleId = $(this).attr("data-id");
			layer.confirm('您确定要删除吗，删除后不可恢复？', {
				title : '删除重要程度',
				btn : [ '是', '否' ],
				btnAlign : 'c'
			// 按钮
			}, function() {
				deleteStuImportance(roleId);
				// layer.alert("删除了，ID为："+roleId);
			}, function() {
				layer.msg('取消删除！', {
					icon : 2
				});
			});
		});
		// 2.编辑
		$("#importance_table").on(
				"click",
				"td a.stu_importance_edit",
				function() {

					// 将数据回显在弹框中
					var that = $(this);
					var roleId = that.attr("data-id");
					var source_name = that.attr("data-importance_param");
					var detail_source = that.attr("data-importance_desc");

					var $sourceName = $("#importanceParam");
					var $detailSource = $("#importanceDesc");

					$sourceName.val(source_name);
					$detailSource.val(detail_source);

					layer.open({
						title : '编辑学员重要程度',
						btn : [ '是', '否' ],
						type : 1,
						// skin: 'layui-layer-rim', //加上边框
						// area: ['620px', 'auto'], //宽高
						area : "35%",
						content : $('#importance_edit_alert'),
						yes : function(index, layerno) {
							if (!$sourceName.val()) {
								layer.msg("亲，重要程度参数不能为空！", {
									icon : 2
								});
								return;
							}
							var form_data = $("#importance_edit_form").serializeArray();
						    var post_data = {};
						    $.map(form_data, function(n, i){
						    	post_data[n['name']] = n['value'];
						    });	
							submitForm(JSON.stringify(post_data), "/studentimportanceedit/"+roleId);
						},
						btn2 : function(index, layerno) {
							layer.msg("取消了", {
								icon : 2
							});
						}
					});

				});

		// 3.添加
		$("#btn_stu_importance_add").click(function() {
			
			var importance_param = $("#importance_param").val();
			if (!importance_param) {
				layer.msg("亲，重要程度参数不能为空！", {
					icon : 2
				});
				return;
			}
			var form_data = $("#stu_importance_form").serializeArray();
		    var post_data = {};
		    $.map(form_data, function(n, i){
		    	post_data[n['name']] = n['value'];
		    });	
			submitForm(JSON.stringify(post_data), "/studentimportanceadd");
		});
	};
	function deleteStuImportance(roleId) {
		$.ajax({
			type : "DELETE",
			dataType : "json",
			url : "/studentimportancedelete/" + roleId,
		// contentType: "application/json;charset=utf-8",
		// data : "roleId="+roleId
		// ,beforeSend : beginWait("研判报告导出中，请稍稍后。。。")
		// ,complete : clearWait()
		}).done(function(data) {
			if (data && data.retCode == 1) {
				layer.msg("恭喜您，删除成功！", {
					icon : 1
				});
//				setTimeout("window.location.reload();", 800)
				setTimeout('$("#importance_table").DataTable().ajax.reload();', 800)

			} else {
				var s = layer.msg("对不起，删除失败！", {
					icon : 2
				});
				layer.close(s);
			}

		}).fail(function(d) {
			layer.msg("服务器无响应", {
				icon : 2
			});

		});
	}
	/**
	 * 提交表单
	 * 
	 * @param data
	 */
	function submitForm(data, url) {
		$.ajax({
			type : "POST",
			dataType : "json",
			url : url,
			contentType : "application/json;charset=utf-8",
			data : data
		// ,beforeSend : beginWait("研判报告导出中，请稍稍后。。。")
		// ,complete : clearWait()
		}).done(function(data) {
			if (data && data.retCode == 1) {
				var l = layer.msg(data.retMsg, {
					icon : 1
				});
				setTimeout('layer.closeAll();$("#importance_table").DataTable().ajax.reload();', 1000)

			} else {
				var s = layer.msg("对不起，添加失败！", {
					icon : 3
				});
				layer.close(s);
			}

		}).fail(function(d) {
			layer.msg("服务器无响应");

		});
	}

	return {
		init : function() {
			initTable();
			event();
		}
	};

}();