var SmallTicket = function () {

    var renderKindEditor = function () {

        KindEditor.ready(function (K) {
            window.editor = K.create('#kindeditor_arrange');
            window.editor1 = K.create('#kindeditor_templet');

            // 同步数据后可以直接取得textarea的value
            editor.sync();
            editor1.sync();
        });

        $("#btn_arrange_template").click(function (e) {
            e.preventDefault();
            var id = $("#arrange_hidden").val();


            var post_data = {};
            post_data.smallticket = editor.html();
            post_data.id = id;
            submitForm(JSON.stringify(post_data), "POST", "/smallticketedit");

        });
        
        $("#btn_contract_template").click(function (e) {
            e.preventDefault();
            var id = $("#templet_hidden").val();


            var post_data = {};
            post_data.contractTemplet = editor1.html();
            post_data.id = id;
            submitForm(JSON.stringify(post_data), "POST", "/contracttempletedit");

        });


        /**
         * 提交表单
         *
         * @param data
         */
        function submitForm(data, type, url) {
            $.ajax({
                type: type,
                dataType: "json",
                url: url,
                contentType: "application/json;charset=utf-8",
                data: data
            }).done(function (data) {
                if (data && data.retCode == 1) {
                    var l = layer.msg(data.retMsg, {
                        icon: 1
                    });

                } else {
                    var s = layer.msg("对不起，添加失败！", {
                        icon: 3
                    });
                    layer.close(s);
                }

            }).fail(function (d) {
                layer.msg("服务器无响应");

            });
        }

    };


    var getSmallTicketTemplet = function (type, $textarea, $id) {

        $.ajax({
            type: "GET",
            dataType: "json",
            url: "/smallticketinfo"
        }).done(function (data) {
            if (data.retObj === "error" ) {
                layer.msg(data.retMsg, {icon: 2});
            } else if(data.retObj) {
                var rule = data.retObj;
                $id.val(rule.id);
                $textarea.text(rule.smallticket);
            }


        }).fail(function (d) {
            layer.msg("服务器无响应");
        });

    };
    
    var getContractTemplet = function (type, $textarea, $id) {

        $.ajax({
            type: "GET",
            dataType: "json",
            url: "/contracttempletinfo"
        }).done(function (data) {
            if (data.retObj === "error" ) {
                layer.msg(data.retMsg, {icon: 2});
            } else if(data.retObj) {
                var rule = data.retObj;
                $id.val(rule.id);
                $textarea.text(rule.contractTemplet);
            }


        }).fail(function (d) {
            layer.msg("服务器无响应");
        });

    };

    return {

        init: function () {
            renderKindEditor();
            getSmallTicketTemplet(1, $("#kindeditor_arrange"), $("#arrange_hidden"));
            getContractTemplet(1, $("#kindeditor_templet"), $("#templet_hidden"))
        }

    };

}();