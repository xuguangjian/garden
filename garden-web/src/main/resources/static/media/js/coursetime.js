var CourseTime = function () {
    let
        AUTHORITY = $("#authority").attr("content");
    var initTable = function () {
        // 渲染列表
        var oTable = $("#course_time_table")
            .DataTable(
                {
                    "bAutoWidth": true,
                    // "bProcessing" : true,
                    "bFilter": true,
                    // "bStateSave" : true,
                    // "sPaginationType" : "full_numbers",
                    "aaSorting": [[0, 'desc']],
                    bSort: false,
                    "oLanguage": { // 汉化
                        "sProcessing": "正在加载数据...",
                        "sLengthMenu": "显示_MENU_条",
                        "sZeroRecords": "没有您要搜索的内容",
                        "sInfo": "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
                        "sInfoEmpty": "记录数为0",
                        "sInfoFiltered": "(全部记录数 _MAX_  条)",
                        "sInfoPostFix": "",
                        "sSearch": "搜索",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "首页",
                            "sPrevious": " 上一页 ",
                            "sNext": " 下一页 ",
                            "sLast": " 末页 "
                        }
                    },
                    "aoColumns": [
                        {
                            "mData": "courseTime",
                            "mRender":function (value,method,row) {
                                var times=value.split(",");
                                return times[0]+":"+times[1]+"-"+times[2]+":"+times[3]
                            }
                        },
                        {
                            "mData": "timeSort"
                        },
                        {
                            "mData": "timeStatus",
                            "mRender":function (value,method,row) {

                                return value==1?"启用中":"禁用中";

                            }
                        },
                        {
                            "mData": "timeRemark"
                        },
                        {
                            "mData": function (source, type, val) {
                                return source;
                            },
                            "mRender": function (value, method, row) {
                                var btn = '';
                                let txt = "结束";
                                if (value.timeStatus == 0) {
                                    txt = "启用";
                                }

                                let
                                    edit = '<a class="btn purple course_time_edit" style="margin-left:10px;" data-id="'
                                        + value.id
                                        + '" data-course_time="'
                                        + value.courseTime
                                        + '" data-time_sort="'
                                        + value.timeSort
                                        + '" data-time_remark="' + value.timeRemark + '"><i class="icon-edit"></i> 编辑</a>';
                                let
                                    update = '<a class="btn blue course_time_state" style="margin-left:10px;" data-id="'
                                        + value.id
                                        + '"><i class="icon-edit"></i>'+txt+'</a>';

                                let
                                    del = '<a class="btn red course_time_delete" style="margin-left:10px;" data-id="'
                                        + value.id
                                        + '"><i class="icon-trash"></i> 删除</a>';

                                if (AUTHORITY
                                        .includes("PERMISSION_COURSE_TIME_EDIT")) {
                                    btn += edit+update;
                                }
                                if (AUTHORITY
                                        .includes("PERMISSION_COURSE_TIME_DELETE")) {
                                    btn += del;
                                }
                                return btn;
                            }
                        }],
                    bServerSide: true,
                    sAjaxSource: "/coursetimelist",
                    fnServerData: function (source, data, fnCallBack) {
                        // alert("data:"+JSON.stringify(data));
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: source,
                            data: "param=" + JSON.stringify(data),
                            // beforeSend:beginWait(),
                            success: function (d) {
                                if (d.retCode != "1")
                                    alert(d.retMsg);
                                else {
                                    fnCallBack(d.retObj);
                                }
                            },
                            failure: function (d) {

                                alert("服务器无响应");
                            },
                        });
                    }
                });

        jQuery('#course_time_table_wrapper .dataTables_filter input').addClass(
            "m-wrap small");
        jQuery('#course_time_table_wrapper .dataTables_length select').addClass(
            "m-wrap small");
        jQuery('#course_time_table_wrapper .dataTables_length select').select2();

    };

    var event = function () {
        // 1.删除
        $("#course_time_table").on("click", "td a.course_time_delete", function () {
            var roleId = $(this).attr("data-id");
            layer.confirm('您确定要删除吗，删除后不可恢复？', {
                title: '删除上课时间',
                btn: ['是', '否'],
                btnAlign: 'c'
                // 按钮
            }, function () {
                deleteCourseTime(roleId);
            }, function () {
            });
        });
        // 2.编辑
        $("#course_time_table").on(
            "click",
            "td a.course_time_edit",
            function () {
                var $that=$(this);
                var roleId = $that.attr("data-id");
                var time_name = $that.attr("data-course_time");
                var time_sort = $that.attr("data-time_sort");
                var time_remark = $that.attr("data-time_remark");

                $("#e_timeSort").val(time_sort);
                $("#e_timeRemark").val(time_remark);
                let times=time_name.split(",");
                $("#e_time1").find("option[value='"+times[0]+"']").attr("selected",true);
                $("#e_time2").find("option[value='"+times[1]+"']").attr("selected",true);
                $("#e_time3").find("option[value='"+times[2]+"']").attr("selected",true);
                $("#e_time4").find("option[value='"+times[3]+"']").attr("selected",true);
                layer.open({
                    title: '编辑上课时间设置',
                    btn: ['是', '否'],
                    type: 1,
                    // skin: 'layui-layer-rim', //加上边框
                    // area: ['620px', 'auto'], //宽高
                    area: ["60%","40%"],
                    content: $("#course_time_edit_alert"),
                    yes: function (index, layerno) {
                        var form_data = $(layerno).find("#course_time_edit_form").serializeArray();
                        var post_data = {};
                        $.map(form_data, function (n, i) {

                            var key = n['name'];
                            var value = n['value'];
                            if (key && post_data.hasOwnProperty(key)) {
                                let originValue = post_data[key];
                                let newValue = originValue + "," + value;
                                post_data[key] = newValue;
                            } else {
                                post_data[key] = value;
                            }

                        });
                        submitForm(JSON.stringify(post_data), "PUT", "/coursetime/"+roleId);

                    },
                    btn2: function (index, layerno) {
                    }
                });

            });

        //修改状态
        $("#course_time_table").on("click", "td a.course_time_state", function () {
            var roleId = $(this).attr("data-id");
            let txt=$(this).text();

            let message="您确定要";
            let status=1;
            if("结束"==txt){
                message+="结束吗?";
                status=0;
            }else{
                message+="启用吗？";
            }
            layer.confirm(message, {
                title: '修改上课时间设置状态',
                btn: ['是', '否'],
                btnAlign: 'c'
                // 按钮
            }, function () {
                updateCourseTimeStatus(roleId,status);
            }, function () {
            });
        });

        // 3.添加
        $("#btn_course_time_add").click(function () {


            var form_data = $("#course_time_form").serializeArray();
            var post_data = {};
            $.map(form_data, function (n, i) {

                var key = n['name'];
                var value = n['value'];
                if (key && post_data.hasOwnProperty(key)) {
                    let originValue = post_data[key];
                    let newValue = originValue + "," + value;
                    post_data[key] = newValue;
                } else {
                    post_data[key] = value;
                }

            });
            // alert(JSON.stringify(post_data));
            submitForm(JSON.stringify(post_data), "POST", "/coursetime");
            //触发reset按钮，清空表单
            $("input[type=reset]").trigger("click");
        });
    };
    function updateCourseTimeStatus(roleId,status) {
        $.ajax({
            type: "PUT",
            dataType: "json",
            url: "/coursetimestate/" + roleId+"?state="+status,
        }).done(function (data) {
            if (data && data.retCode == 1) {
                layer.msg(data.retMsg, {
                    icon: 1
                });
//
                setTimeout('$("#course_time_table").DataTable().ajax.reload();', 800)

            } else {
                var s = layer.msg("对不起，状态更新失败了！", {
                    icon: 2
                });
                layer.close(s);
            }

        }).fail(function (d) {
            layer.msg("服务器无响应", {
                icon: 2
            });

        });
    }

    function deleteCourseTime(roleId) {
        $.ajax({
            type: "DELETE",
            dataType: "json",
            url: "/coursetime/" + roleId,
        }).done(function (data) {
            if (data && data.retCode == 1) {
                layer.msg(data.retMsg, {
                    icon: 1
                });
//
                setTimeout('$("#course_time_table").DataTable().ajax.reload();', 800)

            } else {
                var s = layer.msg("对不起，删除失败！", {
                    icon: 2
                });
                layer.close(s);
            }

        }).fail(function (d) {
            layer.msg("服务器无响应", {
                icon: 2
            });

        });
    }

    /**
     * 提交表单
     *
     * @param data
     */
    function submitForm(data, type, url) {
        $.ajax({
            type: type,
            dataType: "json",
            url: url,
            contentType: "application/json;charset=utf-8",
            data: data
        }).done(function (data) {
            if (data && data.retCode == 1) {
                var l = layer.msg(data.retMsg, {
                    icon: 1
                });
                setTimeout('layer.closeAll();$("#course_time_table").DataTable().ajax.reload();', 800)

            } else {
                var s = layer.msg("对不起，添加失败！", {
                    icon: 3
                });
                layer.close(s);
            }

        }).fail(function (d) {
            layer.msg("服务器无响应");

        });
    }

    return {
        init: function () {
            initTable();
            event();
        }
    };

}();