var StudentArea = function() {
	let
	AUTHORITY = $("#authority").attr("content");
	var initTable = function() {
		// 渲染列表
		var oTable = $("#area_table")
				.DataTable(
						{
							"bAutoWidth" : false,
							// "bProcessing" : true,
							"bFilter" : true,
							// "bStateSave" : true,
							// "sPaginationType" : "full_numbers",
							"aaSorting" : [ [ 0, 'desc' ] ],
							bSort : false,
							"oLanguage" : { // 汉化
								"sProcessing" : "正在加载数据...",
								"sLengthMenu" : "显示_MENU_条",
								"sZeroRecords" : "没有您要搜索的内容",
								"sInfo" : "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
								"sInfoEmpty" : "记录数为0",
								"sInfoFiltered" : "(全部记录数 _MAX_  条)",
								"sInfoPostFix" : "",
								"sSearch" : "搜索",
								"sUrl" : "",
								"oPaginate" : {
									"sFirst" : "首页",
									"sPrevious" : " 上一页 ",
									"sNext" : " 下一页 ",
									"sLast" : " 末页 "
								}
							},
							"aoColumns" : [
									{
										"mData" : "stuArea.areaName",
										"sWidth" : "20%",
									},
									{
										"mData" : function(source, type, val) {
											return source;
										},
										"sWidth" : "60%",
										"mRender":function(value, method, row){
											if(value&&value.childStuArea.length>0){
												var s='<div class="btn-group">';
												$.each(value.childStuArea,function(index,area){
													s+='<a href="#" style="margin-left:5px;" class="btn" >'+area.areaName+' <i class="icon-remove icon-white m-icon-white" data-id="'+area.id+'"></i></a>';
												});
													s+='</div><i class="icon-plus add_child_area" style="float:right;vertical-align: middle;margin-top:10px;" data-parent_id="'+value.stuArea.id+'"></i>';
												return s;
											}else{
												return '<i data-parent_id="'+value.stuArea.id+'" class="icon-plus add_child_area" style="float:right;vertical-align: middle;margin-top:15px;"></i>';
											}
											
										}
									},
									{
										"mData" : function(source, type, val) {
											return source;
										},
										"sWidth" : "20%",
										"mRender" : function(value, method, row) {
											var btn = '';
											let edit='<a class="btn purple stu_area_edit" style="margin-left:10px;" data-id="'+value.stuArea.id+'" data-area_name="'+value.stuArea.areaName+'"><i class="icon-edit"></i> 编辑</a>';
											let
											del = '<a class="btn red stu_area_delete" style="margin-left:10px;" data-id="'
													+ value.stuArea.id
													+ '"><i class="icon-trash"></i> 删除</a>';
											if(AUTHORITY.includes("PERMISSION_ROLE_EDIT")){
												btn+=edit;
											}
											if (AUTHORITY
													.includes("PERMISSION_ROLE_DELETE")) {
												btn += del;
											}
											return btn;
										}
									} ],
							bServerSide : true,
							sAjaxSource : "/studentarealist",
							fnServerData : function(source, data, fnCallBack) {
								// alert("data:"+JSON.stringify(data));
								$.ajax({
									type : "POST",
									dataType : "json",
									url : source,
									data : "param=" + JSON.stringify(data),
									// beforeSend:beginWait(),
									success : function(d) {
										if (d.retCode != "1")
											// bootbox.alert(d.retMsg);
											alert(d.retMsg);
										else {
											// $("#total").html(d.retObj.iTotalRecords);
											fnCallBack(d.retObj);
										}
									},
									failure : function(d) {
										// bootbox.alert("服务器无响应");
										alert("服务器无响应");
									},
								// complete:clearWait()
								});
							}
						});

		jQuery('#area_table_wrapper .dataTables_filter input').addClass(
				"m-wrap small");
		jQuery('#area_table_wrapper .dataTables_length select').addClass(
				"m-wrap small"); 
		jQuery('#area_table_wrapper .dataTables_length select').select2(); 
																		
	};
	
	var event = function() {
		//1.删除一级地区
		$("#area_table").on("click", "td a.stu_area_delete", function() {
			var roleId = $(this).attr("data-id");
			layer.confirm('您确定要删除吗，删除后关联的二级地区也删除了，并且不可恢复？', {
				title : '删除地区',
				btn : [ '是', '否' ],
				btnAlign : 'c'
			// 按钮
			}, function() {
				deleteStuArea(roleId);
//				 layer.alert("删除了，ID为："+roleId);
			}, function() {
				layer.msg('取消删除！', {
					icon : 2
				});
			});
		});
		// 2.删除二级地区
		$("#area_table").on("click", "td i.icon-remove", function() {
			var roleId = $(this).attr("data-id");
			layer.confirm('您确定要删除吗，删除后不可恢复？', {
				title : '删除地区',
				btn : [ '是', '否' ],
				btnAlign : 'c'
			// 按钮
			}, function() {
				deleteStuArea(roleId);
//				 layer.alert("删除了，ID为："+roleId);
			}, function() {
				layer.msg('取消删除！', {
					icon : 2
				});
			});
		});
		// 3.添加二级地区
		$("#area_table").on(
				"click",
				"td i.add_child_area",
				function() {
					// 将数据回显在弹框中
					var that = $(this);
					var roleId = that.attr("data-parent_id");
		
					layer.open({
						title : '添加二级地区',
						btn : [ '是', '否' ],
						type : 1,
						// skin: 'layui-layer-rim', //加上边框
						// area: ['620px', 'auto'], //宽高
						area : "30%",
						content : '<div style="margin:10px; auto;text-align:center-vertical;"><label style="float:left;margin-top:5px;" for="childAreaName">地区（二级）:</label><input type="text" id="childAreaName"></div>',
						yes : function(index, layerno) {
							var $layerno=$(layerno);
							var areaName=$layerno.find("#childAreaName").val();
							if (!areaName) {
								layer.msg("亲，地区不能为空！", {
									icon : 2
								});
								return;
							}
							
						    var post_data = {};
						    post_data.areaName=areaName;
						    post_data.areaParentId=roleId;
							submitForm(JSON.stringify(post_data), "/studentareaadd");
						},
						btn2 : function(index, layerno) {
							layer.msg("取消了", {
								icon : 2
							});
						}
					});

				});

		// 4.添加一级地区
		$("#btn_stu_area_add").click(function() {
			
			var area_name = $("#area_name").val();
			if (!area_name) {
				layer.msg("亲，地区不能为空！", {
					icon : 2
				});
				return;
			}
			var form_data = $("#stu_area_form").serializeArray();
		    var post_data = {};
		    $.map(form_data, function(n, i){
		    	post_data[n['name']] = n['value'];
		    });	
			submitForm(JSON.stringify(post_data), "/studentareaadd");
		});
		//5.编辑一级地区
		$("#area_table").on(
				"click",
				"td a.stu_area_edit",
				function() {
					// 将数据回显在弹框中
					var that = $(this);
					var roleId = that.attr("data-id");
					var area_name = that.attr("data-area_name");
		
					layer.open({
						title : '编辑级地区',
						btn : [ '是', '否' ],
						type : 1,
						// skin: 'layui-layer-rim', //加上边框
						// area: ['620px', 'auto'], //宽高
						area : "30%",
						content : '<div style="margin:10px; auto;text-align:center-vertical;"><label style="float:left;margin-top:5px;" for="childAreaName">地区（二级）:</label><input type="text" id="childAreaName" value="'+area_name+'"></div>',
						yes : function(index, layerno) {
							var $layerno=$(layerno);
							var areaName=$layerno.find("#childAreaName").val();
							if (!areaName) {
								layer.msg("亲，地区不能为空！", {
									icon : 2
								});
								return;
							}
							
						    var post_data = {};
						    post_data.areaName=areaName;
							submitForm(JSON.stringify(post_data), "/studentareaedit/"+roleId);
						},
						btn2 : function(index, layerno) {
							layer.msg("取消了", {
								icon : 2
							});
						}
					});

				});
		
	};
	function deleteStuArea(roleId) {
		$.ajax({
			type : "DELETE",
			dataType : "json",
			url : "/studentareadelete/" + roleId
		}).done(function(data) {
			if (data && data.retCode == 1) {
				layer.msg("恭喜您，删除成功！", {
					icon : 1
				});
//				setTimeout("window.location.reload();", 800)
				setTimeout('$("#area_table").DataTable().ajax.reload();', 800)

			} else {
				var s = layer.msg("对不起，删除失败！", {
					icon : 2
				});
				layer.close(s);
			}

		}).fail(function(d) {
			layer.msg("服务器无响应", {
				icon : 2
			});

		});
	}
	/**
	 * 提交表单
	 * 
	 * @param data
	 */
	function submitForm(data, url) {
		$.ajax({
			type : "POST",
			dataType : "json",
			url : url,
			contentType : "application/json;charset=utf-8",
			data : data
		// ,beforeSend : beginWait("研判报告导出中，请稍稍后。。。")
		// ,complete : clearWait()
		}).done(function(data) {
			if (data && data.retCode == 1) {
				var l = layer.msg(data.retMsg, {
					icon : 1
				});
				setTimeout('layer.closeAll();$("#area_table").DataTable().ajax.reload();', 1000)

			} else {
				var s = layer.msg("对不起，添加失败！", {
					icon : 3
				});
				layer.close(s);
			}

		}).fail(function(d) {
			layer.msg("服务器无响应");

		});
	}

	return {
		init : function() {
			initTable();
			event();
		}
	};

}();