var ContractPackage = function() {
	let
	AUTHORITY = $("#authority").attr("content");
	var initTable = function() {
		// 渲染列表
		var oTable = $("#table")
				.DataTable(
						{
							"bAutoWidth" : true,
							// "bProcessing" : true,
							"bFilter" : true,
							// "bStateSave" : true,
							// "sPaginationType" : "full_numbers",
							"aaSorting" : [ [ 0, 'desc' ] ],
							bSort : false,
							"oLanguage" : { // 汉化
								"sProcessing" : "正在加载数据...",
								"sLengthMenu" : "显示_MENU_条",
								"sZeroRecords" : "没有您要搜索的内容",
								"sInfo" : "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
								"sInfoEmpty" : "记录数为0",
								"sInfoFiltered" : "(全部记录数 _MAX_  条)",
								"sInfoPostFix" : "",
								"sSearch" : "搜索",
								"sUrl" : "",
								"oPaginate" : {
									"sFirst" : "首页",
									"sPrevious" : " 上一页 ",
									"sNext" : " 下一页 ",
									"sLast" : " 末页 "
								}
							},
							"aoColumns" : [
									{
										"mData" : "contractPackageName"
									},
									{
										"mData" : "packageTypeName"
									},
									{
										"mData" : "lessonCount"
									},
									{
										"mData" : "packagePrice"
									},
									{
										"mData" : "packageOrder"
									},
									{
										"mData" : "packageRemark"
									},
									{
										"mData" : function(source, type, val) {
											return source;
										},
										"mRender" : function(value, method, row) {
											var btn = '';

											let
											edit = '<a class="btn purple contract_package_edit" style="margin-left:10px;" data-id="'
													+ value.id
													+ '" contractPackageName="'
													+ value.contractPackageName
													+ '" lessonCount="'
													+ value.lessonCount
													+ '" packagePrice="'
													+ value.packagePrice
													+ '" packageOrder="'
													+ value.packageOrder
													+ '" packageRemark="'
													+ value.packageRemark
													+ '" packageType="'
													+ value.packageType
													+ '"><i class="icon-edit"></i> 编辑</a>';
											let
											del = '<a class="btn red contract_package_delete" style="margin-left:10px;" data-id="'
													+ value.id
													+ '"><i class="icon-trash"></i> 删除</a>';

											if (AUTHORITY
													.includes("PERMISSION_CONTRACT_BASIC_INFO_EDIT")) {
												btn += edit;
											}
											if (AUTHORITY
													.includes("PERMISSION_CONTRACT_BASIC_INFO_DELETE")) {
												btn += del;
											}
											return btn;
										}
									} ],
							bServerSide : true,
							sAjaxSource : "/contactpackagelist",
							fnServerData : function(source, data, fnCallBack) {
								// alert("data:"+JSON.stringify(data));
								$.ajax({
									type : "POST",
									dataType : "json",
									url : source,
									data : "param=" + JSON.stringify(data),
									// beforeSend:beginWait(),
									success : function(d) {
										if (d.retCode != "1")
											// bootbox.alert(d.retMsg);
											alert(d.retMsg);
										else {
											// $("#total").html(d.retObj.iTotalRecords);
											fnCallBack(d.retObj);
										}
									},
									failure : function(d) {
										// bootbox.alert("服务器无响应");
										alert("服务器无响应");
									},
								// complete:clearWait()
								});
							}
						});

		jQuery('#table_wrapper .dataTables_filter input').addClass(
				"m-wrap small");
		jQuery('#table_wrapper .dataTables_length select').addClass(
				"m-wrap small"); 
		jQuery('#table_wrapper .dataTables_length select').select2(); 
																		
	};

	var event = function() {
		// 1.删除
		$("#table").on("click", "td a.contract_package_delete", function() {
			var packageId = $(this).attr("data-id");
			layer.confirm('您确定要删除吗，删除后不可恢复？', {
				title : '删除合同套餐',
				btn : [ '是', '否' ],
				btnAlign : 'c'
			}, function() {
				deleteContractPackage(packageId);
			});
		});
		// 2.编辑
		$("#table").on(
				"click",
				"td a.contract_package_edit",
				function() {

					// 将数据回显在弹框中
					var that = $(this);
					var packageId = that.attr("data-id");
					var contractPackageName = that.attr("contractPackageName");
					var packageType = that.attr("packageType");
					var lessonCount = that.attr("lessonCount");
					var packagePrice = that.attr("packagePrice");
					var packageOrder = that.attr("packageOrder");
					var packageRemark = that.attr("packageRemark");

					var $packageName = $("#package_name");
					var $lessonCount = $("#lesson_count");
					var $packagePrice = $("#package_price");
					var $packageOrder = $("#package_order");
					var $packageRemark = $("#package_remark");

					$packageName.val(contractPackageName);
					if(packageType==1)
						$("input:radio[name='package_type']:first").attr("checked", true);
					else
						$("input:radio[name='package_type']:last").attr("checked", true);
					$lessonCount.val(lessonCount);
					$packagePrice.val(packagePrice);
					$packageOrder.val(packageOrder);
					$packageRemark.val(packageRemark);

					layer.open({
						title : '编辑合同套餐',
						btn : [ '是', '否' ],
						type : 1,
						area : "35%",
						scrollbar:false,
						content : $('#tab2'),
						yes : function(index, layerno) {
							if (!$packageName.val()) {
								layer.msg("亲，合同套餐名称不能为空！", {
									icon : 2
								});
								return;
							}
							var data = {};
							data.contractPackageName = $packageName.val();
							data.lessonCount = $lessonCount.val();
							data.packageType = $("input[name='package_type']:checked").val();
							data.packagePrice = $packagePrice.val();
							data.packageOrder = $packageOrder.val();
							data.packageRemark = $packageRemark.val();

							submitForm(JSON.stringify(data),"PUT", "/contractpackage/" + packageId);
						}
					});

				});

		// 3.添加
		$("#btn_contractpackage_add").click(function() {
			
			var contractPackageName = $("#contractPackageName").val();
			if (!contractPackageName) {
				layer.msg("亲，合同套餐名称不能为空！", {
					icon : 2
				});
				return;
			}
			var lessonCount = $("#lessonCount").val();
			var packageType = $("input[name='packageType']:checked").val();
			var packagePrice = $("#packagePrice").val();
			var packageOrder = $("#packageOrder").val();
			var packageRemark = $("#packageRemark").val();
			var data = {};
			data.contractPackageName = contractPackageName;
			data.lessonCount = lessonCount;
			data.packageType = packageType;
			data.packagePrice = packagePrice;
			data.packageOrder = packageOrder;
			data.packageRemark = packageRemark;
			
			submitForm(JSON.stringify(data),"POST","/contractpackage");
            //触发reset按钮，清空表单
            $("#contract_package_form input[type=reset]").trigger("click");
		});
	};
	function deleteContractPackage(packageId) {
		$.ajax({
			type : "DELETE",
			dataType : "json",
			url : "/contractpackage/" + packageId,
		}).done(function(data) {
			if (data && data.retCode == 1) {
				layer.msg(data.retMsg, {
					icon : 1
				});
//				setTimeout("window.location.reload();", 800)
				setTimeout('$("#table").DataTable().ajax.reload();', 800)

			} else {
				var s = layer.msg(data.retMsg, {
					icon : 2
				});
				layer.close(s);
			}

		}).fail(function(d) {
			layer.msg("服务器无响应", {
				icon : 2
			});

		});
	}
	/**
	 * 提交表单
	 * 
	 * @param data
	 */
	function submitForm(data,type, url) {
		$.ajax({
			type : type,
			dataType : "json",
			url : url,
			contentType : "application/json;charset=utf-8",
			data : data
		}).done(function(data) {
			if (data && data.retCode == 1) {
				var l = layer.msg(data.retMsg, {
					icon : 1
				});
				setTimeout('layer.closeAll();$("#table").DataTable().ajax.reload();', 1000)

			} else {
				var s = layer.msg(data.retMsg, {
					icon : 3
				});
				layer.close(s);
			}

		}).fail(function(d) {
			layer.msg("服务器无响应");

		});
	}

	return {
		init : function() {
			initTable();
			event();
		}
	};

}();