var StudentAssignList = function () {
    let
        AUTHORITY = $("#authority").attr("content");
    var initTable = function () {
        // 渲染列表
        var oTable = $("#table")
            .DataTable(
                {
                    "bAutoWidth": false,
                    "scrollX": true,
                    // "bProcessing" : true,
                    "bFilter": true,
                    // "bStateSave" : true,
                    // "sPaginationType" : "full_numbers",
                    "aaSorting": [[0, 'desc']],
                    bSort: false,
                    "oLanguage": { // 汉化
                        "sProcessing": "正在加载数据...",
                        "sLengthMenu": "显示_MENU_条",
                        "sZeroRecords": "没有您要搜索的内容",
                        "sInfo": "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
                        "sInfoEmpty": "记录数为0",
                        "sInfoFiltered": "(全部记录数 _MAX_  条)",
                        "sInfoPostFix": "",
                        "sSearch": "搜索",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "首页",
                            "sPrevious": " 上一页 ",
                            "sNext": " 下一页 ",
                            "sLast": " 末页 "
                        }
                    },
                    "aoColumns": [
                        {
                            "mData": function (source, type, val) {
                                return source;
                            },
                            "mRender": function (value, method, row) {
                                return '<span class="center"><input type="checkbox" value="'
                                    + value.id + '"></span>'
                            }
                        },
                        {
                            "mData": function (source, type, val) {
                                return "No"+source.id;
                            }

                        },
                        {
                            "mData": "stuName",

                            "mRender": function (value, method, row) {
                                return '<a href="javascript:;">' + value + '</a>';
                            }
                        },
                        {
                            "mData": "stuBirth"
                        },
                        {
                            "mData": "stuBirth"
                        },
                        {
                            "mData": "stuSex",
                            "mRender": function (value, method, row) {
                                return value == 1 ? "男" : "女";
                            }
                        },
                        {
                            "mData": "stuAge"
                        },
                        {
                            "mData": "stuParentName"
                        },
                        {
                            "mData": "stuPhone"
                        },
                        {
                            "mData": "stuState"
                        },
                        {
                            "mData": "stuMarketPerson"
                        },
                        {
                            "mData": "stuAffiliatedAdviser"
                        },
                        {
                            "mData": function (source, type, val) {
                                return source;
                            },
                            "sWidth": "160px",
                            "mRender": function (value, method, row) {
                                var btn = '';

                                let
                                    edit = '<a data-id="'
                                        + value.id
                                        + '" style="text-decoration: none;" id="view_assign">[查看分配历史]</a>';
                                btn += edit;
                                return btn;
                            }
                        }],
                    bServerSide: true,
                    sAjaxSource: "/studentlist",
                    fnServerData: function (source, data, fnCallBack) {
                        // alert("data:"+JSON.stringify(data));
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: source,
                            data: "param=" + JSON.stringify(data),
                            // beforeSend:beginWait(),
                            success: function (d) {
                                if (d.retCode != "1")
                                // bootbox.alert(d.retMsg);
                                    alert(d.retMsg);
                                else {
                                    // $("#total").html(d.retObj.iTotalRecords);
                                    fnCallBack(d.retObj);
                                }
                            },
                            failure: function (d) {
                                // bootbox.alert("服务器无响应");
                                alert("服务器无响应");
                            },
                            // complete:clearWait()
                        });
                    }
                });
        jQuery('#table_wrapper .dataTables_filter input').addClass(
            "m-wrap small"); // modify table search input
        jQuery('#table_wrapper .dataTables_length select').addClass(
            "m-wrap small"); // modify table per page dropdown
        jQuery('#table_wrapper .dataTables_length select').select2();

        var oTableView = $("#table_view")
            .DataTable(
                {
                    "bPaginate":true,
                    "bInfo": true,
                    "bAutoWidth": true,
                    // "scrollX": true,
                    // "bProcessing" : true,
                    "bFilter": false,
                    // "bStateSave" : true,
                    // "sPaginationType" : "full_numbers",
                    "aaSorting": [[0, 'desc']],
                    bSort: false,
                    "oLanguage": { // 汉化
                        "sProcessing": "正在加载数据...",
                        "sLengthMenu": "显示_MENU_条",
                        "sZeroRecords": "没有您要搜索的内容",
                        "sInfo": "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
                        "sInfoEmpty": "记录数为0",
                        "sInfoFiltered": "(全部记录数 _MAX_  条)",
                        "sInfoPostFix": "",
                        "sSearch": "搜索",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "首页",
                            "sPrevious": " 上一页 ",
                            "sNext": " 下一页 ",
                            "sLast": " 末页 "
                        }
                    },
                    "aoColumns": [
                        {
                            "mData":"id"

                        },
                        {
                            "mData":"studentName"

                        },
                        {
                            "mData": "beforeAssignTeacher"
                        },
                        {
                            "mData": "afterAssignTeacher"
                        },
                        {
                            "mData": "assignOperator"
                        },
                        {
                            "mData": "createTime"
                        }
                    ],
                    bServerSide: true,
                    sAjaxSource: "/assignhistorylist",
                    fnServerData: function (source, data, fnCallBack) {
                        // alert("data:"+JSON.stringify(data));
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: source,
                            data: "param=" + JSON.stringify(data),
                            // beforeSend:beginWait(),
                            success: function (d) {
                                if (d.retCode != "1")
                                // bootbox.alert(d.retMsg);
                                    alert(d.retMsg);
                                else {
                                    // $("#total").html(d.retObj.iTotalRecords);
                                    fnCallBack(d.retObj);
                                }
                            },
                            failure: function (d) {
                                // bootbox.alert("服务器无响应");
                                alert("服务器无响应");
                            },
                            // complete:clearWait()
                        });
                    }
                });

        jQuery('#table_view_wrapper .dataTables_filter input').addClass(
            "m-wrap small"); // modify table search input
        jQuery('#table_view_wrapper .dataTables_length select').addClass(
            "m-wrap small"); // modify table per page dropdown
        jQuery('#table_view_wrapper .dataTables_length select').select2(); // initialzie
        // select2
        // dropdown

    };

    var event = function () {
        $("#btn_stu_assign").click(function (e) {
            e.preventDefault();
            var valArr = new Array();
            $("#table input[type='checkbox']").each(function (i) {
                if ($(this).is(':checked')) {
                    valArr.push($(this).val());
                }
            });
            if (valArr.length == 0) {
                layer.msg("请至少选中一个学员进行分配！", {icon: 2});
                return;
            }
            layer.open({
                title: '学员分配',
                btn: ['是', '否'],
                type: 1,
                skin: 'layui-layer-rim', // 加上边框
                area: ["50%", "40%"],
                content: $("#stu_assign_alert"),
                yes: function (index, layerno) {
                    $form = $("#assign_form");
                    var form_data = $form.serializeArray();
                    var post_data = {};

                    $.map(form_data, function (n, i) {

                        var key = n['name'];
                        var value = n['value'];
                        if (key && post_data.hasOwnProperty(key)) {
                            let
                                originValue = post_data[key];
                            let
                                newValue = originValue + "," + value;
                            post_data[key] = newValue;
                        } else {
                            post_data[key] = value;
                        }

                    });

                    post_data.stuId = valArr.join(',');
                    alert(JSON.stringify(post_data));

                    submitForm(JSON.stringify(post_data), "POST", "/studentassign");


                },
                btn2: function (index, layerno) {

                }
            });
        });

//全选
        $("#checkAll").click(function () {

            if ($(this).is(':checked')) {
                // alert("全选");
                $("#table input[type='checkbox']").prop("checked", "checked");
            } else {
                // alert("没选");
                $("#table input[type='checkbox']").removeAttr("checked");
            }
        });
        //查看分配历史
        $("#table").on("click", "td a#view_assign", function () {
            var roleId = $(this).attr("data-id");
            // alert(roleId);
            layer.open({
                title: '查看分配历史',

                type: 2,
                skin: 'layui-layer-lan', // 加上边框
                // area: ['620px', 'auto'], //宽高
                area: ["80%", "70%"],
                scrollbar: false, // 父页面 滚动条 禁止
                content: "/assignhistory/" + roleId,

            });

        });

    };

    function deleteStudent(data) {
        $
            .ajax({
                type: "POST",
                dataType: "json",
                url: "/studentdelete",
                contentType: "application/json;charset=utf-8",
                data: data
            })
            .done(
                function (data) {
                    if (data && data.retCode == 1) {
                        layer.msg("恭喜您，删除成功！", {
                            icon: 1
                        });
                        setTimeout(
                            'layer.closeAll();$("#table").DataTable().ajax.reload();',
                            800)

                    } else {
                        var s = layer.msg("对不起，删除失败！", {
                            icon: 2
                        });
                        layer.close(s);
                    }

                }).fail(function (d) {
            layer.msg("服务器无响应", {
                icon: 2
            });

        });
    }

    /**
     * 提交表单
     *
     * @param data
     */
    function submitForm(data, type, url) {
        $.ajax({
            type: type,
            dataType: "json",
            url: url,
            contentType: "application/json;charset=utf-8",
            data: data
        })
            .done(
                function (data) {
                    if (data && data.retCode == 1) {
                        layer.msg(data.retMsg, {
                            icon: 1
                        });
                        setTimeout(
                            'layer.closeAll();$("#table").DataTable().ajax.reload();',
                            1000)

                    } else {
                        var s = layer.msg("对不起，添加失败！", {
                            icon: 3
                        });
                        layer.close(s);
                    }

                }).fail(function (d) {
            layer.msg("服务器无响应");

        });
    }

    return {
        init: function () {

            initTable();
            event();
        }
    };

}();