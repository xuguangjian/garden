var RecepitTemplate = function () {

    var renderKindEditor = function () {

        KindEditor.ready(function (K) {
            window.editor = K.create('#kindeditor_arrange');
            window.editor2 = K.create('#kindeditor_card');

            // 同步数据后可以直接取得textarea的value
            editor.sync();
            editor2.sync();
        });

        //按次刷卡小票设置
        $("#btn_swipe_template").click(function (e) {
            e.preventDefault();
            var id = $("#swipe_hidden").val();

            var post_data = {};
            post_data.content = editor2.html();
            // $("#kindeditor_card").val();
            post_data.type = 2;


            if (!id) {
                id = 0;
            }
            // alert(JSON.stringify(post_data)+id);
            submitForm(JSON.stringify(post_data), "PUT", "/receipttemplate/" + id);

        });

        $("#btn_arrange_template").click(function (e) {
            e.preventDefault();
            var id = $("#arrange_hidden").val();


            var post_data = {};
            post_data.content = editor.html();
            // $("#kindeditor_card").val();
            post_data.type = 1;
            if (!id) {
                id = 0;
            }

            // alert(JSON.stringify(post_data)+"id="+id);
            submitForm(JSON.stringify(post_data), "PUT", "/receipttemplate/" + id);

        });


        /**
         * 提交表单
         *
         * @param data
         */
        function submitForm(data, type, url) {
            $.ajax({
                type: type,
                dataType: "json",
                url: url,
                contentType: "application/json;charset=utf-8",
                data: data
            }).done(function (data) {
                if (data && data.retCode == 1) {
                    var l = layer.msg(data.retMsg, {
                        icon: 1
                    });

                } else {
                    var s = layer.msg("对不起，添加失败！", {
                        icon: 3
                    });
                    layer.close(s);
                }

            }).fail(function (d) {
                layer.msg("服务器无响应");

            });
        }

    };


    var getReceiptTemplateData = function (type, $textarea, $id) {

        $.ajax({
            type: "GET",
            dataType: "json",
            url: "/receipttemplate?type=" + type
        }).done(function (data) {
            if (data.retObj === "error") {
                layer.msg(data.retMsg, {icon: 2});
            } else {
                var rule = data.retObj;
                $id.val(rule.id);
                $textarea.text(rule.content);
            }


        }).fail(function (d) {
            layer.msg("服务器无响应");
        });

    };

    return {

        init: function () {
            renderKindEditor();
            getReceiptTemplateData(1, $("#kindeditor_arrange"), $("#arrange_hidden"));
            getReceiptTemplateData(2, $("#kindeditor_card"), $("#swipe_hidden"));
        }

    };

}();