var ImportSuccessStudent = function () {
    let
        AUTHORITY = $("#authority").attr("content");
    var initTable = function () {
        // 渲染列表
        var oTable = $("#table_batch")
            .DataTable(
                {
                    "bAutoWidth": true,
                    // "bProcessing" : true,
                    "bFilter": true,
                    // "bStateSave" : true,
                    // "sPaginationType" : "full_numbers",
                    "aaSorting": [[0, 'desc']],
                    bSort: false,
                    "oLanguage": { // 汉化
                        "sProcessing": "正在加载数据...",
                        "sLengthMenu": "显示_MENU_条",
                        "sZeroRecords": "没有您要搜索的内容",
                        "sInfo": "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
                        "sInfoEmpty": "记录数为0",
                        "sInfoFiltered": "(全部记录数 _MAX_  条)",
                        "sInfoPostFix": "",
                        "sSearch": "搜索",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "首页",
                            "sPrevious": " 上一页 ",
                            "sNext": " 下一页 ",
                            "sLast": " 末页 "
                        }
                    },
                    "aoColumns": [
                        {
                            "mData": "id"
                        },
                        {
                            "mData":"stuName"
                        },

                        {
                            "mData": "stuNickName"
                        },
                        {
                            "mData": "stuBirth"

                        },
                        {
                            "mData": "stuSex"
                        },
                        {
                            "mData": "stuAge"
                        },
                        {
                            "mData": "stuParentName"
                        },
                        {
                            "mData": "stuPhone"
                        },

                        {
                            "mData":"stuState"
                        }],
                    bServerSide: true,
                    sAjaxSource: "/studentsuccesslist",
                    fnServerData: function (source, data, fnCallBack) {
                        // alert("data:"+JSON.stringify(data));
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: source,
                            data: "param=" + JSON.stringify(data)+"&batchId="+$("#batchId").val(),
                            // beforeSend:beginWait(),
                            success: function (d) {
                                if (d.retCode != "1")
                                // bootbox.alert(d.retMsg);
                                    alert(d.retMsg);
                                else {
                                    // $("#total").html(d.retObj.iTotalRecords);
                                    fnCallBack(d.retObj);
                                }
                            },
                            failure: function (d) {
                                // bootbox.alert("服务器无响应");
                                alert("服务器无响应");
                            },
                            // complete:clearWait()
                        });
                    }
                });

        jQuery('#table_batch_wrapper .dataTables_filter input').addClass(
            "m-wrap small"); // modify table search input
        jQuery('#table_batch_wrapper .dataTables_length select').addClass(
            "m-wrap small"); // modify table per page dropdown
        jQuery('#table_batch_wrapper .dataTables_length select').select2(); // initialzie
    };

    return {
        init: function () {
            initTable();
        }
    };

}();