var ImportStudentFail = function () {
    let
        AUTHORITY = $("#authority").attr("content");
    var initTable = function () {
        // 渲染列表
        var oTable = $("#table_batch")
            .DataTable(
                {
                    "bAutoWidth": true,
                    // "bProcessing" : true,
                    "bFilter": true,
                    // "bStateSave" : true,
                    // "sPaginationType" : "full_numbers",
                    "aaSorting": [[0, 'desc']],
                    bSort: false,
                    "oLanguage": { // 汉化
                        "sProcessing": "正在加载数据...",
                        "sLengthMenu": "显示_MENU_条",
                        "sZeroRecords": "没有您要搜索的内容",
                        "sInfo": "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
                        "sInfoEmpty": "记录数为0",
                        "sInfoFiltered": "(全部记录数 _MAX_  条)",
                        "sInfoPostFix": "",
                        "sSearch": "搜索",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "首页",
                            "sPrevious": " 上一页 ",
                            "sNext": " 下一页 ",
                            "sLast": " 末页 "
                        }
                    },
                    "aoColumns": [
                        {
                            "mData": "stuName"
                        },
                        {
                            "mData":"stuNickname"
                        },

                        {
                            "mData": "stuParentName"
                        },
                        {
                            "mData": "stuParentRelationship"

                        },
                        {
                            "mData": "stuPhone"
                        },
                        {
                            "mData": "stuFailReason",
                            "mRender":function(value,method,row){
                                if (value==1){
                                    return "手机号码已存在";
                                }else if(value==2){
                                    return "手机号码格式不正确";
                                }else if(value==3){
                                    "内容读取失败";
                                }else{
                                    return "其他";
                                }
                            }
                        },
                        {
                            "mData": "createTime"
                        },
                        {
                            "mData": function (source, type, val) {
                                return source;
                            },
                            "mRender": function (value, method, row) {
                                var btn = '';

                                let
                                    del = '<a class="btn red stu_delete" style="margin-left:5px;" data-id="'
                                        + value.id
                                        + '"><i class="icon-trash"></i> 删除</a>';

                                if (AUTHORITY
                                        .includes("PERMISSION_STUDENT_INFO_DELETE")) {
                                    btn += del;
                                }
                                return btn;
                            }
                        }],
                    bServerSide: true,
                    sAjaxSource: "/studentfaillist",
                    fnServerData: function (source, data, fnCallBack) {
                        // alert("data:"+JSON.stringify(data));
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: source,
                            data: "param=" + JSON.stringify(data)+"&batchId="+$("#batchId").val(),
                            // beforeSend:beginWait(),
                            success: function (d) {
                                if (d.retCode != "1")
                                // bootbox.alert(d.retMsg);
                                    alert(d.retMsg);
                                else {
                                    // $("#total").html(d.retObj.iTotalRecords);
                                    fnCallBack(d.retObj);
                                }
                            },
                            failure: function (d) {
                                // bootbox.alert("服务器无响应");
                                alert("服务器无响应");
                            },
                            // complete:clearWait()
                        });
                    }
                });

        jQuery('#table_batch_wrapper .dataTables_filter input').addClass(
            "m-wrap small"); // modify table search input
        jQuery('#table_batch_wrapper .dataTables_length select').addClass(
            "m-wrap small"); // modify table per page dropdown
        jQuery('#table_batch_wrapper .dataTables_length select').select2(); // initialzie
    };

    var event = function () {

        // 1.删除批次信息
        $("#table_batch").on("click", "td a.stu_delete", function () {
            var roleId = $(this).attr("data-id");
            layer.confirm('您确定要删除吗，删除后不可恢复？', {
                title: '删除导入失败学员',
                btn: ['是', '否'],
                btnAlign: 'c'
                // 按钮
            }, function () {

                deleteImportFailStudent(roleId);
            });
        });

    };

    function deleteImportFailStudent(data) {
        $
            .ajax({
                type: "DELETE",
                dataType: "json",
                url: "/importfailstudent/"+data,
                contentType: "application/json;charset=utf-8",
                data: data
            })
            .done(
                function (data) {
                    if (data && data.retCode == 1) {
                        layer.msg(data.retMsg, {
                            icon: 1
                        });
                        setTimeout(
                            'layer.closeAll();$("#table_batch").DataTable().ajax.reload();',
                            800)

                    } else {
                        var s = layer.msg("对不起，删除失败！", {
                            icon: 2
                        });
                        layer.close(s);
                    }

                }).fail(function (d) {
            layer.msg("服务器无响应", {
                icon: 2
            });

        });
    }

    return {
        init: function () {

            initTable();
            event();
        }
    };

}();