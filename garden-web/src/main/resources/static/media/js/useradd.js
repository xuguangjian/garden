var UserAdd = function() {

	// 表单验证
	var formValid = function() {

		// 手机号码验证
		jQuery.validator
				.addMethod(
						"isMobile",
						function(value, element) {
							var length = value.length;
							var mobile = /^(13[0-9]{9})|(18[0-9]{9})|(14[0-9]{9})|(17[0-9]{9})|(15[0-9]{9})$/;
							return this.optional(element)
									|| (length == 11 && mobile.test(value));
						}, "请正确填写您的手机号码");

		var form = $('#add_user_form');
		var error = $('.alert-error', form);
		var success = $('.alert-success', form);

		form.validate({
			debug : true,
			doNotHideMessage : true,
			errorElement : 'span',
			errorClass : 'validate-inline',
			focusInvalid : false,
			// onsubmit:true,
			// onfocusout:true,
			onfocusout : function(element) {
				$(element).valid()
			},
			// onkeyup:true,
			onkeyup : function(element) {
				$(element).valid()
			},
			// onclick:true,
			rules : {
				userName : {
					// minlength: 5,
					required : true,
					remote : "/usernamecheck"
				},
				userPhone : {
					// minlength: 5,
					required : true,
					isMobile : true
				},
				userRealName : {
					// minlength: 5,
					required : true
				},
				userPassword : {
					minlength : 6,
					required : true
				},
				userPassword2 : {
					minlength : 6,
					required : true,
					equalTo : "#txtPassword"
				}
			},
			messages : {
				userName : {
					required : "请输入用户名",
					remote : "用户名已存在，请更换！"
				},
				userPhone : {
					required : "手机码不能为空.",
					isMobile : "手机号码格式不正确！"
				},
				userRealName : {
					required : "真实姓名不能为空！"
				},
				userPassword : {
					minlength : "密码不能少于6位",
					required : "密码不能为空"
				},
				userPassword2 : {
					minlength : "密码不能少于6位",
					required : "密码不能为空",
					equalTo : "两次密码不一致"
				}
			},
			invalidHandler : function(event, validator) {
				success.hide();
				error.show();

			},

			highlight : function(element) {
				$(element).closest('.help-inline').removeClass('ok');
				$(element).closest('.control-group').removeClass('success')
						.addClass('error');
			},

			unhighlight : function(element) {
				$(element).closest('.control-group').removeClass('error');
			},

			success : function(label) {
				label.addClass('valid ok') // mark the current input as
				// valid and display OK icon
				.closest('.control-group').removeClass('error').addClass(
						'success');
			},

			submitHandler : function(form) {
				success.show();
				error.hide();

			}

		});
	};

	return {
		init : function() {
			formValid();

		}
	};

}();