var ContractField = function() {
	let
	AUTHORITY = $("#authority").attr("content");
	var initTable = function() {
		// 渲染列表
		var oTable = $("#contractfieldtable")
				.DataTable(
						{
							"bAutoWidth" : true,
							// "bProcessing" : true,
							"bFilter" : true,
							// "bStateSave" : true,
							// "sPaginationType" : "full_numbers",
							"aaSorting" : [ [ 0, 'desc' ] ],
							bSort : false,
							"oLanguage" : { // 汉化
								"sProcessing" : "正在加载数据...",
								"sLengthMenu" : "显示_MENU_条",
								"sZeroRecords" : "没有您要搜索的内容",
								"sInfo" : "从_START_ 到 _END_ 条记录——总记录数为 _TOTAL_ 条",
								"sInfoEmpty" : "记录数为0",
								"sInfoFiltered" : "(全部记录数 _MAX_  条)",
								"sInfoPostFix" : "",
								"sSearch" : "搜索",
								"sUrl" : "",
								"oPaginate" : {
									"sFirst" : "首页",
									"sPrevious" : " 上一页 ",
									"sNext" : " 下一页 ",
									"sLast" : " 末页 "
								}
							},
							"aoColumns" : [
									{
										"mData" : "fieldName"
									},
									{
										"mData" : "fieldTypeName"
									},
									{
										"mData" : "fieldComponentName"
									},
									{
										"mData" : "isShowName"
									},
									{
										"mData" : "isRequireName"
									},
									{
										"mData" : function(source, type, val) {
											return source;
										},
										"mRender" : function(value, method, row) {
											var btn = '';

											let
											edit = '<a class="btn purple contract_field_edit" style="margin-left:10px;" data-id="'
													+ value.id
													+ '" fieldName="'
													+ value.fieldName
													+ '" fieldType="'
													+ value.fieldType
													+ '" fieldComponent="'
													+ value.fieldComponent
													+ '" isShow="'
													+ value.isShow
													+ '" isRequire="'
													+ value.isRequire
													+ '"><i class="icon-edit"></i> 编辑</a>';
											let
											del = '<a class="btn red contract_field_delete" style="margin-left:10px;" data-id="'
													+ value.id
													+ '"><i class="icon-trash"></i> 删除</a>';
											
											let
											up = '<a class="btn red contract_field_up" style="margin-left:10px;" data-id="'
													+ value.id
													+ '"><i class="icon-trash"></i> 向上</a>';

											if (AUTHORITY
													.includes("PERMISSION_ROLE_EDIT")) {
												btn += edit;
											}
											if (AUTHORITY
													.includes("PERMISSION_ROLE_DELETE")) {
												btn += del;
											}
											if (AUTHORITY
													.includes("PERMISSION_ROLE_EDIT")) {
												btn += up;
											}
											return btn;
										}
									} ],
							bServerSide : true,
							sAjaxSource : "/contactfieldlist",
							fnServerData : function(source, data, fnCallBack) {
								// alert("data:"+JSON.stringify(data));
								$.ajax({
									type : "POST",
									dataType : "json",
									url : source,
									data : "param=" + JSON.stringify(data),
									// beforeSend:beginWait(),
									success : function(d) {
										if (d.retCode != "1")
											// bootbox.alert(d.retMsg);
											alert(d.retMsg);
										else {
											// $("#total").html(d.retObj.iTotalRecords);
											fnCallBack(d.retObj);
										}
									},
									failure : function(d) {
										// bootbox.alert("服务器无响应");
										alert("服务器无响应");
									},
								// complete:clearWait()
								});
							}
						});

		jQuery('#table_wrapper .dataTables_filter input').addClass(
				"m-wrap small");
		jQuery('#table_wrapper .dataTables_length select').addClass(
				"m-wrap small"); 
		jQuery('#table_wrapper .dataTables_length select').select2(); 
																		
	};

	var event = function() {
		// 0.排序
		$("#contractfieldtable").on("click", "td a.contract_field_up", function() {
			var fieldId = $(this).attr("data-id");
			layer.confirm('您确定要重新排序吗？', {
				title : '重新排序',
				btn : [ '是', '否' ],
				btnAlign : 'c'
			// 按钮
			}, function() {
				orderContractField(fieldId);
				// layer.alert("删除了，ID为："+roleId);
			}, function() {
				layer.msg('取消排序！', {
					icon : 2
				});
			});
		});
		// 1.删除
		$("#contractfieldtable").on("click", "td a.contract_field_delete", function() {
			var fieldId = $(this).attr("data-id");
			layer.confirm('您确定要删除吗，删除后不可恢复？', {
				title : '删除合同字段',
				btn : [ '是', '否' ],
				btnAlign : 'c'
			// 按钮
			}, function() {
				deleteContractField(fieldId);
				// layer.alert("删除了，ID为："+roleId);
			}, function() {
				layer.msg('取消删除！', {
					icon : 2
				});
			});
		});
		// 2.编辑
		$("#contractfieldtable").on(
				"click",
				"td a.contract_field_edit",
				function() {

					// 将数据回显在弹框中
					var that = $(this);
					var fieldId = that.attr("data-id");
					var fieldName = that.attr("fieldName");
					var fieldType = that.attr("fieldType");
					var fieldComponent = that.attr("fieldComponent");
					var isShow = that.attr("isShow");
					var isRequire = that.attr("isRequire");

					var $fieldName = $("#field_name");
					var $fieldType = $("#field_type");
					var $fieldComponent = $("#field_component");
					var $isShow = $("#is_show");
					var $isRequire = $("#is_require");

					$fieldName.val(fieldName);
					$fieldType.find("option[value='"+fieldType+"']").attr("selected",true);
					$fieldComponent.find("option[value='"+fieldComponent+"']").attr("selected",true);
					
					if(isShow == 1)
						$isShow.attr("checked", true);
					else
						$isShow.attr("checked", false);
					if(isRequire == 1)
						$isRequire.attr("checked", true);
					else
						$isRequire.attr("checked", false);
					layer.open({
						title : '编辑合同套餐',
						btn : [ '是', '否' ],
						type : 1,
						// skin: 'layui-layer-rim', //加上边框
						// area: ['620px', 'auto'], //宽高
						area : "35%",
						content : $('#tab2'),
						yes : function(index, layerno) {
							if (!$fieldName.val()) {
								layer.msg("亲，合同字段名称不能为空！", {
									icon : 2
								});
								return;
							}
							var data = {};
							data.fieldName = $fieldName.val();
							data.fieldType = $fieldType.val();
							data.fieldComponent = $fieldComponent.val();
							if($isShow.attr("checked"))
								data.isShow = 1;
							else
								data.isShow = 0;
							
							if($isRequire.attr("checked"))
								data.isRequire = 1;
							else
								data.isRequire = 0;
							submitForm(JSON.stringify(data),
									"/contractfieldedit/" + fieldId);
						},
						btn2 : function(index, layerno) {
							layer.msg("取消了", {
								icon : 2
							});
						}
					});

				});

		// 3.添加
		$("#btn_contractfield_add").click(function() {
			
			var fieldName = $("#fieldName").val();
			if (!fieldName) {
				layer.msg("亲，合同字段名称不能为空！", {
					icon : 2
				});
				return;
			}
			var fieldType = $("#fieldType").val();
			var fieldComponent = $("#fieldComponent").val();
			var isShow = 0;
			if($("#isShow").attr("checked"))
				isShow = 1;
			var isRequire = 0;
			if($("#isRequire").attr("checked"))
				isRequire = 1;
			var data = {};
			data.fieldName = fieldName;
			data.fieldType = fieldType;
			data.fieldComponent = fieldComponent;
			data.isShow = isShow;
			data.isRequire = isRequire;
			
			submitForm(JSON.stringify(data), "/contractfieldadd");
		});
	};
	function deleteContractField(fieldId) {
		$.ajax({
			type : "DELETE",
			dataType : "json",
			url : "/contractfielddelete/" + fieldId,
		// contentType: "application/json;charset=utf-8",
		// data : "roleId="+roleId
		// ,beforeSend : beginWait("研判报告导出中，请稍稍后。。。")
		// ,complete : clearWait()
		}).done(function(data) {
			if (data && data.retCode == 1) {
				layer.msg("恭喜您，删除成功！", {
					icon : 1
				});
//				setTimeout("window.location.reload();", 800)
				setTimeout('$("#contractfieldtable").DataTable().ajax.reload();', 800)

			} else {
				var s = layer.msg("对不起，删除失败！", {
					icon : 2
				});
				layer.close(s);
			}

		}).fail(function(d) {
			layer.msg("服务器无响应", {
				icon : 2
			});

		});
	}
	
	function orderContractField(fieldId) {
		$.ajax({
			type : "POST",
			dataType : "json",
			url : "/contractfieldup/" + fieldId,
		// contentType: "application/json;charset=utf-8",
		// data : "roleId="+roleId
		// ,beforeSend : beginWait("研判报告导出中，请稍稍后。。。")
		// ,complete : clearWait()
		}).done(function(data) {
			if (data && data.retCode == 1) {
				layer.msg("恭喜您，排序成功！", {
					icon : 1
				});
//				setTimeout("window.location.reload();", 800)
				setTimeout('$("#contractfieldtable").DataTable().ajax.reload();', 800)

			} else {
				var s = layer.msg("对不起，排序失败！", {
					icon : 2
				});
				layer.close(s);
			}

		}).fail(function(d) {
			layer.msg("服务器无响应", {
				icon : 2
			});

		});
	}
	/**
	 * 提交表单
	 * 
	 * @param data
	 */
	function submitForm(data, url) {
		$.ajax({
			type : "POST",
			dataType : "json",
			url : url,
			contentType : "application/json;charset=utf-8",
			data : data
		// ,beforeSend : beginWait("研判报告导出中，请稍稍后。。。")
		// ,complete : clearWait()
		}).done(function(data) {
			if (data && data.retCode == 1) {
				var l = layer.msg(data.retMsg, {
					icon : 1
				});
				setTimeout('layer.closeAll();$("#contractfieldtable").DataTable().ajax.reload();', 1000)

			} else {
				var s = layer.msg("对不起，添加失败！", {
					icon : 3
				});
				layer.close(s);
			}

		}).fail(function(d) {
			layer.msg("服务器无响应");

		});
	}

	return {
		init : function() {
			initTable();
			event();
		}
	};

}();