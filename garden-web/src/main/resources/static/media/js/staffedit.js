var StaffEdit = function() {

	// 日期选择框
	var handleDatePickers = function() {

		if (jQuery().datepicker) {
			$('.date-picker').datepicker({
				rtl : App.isRTL(),
				language : 'zh-CN',
				autoclose : true,
				todayHighlight : true,
				format : 'yyyy-mm-dd'
			});
		}
	}
	// 树控件
	var handlePermissionTree = function(id) {
		var setting = {
			view : {
				showIcon : false
			},
			check : {
				// enable: true
				chkDisabledInherit : true,
			// chkDisabled:true

			// ,chkboxType: { "Y": "p", "Y": "s" }
			},
			data : {
				simpleData : {
					enable : true
				}
			}
		};
		// 编辑的Tree
		$.ajax({
			type : "GET",
			dataType : "json",
			url : "/getRoleTreeData/" + id
		}).done(function(data) {
			$.fn.zTree.init($("#treeEdit"), setting, data);

		}).fail(function(d) {
			layer.msg("服务器无响应");
		});

	};

	var event = function() {
		$(".permission a").click(function(e) {
			e.preventDefault();
			var roleId = $(this).attr("data-id");
			handlePermissionTree(roleId);
			layer.open({
				title : '查看权限',
				// btn : [ '是', '否' ],
				type : 1,
				skin : 'layui-layer-lan', // 加上边框
				// area: ['620px', 'auto'], //宽高
				area : [ "40%", "80%" ],
				content : $('#per_tree'),
				scrollbar : false
			});
		});
	}
	var formValid = function() {
		// 手机号码验证
		jQuery.validator
				.addMethod(
						"isMobile",
						function(value, element) {
							var length = value.length;
							var mobile = /^(13[0-9]{9})|(18[0-9]{9})|(14[0-9]{9})|(17[0-9]{9})|(15[0-9]{9})$/;
							return this.optional(element)
									|| (length == 11 && mobile.test(value));
						}, "请正确填写您的手机号码");

		var form = $('#staff_edit_form');
		var error = $('.alert-error', form);
		var success = $('.alert-success', form);

		form.validate({
			debug : true,
			doNotHideMessage : true,
			errorElement : 'span', 
			errorClass : 'validate-inline', 
			focusInvalid : false,
			// onsubmit:true,
			// onfocusout:true,
			onfocusout : function(element) {
				$(element).valid()
			},
			// onkeyup:true,
			onkeyup : function(element) {
				$(element).valid()
			},
			// onclick:true,
			rules : {
				userName : {
					required : true,
					remote : "/usernamecheck"
				},
				staffEmail : {
					required : true,
					email : true
				},
				staffRealName : {
					required : true
				},
				staffPhone : {
					required : true,
					minlength : 11,
					isMobile : true
				}
			},
			messages : {
				userName : {
					required : "登录系统用户名不能为空.",
					remote : "用户名已存在，请更换！"
				},
				staffEmail : {
					required : "Email不能为空",
					email : true
				},
				staffRealName : {
					required : "真实姓名不能为空"
				},
				staffPhone : {
					required : "请输入手机号",
					minlength : "确认手机不能小于11个字符",
					isMobile : "请正确填写您的手机号码"
				}
			},
			invalidHandler : function(event, validator) {
				success.hide();
				error.show();
			},

			highlight : function(element) { 
				$(element).closest('.help-inline').removeClass('ok');
				
				$(element).closest('.control-group').removeClass('success')
						.addClass('error');
			},

			unhighlight : function(element) { 
				$(element).closest('.control-group').removeClass('error');
			},

			success : function(label) {
				label.addClass('valid ok').closest('.control-group').removeClass('error').addClass(
						'success');
			},

			submitHandler : function(form) {
				success.show();
				error.hide();
			}

		});
	};
	return {

		init : function() {
			handleDatePickers();
			event();
			formValid();
		}

	};

}();