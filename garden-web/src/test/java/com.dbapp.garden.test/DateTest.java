package com.dbapp.garden.test;

import com.dbapp.garden.common.WeekUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author by gangzi on 2017/11/17.
 */
public class DateTest {

    public static void main(String[] args){
        int year = 2017;
        int week = 52;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date today = new Date();
        Calendar c = new GregorianCalendar();
        c.setTime(today);
//        System.out.println("current date = " + sdf.format(today));
//        System.out.println("getWeekOfYear = " + getWeekOfYear(today));
//        System.out.println("getMaxWeekNumOfYear = " + getMaxWeekNumOfYear(year));
        System.out.println("getFirstDayOfWeek = " + sdf.format(WeekUtils.getFirstDayOfWeek(year, week)));
        System.out.println("getLastDayOfWeek = " + sdf.format(WeekUtils.getLastDayOfWeek(year, week)));
//        System.out.println("getFirstDayOfWeek = " + sdf.format(getFirstDayOfWeek(today)));
//        System.out.println("getLastDayOfWeek = " + sdf.format(getLastDayOfWeek(today)));
    }


}
