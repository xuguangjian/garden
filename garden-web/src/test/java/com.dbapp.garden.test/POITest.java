package com.dbapp.garden.test;

import com.dbapp.garden.common.ExcelUtils;
import com.dbapp.garden.entity.GardenStudentInfo;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

public class POITest {

    public static void main(String[] args) {

        Workbook wb = null;

//        System.out.println(String.valueOf(null));
        try {
            wb = new XSSFWorkbook(new FileInputStream("/Users/gangzi/Desktop/student_2017-11-15 17-09-15.xlsx"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<GardenStudentInfo> list= ExcelUtils.getDataFromExcel(wb);

        System.out.println("学员总数："+list.toString());

    }

    /**
     * 功能:处理单元格中值得类型
     * @param cell
     * @return
     */
//    public static String getCellValueFormula(Cell cell, FormulaEvaluator formulaEvaluator) {
//        if (cell == null || formulaEvaluator == null) {
//            return null;
//        }
//        cell.getCellType();
//
//        if (cell.getCellType() == Cell.CELL_TYPE_FORMULA) {
//            // return
//            // String.valueOf(formulaEvaluator.evaluate(cell).getNumberValue());
//            return String.valueOf(formulaEvaluator.evaluate(cell).getStringValue());
//        }
//        return getCellValue(cell);
//    }


//    public static String getCellValue(Cell cell) {
//        if (cell == null) {
//            return null;
//        }
//
//        switch (cell.getCellType()) {
//            case Cell.CELL_TYPE_STRING:
//                return cell.getRichStringCellValue().getString().trim();
//            case Cell.CELL_TYPE_NUMERIC:
//                if (DateUtil.isCellDateFormatted(cell)) {
//                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");// 非线程安全
//                    return sdf.format(cell.getDateCellValue());
//                } else {
//                    return String.valueOf(cell.getNumericCellValue());
////				return String.valueOf(cell.getStringCellValue());
//                }
//            case Cell.CELL_TYPE_BOOLEAN:
//                return String.valueOf(cell.getBooleanCellValue());
//            case Cell.CELL_TYPE_FORMULA:
//                return cell.getCellFormula();
//            default:
//                return null;
//        }
//    }

}




