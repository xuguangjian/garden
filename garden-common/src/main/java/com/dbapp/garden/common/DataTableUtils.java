package com.dbapp.garden.common;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class DataTableUtils {
	public static Map<String, Object> parseParam(String param){
		if(StringUtils.isBlank(param)) {
			return null;
		}
		
		JSONArray array = JSONArray.parseArray(param);
		
		String sEcho = "";
		String sSearch = "";
		int iDisplayStart = 0;
		int iDisplayLength = 0;
		for(int i = 0; i<array.size(); i++){
			JSONObject obj = (JSONObject) array.get(i);
			if(obj.get("name").equals("sEcho")){
				sEcho = obj.getString("value");				
			}
			
			if(obj.get("name").equals("iDisplayStart")){
				
				iDisplayStart = obj.getIntValue("value");
			}
			
			if(obj.get("name").equals("iDisplayLength")){
				iDisplayLength = obj.getIntValue("value");				
			}
			
			if(obj.get("name").equals("sSearch")){
				sSearch = obj.getString("value");				
			}
			
		}
		
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("sEcho", sEcho);
		result.put("sSearch", sSearch);
		result.put("iDisplayStart", iDisplayStart);
		result.put("iDisplayLength", iDisplayLength);
		
		return result;
	}
}
