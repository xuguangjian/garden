package com.dbapp.garden.common.com.dbapp.garden.common.exception;

/**
 * @author by gangzi on 2017/11/11.
 */
public class BusinessException extends RuntimeException {

    public BusinessException() {
        super();
    }

    public BusinessException(String message) {

        super(message);
    }
}
