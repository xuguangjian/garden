package com.dbapp.garden.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author by gangzi on 2017/11/15.
 */
public class PhoneNumberUtils {
    /**
     * 校验手机号
     * 移动：134、135、136、137、138、139、150、151、157(TD)、158、159、187、188
     * 联通：130、131、132、152、155、156、185、186
     * 电信：133、153、180、189、（1349卫通）
     * 其他：181 182 183
     *
     * @param phone
     * @return
     */
    private static final Pattern p = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[^4,\\D]))\\d{8}$");

    public static boolean isMobileNumber(String phone) {
        Matcher m = p.matcher(phone);
        return m.matches();
    }
}
