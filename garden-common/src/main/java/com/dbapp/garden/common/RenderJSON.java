package com.dbapp.garden.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;


public class RenderJSON {
	
	public static Map<String,?> makePagenateMap(int code, String mssage, String sEcho, int iTotalRecords, int iTotalDisplayRecords, List<?> aaData, String redirect){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("sEcho", sEcho);
		map.put("iTotalRecords", iTotalRecords);
		map.put("iTotalDisplayRecords", iTotalDisplayRecords);
		map.put("aaData", aaData);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("retCode", code);
		result.put("retMsg", mssage);
		result.put("retObj", map);
		result.put("retRedirect", redirect);
		
		return result;
	}

	public static Map<String, ?> makeMap(int code, String message, Object ret,
			String redirect) {
		Map<String, Object> result = new HashMap<String, Object>();

		result.put("retCode", code);
		result.put("retMsg", message);
		result.put("retObj", ret);
		result.put("retRedirect", redirect);

		return result;
	}


	public static Map<String, ?> makeStandMap(int code, String msg, Object ret) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("retCode", code);
		result.put("retMsg", msg);
		result.put("retObj", ret);
		return result;
	}

	/**
	 * 检查必须的参数
	 * 
	 * @param token
	 * @param phoneType
	 * @param version
	 * @return
	 */
	public static boolean checkMastParam(String version) {
		if (StringUtils.isBlank(version))
			return false;

		return true;
	}

	/**
	 * 返回page视图
	 * 
	 * @param data
	 * @param page
	 * @return
	 */
	// public static ModelAndView outPage(Map<String, ?> data, String page) {
	// ModelAndView mv = new ModelAndView();
	// if (data != null) {
	// Iterator iter = data.keySet().iterator();
	// while (iter.hasNext()) {
	// Map.Entry entry = (Entry) iter.next();
	// String key = (String) entry.getKey();
	// Object value = entry.getValue();
	// mv.addObject(key, value);
	// }
	// }
	// mv.setViewName(page);
	//
	// return mv;
	// }
}
