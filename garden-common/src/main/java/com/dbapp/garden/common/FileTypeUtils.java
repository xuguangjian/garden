package com.dbapp.garden.common;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author  by gangzi on 2017/11/11.
 */
public class FileTypeUtils {

    public static final Set<String> EXCEL_FILE_TYPE=new LinkedHashSet<>();

    static {
        EXCEL_FILE_TYPE.add("xls");
        EXCEL_FILE_TYPE.add("xlsx");
    }
}
