package com.dbapp.garden.service;

import java.util.List;

import com.dbapp.garden.entity.GardenMemberCardTypeInfo;

public interface IGardenMemberCardTypeInfoService {

	/**
	 * 获取会员卡类型列表
	 * @param gardenId
	 * @param search
	 * @param start
	 * @param limit
	 * @return
	 */
	public List<GardenMemberCardTypeInfo> getMemberCardTypeList(int gardenId, String search, int start, int limit);
	
	/**
	 * 获取会员卡类型列表的总数
	 * @param gardenId
	 * @param search
	 * @return
	 */
	public int getMemberCardTypeCount(int gardenId, String search);
	
	/**
	 * 保存会员卡类型信息
	 * @param typeInfo
	 * @return
	 */
	public GardenMemberCardTypeInfo saveMemberCardType(GardenMemberCardTypeInfo typeInfo);
	
	/**
	 * 删除会员卡类型
	 * @param id
	 * @return
	 */
	public int deleteMemberCardTypeInfo(int id);
	
	/**
	 * 根据id查找会员卡类型
	 * @param id
	 * @return
	 */
	public GardenMemberCardTypeInfo findById(int id);
}
