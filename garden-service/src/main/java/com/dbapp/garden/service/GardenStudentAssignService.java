package com.dbapp.garden.service;

import com.dbapp.garden.entity.GardenStudentAssign;

import java.util.List;
import java.util.Map;

/**
 * @author  by gangzi on 2017/11/8.
 */
public interface GardenStudentAssignService {
    /**
     * 保存
     * @param assign
     * @return
     */
    Integer save(GardenStudentAssign assign);

    /**
     * 获取分配记录列表
     * @param paramMap
     * @return
     */
    List<GardenStudentAssign> getStudentAssignList(Map<String, Object> paramMap);

    /**
     * 获取分配记录数量
     * @param paramMap
     * @return
     */
    Integer getStudentAssignCount(Map<String, Object> paramMap);
}
