package com.dbapp.garden.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbapp.garden.entity.GardenDepartment;
import com.dbapp.garden.mapper.GardenDepartmentMapper;
import com.dbapp.garden.service.GardenDepartmentService;

@Service
public class GardenDepartmentServiceImpl implements GardenDepartmentService {
	@Autowired
	private GardenDepartmentMapper deptMapper;

	@Override
	public List<GardenDepartment> getDepartmentList(Map<String, Object> paramMap) {
		if (paramMap == null) {
			return null;
		}
		return deptMapper.getDepartmentList(paramMap);
	}

	@Override
	public GardenDepartment findById(Integer id) {
		if (id == null) {
			return null;
		}
		return deptMapper.selectByPrimaryKey(id);
	}

	@Override
	public Integer getDepartmentCount(Map<String, Object> paramMap) {
		if (paramMap == null) {
			return null;
		}
		return deptMapper.getDepartmentCount(paramMap);
	}

	@Override
	public Integer save(GardenDepartment department) {
		if (department == null) {
			return null;
		}
		if (department.getId() == null) {
			return deptMapper.insertSelective(department);
		} else {
			return deptMapper.updateByPrimaryKeySelective(department);
		}
	}

	@Override
	public Integer delete(Integer id) {
		if (id == null) {
			return null;
		}
		return deptMapper.deleteByPrimaryKey(id);
	}
}
