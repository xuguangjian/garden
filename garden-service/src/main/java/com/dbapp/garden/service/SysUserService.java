package com.dbapp.garden.service;

import java.util.List;
import java.util.Map;

import com.dbapp.garden.entity.SysUser;

public interface SysUserService {

	/**
	 * 增加用户
	 * @param user
	 * @return
	 */
	Integer saveUser(SysUser user);

	/**
	 * 根据用户名查询对应的用户
	 * @param userName
	 * @return
	 */
	SysUser findUserByUserName(String userName);

	List<SysUser> getUserList(Map<String, Object> paramMap);

	Integer getUserCount(Map<String, Object> paramMap);

	/**
	 * 删除该用户，注意要同时删除用户角色对应关系表
	 * @param id
	 * @return
	 */
	Integer delete(Integer id);

	SysUser findUserById(Integer id);

}
