package com.dbapp.garden.service;

import java.util.List;
import java.util.Map;

import com.dbapp.garden.entity.GardenStudentContact;

public interface GardenStudentContactService {

	List<GardenStudentContact> getStudentContactList(Map<String, Object> paramMap);

	Integer getStudentContactCount(Map<String, Object> paramMap);

	Integer save(GardenStudentContact stuContact);

	Integer delete(Integer id);

}
