package com.dbapp.garden.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbapp.garden.entity.GardenStudentFollow;
import com.dbapp.garden.mapper.GardenStudentFollowMapper;
import com.dbapp.garden.service.GardenStudentFollowService;

@Service
public class GardenStudentFollowServiceImpl implements GardenStudentFollowService {

	@Autowired
	private GardenStudentFollowMapper stuFollowMapper;

	@Override
	public List<GardenStudentFollow> getStudentFollowList(Map<String, Object> paramMap) {
		if (paramMap == null) {
			return null;
		}
		return stuFollowMapper.getStudentFollowList(paramMap);
	}

	@Override
	public Integer getStudentFollowCount(Map<String, Object> paramMap) {
		if(paramMap==null){
			return null;
		}
		return stuFollowMapper.getStudentFollowCount(paramMap);
	}

	@Override
	public Integer save(GardenStudentFollow stuFollow) {
		if(stuFollow==null){
			return null;
		}
		if(stuFollow.getId()==null){
			return stuFollowMapper.insertSelective(stuFollow);
		}else{
			return stuFollowMapper.updateByPrimaryKeySelective(stuFollow);
		}
	}

	@Override
	public Integer delete(Integer id) {
		if(id==null){
			return null;
		}
		return stuFollowMapper.deleteByPrimaryKey(id);
	}

}
