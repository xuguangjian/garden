package com.dbapp.garden.service;

import java.util.List;

import com.dbapp.garden.entity.GardenPayWayInfo;

public interface IGardenPayWayInfoService {

	/**
	 * 获取支付方式列表
	 * @param gardenId
	 * @param search
	 * @param start
	 * @param limit
	 * @return
	 */
	public List<GardenPayWayInfo> getPayWayList(int gardenId, String search, int start, int limit);
	
	/**
	 * 获取支付方式列表的总数
	 * @param gardenId
	 * @param search
	 * @return
	 */
	public int getPayWayCount(int gardenId, String search);
	
	/**
	 * 删除支付方式
	 * @param id
	 * @return
	 */
	public int deletePayWay(int id);
	
	/**
	 * 根据id查找
	 * @param id
	 * @return
	 */
	public GardenPayWayInfo findById(int id);
	
	/**
	 * 保存支付方式
	 * @param payWay
	 * @return
	 */
	public GardenPayWayInfo savePayWay(GardenPayWayInfo payWay);
}
