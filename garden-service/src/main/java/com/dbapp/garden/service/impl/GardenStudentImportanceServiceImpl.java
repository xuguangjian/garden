package com.dbapp.garden.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbapp.garden.entity.GardenStudentImportance;
import com.dbapp.garden.mapper.GardenStudentImportanceMapper;
import com.dbapp.garden.service.GardenStudentImportanceService;

@Service
public class GardenStudentImportanceServiceImpl implements GardenStudentImportanceService {

	@Autowired
	private GardenStudentImportanceMapper stuImportanceMapper;

	@Override
	public List<GardenStudentImportance> getStudentImportanceList(Map<String, Object> paramMap) {
		if(paramMap==null){
			return null;
		}
		return stuImportanceMapper.getStudentImportanceList(paramMap);
	}

	@Override
	public Integer getStudentImportanceCount(Map<String, Object> paramMap) {
		if(paramMap==null){
			return null;
		}
		return stuImportanceMapper.getStudentImportanceCount(paramMap);
	}

	@Override
	public Integer save(GardenStudentImportance stuImportance) {
		if(stuImportance==null){
			return null;
		}
		if(stuImportance.getId()==null){
			return stuImportanceMapper.insertSelective(stuImportance);
		}else{
			return stuImportanceMapper.updateByPrimaryKeySelective(stuImportance);
		}
	}

	@Override
	public Integer delete(Integer id) {
		if(id==null){
			return null;
		}
		GardenStudentImportance importance=stuImportanceMapper.selectByPrimaryKey(id);
		if(importance==null){
			return null;
		}
		return stuImportanceMapper.deleteByPrimaryKey(id);
	}

}
