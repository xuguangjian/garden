package com.dbapp.garden.service;

import com.dbapp.garden.entity.GardenClassCourseStudent;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/19.
 */
public interface GardenClassCourseStudentService {
    /**
     * 获取列表
     * @param paramMap
     * @return
     */
    List<GardenClassCourseStudent> getClassStudentList(Map<String, Object> paramMap);

    /**
     * 获取数量
     * @param paramMap
     * @return
     */
    Integer getClassStudentCount(Map<String, Object> paramMap);

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    GardenClassCourseStudent findById(Integer id);
}
