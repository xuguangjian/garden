package com.dbapp.garden.service;

import java.util.List;
import java.util.Map;

import com.dbapp.garden.entity.GardenStudentSource;

public interface GardenStudentSourceService {

	/**
	 * 根据条件查询学生来源
	 * @param paramMap
	 * @return
	 */
	List<GardenStudentSource> getStudentSourceList(Map<String, Object> paramMap);

	/**
	 * 根据条件查询学生来源数量
	 * @param paramMap
	 * @return
	 */
	Integer getStudentSourceCount(Map<String, Object> paramMap);

	/**
	 * 添加学员来源
	 * @param stuSource
	 * @return
	 */
	Integer save(GardenStudentSource stuSource);

	/**
	 * 删除
	 * @param id
	 * @return
	 */
	Integer delete(Integer id);

}
