package com.dbapp.garden.service;

import com.dbapp.garden.entity.GardenImportBatch;

import java.util.List;
import java.util.Map;

/**
 * @author  by gangzi on 2017/11/7.
 */
public interface GardenImportBatchService {
    /**
     * 获取导入批次列表
     * @param paramMap
     * @return
     */
    List<GardenImportBatch> getBatchList(Map<String, Object> paramMap);

    /**
     * 获取导入批次的数量
     * @param paramMap
     * @return
     */
    Integer getBatchCount(Map<String, Object> paramMap);

    /**
     * 增加、修改
     * @param importBatch
     * @return
     */
    Integer save(GardenImportBatch importBatch);

    /**
     * 删除该批次
     * @param id
     * @return
     */
    Integer delete(Integer id);

    GardenImportBatch findById(Integer id);
}
