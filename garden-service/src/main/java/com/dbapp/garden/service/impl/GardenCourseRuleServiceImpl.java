package com.dbapp.garden.service.impl;

import com.dbapp.garden.entity.GardenCourseRule;
import com.dbapp.garden.mapper.GardenCourseRuleMapper;
import com.dbapp.garden.service.GardenCourseRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/13.
 */
@Service
public class GardenCourseRuleServiceImpl implements GardenCourseRuleService {

    @Autowired
    private GardenCourseRuleMapper courseRuleMapper;

    @Override
    public List<GardenCourseRule> getCourseRuleList(Map<String, Object> map) {
        if (map==null){
            return null;
        }
        return courseRuleMapper.getCourseRuleList(map);
    }

    @Override
    public GardenCourseRule findById(Integer id) {
        if (id==null){
            return null;
        }
        return courseRuleMapper.selectByPrimaryKey(id);
    }

    @Override
    public Integer save(GardenCourseRule rule) {
        if (rule==null){
            return null;
        }
        if (rule.getId()==null){
            return courseRuleMapper.insertSelective(rule);
        }else{
            return courseRuleMapper.updateByPrimaryKeySelective(rule);
        }
    }
}
