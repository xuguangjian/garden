package com.dbapp.garden.service;

import java.util.List;

import com.dbapp.garden.entity.GardenContractField;

public interface IGardenContractFieldService {

	/**
	 * 列出所有合同字段
	 * @param gardenId
	 * @return
	 */
	public List<GardenContractField> getGardenContractFields(int gardenId);
	
	/**
	 * 列出合同字段列表
	 * @param gardenId
	 * @param search
	 * @param start
	 * @param limit
	 * @return
	 */
	public List<GardenContractField> getGardenContractFields(int gardenId, String search, int start, int limit);
	
	/**
	 * 获取最大的排序号
	 * @param gardenId
	 * @return
	 */
	public int getMaxOrderField(int gardenId);
	
	/**
	 * 列表的总数
	 * @param gardenId
	 * @param search
	 * @return
	 */
	public int getGardenContractFieldCount(int gardenId, String search);
	
	/**
	 * 根据id查找
	 * @param id
	 * @return
	 */
	public GardenContractField findById(int id);
	
	/**
	 * 根据id删除
	 * @param id
	 * @return
	 */
	public int deleteContractField(int id);
	
	/**
	 * 保存
	 * @param field
	 * @return
	 */
	public GardenContractField saveContractField(GardenContractField field);
	
	/**
	 * 往上移动
	 * @param id
	 * @return
	 */
	public GardenContractField upOrder(int id);
}
