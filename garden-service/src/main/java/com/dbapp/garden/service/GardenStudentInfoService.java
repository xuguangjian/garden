package com.dbapp.garden.service;

import java.util.List;
import java.util.Map;

import com.dbapp.garden.entity.GardenCareStudentSignIn;
import com.dbapp.garden.entity.GardenStudentInfo;

public interface GardenStudentInfoService {

	/**
	 * 根据条件获取学生列表
	 * 
	 * @param paramMap
	 * @return
	 */
	List<GardenStudentInfo> getStudentList(Map<String, Object> paramMap);

	/**
	 * 根据条件获取学生数量
	 * 
	 * @param paramMap
	 * @return
	 */
	Integer getStudentCount(Map<String, Object> paramMap);

	/**
	 * 根据ID查询学生
	 * 
	 * @param id
	 * @return
	 */
	GardenStudentInfo findById(Integer id);

	/**
	 * 添加学员
	 * @param stuInfo
	 * @return
	 */
	Integer save(GardenStudentInfo stuInfo);

	/**
	 * 批量删除学员
	 * @param ids
	 * @return
	 */
	Integer batchDeleteStudent(Integer[] ids);


	/**
	 * 批量插入学生信息
	 * @param studentList
	 * @return
	 */
    Integer batchInsertStudent(List<GardenStudentInfo> studentList);


	/**
	 * 获取托班当天需要签到的学员列表
	 * @param paramMap
	 * @return
	 */
	List<GardenStudentInfo> getSignInStudentList(Map<String, Object> paramMap);

	/**
	 * 获取托班当天需要签到的学员的数量
	 * @param paramMap
	 * @return
	 */
	Integer getSignInStudentCount(Map<String, Object> paramMap);


	/**
	 * 根据学员ID,查询所有学员
	 * @param ids
	 * @return
	 */
    List<GardenStudentInfo> getStudentByIds(Integer[] ids);

	/**
	 * 批量更新学员信息
	 * @param readyToUpdate
	 * @return
	 */
	Integer batchUpdateStudent(List<GardenStudentInfo> readyToUpdate);
}
