package com.dbapp.garden.service.impl;

import com.dbapp.garden.entity.GardenCareStudentSignIn;
import com.dbapp.garden.mapper.GardenCareStudentSignInMapper;
import com.dbapp.garden.service.GardenCareStudentSignInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/14.
 */
@Service
public class GardenCareStudentSignInServiceImpl implements GardenCareStudentSignInService {

    @Autowired
   private GardenCareStudentSignInMapper signInMapper;

    @Override
    public Integer save(GardenCareStudentSignIn signIn) {
        if (signIn==null){
            return null;
        }
        if (signIn.getId()==null){
            return signInMapper.insertSelective(signIn);
        }else{
            return signInMapper.updateByPrimaryKeySelective(signIn);
        }
    }

    @Override
    public Integer batchInsert(List<GardenCareStudentSignIn> signInList) {
        if (signInList==null||signInList.isEmpty()){
            return null;
        }
        return signInMapper.batchInsert(signInList);
    }

    @Override
    public List<GardenCareStudentSignIn> getSignInRecordList(Map<String, Object> paramMap) {
        if (paramMap==null){
            return null;
        }
        return signInMapper.getSignInRecordList(paramMap);
    }

    @Override
    public Integer getSignInRecordCount(Map<String, Object> paramMap) {
        if (paramMap==null){
            return null;
        }
        return signInMapper.getSignInRecordCount(paramMap);
    }

    @Override
    public GardenCareStudentSignIn findById(Integer id) {
        if (id==null){
            return null;
        }

        return signInMapper.selectByPrimaryKey(id);
    }

    @Override
    public Integer delete(Integer id) {
        if (id==null){
            return null;
        }

        return signInMapper.deleteByPrimaryKey(id);
    }
}
