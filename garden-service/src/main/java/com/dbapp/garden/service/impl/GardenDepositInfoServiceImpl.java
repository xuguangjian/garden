package com.dbapp.garden.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbapp.garden.entity.GardenDepositInfo;
import com.dbapp.garden.entity.GardenDepositInfoExample;
import com.dbapp.garden.entity.GardenDepositInfoExample.Criteria;
import com.dbapp.garden.entity.GardenDepositInfoExtra;
import com.dbapp.garden.entity.GardenStudentInfo;
import com.dbapp.garden.entity.GardenStudentState;
import com.dbapp.garden.mapper.GardenDepositInfoMapper;
import com.dbapp.garden.service.GardenStudentInfoService;
import com.dbapp.garden.service.IGardenDepositInfoService;

@Service
public class GardenDepositInfoServiceImpl implements IGardenDepositInfoService {

	@Autowired
	GardenDepositInfoMapper depositDao;
	
	@Autowired
	GardenStudentInfoService studentService;
	
	@Override
	public List<GardenDepositInfo> getDepositList(int gardenId, String search, int start, int limit) {
		// TODO Auto-generated method stub
		GardenDepositInfoExample query = new GardenDepositInfoExample();
		Criteria queryCondition = query.createCriteria();
		if(gardenId > 0)
			queryCondition.andGarden_idEqualTo(gardenId);
		
		if(start < 0)
			start = 0;
		if(limit < 0)
			limit = 10;
		query.setLimitStart(start);
		query.setLimitEnd(limit);
		return depositDao.selectByExample(query);
	}

	@Override
	public List<GardenDepositInfoExtra> getDepositExtratList(int gardenId, String search, int start, int limit) {
		// TODO Auto-generated method stub
		Map paramMap = new HashMap();
		paramMap.put("gardenId", gardenId);
		paramMap.put("start", start);
		paramMap.put("limit", limit);
		paramMap.put("search", search);
		
		return depositDao.selectForPage(paramMap);
	}
	
	@Override
	public int getDepositCount(int gardenId, String search) {
		// TODO Auto-generated method stub
		Map paramMap = new HashMap();
		paramMap.put("gardenId", gardenId);
		paramMap.put("search", search);
		
		return depositDao.selectForPageCount(paramMap);
	}

	@Override
	public GardenDepositInfo findById(int id) {
		// TODO Auto-generated method stub
		return depositDao.selectByPrimaryKey(id);
	}

	@Override
	public GardenDepositInfo saveDeposit(GardenDepositInfo depositInfo) {
		// TODO Auto-generated method stub
		if(depositInfo == null) return null;
		if(depositInfo.getId() == null)
			depositDao.insertSelective(depositInfo);
		else
			depositDao.updateByPrimaryKeySelective(depositInfo);
		
		GardenStudentInfo student = studentService.findById(depositInfo.getStudentId());
		if(student != null){
			student.setStuState(depositInfo.getStudentState());
			studentService.save(student);
		}
		return depositInfo;
	}

	@Override
	public int deleteDeposit(int id) {
		// TODO Auto-generated method stub
		return depositDao.deleteByPrimaryKey(id);
	}

	@Override
	public List<GardenStudentState> getStudentStateList(int gardenId) {
		// TODO Auto-generated method stub
		return depositDao.selectStudentStateForDeposit(gardenId);
	}

	@Override
	public String generateDepositNumber(int gardenId) {
		// TODO Auto-generated method stub
		int count = depositDao.selectCurrentDayCount(gardenId);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String result = "DJ" + sdf.format(new Date());
		String strCount = (count+1) + "";
		while(strCount.length() < 4)
			strCount += "0" + strCount;
		
		return result + strCount;
	}

	

}
