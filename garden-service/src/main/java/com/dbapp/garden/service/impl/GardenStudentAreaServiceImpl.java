package com.dbapp.garden.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbapp.garden.entity.GardenStudentArea;
import com.dbapp.garden.mapper.GardenStudentAreaMapper;
import com.dbapp.garden.service.GardenStudentAreaService;

@Service
public class GardenStudentAreaServiceImpl implements GardenStudentAreaService {

	@Autowired
	private GardenStudentAreaMapper stuAreaMapper;
	
	@Override
	public List<GardenStudentArea> getStudentAreaList(Map<String, Object> paramMap) {
		if(paramMap==null){
			return null;
		}
		return stuAreaMapper.getStudentAreaList(paramMap);
	}

	@Override
	public Integer getStudentAreaCount(Map<String, Object> paramMap) {
		if(paramMap==null){
			return null;
		}
		
		return stuAreaMapper.getStudentAreaCount(paramMap);
	}

	@Override
	public Integer save(GardenStudentArea stuArea) {
		if(stuArea==null){
			return null;
		}
		if(stuArea.getId()==null){
			return stuAreaMapper.insertSelective(stuArea);
		}else{
			return stuAreaMapper.updateByPrimaryKeySelective(stuArea);
		}
	}

	@Override
	public Integer delete(Integer id) {
		if(id==null){
			return null;
		}
		GardenStudentArea area=stuAreaMapper.selectByPrimaryKey(id);
		if(area==null){
			return null;
		}
		//如果是二级地区直接删除
		if(area.getAreaParentId()>0){
			return stuAreaMapper.deleteByPrimaryKey(id);
		}else{
			//如果是一级地区，则删除关联的二级地区
			stuAreaMapper.deleteByPrimaryKey(id);
			return stuAreaMapper.deleteByParentId(id);
		}
		
	}

}
