package com.dbapp.garden.service.impl;

import com.dbapp.garden.mapper.GardenStudentSuccessMapper;
import com.dbapp.garden.service.GardenStudentSuccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author by gangzi on 2017/11/16.
 */
@Service
public class GardenStudentSuccessImpl implements GardenStudentSuccess {

    @Autowired
    private GardenStudentSuccessMapper studentSuccessMapper;
}
