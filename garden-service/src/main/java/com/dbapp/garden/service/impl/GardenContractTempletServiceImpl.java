package com.dbapp.garden.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbapp.garden.entity.GardenContractTemplet;
import com.dbapp.garden.entity.GardenContractTempletExample;
import com.dbapp.garden.mapper.GardenContractTempletMapper;
import com.dbapp.garden.service.IGardenContractTempletService;

@Service
public class GardenContractTempletServiceImpl implements IGardenContractTempletService {

	@Autowired
	GardenContractTempletMapper templetDao;
	
	@Override
	public GardenContractTemplet findByGardenId(int gardenId) {
		// TODO Auto-generated method stub
		GardenContractTempletExample query = new GardenContractTempletExample();
		query.or().andGarden_idEqualTo(gardenId);
		List<GardenContractTemplet> results = templetDao.selectByExample(query);
		if(results == null || results.isEmpty())
			return null;
		
		return results.get(0);
	}

	@Override
	public GardenContractTemplet saveTemplet(GardenContractTemplet templetInfo) {
		// TODO Auto-generated method stub
		if(templetInfo == null) return null;
		if(templetInfo.getId() == null){
			templetInfo.setCreateDate(new Date());
			templetDao.insertSelective(templetInfo);
		}
		else
			templetDao.updateByPrimaryKeySelective(templetInfo);
		
		return templetInfo;
	}

	@Override
	public GardenContractTemplet findById(int id) {
		// TODO Auto-generated method stub
		return templetDao.selectByPrimaryKey(id);
	}

}
