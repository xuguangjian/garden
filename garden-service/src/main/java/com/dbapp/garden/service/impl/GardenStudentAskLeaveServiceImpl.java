package com.dbapp.garden.service.impl;

import com.dbapp.garden.entity.GardenStudentAskLeave;
import com.dbapp.garden.mapper.GardenStudentAskLeaveMapper;
import com.dbapp.garden.service.GardenStudentAskLeaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/19.
 */
@Service
public class GardenStudentAskLeaveServiceImpl implements GardenStudentAskLeaveService {

    @Autowired
    private GardenStudentAskLeaveMapper askLeaveMapper;

    @Override
    public Integer save(GardenStudentAskLeave leave) {
        if (leave == null) {
            return null;
        }
        if (leave.getId() == null) {
            return askLeaveMapper.insertSelective(leave);
        } else {
            return askLeaveMapper.updateByPrimaryKeySelective(leave);
        }
    }

    @Override
    public List<GardenStudentAskLeave> getAskLeaveList(Map<String, Object> paramMap) {
        if (paramMap==null){
            return null;
        }

       return askLeaveMapper.getAskLeaveList(paramMap);

    }

    @Override
    public Integer getAskLeaveCount(Map<String, Object> paramMap) {
        if (paramMap==null){
            return null;
        }

        return askLeaveMapper.getAskLeaveCount(paramMap);
    }
}
