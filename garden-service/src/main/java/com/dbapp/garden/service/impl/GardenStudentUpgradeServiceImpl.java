package com.dbapp.garden.service.impl;

import com.dbapp.garden.entity.GardenStudentUpgrade;
import com.dbapp.garden.mapper.GardenStudentUpgradeMapper;
import com.dbapp.garden.service.GardenStudentUpgradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/20.
 */
@Service
public class GardenStudentUpgradeServiceImpl implements GardenStudentUpgradeService {

    @Autowired
    private GardenStudentUpgradeMapper upgradeMapper;

    @Override
    public Integer save(GardenStudentUpgrade studentUpgrade) {
        if (studentUpgrade == null) {
            return null;
        }

        if (studentUpgrade.getId() == null) {
            return upgradeMapper.insertSelective(studentUpgrade);
        }else{
            return upgradeMapper.updateByPrimaryKeySelective(studentUpgrade);
        }
    }

    @Override
    public List<GardenStudentUpgrade> getStudentUpgradeList(Map<String, Object> paramMap) {
        if (paramMap == null) {
            return null;
        }

        return upgradeMapper.getStudentUpgradeList(paramMap);
    }

    @Override
    public Integer getStudentUpgradeCount(Map<String, Object> paramMap) {
        if (paramMap == null) {
            return null;
        }

        return upgradeMapper.getStudentUpgradeCount(paramMap);
    }

    @Override
    public Integer delete(Integer id) {
        if (id == null) {
            return null;
        }

        return upgradeMapper.deleteByPrimaryKey(id);
    }
}
