package com.dbapp.garden.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbapp.garden.entity.SysResource;
import com.dbapp.garden.mapper.SysResourceMapper;
import com.dbapp.garden.service.SysResourceService;
@Service
public class SysResourceServiceImpl implements SysResourceService {
	@Autowired
	private SysResourceMapper sysResourceMapper;

	@Override
	public List<SysResource> getUserResources(Integer userid) {
		if(userid==null){
			return null;
		}
		//获取当前用户拥有的资源
		List<SysResource> resourceList=sysResourceMapper.getUserResources(userid);
		
		return resourceList;
	}

	


	@Override
	public List<SysResource> findAllResources(Map<String,Object> map) {
		List<SysResource> resourceList=sysResourceMapper.findAll(map);
		return resourceList;
	}




	@Override
	public List<SysResource> findByRoleId(Integer roleId) {
		if(roleId==null){
			return null;
		}
		return sysResourceMapper.findByRoleId(roleId);
	}
}