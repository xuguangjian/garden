package com.dbapp.garden.service;

import java.util.List;
import java.util.Map;

import com.dbapp.garden.entity.GardenTeacherLevel;

public interface GardenTeacherLevelService {

	List<GardenTeacherLevel> getTeacherLevelList(Map<String, Object> paramMap);

	Integer getTeacherLevelCount(Map<String, Object> paramMap);

	Integer save(GardenTeacherLevel stuImportance);

	Integer delete(Integer id);

    GardenTeacherLevel findById(Integer id);
}
