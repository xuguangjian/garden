package com.dbapp.garden.service;

import com.dbapp.garden.entity.GardenCourse;

import java.util.List;
import java.util.Map;

/**
 * @author  by gangzi on 2017/11/11.
 */
public interface GardenCourseService {
    List<GardenCourse> getCourseList(Map<String, Object> paramMap);

    Integer getCourseCount(Map<String, Object> paramMap);

    Integer save(GardenCourse course);

    Integer delete(Integer id);

    GardenCourse findById(Integer id);
}
