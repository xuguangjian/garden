package com.dbapp.garden.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbapp.garden.entity.GardenContractField;
import com.dbapp.garden.entity.GardenContractFieldExample;
import com.dbapp.garden.entity.GardenContractPackageInfoExample;
import com.dbapp.garden.entity.GardenContractFieldExample.Criteria;
import com.dbapp.garden.mapper.GardenContractFieldMapper;
import com.dbapp.garden.service.IGardenContractFieldService;

@Service
public class GardenContractFieldServiceImpl implements IGardenContractFieldService {

	@Autowired
	GardenContractFieldMapper fieldDao;
	
	@Override
	public List<GardenContractField> getGardenContractFields(int gardenId) {
		// TODO Auto-generated method stub
		GardenContractFieldExample query = new GardenContractFieldExample();
		query.or().andGarden_idEqualTo(gardenId);
		query.setOrderByClause("fieldOrder asc");
		return fieldDao.selectByExample(query);
	}

	@Override
	public GardenContractField findById(int id) {
		// TODO Auto-generated method stub
		return fieldDao.selectByPrimaryKey(id);
	}

	@Override
	public int deleteContractField(int id) {
		// TODO Auto-generated method stub
		return fieldDao.deleteByPrimaryKey(id);
	}

	@Override
	public GardenContractField saveContractField(GardenContractField field) {
		// TODO Auto-generated method stub
		if(field == null) return null;
		if(field.getId() == null)
			fieldDao.insertSelective(field);
		else
			fieldDao.updateByPrimaryKeySelective(field);
		
		return field;
	}

	@Override
	public GardenContractField upOrder(int id) {
		// TODO Auto-generated method stub
		GardenContractField field = this.findById(id);
		if(field == null) return null;
		Integer fieldOrder = field.getFieldOrder();
		
		GardenContractFieldExample query = new GardenContractFieldExample();
		Criteria queryCondition = query.createCriteria();
		queryCondition.andGarden_idEqualTo(field.getGarden_id());
		queryCondition.andFieldOrderLessThan(field.getFieldOrder());
		query.setOrderByClause("fieldOrder desc");
		List<GardenContractField> fields = fieldDao.selectByExample(query);
		if(fields == null || fields.isEmpty())
			return field;
		
		GardenContractField upField = fields.get(0);
		field.setFieldOrder(upField.getFieldOrder());
		upField.setFieldOrder(fieldOrder);
		
		fieldDao.updateByPrimaryKeySelective(upField);
		fieldDao.updateByPrimaryKeySelective(field);
		return field;
	}

	@Override
	public List<GardenContractField> getGardenContractFields(int gardenId, String search, int start, int limit) {
		// TODO Auto-generated method stub
		GardenContractFieldExample query = new GardenContractFieldExample();
		Criteria queryCondition = query.createCriteria();
		if(gardenId > 0)
			queryCondition.andGarden_idEqualTo(gardenId);
		
		if(StringUtils.isNotBlank(search))
			queryCondition.andFieldNameLike("%"+search+"%");
		if(start < 0)
			start = 0;
		if(limit < 0)
			limit = 10;
		query.setLimitStart(start);
		query.setLimitEnd(limit);
		query.setOrderByClause("fieldOrder asc");
		return fieldDao.selectByExample(query);
	}

	@Override
	public int getGardenContractFieldCount(int gardenId, String search) {
		// TODO Auto-generated method stub
		GardenContractFieldExample query = new GardenContractFieldExample();
		Criteria queryCondition = query.createCriteria();
		if(gardenId > 0)
			queryCondition.andGarden_idEqualTo(gardenId);
		
		if(StringUtils.isNotBlank(search))
			queryCondition.andFieldNameLike("%"+search+"%");
		return (int) fieldDao.countByExample(query);
	}

	@Override
	public int getMaxOrderField(int gardenId) {
		// TODO Auto-generated method stub
		GardenContractFieldExample query = new GardenContractFieldExample();
		Criteria queryCondition = query.createCriteria();
		if(gardenId > 0)
			queryCondition.andGarden_idEqualTo(gardenId);
		query.setOrderByClause("fieldOrder desc");
		List<GardenContractField> results = fieldDao.selectByExample(query);
		if(results == null || results.isEmpty())
			return 0;
		
		return results.get(0).getFieldOrder();
	}

}
