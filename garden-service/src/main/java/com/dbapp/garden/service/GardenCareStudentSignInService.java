package com.dbapp.garden.service;

import com.dbapp.garden.entity.GardenCareStudentSignIn;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/14.
 */
public interface GardenCareStudentSignInService {

    Integer save(GardenCareStudentSignIn signIn);

    Integer batchInsert(List<GardenCareStudentSignIn> signInList);

    /**
     * 获取签到记录
     * @param paramMap
     * @return
     */
    List<GardenCareStudentSignIn> getSignInRecordList(Map<String, Object> paramMap);

    /**
     * 获取签到记录数量
     * @param paramMap
     * @return
     */
    Integer getSignInRecordCount(Map<String, Object> paramMap);

    GardenCareStudentSignIn findById(Integer id);

    Integer delete(Integer id);
}
