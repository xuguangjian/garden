package com.dbapp.garden.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbapp.garden.entity.SysRoleUser;
import com.dbapp.garden.mapper.SysRoleUserMapper;
import com.dbapp.garden.service.SysRoleUserService;

@Service
public class SysRoleUserServiceImpl implements SysRoleUserService {

	@Autowired
	private SysRoleUserMapper roleUserMapper;

	@Override
	public Integer save(SysRoleUser roleUser) {
		if (roleUser == null) {
			return null;
		}
		if (roleUser.getId() == null) {
			return roleUserMapper.insertSelective(roleUser);
		}
		return roleUserMapper.updateByPrimaryKeySelective(roleUser);
	}

	@Override
	public SysRoleUser findByUserId(Integer id) {
		if (id == null) {
			return null;
		}
		return roleUserMapper.findByUserId(id);
	}
}
