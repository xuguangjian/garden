package com.dbapp.garden.service;

import com.dbapp.garden.entity.GardenCourseComment;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/13.
 */
public interface GardenCourseCommentService {
    List<GardenCourseComment> getCourseCommentList(Map<String, Object> paramMap);

    Integer getCourseCommentCount(Map<String, Object> paramMap);

    Integer save(GardenCourseComment comment);

    Integer delete(Integer id);

    GardenCourseComment findById(Integer id);
}
