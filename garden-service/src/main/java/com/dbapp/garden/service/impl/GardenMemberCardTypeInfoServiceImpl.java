package com.dbapp.garden.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbapp.garden.entity.GardenMemberCardTypeInfo;
import com.dbapp.garden.entity.GardenMemberCardTypeInfoExample;
import com.dbapp.garden.entity.GardenMemberCardTypeInfoExample.Criteria;
import com.dbapp.garden.mapper.GardenMemberCardTypeInfoMapper;
import com.dbapp.garden.service.IGardenMemberCardTypeInfoService;

@Service
public class GardenMemberCardTypeInfoServiceImpl implements IGardenMemberCardTypeInfoService {

	@Autowired
	GardenMemberCardTypeInfoMapper typeDao;
	
	@Override
	public List<GardenMemberCardTypeInfo> getMemberCardTypeList(int gardenId, String search, int start, int limit) {
		// TODO Auto-generated method stub
		GardenMemberCardTypeInfoExample query = new GardenMemberCardTypeInfoExample();
		Criteria queryCondition = query.createCriteria();
		if(gardenId > 0)
			queryCondition.andGarden_idEqualTo(gardenId);
		
		if(StringUtils.isNotBlank(search))
			queryCondition.andTypeNameLike("%"+search+"%");
		
		if(start < 0)
			start = 0;
		if(limit < 0)
			limit = 10;
		query.setLimitStart(start);
		query.setLimitEnd(limit);
		return typeDao.selectByExample(query);
	}

	@Override
	public int getMemberCardTypeCount(int gardenId, String search) {
		// TODO Auto-generated method stub
		GardenMemberCardTypeInfoExample query = new GardenMemberCardTypeInfoExample();
		Criteria queryCondition = query.createCriteria();
		if(gardenId > 0)
			queryCondition.andGarden_idEqualTo(gardenId);
		
		if(StringUtils.isNotBlank(search))
			queryCondition.andTypeNameLike(search);
		
		return (int) typeDao.countByExample(query);
	}

	@Override
	public GardenMemberCardTypeInfo saveMemberCardType(GardenMemberCardTypeInfo typeInfo) {
		// TODO Auto-generated method stub
		if(typeInfo == null) return null;
		if(typeInfo.getId() == null)
			typeDao.insertSelective(typeInfo);
		else
			typeDao.updateByPrimaryKeySelective(typeInfo);
		
		return typeInfo;
	}

	@Override
	public int deleteMemberCardTypeInfo(int id) {
		// TODO Auto-generated method stub
		return typeDao.deleteByPrimaryKey(id);
	}

	@Override
	public GardenMemberCardTypeInfo findById(int id) {
		// TODO Auto-generated method stub
		return typeDao.selectByPrimaryKey(id);
	}

}
