package com.dbapp.garden.service.impl;

import com.dbapp.garden.entity.GardenClassCourse;
import com.dbapp.garden.mapper.GardenClassCourseMapper;
import com.dbapp.garden.service.GardenClassCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/18.
 */
@Service
public class GardenClassCourseServiceImpl implements GardenClassCourseService {

    @Autowired
    private GardenClassCourseMapper classCourseMapper;


    @Override
    public List<GardenClassCourse> getClassCourseList(Map<String, Object> query) {
        if (query==null){
            return null;
        }
        return classCourseMapper.getClassCourseList(query);
    }

    @Override
    public GardenClassCourse findById(Integer classId) {
        if (classId==null){
            return null;
        }

        return classCourseMapper.selectByPrimaryKey(classId);
    }
}
