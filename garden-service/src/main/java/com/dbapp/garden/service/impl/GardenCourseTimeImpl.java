package com.dbapp.garden.service.impl;

import com.dbapp.garden.entity.GardenCourseTime;
import com.dbapp.garden.mapper.GardenCourseTimeMapper;
import com.dbapp.garden.service.GardenCourseTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/13.
 */

@Service
public class GardenCourseTimeImpl implements GardenCourseTimeService {

    @Autowired
    private GardenCourseTimeMapper timeMapper;

    @Override
    public List<GardenCourseTime> getCourseTimeList(Map<String, Object> paramMap) {
        if (paramMap==null){
            return  null;
        }

        return timeMapper.getCourseTimeList(paramMap);
    }

    @Override
    public Integer getCourseCount(Map<String, Object> paramMap) {
        if (paramMap==null){
            return  null;
        }

        return timeMapper.getCourseTimeCount(paramMap);
    }

    @Override
    public Integer save(GardenCourseTime courseTime) {
        if (courseTime==null){
            return null;
        }
        if (courseTime.getId()==null){
            return timeMapper.insertSelective(courseTime);
        }else{
            return timeMapper.updateByPrimaryKeySelective(courseTime);
        }
    }

    @Override
    public Integer delete(Integer id) {
        if (id==null){
            return null;
        }
        return timeMapper.deleteByPrimaryKey(id);
    }

    @Override
    public GardenCourseTime findById(Integer id) {
        if (id==null){
            return null;
        }
        return timeMapper.selectByPrimaryKey(id);
    }
}
