package com.dbapp.garden.service;

import com.dbapp.garden.entity.GardenSmallTicketInfo;

public interface IGardenSmallTicketInfoService {

	/**
	 * 根据园id找出小票信息
	 * @param gardenId
	 * @return
	 */
	public GardenSmallTicketInfo findByGardenId(int gardenId);
	
	/**
	 * 保存小票信息
	 * @param ticketInfo
	 * @return
	 */
	public GardenSmallTicketInfo saveSmallTicket(GardenSmallTicketInfo ticketInfo);
	
	/**
	 * 根据id查找
	 * @param id
	 * @return
	 */
	public GardenSmallTicketInfo findById(int id);
}
