package com.dbapp.garden.service;

import com.dbapp.garden.entity.GardenClassroom;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/12.
 */
public interface GardenClassroomService {
    List<GardenClassroom> getClassroomList(Map<String, Object> paramMap);

    Integer getClassroomCount(Map<String, Object> paramMap);

    Integer save(GardenClassroom room);

    Integer delete(Integer id);

    GardenClassroom findById(Integer id);
}
