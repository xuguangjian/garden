package com.dbapp.garden.service;

import java.util.List;
import java.util.Map;

import com.dbapp.garden.entity.GardenStudentState;

public interface GardenStudentStateService {

	/**
	 * 获取学员状态列表
	 * @param paramMap
	 * @return
	 */
	List<GardenStudentState> getStudentStateList(Map<String, Object> paramMap);

	Integer getStudentStateCount(Map<String, Object> paramMap);

	Integer save(GardenStudentState stuState);

	Integer delete(Integer id);

}
