package com.dbapp.garden.service;

import java.util.List;
import java.util.Map;

import com.dbapp.garden.entity.GardenContractPackageInfo;

public interface IGardenContractPackageInfoService {

	/**
	 * 获取表格列表
	 * @param search
	 * @param start
	 * @param limit
	 * @return
	 */
	public List<GardenContractPackageInfo> getContractPackageList(int gardenId, String search, int start, int limit);
	
	/**
	 * 获取表格列表的总数
	 * @param search
	 * @return
	 */
	public int getContractPackageCount(int gardenId,String search);
	
	/**
	 * 保存合同套餐信息
	 * @param contractPackage
	 * @return
	 */
	public GardenContractPackageInfo saveContractPackage(GardenContractPackageInfo contractPackage);
	
	/**
	 * 删除合同套餐
	 * @param id
	 */
	public int deleteContractPackageInfo(int id);
	
	/**
	 * 根据合同套餐id获取合同套餐信息
	 * @param id
	 * @return
	 */
	public GardenContractPackageInfo findById(int id);
}
