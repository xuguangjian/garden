package com.dbapp.garden.service;

import java.util.List;
import java.util.Map;

import com.dbapp.garden.entity.GardenStudentImportance;

public interface GardenStudentImportanceService {

	List<GardenStudentImportance> getStudentImportanceList(Map<String, Object> paramMap);

	Integer getStudentImportanceCount(Map<String, Object> paramMap);

	Integer save(GardenStudentImportance stuImportance);

	Integer delete(Integer id);

}
