package com.dbapp.garden.service;

import com.dbapp.garden.entity.GardenClassCourse;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/18.
 */
public interface GardenClassCourseService {
    /**
     * 根据条件获取排课列表
     * @return
     * @param query
     */
    List<GardenClassCourse> getClassCourseList(Map<String, Object> query);

    /**
     * 根据ID获取排课
     * @param classId
     * @return
     */
    GardenClassCourse findById(Integer classId);
}
