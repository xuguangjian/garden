package com.dbapp.garden.service;

import com.dbapp.garden.entity.GardenContractTemplet;

public interface IGardenContractTempletService {

	/**
	 * 根据园id找出合同模板
	 * @param gardenId
	 * @return
	 */
	public GardenContractTemplet findByGardenId(int gardenId);
	
	/**
	 * 保存模板信息
	 * @param templetInfo
	 * @return
	 */
	public GardenContractTemplet saveTemplet(GardenContractTemplet templetInfo);
	
	/**
	 * 根据id查找
	 * @param id
	 * @return
	 */
	public GardenContractTemplet findById(int id);
}
