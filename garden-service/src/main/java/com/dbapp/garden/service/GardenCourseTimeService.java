package com.dbapp.garden.service;

import com.dbapp.garden.entity.GardenCourseTime;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/13.
 */
public interface GardenCourseTimeService {
    
    List<GardenCourseTime> getCourseTimeList(Map<String, Object> paramMap);

    Integer getCourseCount(Map<String, Object> paramMap);

    Integer save(GardenCourseTime courseTime);

    Integer delete(Integer id);

    GardenCourseTime findById(Integer id);
}
