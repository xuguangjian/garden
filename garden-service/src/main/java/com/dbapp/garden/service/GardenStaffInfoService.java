package com.dbapp.garden.service;

import java.util.List;
import java.util.Map;

import com.dbapp.garden.entity.GardenStaffInfo;

public interface GardenStaffInfoService {

	GardenStaffInfo findById(Integer id);

	List<GardenStaffInfo> getStaffList(Map<String, Object> paramMap);

	Integer getStaffCount(Map<String, Object> paramMap);

	Integer save(GardenStaffInfo gardenStaffInfo);

	Integer delete(Integer id);

}
