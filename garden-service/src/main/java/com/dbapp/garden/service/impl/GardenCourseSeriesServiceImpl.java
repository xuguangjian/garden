package com.dbapp.garden.service.impl;

import com.dbapp.garden.entity.GardenCourseSeries;
import com.dbapp.garden.mapper.GardenCourseSeriesMapper;
import com.dbapp.garden.service.GardenCourseSeriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/10.
 */
@Service
public class GardenCourseSeriesServiceImpl implements GardenCourseSeriesService {
    @Autowired
    private GardenCourseSeriesMapper seriesMapper;

    @Override
    public List<GardenCourseSeries> getCouseSeriesList(Map<String, Object> paramMap) {
        if (paramMap==null){
            return null;
        }
        return seriesMapper.getCourseSeriesList(paramMap);
    }

    @Override
    public Integer getCouseSeriesCount(Map<String, Object> paramMap) {
        if (paramMap==null){
            return null;
        }
        return seriesMapper.getCourseSeriesCount(paramMap);
    }

    @Override
    public Integer save(GardenCourseSeries series) {
        if (series==null){
            return null;
        }
        if (series.getId()==null){
            return seriesMapper.insertSelective(series);
        }else{
            return seriesMapper.updateByPrimaryKeySelective(series);
        }
    }

    @Override
    public Integer delete(Integer id) {
        if (id==null){
            return null;
        }
        return seriesMapper.deleteByPrimaryKey(id);
    }

    @Override
    public GardenCourseSeries findById(Integer id) {
        if (id==null){
            return null;
        }
        return seriesMapper.selectByPrimaryKey(id);
    }
}
