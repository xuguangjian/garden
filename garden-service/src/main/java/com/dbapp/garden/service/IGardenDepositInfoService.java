package com.dbapp.garden.service;

import java.util.List;

import com.dbapp.garden.entity.GardenDepositInfo;
import com.dbapp.garden.entity.GardenDepositInfoExtra;
import com.dbapp.garden.entity.GardenStudentState;

public interface IGardenDepositInfoService {

	/**
	 * 获取列表
	 * @param gardenId
	 * @param search
	 * @param start
	 * @param limit
	 * @return
	 */
	public List<GardenDepositInfo> getDepositList(int gardenId, String search, int start, int limit);
	
	/**
	 * 获取会员状态列表
	 * @param gardenId
	 * @return
	 */
	public List<GardenStudentState> getStudentStateList(int gardenId);
	
	/**
	 * 获取列表
	 * @param gardenId
	 * @param search
	 * @param start
	 * @param limit
	 * @return
	 */
	public List<GardenDepositInfoExtra> getDepositExtratList(int gardenId, String search, int start, int limit);
	
	/**
	 * 获取列表总数
	 * @param gardenId
	 * @param search
	 * @return
	 */
	public int getDepositCount(int gardenId, String search);
	
	/**
	 * 根据id查找
	 * @param id
	 * @return
	 */
	public GardenDepositInfo findById(int id);
	
	/**
	 * 保存
	 * @param depositInfo
	 * @return
	 */
	public GardenDepositInfo saveDeposit(GardenDepositInfo depositInfo);
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	public int deleteDeposit(int id);
	
	/**
	 * 生成订单号
	 * @param gardenId
	 * @return
	 */
	public String generateDepositNumber(int gardenId);
}
