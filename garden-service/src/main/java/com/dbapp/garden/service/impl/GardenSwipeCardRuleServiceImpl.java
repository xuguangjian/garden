package com.dbapp.garden.service.impl;

import com.dbapp.garden.entity.GardenSwipeCardRule;
import com.dbapp.garden.mapper.GardenSwipeCardRuleMapper;
import com.dbapp.garden.service.GardenSwipeCardRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/12.
 */
@Service
public class GardenSwipeCardRuleServiceImpl implements GardenSwipeCardRuleService {
    @Autowired
    private GardenSwipeCardRuleMapper ruleMapper;


    @Override
    public List<GardenSwipeCardRule> getCardRuleList(Map<String, Object> paramMap) {
        if (paramMap==null){
            return null;
        }
        return ruleMapper.getCardRuleList(paramMap);
    }

    @Override
    public Integer getCardRuleCount(Map<String, Object> paramMap) {
        if (paramMap==null){
            return null;
        }

        return ruleMapper.getCardRuleCount(paramMap);
    }

    @Override
    public Integer save(GardenSwipeCardRule cardRule) {
        if (cardRule==null){
            return null;
        }
       if (cardRule.getId()==null){
            return ruleMapper.insertSelective(cardRule);
       }else{
           return ruleMapper.updateByPrimaryKeySelective(cardRule);
       }
    }

    @Override
    public Integer delete(Integer id) {
        if (id==null){
            return null;
        }

        return ruleMapper.deleteByPrimaryKey(id);
    }


    @Override
    public GardenSwipeCardRule findById(Integer id) {
        if (id==null){
            return null;
        }
        return ruleMapper.selectByPrimaryKey(id);
    }
}
