package com.dbapp.garden.service;

import com.dbapp.garden.entity.GardenReceiptTemplate;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/14.
 */
public interface GardenReceiptTemplateService {
    GardenReceiptTemplate findById(Integer id);

    Integer save(GardenReceiptTemplate template);

    List<GardenReceiptTemplate> getReceiptTemplate(Map<String, Object> map);
}
