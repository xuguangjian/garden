package com.dbapp.garden.service;

import java.util.List;
import java.util.Map;

import com.dbapp.garden.entity.GardenStudentFollow;

public interface GardenStudentFollowService {

	List<GardenStudentFollow> getStudentFollowList(Map<String, Object> paramMap);

	Integer getStudentFollowCount(Map<String, Object> paramMap);

	Integer save(GardenStudentFollow stuFollow);

	Integer delete(Integer id);

}
