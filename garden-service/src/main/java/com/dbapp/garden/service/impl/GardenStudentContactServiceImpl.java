package com.dbapp.garden.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbapp.garden.entity.GardenStudentContact;
import com.dbapp.garden.mapper.GardenStudentContactMapper;
import com.dbapp.garden.service.GardenStudentContactService;

@Service
public class GardenStudentContactServiceImpl implements GardenStudentContactService {

	@Autowired
	private GardenStudentContactMapper stuContactMapper;
	
	@Override
	public List<GardenStudentContact> getStudentContactList(Map<String, Object> paramMap) {
		if(paramMap==null){
			return null;
		}
		return stuContactMapper.getStudentContactList(paramMap);
	}

	@Override
	public Integer getStudentContactCount(Map<String, Object> paramMap) {
		if(paramMap==null){
			return null;
		}
		return stuContactMapper.getStudentContactCount(paramMap);
	}

	@Override
	public Integer save(GardenStudentContact stuContact) {
		if(stuContact==null){
			return null;
		}
		if(stuContact.getId()==null){
			return stuContactMapper.insertSelective(stuContact);
		}else{
			return stuContactMapper.updateByPrimaryKeySelective(stuContact);
		}
	}

	@Override
	public Integer delete(Integer id) {
		if(id==null){
			return null;
		}
		GardenStudentContact contact=stuContactMapper.selectByPrimaryKey(id);
		if(contact==null){
			return null;
		}
		return stuContactMapper.deleteByPrimaryKey(id);
	}
}
