package com.dbapp.garden.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbapp.garden.entity.GardenTeacherLevel;
import com.dbapp.garden.mapper.GardenTeacherLevelMapper;
import com.dbapp.garden.service.GardenTeacherLevelService;

@Service
public class GardenTeacherLevelServiceImpl implements GardenTeacherLevelService {
	@Autowired
	private GardenTeacherLevelMapper teacherLevelMapper;

	@Override
	public List<GardenTeacherLevel> getTeacherLevelList(Map<String, Object> paramMap) {
		if (paramMap == null) {
			return null;
		}
		return teacherLevelMapper.getTeacherLevelList(paramMap);
	}

	@Override
	public Integer getTeacherLevelCount(Map<String, Object> paramMap) {
		if (paramMap == null) {
			return null;
		}
		return teacherLevelMapper.getTeacherLevelCount(paramMap);
	}

	@Override
	public Integer save(GardenTeacherLevel level) {
		if (level == null) {
			return null;
		}
		if (level.getId() == null) {
			return teacherLevelMapper.insertSelective(level);
		} else {
			return teacherLevelMapper.updateByPrimaryKeySelective(level);
		}
	}

	@Override
	public Integer delete(Integer id) {
		if (id == null) {
			return null;
		}

		return teacherLevelMapper.deleteByPrimaryKey(id);
	}

	@Override
	public GardenTeacherLevel findById(Integer id) {
		if (id == null) {
			return null;
		}

		return teacherLevelMapper.selectByPrimaryKey(id);
	}

}
