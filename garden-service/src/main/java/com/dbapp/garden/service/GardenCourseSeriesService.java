package com.dbapp.garden.service;

import com.dbapp.garden.entity.GardenCourseSeries;

import java.util.List;
import java.util.Map;

/**
 * @author  by gangzi on 2017/11/10.
 */
public interface GardenCourseSeriesService {
    /**
     * 获取课程系列列表
     * @param paramMap
     * @return
     */
    List<GardenCourseSeries> getCouseSeriesList(Map<String, Object> paramMap);

    /**
     * 获取课程系列数量
     * @param paramMap
     * @return
     */
    Integer getCouseSeriesCount(Map<String, Object> paramMap);

    /**
     * 添加课程系列
     * @param series
     * @return
     */
    Integer save(GardenCourseSeries series);

    /**
     * 删除课程系列
     * @param id
     * @return
     */
    Integer delete(Integer id);

    /**
     * 根据主键查询课程系列
     * @param id
     * @return
     */
    GardenCourseSeries findById(Integer id);
}
