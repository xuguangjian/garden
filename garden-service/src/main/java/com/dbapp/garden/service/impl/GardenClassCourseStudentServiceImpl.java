package com.dbapp.garden.service.impl;

import com.dbapp.garden.entity.GardenClassCourseStudent;
import com.dbapp.garden.mapper.GardenClassCourseStudentMapper;
import com.dbapp.garden.service.GardenClassCourseStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/19.
 */
@Service
public class GardenClassCourseStudentServiceImpl implements GardenClassCourseStudentService {

    @Autowired
    private GardenClassCourseStudentMapper classCourseStudentMapper;

    @Override
    public List<GardenClassCourseStudent> getClassStudentList(Map<String, Object> paramMap) {
        if (paramMap==null){
            return null;
        }
        return classCourseStudentMapper.getClassStudentList(paramMap);
    }

    @Override
    public Integer getClassStudentCount(Map<String, Object> paramMap) {
        if (paramMap==null){
            return null;
        }

        return classCourseStudentMapper.getClassStudentCount(paramMap);
    }

    @Override
    public GardenClassCourseStudent findById(Integer id) {
        if (id==null){
            return null;
        }

        return classCourseStudentMapper.selectByPrimaryKey(id);
    }
}
