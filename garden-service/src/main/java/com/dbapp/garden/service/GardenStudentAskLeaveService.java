package com.dbapp.garden.service;

import com.dbapp.garden.entity.GardenStudentAskLeave;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/19.
 */
public interface GardenStudentAskLeaveService {
    /**
     * 添加请假记录
     * @param leave
     * @return
     */
    Integer save(GardenStudentAskLeave leave);

    /**
     * 获取学员请假记录
     * @param paramMap
     * @return
     */
    List<GardenStudentAskLeave> getAskLeaveList(Map<String, Object> paramMap);

    /**
     * 获取学员请假记录数量
     * @param paramMap
     * @return
     */
    Integer getAskLeaveCount(Map<String, Object> paramMap);
}
