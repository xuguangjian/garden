package com.dbapp.garden.service.impl;

import com.dbapp.garden.entity.GardenImportBatch;
import com.dbapp.garden.entity.GardenStudentFail;
import com.dbapp.garden.mapper.GardenImportBatchMapper;
import com.dbapp.garden.mapper.GardenStudentFailMapper;
import com.dbapp.garden.mapper.GardenStudentSuccessMapper;
import com.dbapp.garden.service.GardenImportBatchService;
import com.dbapp.garden.service.GardenStudentSuccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author  by gangzi on 2017/11/7.
 */
@Service
public class GardenImportBatchServiceImpl implements GardenImportBatchService {
    @Autowired
    private GardenImportBatchMapper batchMapper;

    @Autowired
    private GardenStudentSuccessMapper studentSuccessMapper;

    @Autowired
    private GardenStudentFailMapper studentFailMapper;

    @Override
    public List<GardenImportBatch> getBatchList(Map<String, Object> paramMap) {
        if (paramMap==null){
            return null;
        }
        return batchMapper.getBatchList(paramMap);
    }

    @Override
    public Integer getBatchCount(Map<String, Object> paramMap) {
        if (paramMap==null){
            return null;
        }
        return batchMapper.getBatchCount(paramMap);
    }

    @Override
    public Integer save(GardenImportBatch importBatch) {
        if(importBatch==null){
            return null;
        }
        return batchMapper.insertSelective(importBatch);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer delete(Integer id) {
        if (id==null){
            return null;
        }
        studentFailMapper.deleteByBatchId(id);
        studentSuccessMapper.deleteByBatchId(id);
        return batchMapper.deleteByPrimaryKey(id);
    }

    @Override
    public GardenImportBatch findById(Integer id) {
        if (id==null){
            return null;
        }

        return batchMapper.selectByPrimaryKey(id);
    }
}
