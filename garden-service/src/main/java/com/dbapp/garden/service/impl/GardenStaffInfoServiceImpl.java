package com.dbapp.garden.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbapp.garden.entity.GardenStaffInfo;
import com.dbapp.garden.mapper.GardenStaffInfoMapper;
import com.dbapp.garden.service.GardenStaffInfoService;

@Service
public class GardenStaffInfoServiceImpl implements GardenStaffInfoService {

	@Autowired
	private GardenStaffInfoMapper staffMapper;

	@Override
	public GardenStaffInfo findById(Integer id) {
		if (id == null) {
			return null;
		}
		return staffMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<GardenStaffInfo> getStaffList(Map<String, Object> paramMap) {
		if (paramMap == null) {
			return null;
		}
		return staffMapper.getStaffList(paramMap);
	}

	@Override
	public Integer getStaffCount(Map<String, Object> paramMap) {
		if (paramMap == null) {
			return null;
		}
		return staffMapper.getStaffCount(paramMap);
	}

	@Override
	public Integer save(GardenStaffInfo gardenStaffInfo) {
		if (gardenStaffInfo == null) {
			return null;
		}
		if (gardenStaffInfo.getId() == null) {
			return staffMapper.insertSelective(gardenStaffInfo);
		} else {
			return staffMapper.updateByPrimaryKeySelective(gardenStaffInfo);
		}
	}

	@Override
	public Integer delete(Integer id) {
		if (id == null) {
			return null;
		}
		return staffMapper.deleteByPrimaryKey(id);
	}

}
