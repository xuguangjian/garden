package com.dbapp.garden.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbapp.garden.entity.GardenStudentSource;
import com.dbapp.garden.mapper.GardenStudentSourceMapper;
import com.dbapp.garden.service.GardenStudentSourceService;

@Service
public class GardenStudentSourceServiceImpl implements GardenStudentSourceService {

	@Autowired
	private GardenStudentSourceMapper stuSourceMapper;

	@Override
	public List<GardenStudentSource> getStudentSourceList(Map<String, Object> paramMap) {
		if (paramMap == null) {
			return null;
		}
		return stuSourceMapper.getStudentSourceList(paramMap);

	}

	@Override
	public Integer getStudentSourceCount(Map<String, Object> paramMap) {
		if (paramMap == null) {
			return null;
		}
		return stuSourceMapper.getStudentSourceCount(paramMap);
	}

	@Override
	public Integer save(GardenStudentSource stuSource) {
		if (stuSource == null) {
			return null;
		}
		if(stuSource.getId()==null){
			return stuSourceMapper.insertSelective(stuSource);
		}else{
			return stuSourceMapper.updateByPrimaryKeySelective(stuSource);
		}
		
	}

	@Override
	public Integer delete(Integer id) {
		if (id == null) {
			return null;
		}
		GardenStudentSource stu = stuSourceMapper.selectByPrimaryKey(id);
		if (stu != null) {
			return stuSourceMapper.deleteByPrimaryKey(id);
		} else {
			return null;
		}

	}
}
