package com.dbapp.garden.service;

import com.dbapp.garden.entity.GardenCourseRule;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/13.
 */
public interface GardenCourseRuleService {

    List<GardenCourseRule> getCourseRuleList(Map<String, Object> map);

    GardenCourseRule findById(Integer id);

    Integer save(GardenCourseRule rule);
}
