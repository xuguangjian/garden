package com.dbapp.garden.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dbapp.garden.entity.GardenStudentInfo;
import com.dbapp.garden.mapper.GardenStudentInfoMapper;
import com.dbapp.garden.service.GardenStudentInfoService;

@Service
public class GardenStudentInfoServiceImpl implements GardenStudentInfoService {

	@Autowired
	private GardenStudentInfoMapper stuMapper;

	@Override
	public List<GardenStudentInfo> getStudentList(Map<String, Object> paramMap) {
		if (paramMap == null) {
			return null;
		}
		return stuMapper.getStudentList(paramMap);
	}

	@Override
	public Integer getStudentCount(Map<String, Object> paramMap) {
		if (paramMap == null) {
			return null;
		}
		return stuMapper.getStudentCount(paramMap);
	}

	@Override
	public GardenStudentInfo findById(Integer id) {
		if (id == null) {
			return null;
		}
		return stuMapper.selectByPrimaryKey(id);
	}

	@Override
	public Integer save(GardenStudentInfo stuInfo) {
		if (stuInfo == null) {
			return null;
		}
		if (stuInfo.getId() == null) {
			return stuMapper.insertSelective(stuInfo);
		} else {
			return stuMapper.updateByPrimaryKeySelective(stuInfo);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Integer batchDeleteStudent(Integer[] ids) {
		if (ids == null || ids.length <= 0) {
			return null;
		}
		if (ids.length == 1) {
			return stuMapper.deleteByPrimaryKey(ids[0]);
		} else {
			return stuMapper.batchDeleteStudent(ids);
		}

	}

	@Override
	public Integer batchInsertStudent(List<GardenStudentInfo> studentList) {
		if (studentList==null||studentList.isEmpty()){
			return null;
		}
		return stuMapper.batchInsertStudent(studentList);
	}

	@Override
	public List<GardenStudentInfo> getSignInStudentList(Map<String, Object> paramMap) {
		if (paramMap == null) {
			return null;
		}

		return stuMapper.getStudentList(paramMap);
	}

	@Override
	public Integer getSignInStudentCount(Map<String, Object> paramMap) {
		if (paramMap == null) {
			return null;
		}

		return stuMapper.getStudentCount(paramMap);
	}

	@Override
	public List<GardenStudentInfo> getStudentByIds(Integer[] ids) {
		if (ids==null||ids.length<=0){
			return null;
		}
		return stuMapper.getStudentByIds(ids);
	}

	@Override
	public Integer batchUpdateStudent(List<GardenStudentInfo> readyToUpdate) {
		if (readyToUpdate==null||readyToUpdate.isEmpty()){
			return null;
		}
		return stuMapper.batchUpdateStudent(readyToUpdate);
	}
}
