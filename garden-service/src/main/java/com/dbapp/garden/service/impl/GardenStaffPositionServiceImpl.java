package com.dbapp.garden.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbapp.garden.entity.GardenStaffPosition;
import com.dbapp.garden.mapper.GardenStaffPositionMapper;
import com.dbapp.garden.service.GardenStaffPositionService;

@Service
public class GardenStaffPositionServiceImpl implements GardenStaffPositionService {

	@Autowired
	private GardenStaffPositionMapper gardenStaffMapper;

	@Override
	public GardenStaffPosition findById(Integer id) {
		if (id == null) {
			return null;
		}

		return gardenStaffMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<GardenStaffPosition> getPositionList(Map<String, Object> paramMap) {
		if (paramMap == null) {
			return null;
		}
		return gardenStaffMapper.getPositionList(paramMap);
	}

	@Override
	public Integer getPositionCount(Map<String, Object> paramMap) {
		if (paramMap == null) {
			return null;
		}
		return gardenStaffMapper.getPositionCount(paramMap);
	}

	@Override
	public Integer save(GardenStaffPosition gardenStaffPosition) {
		if (gardenStaffPosition == null) {
			return null;
		}
		if (gardenStaffPosition.getId() == null) {
			return gardenStaffMapper.insertSelective(gardenStaffPosition);
		} else {
			return gardenStaffMapper.updateByPrimaryKeySelective(gardenStaffPosition);
		}

	}

	@Override
	public Integer delete(Integer id) {
		if (id == null) {
			return null;
		}
		return gardenStaffMapper.deleteByPrimaryKey(id);
	}

}
