package com.dbapp.garden.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbapp.garden.entity.GardenStudentState;
import com.dbapp.garden.mapper.GardenStudentStateMapper;
import com.dbapp.garden.service.GardenStudentStateService;

@Service
public class GardenStudentStateServiceImpl implements GardenStudentStateService {

	@Autowired
	private GardenStudentStateMapper stuStateMapper;

	@Override
	public List<GardenStudentState> getStudentStateList(Map<String, Object> paramMap) {
		if (paramMap == null) {
			return null;
		}
		return stuStateMapper.getStudentStateList(paramMap);
	}

	@Override
	public Integer getStudentStateCount(Map<String, Object> paramMap) {
		if (paramMap == null) {
			return null;
		}
		return stuStateMapper.getStudentStateCount(paramMap);
	}

	@Override
	public Integer save(GardenStudentState stuState) {
		if (stuState == null) {
			return null;
		}
		if (stuState.getId() == null) {
			return stuStateMapper.insertSelective(stuState);
		} else {
			return stuStateMapper.updateByPrimaryKeySelective(stuState);
		}
	}

	@Override
	public Integer delete(Integer id) {
		if (id == null) {
			return null;
		}
		GardenStudentState state = stuStateMapper.selectByPrimaryKey(id);
		if (state == null) {
			return null;
		}
		return stuStateMapper.deleteByPrimaryKey(id);
	}

}
