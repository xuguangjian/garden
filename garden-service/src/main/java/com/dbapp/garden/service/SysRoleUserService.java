package com.dbapp.garden.service;

import com.dbapp.garden.entity.SysRoleUser;

public interface SysRoleUserService {

	Integer save(SysRoleUser roleUser);

	SysRoleUser findByUserId(Integer id);


}
