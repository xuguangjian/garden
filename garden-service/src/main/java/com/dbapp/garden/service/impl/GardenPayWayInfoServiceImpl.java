package com.dbapp.garden.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbapp.garden.entity.GardenPayWayInfo;
import com.dbapp.garden.entity.GardenPayWayInfoExample;
import com.dbapp.garden.entity.GardenPayWayInfoExample.Criteria;
import com.dbapp.garden.mapper.GardenPayWayInfoMapper;
import com.dbapp.garden.service.IGardenPayWayInfoService;

@Service
public class GardenPayWayInfoServiceImpl implements IGardenPayWayInfoService {

	@Autowired
	GardenPayWayInfoMapper payWayDao;
	
	@Override
	public List<GardenPayWayInfo> getPayWayList(int gardenId, String search, int start, int limit) {
		GardenPayWayInfoExample query = new GardenPayWayInfoExample();
		Criteria queryCondition = query.createCriteria();
		if(gardenId > 0)
			queryCondition.andGarden_idEqualTo(gardenId);
		
		if(StringUtils.isNotBlank(search))
			queryCondition.andPayWayLike("%"+search+"%");
		if(start < 0)
			start = 0;
		if(limit < 0)
			limit = 10;
		query.setLimitStart(start);
		query.setLimitEnd(limit);
		return payWayDao.selectByExample(query);
	}

	@Override
	public int getPayWayCount(int gardenId, String search) {
		// TODO Auto-generated method stub
		GardenPayWayInfoExample query = new GardenPayWayInfoExample();
		Criteria queryCondition = query.createCriteria();
		if(gardenId > 0)
			queryCondition.andGarden_idEqualTo(gardenId);
		
		if(StringUtils.isNotBlank(search))
			queryCondition.andPayWayLike(search);
		return (int) payWayDao.countByExample(query);
	}

	@Override
	public int deletePayWay(int id) {
		// TODO Auto-generated method stub
		return payWayDao.deleteByPrimaryKey(id);
	}

	@Override
	public GardenPayWayInfo findById(int id) {
		// TODO Auto-generated method stub
		return payWayDao.selectByPrimaryKey(id);
	}

	@Override
	public GardenPayWayInfo savePayWay(GardenPayWayInfo payWay) {
		// TODO Auto-generated method stub
		if(payWay == null) return null;
		if(payWay.getId() == null)
			payWayDao.insertSelective(payWay);
		else
			payWayDao.updateByPrimaryKeySelective(payWay);
		return payWay;
	}

	
}
