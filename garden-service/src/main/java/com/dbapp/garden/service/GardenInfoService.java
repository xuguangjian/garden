package com.dbapp.garden.service;

import java.util.List;
import java.util.Map;

import com.dbapp.garden.entity.GardenInfo;

public interface GardenInfoService {
	
	Integer save(GardenInfo garden);

	List<GardenInfo> getGardenList(Map<String, Object> paramMap);

	Integer getGardenCount(Map<String, Object> paramMap);

	Integer delete(Integer id);

	GardenInfo findById(Integer id);

}
