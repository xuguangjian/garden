package com.dbapp.garden.service;

import java.util.List;
import java.util.Map;

import com.dbapp.garden.entity.SysResource;



public interface SysResourceService {

	/**
	 * 获取分配给当前用户的菜单
	 * @param userid 用户ID
	 * @return
	 */
	List<SysResource> getUserResources(Integer userid) ;

	/**
	 * 获取所有资源
	 * @return
	 */
	List<SysResource> findAllResources(Map<String,Object> query);

	List<SysResource> findByRoleId(Integer roleId);

}
