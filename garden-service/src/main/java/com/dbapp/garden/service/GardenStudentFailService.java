package com.dbapp.garden.service;

import com.dbapp.garden.entity.GardenStudentFail;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/15.
 */
public interface GardenStudentFailService {
    /**
     * 批量插入导入失败学生名单
     * @param failList
     * @return
     */
    Integer batchInsert(List<GardenStudentFail> failList);

    /**
     * 获取导入失败学生名单
     * @param paramMap
     * @return
     */
    List<GardenStudentFail> getImportStudentFailList(Map<String, Object> paramMap);

    /**
     * 导入失败的数量
     * @param paramMap
     * @return
     */
    Integer getImportStudentFailCount(Map<String, Object> paramMap);

    /**
     * 删除
     * @param id
     * @return
     */
    Integer delete(Integer id);

    /**
     * 根据ID查询
     * @param id
     * @return
     */
    GardenStudentFail findById(Integer id);
}
