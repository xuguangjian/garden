package com.dbapp.garden.service;

import java.util.List;
import java.util.Map;

import com.dbapp.garden.entity.GardenStaffPosition;

public interface GardenStaffPositionService {

	GardenStaffPosition findById(Integer id);

	List<GardenStaffPosition> getPositionList(Map<String, Object> paramMap);

	Integer getPositionCount(Map<String, Object> paramMap);

	Integer save(GardenStaffPosition gardenStaffPosition);

	Integer delete(Integer id);

}
