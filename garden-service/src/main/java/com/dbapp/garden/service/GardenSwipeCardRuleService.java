package com.dbapp.garden.service;

import com.dbapp.garden.entity.GardenSwipeCardRule;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/12.
 */
public interface GardenSwipeCardRuleService {
    List<GardenSwipeCardRule> getCardRuleList(Map<String, Object> paramMap);

    Integer getCardRuleCount(Map<String, Object> paramMap);

    Integer save(GardenSwipeCardRule cardRule);

    Integer delete(Integer id);

    GardenSwipeCardRule findById(Integer id);
}
