package com.dbapp.garden.service.impl;

import com.dbapp.garden.entity.GardenStudentAssign;
import com.dbapp.garden.mapper.GardenStudentAssignMapper;
import com.dbapp.garden.service.GardenStudentAssignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author  by gangzi on 2017/11/8.
 */
@Service
public class GardenStudentAssignServiceImpl implements GardenStudentAssignService {
    @Autowired
    private GardenStudentAssignMapper assignMapper;

    @Override
    public Integer save(GardenStudentAssign assign) {
        if(assign==null){
            return null;
        }
        if (assign.getId()==null){
            return assignMapper.insertSelective(assign);
        }else{
            return assignMapper.updateByPrimaryKeySelective(assign);
        }
    }

    @Override
    public List<GardenStudentAssign> getStudentAssignList(Map<String, Object> paramMap) {
        if (paramMap==null){
            return null;
        }
        return assignMapper.getStudentAssignList(paramMap);
    }

    @Override
    public Integer getStudentAssignCount(Map<String, Object> paramMap) {
        if (paramMap==null){
            return null;
        }

        return assignMapper.getStudentAssignCount(paramMap);
    }
}
