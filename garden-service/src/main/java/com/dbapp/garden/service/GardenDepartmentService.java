package com.dbapp.garden.service;

import java.util.List;
import java.util.Map;

import com.dbapp.garden.entity.GardenDepartment;

public interface GardenDepartmentService {

	List<GardenDepartment> getDepartmentList(Map<String, Object> paramMap);

	GardenDepartment findById(Integer id);

	Integer getDepartmentCount(Map<String, Object> paramMap);

	Integer save(GardenDepartment department);

	Integer delete(Integer id);

}
