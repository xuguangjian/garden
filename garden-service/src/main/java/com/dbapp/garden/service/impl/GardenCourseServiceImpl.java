package com.dbapp.garden.service.impl;

import com.dbapp.garden.entity.GardenCourse;
import com.dbapp.garden.mapper.GardenCourseMapper;
import com.dbapp.garden.service.GardenCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/11.
 */
@Service
public class GardenCourseServiceImpl implements GardenCourseService {

    @Autowired
    private GardenCourseMapper courseMapper;

    @Override
    public List<GardenCourse> getCourseList(Map<String, Object> paramMap) {
        if (paramMap == null) {
            return null;
        }
        return courseMapper.getCourseList(paramMap);
    }

    @Override
    public Integer getCourseCount(Map<String, Object> paramMap) {
        if (paramMap == null) {
            return null;
        }
        return courseMapper.getCourseCount(paramMap);
    }

    @Override
    public Integer save(GardenCourse course) {
        if (course == null) {
            return null;
        }
        if (course.getId() == null) {
            return courseMapper.insertSelective(course);
        } else {
            return courseMapper.updateByPrimaryKeySelective(course);
        }

    }

    @Override
    public Integer delete(Integer id) {
        if (id == null) {
            return null;
        }
        return courseMapper.deleteByPrimaryKey(id);
    }

    @Override
    public GardenCourse findById(Integer id) {
        if (id == null) {
            return null;
        }
        return courseMapper.selectByPrimaryKey(id);
    }
}
