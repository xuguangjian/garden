package com.dbapp.garden.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbapp.garden.entity.GardenContractPackageInfo;
import com.dbapp.garden.entity.GardenContractPackageInfoExample;
import com.dbapp.garden.entity.GardenContractPackageInfoExample.Criteria;
import com.dbapp.garden.mapper.GardenContractPackageInfoMapper;
import com.dbapp.garden.service.IGardenContractPackageInfoService;

@Service
public class GardenContractPackageInfoServiceImpl implements IGardenContractPackageInfoService {

    @Autowired
    GardenContractPackageInfoMapper contractPackageDao;

    @Override
    public GardenContractPackageInfo saveContractPackage(GardenContractPackageInfo contractPackage) {
        if (contractPackage == null) {
            return null;
        }
        if (contractPackage.getId() == null) {
            contractPackageDao.insertSelective(contractPackage);
        } else {
            contractPackageDao.updateByPrimaryKeySelective(contractPackage);
        }
        return contractPackage;
    }

    @Override
    public int deleteContractPackageInfo(int id) {

        return contractPackageDao.deleteByPrimaryKey(id);
    }

    @Override
    public GardenContractPackageInfo findById(int id) {

        return contractPackageDao.selectByPrimaryKey(id);
    }

    @Override
    public List<GardenContractPackageInfo> getContractPackageList(int gardenId, String search, int start, int limit) {

        GardenContractPackageInfoExample query = new GardenContractPackageInfoExample();
        Criteria queryCondition = query.createCriteria();
        if (gardenId > 0)
            queryCondition.andGarden_idEqualTo(gardenId);

        if (StringUtils.isNotBlank(search))
            queryCondition.andContractPackageNameLike("%" + search + "%");
        if (start < 0)
            start = 0;
        if (limit < 0)
            limit = 10;
        query.setLimitStart(start);
        query.setLimitEnd(limit);
        return contractPackageDao.selectByExample(query);
    }

    @Override
    public int getContractPackageCount(int gardenId, String search) {
        // TODO Auto-generated method stub
        GardenContractPackageInfoExample query = new GardenContractPackageInfoExample();
        Criteria queryCondition = query.createCriteria();
        if (gardenId > 0)
            queryCondition.andGarden_idEqualTo(gardenId);

        if (StringUtils.isNotBlank(search))
            queryCondition.andContractPackageNameLike(search);

        return (int) contractPackageDao.countByExample(query);
    }

}
