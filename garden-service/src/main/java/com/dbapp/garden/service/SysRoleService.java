package com.dbapp.garden.service;

import java.util.List;
import java.util.Map;

import com.dbapp.garden.entity.SysRole;

public interface SysRoleService {

	List<SysRole> getRoleList(Map<String, Object> map);

	Integer getRoleCount(Map<String, Object> map);

	SysRole findRoleByName(Map<String,Object> map);

	Integer saveRole(SysRole roleVm,List<Integer> resIds);

	Integer deleteRole(Integer roleId);

	SysRole findRoleById(Integer id);

}
