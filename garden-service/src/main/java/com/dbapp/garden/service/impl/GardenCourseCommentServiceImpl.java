package com.dbapp.garden.service.impl;

import com.dbapp.garden.entity.GardenCourseComment;
import com.dbapp.garden.mapper.GardenCourseCommentMapper;
import com.dbapp.garden.service.GardenCourseCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/13.
 */
@Service
public class GardenCourseCommentServiceImpl implements GardenCourseCommentService {

    @Autowired
    private GardenCourseCommentMapper commentMapper;

    @Override
    public List<GardenCourseComment> getCourseCommentList(Map<String, Object> paramMap) {
        if (paramMap==null){
            return null;
        }
        return commentMapper.getCourseCommentList(paramMap);
    }

    @Override
    public Integer getCourseCommentCount(Map<String, Object> paramMap) {
        if (paramMap==null){
            return null;
        }
        return commentMapper.getCourseCommentCount(paramMap);
    }

    @Override
    public Integer save(GardenCourseComment comment) {
        if (comment==null){
         return null;
        }
        if (comment.getId()==null){
            return commentMapper.insertSelective(comment);
        }else{
            return commentMapper.updateByPrimaryKeySelective(comment);
        }
    }

    @Override
    public Integer delete(Integer id) {
        if (id==null){
            return null;
        }

        return commentMapper.deleteByPrimaryKey(id);
    }

    @Override
    public GardenCourseComment findById(Integer id) {
        if (id==null){
            return null;
        }
        return commentMapper.selectByPrimaryKey(id);
    }
}
