package com.dbapp.garden.service.impl;

import com.dbapp.garden.entity.GardenStudentFail;
import com.dbapp.garden.mapper.GardenStudentFailMapper;
import com.dbapp.garden.service.GardenStudentFailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/15.
 */
@Service
public class GardenStudentFailServiceImpl implements GardenStudentFailService {

    @Autowired
    private GardenStudentFailMapper failMapper;

    @Override
    public Integer batchInsert(List<GardenStudentFail> failList) {
        if (failList==null||failList.isEmpty()){
            return null;
        }
        return failMapper.batchInsert(failList);
    }

    @Override
    public List<GardenStudentFail> getImportStudentFailList(Map<String, Object> paramMap) {
        if (paramMap==null){
            return null;
        }
        return failMapper.getImportStudentFailList(paramMap);
    }

    @Override
    public Integer getImportStudentFailCount(Map<String, Object> paramMap) {
        if (paramMap==null){
            return null;
        }
        return failMapper.getImportStudentFailCount(paramMap);
    }

    @Override
    public Integer delete(Integer id) {
        if (id==null){
            return null;
        }
        return failMapper.deleteByPrimaryKey(id);
    }

    @Override
    public GardenStudentFail findById(Integer id) {
        if (id==null){
            return null;
        }

        return failMapper.selectByPrimaryKey(id);
    }
}
