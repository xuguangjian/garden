package com.dbapp.garden.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbapp.garden.entity.GardenInfo;
import com.dbapp.garden.mapper.GardenInfoMapper;
import com.dbapp.garden.service.GardenInfoService;

@Service
public class GardenInfoServiceImpl implements GardenInfoService {
	@Autowired
	private GardenInfoMapper gardenInfoMapper;

	@Override
	public Integer save(GardenInfo garden) {
		if (garden == null) {
			return null;
		}
		if (garden.getId() == null) {
			return gardenInfoMapper.insertSelective(garden);
		} else {
			return gardenInfoMapper.updateByPrimaryKeySelective(garden);
		}
	}

	@Override
	public List<GardenInfo> getGardenList(Map<String, Object> paramMap) {
		if (paramMap == null) {
			return null;
		}
		return gardenInfoMapper.getGardenList(paramMap);
	}

	@Override
	public Integer getGardenCount(Map<String, Object> paramMap) {
		if (paramMap == null) {
			return null;
		}
		return gardenInfoMapper.getGardenCount(paramMap);
	}

	@Override
	public Integer delete(Integer id) {
		if (id == null) {
			return null;
		}
		GardenInfo garden = gardenInfoMapper.selectByPrimaryKey(id);
		if (garden == null) {
			return null;
		}
		return gardenInfoMapper.deleteByPrimaryKey(id);
	}

	@Override
	public GardenInfo findById(Integer id) {
		if (id == null) {
			return null;
		}
		return gardenInfoMapper.selectByPrimaryKey(id);
	}
}
