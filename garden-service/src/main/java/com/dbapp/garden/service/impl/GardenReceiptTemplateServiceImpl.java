package com.dbapp.garden.service.impl;

import com.dbapp.garden.entity.GardenReceiptTemplate;
import com.dbapp.garden.mapper.GardenReceiptTemplateMapper;
import com.dbapp.garden.service.GardenReceiptTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/14.
 */

@Service
public class GardenReceiptTemplateServiceImpl implements GardenReceiptTemplateService {
    @Autowired
    private GardenReceiptTemplateMapper templateMapper;

    @Override
    public GardenReceiptTemplate findById(Integer id) {
        if (id == null) {
            return null;
        }
        return templateMapper.selectByPrimaryKey(id);
    }

    @Override
    public Integer save(GardenReceiptTemplate template) {
        if (template == null) {
            return null;
        }
        if (template.getId() == null) {
            return templateMapper.insertSelective(template);
        } else {
            return templateMapper.updateByPrimaryKeySelective(template);
        }
    }

    @Override
    public List<GardenReceiptTemplate> getReceiptTemplate(Map<String, Object> map) {
        if (map==null){
            return null;
        }
        return templateMapper.getReceiptTemplate(map);
    }
}
