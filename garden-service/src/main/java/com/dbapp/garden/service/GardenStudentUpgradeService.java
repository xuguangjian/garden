package com.dbapp.garden.service;

import com.dbapp.garden.entity.GardenStudentUpgrade;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/20.
 */
public interface GardenStudentUpgradeService {
    Integer save(GardenStudentUpgrade studentUpgrade);

    List<GardenStudentUpgrade> getStudentUpgradeList(Map<String, Object> paramMap);

    Integer getStudentUpgradeCount(Map<String, Object> paramMap);

    Integer delete(Integer id);
}
