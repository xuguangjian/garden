package com.dbapp.garden.service;

import java.util.List;
import java.util.Map;

import com.dbapp.garden.entity.GardenStudentArea;

public interface GardenStudentAreaService {

	List<GardenStudentArea> getStudentAreaList(Map<String, Object> paramMap);

	Integer getStudentAreaCount(Map<String, Object> paramMap);

	Integer save(GardenStudentArea stuArea);

	Integer delete(Integer id);

}
