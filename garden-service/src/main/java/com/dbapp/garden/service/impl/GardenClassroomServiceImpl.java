package com.dbapp.garden.service.impl;

import com.dbapp.garden.entity.GardenClassroom;
import com.dbapp.garden.mapper.GardenClassroomMapper;
import com.dbapp.garden.service.GardenClassroomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author by gangzi on 2017/11/12.
 */
@Service
public class GardenClassroomServiceImpl implements GardenClassroomService {
    @Autowired
    private GardenClassroomMapper classroomMapper;

    @Override
    public List<GardenClassroom> getClassroomList(Map<String, Object> paramMap) {
        if (paramMap == null) {
            return null;
        }
        return classroomMapper.getClassroomList(paramMap);
    }

    @Override
    public Integer getClassroomCount(Map<String, Object> paramMap) {
        if (paramMap == null) {
            return null;
        }
        return classroomMapper.getClassroomCount(paramMap);
    }

    @Override
    public Integer save(GardenClassroom room) {
        if (room == null) {
            return null;
        }

        if (room.getId() == null) {
            return classroomMapper.insertSelective(room);
        } else {
            return classroomMapper.updateByPrimaryKeySelective(room);
        }
    }

    @Override
    public Integer delete(Integer id) {
        if (id == null) {
            return null;
        }
        return classroomMapper.deleteByPrimaryKey(id);
    }

    @Override
    public GardenClassroom findById(Integer id) {
        if (id == null) {
            return null;
        }

        return classroomMapper.selectByPrimaryKey(id);
    }

}
