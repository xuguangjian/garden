package com.dbapp.garden.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dbapp.garden.entity.GardenSmallTicketInfo;
import com.dbapp.garden.entity.GardenSmallTicketInfoExample;
import com.dbapp.garden.mapper.GardenSmallTicketInfoMapper;
import com.dbapp.garden.service.IGardenSmallTicketInfoService;

@Service
public class GardenSmallTicketInfoServiceImpl implements IGardenSmallTicketInfoService {

	@Autowired
	GardenSmallTicketInfoMapper ticketDao;
	
	@Override
	public GardenSmallTicketInfo findByGardenId(int gardenId) {
		// TODO Auto-generated method stub
		GardenSmallTicketInfoExample query = new GardenSmallTicketInfoExample();
		query.or().andGarden_idEqualTo(gardenId);
		List<GardenSmallTicketInfo> results = ticketDao.selectByExample(query);
		if(results == null || results.isEmpty())
			return null;
		
		return results.get(0);
	}

	@Override
	public GardenSmallTicketInfo saveSmallTicket(GardenSmallTicketInfo ticketInfo) {
		// TODO Auto-generated method stub
		if(ticketInfo == null)
			return null;
		if(ticketInfo.getId() == null){
			ticketInfo.setCreateDate(new Date());
			ticketDao.insertSelective(ticketInfo);
		}
		else
			ticketDao.updateByPrimaryKeySelective(ticketInfo);
		
		return ticketInfo;
	}

	@Override
	public GardenSmallTicketInfo findById(int id) {
		// TODO Auto-generated method stub
		return ticketDao.selectByPrimaryKey(id);
	}

}
