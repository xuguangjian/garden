package com.dbapp.garden.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dbapp.garden.entity.SysUser;
import com.dbapp.garden.mapper.SysRoleUserMapper;
import com.dbapp.garden.mapper.SysUserMapper;
import com.dbapp.garden.service.SysUserService;

@Service
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private SysUserMapper userMapper;
    @Autowired
    private SysRoleUserMapper roleUserMapper;

    @Override
    public Integer saveUser(SysUser user) {
        if (user == null) {
            return null;
        }
        if (user.getId() == null) {
            return userMapper.insertSelective(user);
        } else {
            return userMapper.updateByPrimaryKeySelective(user);
        }

    }

    @Override
    public SysUser findUserByUserName(String userName) {
        if (StringUtils.isEmpty(userName)) {
            return null;
        }
        return userMapper.querySingleUser(userName);
    }

    @Override
    public List<SysUser> getUserList(Map<String, Object> paramMap) {
        if (paramMap == null) {
            return null;
        }
        return userMapper.getUserList(paramMap);
    }

    @Override
    public Integer getUserCount(Map<String, Object> paramMap) {
        if (paramMap == null) {
            return null;
        }
        return userMapper.getUserCount(paramMap);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer delete(Integer id) {
        if (id == null) {
            return null;
        }
        roleUserMapper.deleteByUserId(id);
        return userMapper.deleteByPrimaryKey(id);
    }

    @Override
    public SysUser findUserById(Integer id) {
        if (id == null) {
            return null;
        }
        return userMapper.selectByPrimaryKey(id);
    }
}
